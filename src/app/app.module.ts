import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './home/home.component';
import { GlobalService } from './global.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule, /* other http imports */ } from "@angular/common/http";
import { StorageServiceModule } from 'angular-webstorage-service';
import { AssessmentComponent } from './pages/assessment/assessment.component';
import { ExamScheduleComponent } from './pages/exam-schedule/exam-schedule.component';
import { AnnouncementComponent } from './pages/announcement/announcement.component';
import { WifiStatusComponent } from './pages/wifi-status/wifi-status.component';
import { ClassScheduleComponent } from './pages/class-schedule/class-schedule.component';
import { GradesComponent } from './pages/grades/grades.component';
import { PersonalDataComponent } from './pages/personal-data/personal-data.component';
import { OpenCodesComponent } from './pages/open-codes/open-codes.component';
import { EnrollmentComponent } from './pages/enrollment/enrollment.component';
import { AddressLookupComponent } from './pages/address-lookup/address-lookup.component';
import { FaqsComponent } from './popup/faqs/faqs.component';
import { DisclaimerComponent } from './popup/disclaimer/disclaimer.component';
import { TermsOfUseComponent } from './popup/terms-of-use/terms-of-use.component';
import { ChangePasswordComponent } from './main/change-password/change-password.component';
import { ChecklistComponent } from './pages/checklist/checklist.component';
import { RemainingComponent } from './pages/checklist/remaining/remaining.component';
import { EnrolmentFormComponent } from './pages/enrolment-form/enrolment-form.component';
import { OtherInfoComponent } from './pages/other-info/other-info.component';
import { EmployeeRecordsComponent } from './pages/employee-records/employee-records.component';
import { DailyTimeRecordsComponent } from './pages/daily-time-records/daily-time-records.component';
import { LeavesComponent } from './pages/leaves/leaves.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { SeminarsTrainingsComponent } from './popup/seminars-trainings/seminars-trainings.component';
import { OrganizationsComponent } from './popup/organizations/organizations.component';
import { ResearchPubComponent } from './popup/research-pub/research-pub.component';
import { DeleteConfirmationDialogComponent } from './popup/delete-confirmation-dialog/delete-confirmation-dialog.component';
import { GenerateCvComponent } from './pages/generate-cv/generate-cv.component';
import { CommunityEngagementComponent } from './popup/community-engagement/community-engagement.component';
import { ApplicationPickComponent } from './pages/application-pick/application-pick.component';
import { PersonalDataEmpComponent } from './pages/personal-data-emp/personal-data-emp.component';
import { AwardsComponent } from './popup/awards/awards.component';
import { SpeakingEngagementComponent } from './popup/speaking-engagement/speaking-engagement.component';
import { DatePipe } from '@angular/common';
import { ClasslistComponent } from './pages/classlist/classlist.component';
import { ExcelService } from './pages/classlist/services/sharedServices/excel.service';
import { ShortTermEnrollmentComponent } from './pages/short-term-enrollment/short-term-enrollment.component';
import { GsEnrollmentComponent } from './pages/gs-enrollment/gs-enrollment.component';
import { AdmittedDialogComponent } from './pages/classlist/admitted-dialog/admitted-dialog.component';
import { LmsAccessCodesComponent } from './pages/lms-access-codes/lms-access-codes.component';
import { HsEnrollmentComponent } from './pages/hs-enrollment/hs-enrollment.component';
import { LeaveComponent } from './popup/leave/leave.component';
import { FilterComponent } from './popup/leave/filter/filter.component';
import { AddDateComponent } from './popup/leave/add-date/add-date.component';

import { AmazingTimePickerModule } from 'amazing-time-picker';
import { HsElectiveComponent } from './pages/hs-elective/hs-elective.component';
import { ModalityComponent } from './pages/modality/modality.component';

import { HsClassScheduleComponent } from './pages/hs-class-schedule/hs-class-schedule.component';

import { BasicEdClasslistComponent } from './pages/classlist/basic-ed-classlist/basic-ed-classlist.component';
import { BasicEdClassScheduleComponent } from './pages/basic-ed-class-schedule/basic-ed-class-schedule.component';

import { EGSComponent } from './pages/egs/egs.component';
import { EgsPrintComponent } from './pages/egs/egs-print/egs-print.component';
import { PayslipComponent } from './pages/payslip/payslip.component';
import { PreEnrollmentComponent } from './pages/pre-enrollment/pre-enrollment.component';
import { PreEnrollmentModalComponent } from './pages/pre-enrollment/pre-enrollment-modal/pre-enrollment-modal.component';

import { OfficialBusinessComponent } from './popup/leave/official-business/official-business.component';

import { LeaveService } from './pages/leaves/services/empportal-services/leave-service.service';
import { LeavePushService } from './pages/leaves/services/empportal-services/leave-push-service.service';
import { LeaveValidationService } from './pages/leaves/services/empportal-services/leave-validation-service.service';
import { HsEnrollmentFamilyBackgroundComponent } from './pages/hs-enrollment/hs-enrollment-family-background/hs-enrollment-family-background.component';
import { HsEnrollmentFamilyBackgroundFamilymemberComponent } from './pages/hs-enrollment/hs-enrollment-family-background-familymember/hs-enrollment-family-background-familymember.component';
import { HsEnrollmentFamilyBackgroundGuardianComponent } from './pages/hs-enrollment/hs-enrollment-family-background-guardian/hs-enrollment-family-background-guardian.component';
import { ElemClassScheduleComponent } from './pages/elem-class-schedule/elem-class-schedule.component';
import { StudentApiService } from './student-api.service';
import { PasswordResetService } from './password-reset-service.service';
import { EducationalBcgComponent } from './popup/educational-bcg/educational-bcg.component';
import { ShsCollegePreEnrollmentComponent } from './pages/pre-enrollment/shs-college-pre-enrollment/shs-college-pre-enrollment.component';

import { BuildDetailsHttpService } from './build-details/build-details.http.service';
import { FacultyEvaluationComponent } from './pages/faculty-evaluation/faculty-evaluation.component';
import { QuestionnaireComponent } from './pages/faculty-evaluation/questionnaire/questionnaire.component';
import { EnrollmentFormComponent } from './pages/enrollment/incoming-enrollment/enrollment-form/enrollment-form.component';
import { LeaveManagementComponent } from './popup/leave-management/leave-management.component';
import { AddDateTimeComponent } from './popup/leave-management/add-date-time/add-date-time.component';
import { EditLeaveDurationsComponent } from './popup/leave-management/edit-leave-durations/edit-leave-durations.component';
import { CheckScheduleComponent } from './popup/leave-management/check-schedule/check-schedule.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { AddDateTimeFacultyComponent } from './popup/leave-management/add-date-time-faculty/add-date-time-faculty.component';
import { JwtModule, JWT_OPTIONS, JwtHelperService } from "@auth0/angular-jwt";
import { AuthGuardGuard } from './auth-guard/auth-guard.guard';


export function fetchBuildDetails(buildDetailsService: BuildDetailsHttpService) {
  return () => buildDetailsService.fetchBuildDetails();
}

export function jwtOptionsFactory(GlobalService) {
  return {
    tokenGetter: () => {
      if(GlobalService.getTokenStatus())
        GlobalService.logout();
      else
        return GlobalService.getSession();
    },
    // whitelistedDomains: ["example.com"]
  }
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    HomeComponent,
    AssessmentComponent,
    ExamScheduleComponent,
    AnnouncementComponent,
    WifiStatusComponent,
    ClassScheduleComponent,
    GradesComponent,
    PersonalDataComponent,
    OpenCodesComponent,
    EnrollmentComponent,
    AddressLookupComponent,
    FaqsComponent,
    DisclaimerComponent,
    TermsOfUseComponent,
    ChangePasswordComponent,
    ChecklistComponent,
    RemainingComponent,
    EnrolmentFormComponent,

    OtherInfoComponent,
    EmployeeRecordsComponent,
    DailyTimeRecordsComponent,
    LeavesComponent,
    SeminarsTrainingsComponent,
    OrganizationsComponent,
    ResearchPubComponent,
    DeleteConfirmationDialogComponent,
    GenerateCvComponent,
    CommunityEngagementComponent,
    ApplicationPickComponent,
    PersonalDataEmpComponent,
    AwardsComponent,
    SpeakingEngagementComponent,
    ClasslistComponent,
    ShortTermEnrollmentComponent,
    GsEnrollmentComponent,
    AdmittedDialogComponent,
    LmsAccessCodesComponent,
    HsEnrollmentComponent,
    LeaveComponent,
    FilterComponent,
    AddDateComponent,
    HsElectiveComponent,
    ModalityComponent,
    HsClassScheduleComponent,
    BasicEdClasslistComponent,
    BasicEdClassScheduleComponent,
    EGSComponent,
    EgsPrintComponent,
    PayslipComponent,
    PreEnrollmentComponent,
    PreEnrollmentModalComponent,

    OfficialBusinessComponent,

    HsEnrollmentFamilyBackgroundComponent,

    HsEnrollmentFamilyBackgroundFamilymemberComponent,

    HsEnrollmentFamilyBackgroundGuardianComponent,

    ElemClassScheduleComponent,

    EducationalBcgComponent,

    ShsCollegePreEnrollmentComponent,

    FacultyEvaluationComponent,

    QuestionnaireComponent,
    EnrollmentFormComponent,
    LeaveManagementComponent,
    AddDateTimeComponent,
    EditLeaveDurationsComponent,
    CheckScheduleComponent,
    AddDateTimeFacultyComponent
  ],
  entryComponents: [
    AnnouncementComponent,
    WifiStatusComponent,
    AddressLookupComponent,
    FaqsComponent,
    DisclaimerComponent,
    TermsOfUseComponent,
    ChangePasswordComponent,
    RemainingComponent,
    EnrolmentFormComponent,
    SeminarsTrainingsComponent,
    OrganizationsComponent,
    ResearchPubComponent,
    DeleteConfirmationDialogComponent,
    GenerateCvComponent,
    CommunityEngagementComponent,
    ApplicationPickComponent,
    AwardsComponent,
    SpeakingEngagementComponent,
    ClasslistComponent,
    AdmittedDialogComponent,
    LeaveComponent,
    FilterComponent,
    AddDateComponent,
    BasicEdClasslistComponent,
    BasicEdClassScheduleComponent,
    EgsPrintComponent,
    PreEnrollmentModalComponent,
    OfficialBusinessComponent,
    HsEnrollmentFamilyBackgroundComponent,
    HsEnrollmentFamilyBackgroundFamilymemberComponent,
    HsEnrollmentFamilyBackgroundGuardianComponent,
    EducationalBcgComponent,

    QuestionnaireComponent,
    AddDateTimeComponent,
    CheckScheduleComponent,
    AddDateTimeFacultyComponent
  ],
  imports: [
    BrowserModule,
    AmazingTimePickerModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpModule, StorageServiceModule,
    NgxPaginationModule, HttpClientModule,
    NgxMaterialTimepickerModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [GlobalService]
      }
    })
  ],
  providers: [StudentApiService, GlobalService, DatePipe, ExcelService, LeaveService, LeavePushService, LeaveValidationService, PasswordResetService, BuildDetailsHttpService, AuthGuardGuard],
  bootstrap: [AppComponent]


})
export class AppModule { }

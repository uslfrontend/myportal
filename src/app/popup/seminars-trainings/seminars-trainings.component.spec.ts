import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarsTrainingsComponent } from './seminars-trainings.component';

describe('SeminarsTrainingsComponent', () => {
  let component: SeminarsTrainingsComponent;
  let fixture: ComponentFixture<SeminarsTrainingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarsTrainingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarsTrainingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

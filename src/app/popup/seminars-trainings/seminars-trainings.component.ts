import { Component, OnInit } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';


import { DeleteConfirmationDialogComponent } from './../../popup/delete-confirmation-dialog/delete-confirmation-dialog.component';

@Component({
  selector: 'app-seminars-trainings',
  templateUrl: './seminars-trainings.component.html',
  styleUrls: ['./seminars-trainings.component.css']
})
export class SeminarsTrainingsComponent implements OnInit {

	progTypeArr
	seminardesc
	companyname
	sdate
	edate
	STID
	venue

	targetupdateID
	type


  constructor(public dialogRef: MatDialogRef<SeminarsTrainingsComponent>,
  	@Inject(MAT_DIALOG_DATA) public data: any,
  	public dialog: MatDialog,
  	private global: GlobalService,
  	private http: Http,
    private fb: FormBuilder,
  	) {
  	this.http.get(this.global.api+'EmployeePortal/TraningType',this.global.option)
	  .map(response => response.json())
	  .subscribe(res => {
	   this.progTypeArr = res.data;
	  },Error=>{
	    this.global.swalAlertError();
	  });
  }

  ngOnInit() {
  	if(this.data.type == "Update"){
  		this.type = this.data.type;
  		this.seminardesc = this.data.selectedData.seminardescription;
		  this.companyname = this.data.selectedData.companyname;
		  var sd = new Date(this.data.selectedData.startdate);
	  	var ed = new Date(this.data.selectedData.enddate);
      

	  	this.sdate = sd;
	  	this.edate = ed;
  		this.STID = this.data.selectedData.trainingTypeID.toString();
  		this.venue = this.data.selectedData.venue
  		this.targetupdateID = this.data.selectedSID
  	}
  }

  onNoClickclose(): void {
    this.dialogRef.close({result:'cancel'});
  }
  

  save(): void {

  	if(this.data.type == "Update"){
  		this.global.swalLoading('Updating Seminar and Training');
	    this.http.put(this.global.api+'EmployeePortal/SeminarsAndTrainings/'+this.targetupdateID,{
	              "seminardesc": this.seminardesc,
              "companyname": this.companyname,
              "sdate": this.sdate.toLocaleString(),
              "edate": this.edate.toLocaleString(),
              "trainingTypeID": this.STID,
              "statusID": 0,
              "venue": this.venue
            },this.global.option)
                                .map(response => response.json())
                                .subscribe(res => {
                                  //console.log(res)
                                  this.global.swalClose();
                                  //this.global.swalAlert(res.message,"",'success');
                                  this.dialogRef.close({result:"Update success"});    
                                },Error=>{
                                  this.global.swalAlertError();
                                  console.log(Error)
                                });
  	}else{
  		this.global.swalLoading('Adding Seminar and Training');
    	this.http.post(this.global.api+'EmployeePortal/SeminarsAndTrainings/'+this.global.requestid(),{
              "seminardesc": this.seminardesc,
              "companyname": this.companyname,
              "sdate": this.sdate,
              "edate": this.edate,
              "trainingTypeID": this.STID,
              "venue": this.venue
            },this.global.option)
                                .map(response => response.json())
                                .subscribe(res => {
                                  //console.log(res)
                                  this.global.swalClose();
                                  //this.global.swalAlert(res.message,"",'success');
                                  this.dialogRef.close({result:"Adding Success"});    
                                },Error=>{
                                  this.global.swalAlertError();
                                  console.log(Error)
                                });
  	}                            
  }
  deletesat(){
  	const dialogRef = this.dialog.open(DeleteConfirmationDialogComponent, {
          width: '500px', disableClose: true,data:{message:"the selected item"}
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.result=='deleteConfirm') {
            this.http.delete(this.global.api+'EmployeePortal/SeminarsAndTrainings/'+this.targetupdateID,this.global.option)
            .map(response => response.json())
            .subscribe(res => {

                this.global.swalClose();
              	this.dialogRef.close({result:"Delete Success"}); 
            },Error=>{
                      //console.log(Error);
                      this.global.swalAlertError();
                      //console.log(Error)
            });
          }
          else{

          }
        });
  }

}

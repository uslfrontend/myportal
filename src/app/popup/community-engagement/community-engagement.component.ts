import { Component, OnInit } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';


import { DeleteConfirmationDialogComponent } from './../../popup/delete-confirmation-dialog/delete-confirmation-dialog.component';
@Component({
  selector: 'app-community-engagement',
  templateUrl: './community-engagement.component.html',
  styleUrls: ['./community-engagement.component.css']
})
export class CommunityEngagementComponent implements OnInit {

  
  date
  location
  activity

  type='';
  targetUpdateID


  constructor(public dialogRef: MatDialogRef<CommunityEngagementComponent>,
  	@Inject(MAT_DIALOG_DATA) public data: any,
  	public dialog: MatDialog,
  	public global: GlobalService,
  	private http: Http,
    private fb: FormBuilder,) { }

  ngOnInit() {
  	this.type = this.data.type;
  	if (this.data.type == "Update") {
  		this.activity = this.data.selectedData.activity
	  	this.location = this.data.selectedData.location
	  	var sdate = new Date(this.data.selectedData.dateConducted);
	    this.date=sdate;
	    this.targetUpdateID = this.data.selectedCEID
  	}
  }

  onNoClickclose(): void {
    this.dialogRef.close({result:'cancel'});
  }
  

  save(): void {

  	if(this.data.type == "Update"){
  		this.http.put(this.global.api+'EmployeePortal/CommunityExtension/'+this.data.selectedCEID,{
              "activity": this.activity,
              "dateConducted": this.date,
              "statusID": 1,
              "location": this.location,
            },this.global.option)
                                .map(response => response.json())
                                .subscribe(res => {
                                  console.log(res)
                                  this.global.swalAlert(res.message,"",'success');
                                  this.dialogRef.close({result:"Update success"});    
                                },Error=>{
                                  this.global.swalAlertError();
                                  console.log(Error)
                                });
  	}else{
  		console.log(this.activity)
  		this.http.post(this.global.api+'EmployeePortal/CommunityExtension/'+this.global.requestid(),{
              "activity": this.activity,
              "dateConducted": this.date,
              "location": this.location,
            },this.global.option)
                                .map(response => response.json())
                                .subscribe(res => {
                                  console.log(res)
                                  this.global.swalAlert(res.message,"",'success');
                                  this.dialogRef.close({result:"Adding Success"});    
                                },Error=>{
                                  this.global.swalAlertError();
                                  console.log(Error)
                                });
  	}                            
  }
  deleteRes(){
  	const dialogRef = this.dialog.open(DeleteConfirmationDialogComponent, {
          width: '500px', disableClose: true,data:{message:"the selected item"}
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.result=='deleteConfirm') {
            this.http.delete(this.global.api+'EmployeePortal/Research/'+this.targetUpdateID,this.global.option)
            .map(response => response.json())
            .subscribe(res => {

                this.global.swalClose();
              	this.dialogRef.close({result:"Delete Success"}); 
            },Error=>{
                      //console.log(Error);
                      this.global.swalAlertError();
                      //console.log(Error)
            });
          }
          else{

          }
        });
  }

}

import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {


  reasonfilter = ''
  fromdatefilter
  todatefilter
  fterm

  dateFiled = ''
  leaveType = ''
  reason = ''
  from = ''
  to = ''
  totalMinutes = ''
  action = ''
  leaveHistoryArray
  filteredLeaveHistoryArray = []

  constructor(public dialogRef: MatDialogRef<FilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public global: GlobalService,
    private http: Http,
    private fb: FormBuilder,) { }

  ngOnInit() {
  }


  filterall() {

  }



  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

  save() {
    
    this.leaveHistoryArray = this.data

    this.filteredLeaveHistoryArray = []

    if (this.dateFiled != '' && this.dateFiled != undefined) {
      var dateString = this.dateFiled;
      var dateObj = new Date(dateString);
      var year = dateObj.getFullYear();
      var month = ('0' + (dateObj.getMonth() + 1)).slice(-2);
      var day = ('0' + dateObj.getDate()).slice(-2);
      var convertedDate = year + '-' + month + '-' + day + 'T00:00:00';
    }else{
      convertedDate = ''
    }

    for (var x = 0; x < this.leaveHistoryArray.leaveHistoryArr.length; x++) {
      if (
        (this.leaveHistoryArray.leaveHistoryArr[x].dateFiled.includes(convertedDate)) &&
        (this.leaveHistoryArray.leaveHistoryArr[x].leaveTypeDescription.toLowerCase().includes(this.leaveType.toLowerCase())) &&
        (this.leaveHistoryArray.leaveHistoryArr[x].leaveDescription.toString().toLowerCase().includes(this.reasonfilter.toString().toLowerCase())) &&
        (this.leaveHistoryArray.leaveHistoryArr[x].totalMinutes.toString().includes(this.totalMinutes.toString()))
      ) {
        this.filteredLeaveHistoryArray.push(this.leaveHistoryArray.leaveHistoryArr[x])
      }
    }
    this.dialogRef.close({ result: this.filteredLeaveHistoryArray });
  }

}
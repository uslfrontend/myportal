import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import {FormBuilder} from '@angular/forms';
import { GlobalService } from './../../../global.service';

@Component({
  selector: 'app-official-business',
  templateUrl: './official-business.component.html',
  styleUrls: ['./official-business.component.css']
})
export class OfficialBusinessComponent implements OnInit {

	venue = '';
	companyname = '';
	STID = 0;
	stidname

	progTypeArr = [];
	 
  constructor(public dialogRef: MatDialogRef<OfficialBusinessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private http: Http,
    private fb: FormBuilder,
  	public global: GlobalService,) { }

  ngOnInit() {
  	this.progTypeArr = this.getProgramType();
    this.venue = this.data.venue;
    this.companyname = this.data.sponsor;
    this.STID = this.data.stid;
  }

  getProgramType(){
      var that = this;
      that.progTypeArr = [];
      this.http.get(this.global.api+'EmployeePortal/TraningType',this.global.option)
      .map(response => response.json())
      .subscribe(res => {
       res.data.forEach(function(row){
         that.progTypeArr.push(row)
       });

      },Error=>{
        this.global.swalAlertError();
      });

      return this.progTypeArr;
    }

  	setTrainingType(x){
  		// console.log(x)
  		this.stidname = x;
  	}
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  onNoClickclose(): void {
	    this.dialogRef.close({result:'cancel',obtriggerctr:false});
	  }

	save(): void {
    if(this.venue.length>0 && this.companyname.length>0 && this.STID>0){
		  this.dialogRef.close({
        result:'Success',
        obDetails:[{
          "STID": this.STID,
          "venue":this.venue,
          "sponsor":this.companyname,
          "stidname":this.stidname
        }],
        obtriggerctr:true})
    }
    else{
      this.global.swalAlert("Warning","Please fill in all fields","warning")
    }

	}  
}

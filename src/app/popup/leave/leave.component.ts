import { Component, OnInit } from '@angular/core';
import { Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { LeaveService } from './../../pages/leaves/services/empportal-services/leave-service.service';
import { LeavePushService } from './../../pages/leaves/services/empportal-services/leave-push-service.service';
import { LeaveValidationService } from './../../pages/leaves/services/empportal-services/leave-validation-service.service';


import { AddDateComponent } from './add-date/add-date.component';
import { DeleteConfirmationDialogComponent } from './../../popup/delete-confirmation-dialog/delete-confirmation-dialog.component';


import { OfficialBusinessComponent } from './../../popup/leave/official-business/official-business.component';



// import * as moment from 'moment';

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.css']
})
export class LeaveComponent implements OnInit {

  appointmentList
  AID
  LID
  activeAppoinments = []
  Ltype = 0;

  myDate = new Date();


  reason
  venue = ''
  selectedDept
  companyname = ''
  STID = 0;


  leaveDeductionHrs = [{
    'multiplier': 6.5,///MGCQ
    'active': true,
  }, {
    'multiplier': 7,///MECQ
    'active': false
  }, {
    'multiplier': 9,///MTWTh
    'active': false
  }, {
    'multiplier': 8,///Fri
    'active': false
  }
  ];



  days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  dayofweek = '';
  obtriggerctr = false;
  selectedLeaveType = 0
  leave_type = 0;

  apptCtrl = 0;

  type;

  tempdata
  tempctr = 0;
  lastrecordedDate = '';
  isVLdisabled = false;
  isSLdisabled = false;
  remarks = ''

  obfromdate
  obtodate

  reasonFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(public dialogRef: MatDialogRef<LeaveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public global: GlobalService,
    private http: Http,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    public leaveService: LeaveService,
    public LeavePushService: LeavePushService,
    public LeaveValidationService: LeaveValidationService,) {

    //this.ngOnInit()
  }

  ngOnInit() {

    this.LeavePushService.tempDatesArr = [];
    this.tempctr = 0;
    this.LeaveValidationService.curSL = [];
    this.LeaveValidationService.curVL = [];
    this.type = this.data.type;
    this.LeaveValidationService.curSL = this.data.currentSL;
    this.LeaveValidationService.curVL = this.data.currentVL;
    this.LeaveValidationService.futSL = this.data.futureSL;
    this.LeaveValidationService.futVL = this.data.futureVL;

    this.getAppointment();
    if (this.data.futureVL[0].totminutes <= 0 && this.data.futureSL[0].totminutes > 0) //condition for validating the remaining vacation leave
    {
      this.isVLdisabled = true;
      console.log(this.data.futureVL[0].totminutes , this.data.futureSL[0].totminutes)
      console.log(this.isSLdisabled)
    }

    if (this.type == 'Add') {
      this.verifyEmpPosition();
    }
    else {
      console.log(this.data.selectedData[0].officeId)
      this.getDepartment(this.data.selectedData[0].officeId);
      // this.selectedDept = this.leaveService.getDepartment(this.data.selectedData[0].officeId)
      // console.log(this.selectedDept)
      this.myDate = this.data.selectedData[0].dateFiled;
      this.reason = this.data.selectedData[0].leaveDescription;
      this.remarks = this.data.selectedData[0].remarks;
      this.selectedLeaveType = this.data.selectedData[0].leaveTypeId.toString();
      this.leave_type = this.data.selectedData[0].leaveTypeId.toString();
      this.LID = this.data.selectedData[0].leaveId
      this.AID = this.data.selectedData[0].officeId;

      this.obtriggerctr = true;
      this.getOBDetails(this.LID);

      var that = this;
      this.data.selectedData.forEach(function (row) {
        let totRes = that.leaveService.getTotalMins(row.startDate.substring(11, 16), row.endDate.substring(11, 16))
        var minDiff = (totRes.mindiff)
        that.lastrecordedDate = row.dateFiled;
        that.LeavePushService.tempDatesArr.push({
          tdate: row.startDate.substring(0, 10),
          tin: row.startDate.substring(11, 16),
          tout: row.endDate.substring(11, 16),
          LID: row.leaveId,
          AID: row.officeId,
          totMins: minDiff
        });
        that.tempctr += 1;
      });
    }
  }

  getDepartment(deptID) {
    console.log(deptID)
    this.http.get(this.global.api + 'HRISMaintenance/Department?departmentID=' + deptID, this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.selectedDept = res.data[0].departmentName;
      }, Error => {
        this.global.swalAlertError();
      });
  }

  getAppointment() {
    this.appointmentList = []
    var that = this;
    this.http.get(this.global.api + 'EmployeePortal/Appointment/' + this.global.empInfo.idnumber + '?active=1', this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.apptCtrl = res.data.length;
        if (this.apptCtrl == 1) {
          this.selectedDept = res.data[0].departmentName
          this.AID = res.data[0].departmentID
          this.getDepartment(this.AID)

        }
        else {

          this.appointmentList = res.data

          //block of codes when employee has more than 1 active appoinment
        }

      }, Error => {
        this.global.swalAlertError();
      });
  }

  getOBDetails(LID) { // retrieval of Official Business leave information/details

  }

  recordDname(param) {
    // this.selectedDept = param
    // console.log(param)
    // this.getDepartment(param)
    this.http.get(this.global.api + 'HRISMaintenance/Department?departmentID=' + param, this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        this.selectedDept = res.data[0].departmentName;
      }, Error => {
        this.global.swalAlertError();
      });
  }

  assignLeaveType(param) {
    this.Ltype = param
  }

  compare(i, date): boolean {
    if (i > 0) {

      if (this.LeavePushService.tempDatesArr[i].tdate == this.LeavePushService.tempDatesArr[i - 1].tdate) {

        return true
      } else {

        return false;
      }
    }
    else {
      return false
    }
  }

  stidname
  openOBForm() {
    // console.log('obform')
    const dialogRef = this.dialog.open(OfficialBusinessComponent, {
      width: '500px', disableClose: true, data: { venue: this.venue, stid: this.STID, sponsor: this.companyname }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'Success') {
        console.log(result.obDetails)
        // console.log(result.obDetails[0].sponsor)
        this.obtriggerctr = result.obtriggerctr;
        this.venue = result.obDetails[0].venue;
        this.companyname = result.obDetails[0].sponsor;
        this.STID = result.obDetails[0].STID;
        this.stidname = result.obDetails[0].stidname
      }
      else {
        this.obtriggerctr = result.obtriggerctr;
        this.selectedLeaveType = 0;
        this.leave_type = 0;
      }
    });
  }

  /////////////////////////////////////////////////////////

  addEntry(param) {
    console.table(param)
    if (this.selectedLeaveType <= 0) {
      this.global.swalAlert('Warning', 'Please select a valid leave type', 'error')
    }
    else {
      var that = this;
      var totalHour = 0;
      var minDiff
      var diff

      var datetoday
      var timeStart
      var timeEnd

      const dialogRef = this.dialog.open(AddDateComponent, {
        width: '600px', disableClose: false, data: {
          selectedData: this.tempdata,
          type: param,
          lastDate: this.lastrecordedDate,
          AID: this.AID,
          leavetype: this.selectedLeaveType
        }
      });

      dialogRef.afterClosed().subscribe(result => {

        if (result.result == 'Success') {
          this.obfromdate = result.obfromdate;
          this.obtodate = result.obtodate;
          /*var datetoday = new Date(result.tdate);
          for (var i = this.days.length - 1; i >= 0; i--) {
            if(datetoday.getDay()== i){
              this.dayofweek = this.days[i];
              break;
            }
          }*///this is use to get the dayOfWeek of selected date

        }
        else if (result.result == "Update") {


        }
      });
    }

    ///////////////////////////////////////////////////////////FUNCTIONS
    function getDayTotalHour(x) {

      that.http.get(that.global.api + 'Employee/Schedule/' + that.global.requestid(), that.global.option)
        .map(response => response.json())
        .subscribe(res => {
          if (res.data != null) {

            res.data.forEach(function (row) {
              if (row.dayOfWeek.toLowerCase() == x.toLowerCase) {
                totalHour += row.hours;
              }
            });
            that.global.swalClose();
          }
          //console.log(res.data)
        }, Error => {
          console.log(Error)
        });
    }

  }
  leavetoDelete = [];
  deleteEntry() {
    let aCindex = this.LeavePushService.tempDatesArr.findIndex(item => item === this.tempdata.id);
    //console.log(aCindex)
    this.LeavePushService.tempDatesArr.splice(aCindex, 1);
    if (this.type == 'Update')
      this.leavetoDelete.push({ 'lid': this.tempdata.LID });

  }

  setdata(param) {
    this.tempdata = param
  }

  executeDeleteLeave() {
    var that = this;

    this.leavetoDelete.forEach(function (row) {
      //console.log(row)
      that.http.delete(that.global.api + 'LeaveManagement/Leave/' + that.global.requestid() + '?leaveId=' + row.lid, that.global.option)
        .map(response => response.json())
        .subscribe(res => {
          that.global.swalClose();
        }, Error => {
          //console.log(Error);
          that.global.swalAlertError();
          //console.log(Error)
        });
    });
  }

  /////////////////////////////////////////////////////////////////////////////////////

  onNoClickclose(): void {

    this.dialogRef.close({ result: 'cancel' });
  }

  newlyInsertedOBSeminarID = [];
  save() {
    var that = this;
    console.log(that.global.empInfo.idnumber)
    console.log(that.global.sy)
    console.log(that.myDate)
    console.log(that.selectedLeaveType)
    console.log(that.reason)
    console.log(parseInt(that.AID))
   
    if (this.type == "Add") {
      that.LeavePushService.tempDatesArr.forEach(function (row) {
        let latest_date = that.datePipe.transform(row.tdate, 'yyyy-MM-dd');
        that.http.post(that.global.api + 'LeaveManagement/Leave', {
          "IdNumber": that.global.empInfo.idnumber,
          "SchoolYear": that.global.sy,
          "Date": that.myDate,
          "Type": that.selectedLeaveType,
          "Description": that.reason,
          "SDate": (latest_date + 'T' + row.tin).toString(),
          "EDate": (latest_date + 'T' + row.tout).toString(),
          "TotalMinutes": parseInt(row.totMins),
          "OfficeId": parseInt(that.AID),
          "Remarks": that.remarks
        }, that.global.option)
          .map(response => response.json())
          .subscribe(res => {
            console.log(res, 'add')
            if (that.selectedLeaveType == 4) {
              that.http.post(that.global.api + 'EmployeePortal/SeminarsAndTrainings/' + that.global.requestid(), {
                "seminardesc": "OB",
                "companyname": that.companyname,
                "sdate": that.obfromdate,
                "edate": that.obtodate,
                "trainingTypeID": that.STID,
                "venue": that.venue
              },
                that.global.option).map(response => response.json())
                .subscribe(res1 => {
                  insertOBLeaveSemID(res.data, res1.data);
                  that.global.swalClose();
                  //this.global.swalAlert(res.message,"",'success');
                  that.dialogRef.close({ result: "Adding Success" });
                }, Error => {
                  that.global.swalAlertError();
                  console.log(Error)
                });
            }
            that.global.swalClose();
            that.LeavePushService.tempDatesArr = [];
            that.dialogRef.close({ result: 'Added' });
          }, Error => {
            that.global.swalAlertError();
            console.log(Error)
          });
      });
    }
    else {
      that.LeavePushService.tempDatesArr.forEach(function (row) {
        let latest_date = that.datePipe.transform(row.tdate, 'yyyy-MM-dd');
        that.http.put(that.global.api + 'LeaveManagement/Leave/' + that.global.empInfo.idnumber + '?leaveId=' + row.LID, {
          "IdNumber": that.global.empInfo.idnumber,
          "SchoolYear": that.global.sy,
          "Date": that.myDate,
          "Type": that.selectedLeaveType,
          "Description": that.reason,
          "SDate": (latest_date + 'T' + row.tin).toString(),
          "EDate": (latest_date + 'T' + row.tout).toString(),
          "TotalMinutes": parseInt(row.totMins),
          "OfficeId": that.AID,
          "Remarks": that.remarks

        }, that.global.option)
          .map(response => response.json())
          .subscribe(res => {
            console.log(res)
            // console.log((row.LID).toString()+'-'+(row.AID).toString()+'-'+(latest_date).toString());
            that.global.swalClose();
            if (that.leavetoDelete.length > 0)
              that.executeDeleteLeave();
            that.dialogRef.close({ result: 'Updated' });
          }, Error => {
            that.global.swalAlertError();
            console.log(Error)
          });
      });
    }
    that.LeavePushService.tempDatesArr = [];

    //Functions//////////////////////////////////////////////////////////////////////
    function insertOBLeaveSemID(leaveID, seminarID) {
      console.log("LID: " + leaveID + "--" + "SID: " + seminarID)
      that.http.post(that.global.api + 'LeaveManagement/Leave/SeminarsAndTrainings/', {
        "SeminarId": parseInt(seminarID),
        "LeaveId": parseInt(leaveID)
      }, that.global.option)
        .map(response => response.json())
        .subscribe(res => {
          console.log(res)
        }), Error => {
          that.global.swalAlertError();
          console.log(Error)
        };
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////
  obtrigger(param) {
    this.obtriggerctr = param
  }
  setRdbValue(param) {
    this.selectedLeaveType = param
    if (param == 4) {
      if (this.LeaveValidationService.validationObtriggerctr == true)
        this.LeaveValidationService.validationObtriggerctr = false;
      else
        this.LeaveValidationService.validationObtriggerctr = true;
    }
  }



  basicEdCtrl = false;
  higherEdCtrl = false;
  ntp = false;
  formCtrl = 0;
  verifyEmpPosition() {
    console.log(this.global.empInfo.position)
    if (this.global.empInfo.position.toLowerCase().includes('teacher')) {
      this.basicEdCtrl = true;
      this.formCtrl += 1;
    }
    if (this.global.empInfo.position.toLowerCase().includes('principal')) {
      this.basicEdCtrl = true;
      this.formCtrl += 1;
    }
    if (this.global.empInfo.position.toLowerCase().includes('instructor')) {
      this.higherEdCtrl = true;
      this.formCtrl += 1;
    }
    if (this.global.empInfo.position.toLowerCase().includes('dean')) {
      this.higherEdCtrl = true;
      this.formCtrl += 1;
    }
    if (this.global.empInfo.position.toLowerCase().includes('professor')) {
      this.higherEdCtrl = true;
      this.formCtrl += 1;
      console.log(this.formCtrl, 'here')
    }
    else {
      this.ntp = true;
      this.formCtrl += 1;
    }

    if (this.formCtrl == 1 && this.ntp == true) {
      this.Ltype = 1;

    }
    console.log(this.global.empInfo.position.toLowerCase())
    console.log(this.formCtrl)
    console.log(this.Ltype)
  }
}

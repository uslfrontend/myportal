import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../global.service';
import {FormBuilder} from '@angular/forms';

import { LeaveService } from './../../../pages/leaves/services/empportal-services/leave-service.service';
import { LeavePushService } from './../../../pages/leaves/services/empportal-services/leave-push-service.service';
import { LeaveValidationService } from './../../../pages/leaves/services/empportal-services/leave-validation-service.service';
import { LeavePostService } from './../../../pages/leaves/services/empportal-services/leave-post.service';

import { AmazingTimePickerService } from 'amazing-time-picker';
import * as moment from 'moment';

// var moment = require('moment'); // require
// moment().format(); 

import Swal from 'sweetalert2';
const swal = Swal;
@Component({
  selector: 'app-add-date',
  templateUrl: './add-date.component.html',
  styleUrls: ['./add-date.component.css']
})
export class AddDateComponent implements OnInit {

  disabled = true;
  fromdate
  todate
  addedTimeArr_12hour = [];
  addedTimeArr_24hour = [];
  appendtimeTrigger = false;
  fireTimeTrigger = false;

  activeDays = [
    {day:'Monday','ctrl':false},
    {day:'Tuesday','ctrl':false},
    {day:'Wednesday','ctrl':false},
    {day:'Thursday','ctrl':false},
    {day:'Friday','ctrl':false},
    {day:'Saturday','ctrl':false}
  ];

  constructor(public dialogRef: MatDialogRef<AddDateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private http: Http,
    private fb: FormBuilder,
    private atp: AmazingTimePickerService,
    public leaveService:LeaveService,
    public LeavePushService:LeavePushService,
    public LeaveValidationService:LeaveValidationService,
    public LeavePostService:LeavePostService,
    ) { }
	
	  date
  	timeIn = "8:00";
  	TimeOut = "12:00";
  	recordTimeIn_24hour  = "";
  	recordTimeOut_24hour  = "";
    recordTimeIn_12hour  = "";
    recordTimeOut_12hour  = "";
    totalComputedMins = 0;
    recordDate;
  	type
    pretype
    AID
    Schedule;

  ngOnInit() {
    this.Schedule=this.leaveService.getSchedule();
    // console.log(this.Schedule)
  	this.type= this.data.type;
    if(this.data.lastDate != '')
      this.date = this.data.lastDate;
    if(this.type == 'date' || this.type == 'time')
      this.pretype = "Add";
    else{
      // console.log(this.data.selectedData)
      this.pretype = "Update";
      this.fromdate = this.data.selectedData.tdate
      this.todate = this.data.selectedData.tdate
      this.recordTimeIn_12hour = this.LeaveValidationService.validatetime(this.data.selectedData.tin);
      this.recordTimeOut_12hour = this.LeaveValidationService.validatetime(this.data.selectedData.tout);
      this.recordTimeIn_12hour = this.LeaveValidationService.validatetime(this.data.selectedData.tin);
      this.recordTimeOut_12hour = this.LeaveValidationService.validatetime(this.data.selectedData.tout);
      this.recordTimeIn_24hour = this.data.selectedData.tin;
      this.recordTimeOut_24hour = this.data.selectedData.tout;
      this.timeIn = this.recordTimeIn_12hour;
      this.TimeOut = this.recordTimeOut_12hour;
      this.appendtime()
    }
    this.AID = this.data.AID;
  }
  

  setDay(i){
    if(this.activeDays[i].ctrl == true)
      this.activeDays[i].ctrl = false;
    else
      this.activeDays[i].ctrl = true;
  }

  setDR(){

    if(this.disabled == true)
      this.disabled = false;
    else
      this.disabled = true;
  }

  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Prevent Saturday and Sunday from being selected.
    return day !== 0 && day !== 6;
    // return day !== 0;
  }

  cleartime(){
    this.recordTimeIn_12hour = '';
    this.recordTimeOut_12hour = '';
    this.recordTimeIn_24hour = '';
    this.recordTimeOut_24hour = '';


    this.fireTimeTrigger = false;
    //this.totalComputedMins = 0;
  }

  

  appendtime(){

    let startdate = moment(this.fromdate);
    let enddate = moment(this.todate);
    var vvl = this.LeaveValidationService.curVL[0].vvlTotalMin;
    var ssl = this.LeaveValidationService.curSL[0].sslTotalMin;

    var fvvl = this.LeaveValidationService.futVL[0].totminutes;
    var fssl = this.LeaveValidationService.futSL[0].totminutes;
    console.log('fvvl='+this.LeaveValidationService.futVL[0].totminutes)
    var that = this;
    
    if(this.totalComputedMins==0){
       var temptotalComputedMins = this.leaveService.verifyTotalMinutes(
        startdate,
        enddate,
        this.recordTimeIn_24hour,
        this.recordTimeOut_24hour,
        this.totalComputedMins,
      );

      this.totalComputedMins +=temptotalComputedMins; 
    }
    

    let start = moment(this.fromdate);
    let end = moment(this.todate);

    var totalDays = end.diff(start,'days')+1;
    if(totalDays>5)
      totalDays = totalDays-(Math.round(totalDays/7)*2); //removes Sundays and Saturdays

    if(fvvl >0 || fssl>0){
      if(this.data.leavetype == 2){
        if((this.totalComputedMins)<=fvvl){
          this.appendtimeTrigger = true;
          this.pushTime(this.recordTimeIn_24hour,this.recordTimeOut_24hour);
        }
        else{
          this.global.swalAlert('',"Insufficient leave credits on selected leave type."+'\n'+"Available vacation leave credit/s: "+((fvvl/60))+"hrs","error");
          this.cleartime();
        }
      }
      else if(this.data.leavetype == 1){
        if((this.totalComputedMins)<=fssl){
          this.appendtimeTrigger = true;
          this.pushTime(this.recordTimeIn_24hour,this.recordTimeOut_24hour);
        }
        else{
          this.global.swalAlert('',"Insufficient leave credits on selected leave type. Available sick leave credit/s: "+((fssl/60))+"hrs","error");
          this.cleartime();
        }
      }
      else{
        this.appendtimeTrigger = true;
        this.pushTime(this.recordTimeIn_24hour,this.recordTimeOut_24hour);
      }
    }
    else{
      if(this.data.leavetype == 1 || this.data.leavetype == 2) //todo: ask confirmation if employee still wants to leave or not
        this.global.swalAlert('',"Leave credits on selected leave type is now 0,\nleave application will automatically treated as salary deduction",'');
      this.appendtimeTrigger = true;
      this.pushTime(this.recordTimeIn_24hour,this.recordTimeOut_24hour);
    }
    
  }
  pushTime(tin,tout){
    let totRes = this.leaveService.getTotalMins(tin,tout)
    var mins = (totRes.mindiff)
    this.addedTimeArr_24hour.push({
        from:tin,
        to:tout,
        totMins:mins
      });
  }
  resetMins(x){
    console.log(this.totalComputedMins)
    
    this.totalComputedMins = this.totalComputedMins-x;
    console.log(this.totalComputedMins)
  }

  openIndialog(){
    var _24hour = '';
  	const ref = this.atp.open({
  		time: this.timeIn,
  		theme: 'material-blue',
  	});

  	ref.afterClose().subscribe((data) => {
      
      _24hour = data;
      this.recordTimeIn_24hour = _24hour;
  		this.recordTimeIn_12hour = this.LeaveValidationService.validatetime(data);
      this.fireTimeTrigger = this.leaveService.checkfireTrigger(this.recordTimeIn_24hour,this.recordTimeOut_24hour);
  	});
  	
  }
  openIndialog1(){
    var _24hour = '';
    const ref = this.atp.open({
      time: this.TimeOut,
      theme: 'material-blue',
    });
    ref.afterClose().subscribe((data) => {
      
      _24hour = data;
      this.recordTimeOut_24hour = _24hour;
      this.recordTimeOut_12hour = this.LeaveValidationService.validatetime(data);
      this.fireTimeTrigger = this.leaveService.checkfireTrigger(this.recordTimeIn_24hour,this.recordTimeOut_24hour);
    });
    
  }

  // deleteAppendedTime(i){
  //   // console.log(this.addedTimeArr_24hour[i])
  //   this.addedTimeArr_24hour.splice(i, 1)
  // }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  onNoClickclose(): void {
    this.dialogRef.close({result:'cancel'});
  }

  

  save(): void {

    let start = moment(this.fromdate);
    let end = moment(this.todate);

    var tempDateStart = start;

    var data = [];
    var timeStart
    var timeEnd
    var that = this;
    var totalHour = 0;
    var minDiff
    var diff

    var totalDays = end.diff(start,'days')+1;
    // if(totalDays>5)
    //   totalDays = totalDays-(Math.round(totalDays/7)*2); //removes Sundays and Saturdays

    if(that.pretype == "Add"){
      // console.log('totaldays: '+end.diff(start,'days'))
      this.LeaveValidationService.validationOBFromDate = tempDateStart.format('L');
      for(var x = 0; x<totalDays;x++)
      {
        
        that.addedTimeArr_24hour.forEach(function(row){
          let totRes = that.leaveService.getTotalMins(row.from,row.to)
          minDiff = (totRes.mindiff)

          if(tempDateStart.isoWeekday() === 6 || tempDateStart.isoWeekday() === 7){
            // console.log("aaa")
          }
          else{
            that.LeavePushService.pushDateTimeDetails(
              tempDateStart.format('L'),
              row.from,
              row.to,
              "",
              that.AID,
              minDiff
            );
          }
          
        });
        tempDateStart.add(1,'days')
      }
      this.LeaveValidationService.validationOBtoDate = tempDateStart.format('L');
      that.dialogRef.close({
        result:"Success" , obfromdate:this.fromdate,obtodate:this.todate});
    }
    else{
      
    }
 
    
    // if(this.recordTimeIn_24hour != ""&&this.recordTimeOut_24hour!=""){
    //   if(this.pretype == "Add")
    //     this.dialogRef.close({
    //       result:"Success" , 
    //       tdate:this.date , 
    //       tin:this.recordTimeIn_12hour, 
    //       tout:this.recordTimeOut_12hour, 
    //       tin_24:this.recordTimeIn_24hour , 
    //       tout_24:this.recordTimeOut_24hour});
    //   else{
    //     this.dialogRef.close({
    //       result:"Update" , 
    //       tdate:this.date , 
    //       tin:this.recordTimeIn_12hour , 
    //       tout:this.recordTimeOut_12hour, 
    //       tin_24:this.recordTimeIn_24hour , 
    //       tout_24:this.recordTimeOut_24hour})
    //   }
    // }
    // else
    //   this.global.swalAlert("Invalid Input","Please double check inputs","error")              
  }

}


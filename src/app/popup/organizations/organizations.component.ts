import { Component, OnInit } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';


import { DeleteConfirmationDialogComponent } from './../../popup/delete-confirmation-dialog/delete-confirmation-dialog.component';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css']
})
export class OrganizationsComponent implements OnInit {


  organization
  position
  expDate

  type
  targetUpdateID
  

  constructor(public dialogRef: MatDialogRef<OrganizationsComponent>,
  	@Inject(MAT_DIALOG_DATA) public data: any,
  	public dialog: MatDialog,
  	private global: GlobalService,
  	private http: Http,
    private fb: FormBuilder,
  	) { 
    

  }

  ngOnInit() {
  	if(this.data.type == "Update"){
  		this.type = this.data.type;
  		this.position = this.data.selectedData.position;
  		this.organization = this.data.selectedData.organization;
	  	var ed = new Date(this.data.selectedData.expirationDate);
	  	this.expDate = ed;
  		this.targetUpdateID = this.data.selectedOID
  	}
  }
onNoClickclose(): void {
    this.dialogRef.close({result:'cancel'});
  }
  

  save(): void {

  	if(this.data.type == "Update"){
  		this.global.swalLoading('Updating Organization');
	    this.http.put(this.global.api+'EmployeePortal/Organization/'+this.targetUpdateID,{
	          "position": this.position,
              "organization": this.organization,
              "expirationDate": this.expDate,
              
            },this.global.option)
                                .map(response => response.json())
                                .subscribe(res => {
                                  //console.log(res)
                                  this.global.swalClose();
                                  //this.global.swalAlert(res.message,"",'success');
                                  this.dialogRef.close({result:"Update success"});    
                                },Error=>{
                                  this.global.swalAlertError();
                                  console.log(Error)
                                });
  	}else{

  		this.global.swalLoading('Adding Organization');
    	this.http.post(this.global.api+'EmployeePortal/Organization/'+this.global.requestid(),{
              "position": this.position,
              "organization": this.organization,
              "expirationDate": this.expDate,
            },this.global.option)
	            .map(response => response.json())
	            .subscribe(res => {
	            		//console.log(this.global.requestid()+"\n"+this.expDate)
		              this.global.swalClose();
		              this.dialogRef.close({result:"Adding Success"});    
	            },Error=>{
	              this.global.swalAlertError();
	              console.log(Error)
	            });
  	}                            
  }
  deleteorg(){
  	const dialogRef = this.dialog.open(DeleteConfirmationDialogComponent, {
          width: '500px', disableClose: true,data:{message:"the selected item"}
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.result=='deleteConfirm') {
            this.http.delete(this.global.api+'EmployeePortal/Organization/'+this.targetUpdateID,this.global.option)
            .map(response => response.json())
            .subscribe(res => {

                this.global.swalClose();
              	this.dialogRef.close({result:"Delete Success"}); 
            },Error=>{
                      //console.log(Error);
                      this.global.swalAlertError();
                      //console.log(Error)
            });
          }
          else{

          }
        });
  }
}

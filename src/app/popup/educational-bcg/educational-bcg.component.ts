import { Component, OnInit } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions,} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';


import { AddressLookupComponent } from './../../pages/address-lookup/address-lookup.component';

export interface School {
  address: string;
  companyID: string;
  companyName: string;
}
@Component({
  selector: 'app-educational-bcg',
  templateUrl: './educational-bcg.component.html',
  styleUrls: ['./educational-bcg.component.css']
})
export class EducationalBcgComponent implements OnInit {

  progLevelArr
  progNameArr
  schNameArr

  progLVl='';
  progID='';
  progName='';
  companyID = '';
  schName='';
  yrGrad='';
  sonumber='';
  UnitsEarned = '';


   myControl = new FormControl();
  options: string[] = [];
  programIDwithTitle = [];
  filteredOptions: Observable<string[]>;

  schoolCtrl = new FormControl();
  filteredSchools: Observable<School[]>;

  filteredSchs: Schoollist[] = [];
  usersForm: FormGroup;
  isLoading = false;

  selectedSchName = '';

  schoolAddress = '';




  constructor(public dialogRef: MatDialogRef<EducationalBcgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private global: GlobalService,
    private http: Http,
    private fb: FormBuilder,
    private httpC: HttpClient,
    ) {
  	this.http.get(this.global.api+'PublicAPI/ProgramLevels',this.global.option)
	  .map(response => response.json())
	  .subscribe(res => {
	   this.progLevelArr = res;
	  },Error=>{
	    this.global.swalAlertError();
	  });
 }

  ngOnInit() {



    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.usersForm = this.fb.group({
      userInput: null
    })

    this.isLoading = false
      this.usersForm
      .get('userInput')
      .valueChanges
      .pipe(
        switchMap(value => this.search(this.usersForm
        .get('userInput').value)
        )
      )
      .subscribe(users => this.filteredSchs = users.results);

      if(this.data.type == 2){
        this.progLVl = this.data.selecteddata.programLevel
        this.progName = this.data.selecteddata.programName
        this.myControl.setValue(this.progName);
        this.usersForm.get('userInput').setValue({companyName:this.data.selecteddata.schoolName})
        this.yrGrad = this.data.selecteddata.yearGraduated;
        this.progID = this.data.selecteddata.programID
        this.companyID = this.data.selecteddata.companyID
      }

  }


  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    this.progName = value;
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }


  displayFn(sch: Schoollist) {if (sch) { return sch.companyName; }}

  identifier
  search(filter:any): Observable<IUserResponse> {

  this.isLoading = true
    return this.httpC.get<IUserResponse>(this.global.api+'PublicAPI/Schools',{observe: 'body', responseType: 'json'})
    .pipe(
      tap((response: IUserResponse) => {
        var x= filter;
        if (x.companyName==undefined) {
          this.identifier = '1'
          x= filter;
        }else
        x= filter.companyName;
        this.schNameArr = response

        response.results = this.schNameArr
          .map(sch => new Schoollist(sch.companyID, sch.companyName))
          .filter(sch => sch.companyName.toLowerCase().includes(x.toLowerCase()))

    this.isLoading = false
        return response;
      })
      );
  }

  recordSelectedSchname(sch){

    this.identifier = '2'
    this.selectedSchName = sch.companyName;
    this.companyID = sch.companyID;
  }

  recordSelectedProgname(selectedProgName){
  	this.progName = selectedProgName;
    this.progID = this.getProgramID(this.progName)
  }
  getProgramID(selectedProgName){
    var val=''
    for(let x = 0;x<this.programIDwithTitle.length;x++){
      if(selectedProgName == this.programIDwithTitle[x].programTitle)
        val = this.programIDwithTitle[x].programID
    }
    return val
  }
  currentSelectedProglvl = ''
  getProgramName(selectedProglvl){
    console.log(selectedProglvl)
    this.currentSelectedProglvl = selectedProglvl;
    this.options = [];
    // this.filteredOptions = null;

  	this.http.get(this.global.api+'PublicAPI/ProgramNames/'+selectedProglvl.programLevel,this.global.option)
	  .map(response => response.json())
	  .subscribe(res => {
      console.log(res)
  	   this.progNameArr = res
  	   for (var i = 0; i < this.progNameArr.length; ++i)
        {
            this.options.push(this.progNameArr[i].programTitle);
            this.programIDwithTitle.push({
              "programID": this.progNameArr[i].programID,
              "programTitle": this.progNameArr[i].programTitle
            });
        }
	  },Error=>{
	    this.global.swalAlertError();
	  });

    console.log(this.options)

  }

  verifyClick(){
    if(this.currentSelectedProglvl != ''){
      this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    }
  }


  openDialogSchAdress(): void {
    const dialogRef = this.dialog.open(AddressLookupComponent, {
      width: '500px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result!='cancel') {
        // console.log(lookup)
        this.schoolAddress = result.result
      }
    });
  }




  onNoClickclose(): void {
    this.dialogRef.close({result:'cancel'});
  }

  recordschname
  save(): void {
    //console.log(this.progName+"-----"+this.selectedSchName+"-----"+this.yrGrad);
    this.progName = this.myControl.value;

    if(this.identifier == '1')
      this.recordschname = this.usersForm.get('userInput').value
    else
      this.recordschname = this.selectedSchName;

    //console.log(this.data.selectedEID+"--"+this.progName+"--"+this.recordschname+"--"+this.sonumber+"--"+this.yrGrad);
    this.global.swalLoading('Adding Educational Background');
    this.http.post(this.global.api+'StudentPortal/EducBG',{
              "programlevelid": this.progLVl.toString(),
              "programID": this.progID,
              "programname": this.progName,
              "companyID": this.companyID,
              "school": this.recordschname,
              "yeargraduated": this.yrGrad,
              "otherInfo": this.schoolAddress,
              "sonumber": this.sonumber,
            },this.global.option)
                                .map(response => response.json())
                                .subscribe(res => {
                                  // console.log(res)
                                  this.global.swalClose();
                                  //this.global.swalAlert(res.message,"",'success');
                                  this.dialogRef.close({result:"saved"});
                                },Error=>{
                                  this.global.swalAlertError();
                                  console.log(Error)
                                });

  }
}
export class Schoollist {
  constructor(public companyID: string, public companyName: string) {}
}
export interface IUserResponse {
  total: string;
  results: Schoollist[];
}



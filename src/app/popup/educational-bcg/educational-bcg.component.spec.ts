import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationalBcgComponent } from './educational-bcg.component';

describe('EducationalBcgComponent', () => {
  let component: EducationalBcgComponent;
  let fixture: ComponentFixture<EducationalBcgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationalBcgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationalBcgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

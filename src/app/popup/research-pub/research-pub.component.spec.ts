import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchPubComponent } from './research-pub.component';

describe('ResearchPubComponent', () => {
  let component: ResearchPubComponent;
  let fixture: ComponentFixture<ResearchPubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchPubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchPubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

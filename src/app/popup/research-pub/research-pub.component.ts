import { Component, OnInit } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';


import { DeleteConfirmationDialogComponent } from './../../popup/delete-confirmation-dialog/delete-confirmation-dialog.component';


@Component({
  selector: 'app-research-pub',
  templateUrl: './research-pub.component.html',
  styleUrls: ['./research-pub.component.css']
})
export class ResearchPubComponent implements OnInit {

  rtitle:any = '';
  rmarks:any = '';
  cyear:any = '';
  date:any = '';
  resTypeArr= '';
  RID=''
  type;

  targetUpdateID

  constructor(public dialogRef: MatDialogRef<ResearchPubComponent>,
  	@Inject(MAT_DIALOG_DATA) public data: any,
  	public dialog: MatDialog,
  	public global: GlobalService,
  	private http: Http,
    private fb: FormBuilder,
  	) { 
  	this.http.get(this.global.api+'HRISMaintenance/ResearchTypes',this.global.option)
    .map(response => response.json())
    .subscribe(res => {
      //console.table(res.data)
     this.resTypeArr = res.data;
    },Error=>{
      this.global.swalAlertError();
    });
  }

  ngOnInit() {
    this.type = this.data.type;
    	if(this.data.type == "Update"){
  	  	this.rtitle = this.data.selectedData.title;
  	  	this.rmarks = this.data.selectedData.remarks;
  	  	this.cyear = this.data.selectedData.copyrightYear;
  	  	var sdate = new Date(this.data.selectedData.dateCompleted);
  	    this.date=sdate;
  	    this.RID = this.data.selectedData.researchTypeID.toString();
  	    this.targetUpdateID = this.data.selectedRID
        
  	}
  }

  onNoClickclose(): void {
    this.dialogRef.close({result:'cancel'});
  }
  

  save(): void {

  	if(this.data.type == "Update"){
  		this.global.swalLoading('Updating Research');
	    this.http.put(this.global.api+'EmployeePortal/Research/'+this.targetUpdateID,{
	          "title": this.rtitle,
              "dateCompleted": this.date,
              "remarks": this.rmarks,
              "copyrightYear": this.cyear,
              "researchTypeID": this.RID
              
            },this.global.option)
                                .map(response => response.json())
                                .subscribe(res => {
                                  //console.log(res)
                                  this.global.swalClose();
                                  //this.global.swalAlert(res.message,"",'success');
                                  this.dialogRef.close({result:"Update success"});    
                                },Error=>{
                                  this.global.swalAlertError();
                                  console.log(Error)
                                });
  	}else{

  		this.global.swalLoading('Adding Research');
    	this.http.post(this.global.api+'EmployeePortal/Research/'+this.global.requestid(),{
              "title": this.rtitle,
              "dateCompleted": this.date,
              "remarks": this.rmarks,
              "copyrightYear": this.cyear,
              "researchTypeID": this.RID
              
            },this.global.option)
	            .map(response => response.json())
	            .subscribe(res => {
	            		//console.log(this.global.requestid()+"\n"+this.expDate)
		              this.global.swalClose();
		              this.dialogRef.close({result:"Adding Success"});    
	            },Error=>{
	              this.global.swalAlertError();
	              console.log(Error)
	            });
  	}                            
  }
  deleteRes(){
  	const dialogRef = this.dialog.open(DeleteConfirmationDialogComponent, {
          width: '500px', disableClose: true,data:{message:"the selected item"}
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.result=='deleteConfirm') {
            this.http.delete(this.global.api+'EmployeePortal/Research/'+this.targetUpdateID,this.global.option)
            .map(response => response.json())
            .subscribe(res => {

                this.global.swalClose();
              	this.dialogRef.close({result:"Delete Success"}); 
            },Error=>{
                      //console.log(Error);
                      this.global.swalAlertError();
                      //console.log(Error)
            });
          }
          else{

          }
        });
  }

}

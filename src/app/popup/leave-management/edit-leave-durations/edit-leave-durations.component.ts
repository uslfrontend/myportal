import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { GlobalService } from 'src/app/global.service';
import { HrisLeaveManagementApiService } from 'src/app/hris-leave-management-api.service';

@Component({
  selector: 'app-edit-leave-durations',
  templateUrl: './edit-leave-durations.component.html',
  styleUrls: ['./edit-leave-durations.component.scss']
})
export class EditLeaveDurationsComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EditLeaveDurationsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private HrisApi: HrisLeaveManagementApiService,
    private global: GlobalService,
  ) { }

  daysOftheWeek = []
  empSchedule = []
  fromValue = ''
  toValue = ''
  date
  scheduleChecker = []
  leaveDurationArray = []

  facultySchedule = []
  besFacultySchedule = []
  facultyType

  fromValueNTime = ''
  toValueNTime = '';
  fromAmPassValue = ''
  toAmPassValue = ''

  ngOnInit() {

    this.getEmployeeLeaveManagementSchedule()
    const date = new Date(this.data.sdate)
    this.date = date
    this.fromValue = this.timeFormat(this.data.sdate)
    this.toValue = this.timeFormat(this.data.edate)
    this.leaveDurationArray = this.data.value.leave_LeaveDurations

  }

  convertTime(value, num) {

    if (this.facultyType == 3) {

      if (value != null && value != '') {
        // Split the input time into hours and minutes
        const [hours, minutes] = value.split(':');

        // Parse hours and minutes as integers
        const hoursInt = parseInt(hours, 10);
        const minutesInt = parseInt(minutes, 10);

        // Determine the period (AM or PM)
        const period = hoursInt >= 12 ? 'PM' : 'AM';

        // Convert hours to 12-hour format
        const formattedHours = hoursInt % 12 || 12;

        // Format the time as "h:mm a" (e.g., "3:43 PM")
        const formattedTime = `${formattedHours}:${minutesInt.toString().padStart(2, '0')} ${period}`;

        // Add a console.log statement for debugging
        // console.log(`Input Time: ${value}, Formatted Time: ${formattedTime}`);

        if (num == 1) { this.fromAmPassValue = formattedTime }
        else if (num == 2) { this.toAmPassValue = formattedTime }
        return formattedTime;
      }
      else { return null }

    } else if (this.facultyType == 2) {
      if (value != null && value != '') {
        // Split the input time into hours and minutes
        const [hours, minutes] = value.split(':');

        // Parse hours and minutes as integers
        const hoursInt = parseInt(hours, 10);
        const minutesInt = parseInt(minutes, 10);

        // Determine the period (AM or PM)
        const period = hoursInt >= 12 ? 'PM' : 'AM';

        // Convert hours to 12-hour format
        const formattedHours = hoursInt % 12 || 12;

        // Format the time as "h:mm a" (e.g., "3:43 PM")
        const formattedTime = `${formattedHours}:${minutesInt.toString().padStart(2, '0')} ${period}`;

        // Add a console.log statement for debugging
        // console.log(`Input Time: ${value}, Formatted Time: ${formattedTime}`);

        if (num == 1) { this.fromValueNTime = formattedTime }
        else if (num == 2) { this.toValueNTime = formattedTime }


        return formattedTime;
      }
      else { return null }
    }
  }

  timeFormat(value) {
    const datetimeString = value; // Your datetime string
    const date = new Date(datetimeString);

    // Get the hours and minutes in HH:MM format
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    const timeOnly = hours + ':' + minutes;
    return timeOnly
  }

  myFilter = (d: Date): boolean => {
    const day = d.getDay();
    // Check if the day of the week is in the array. If not, disable the date.
    return this.daysOftheWeek.includes(this.getDayName(day));
  }

  getDayName(day: number): string {
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return weekdays[day];
  }

  separateDays(dayString, x) {
    const daysOfWeek = ['M', 'T', 'W', 'TH', 'F', 'SAT', 'SUN'];
    const result = [];
    let i = 0;

    while (i < dayString.length) {
      let day = dayString[i];
      if (day === 'T' && dayString[i + 1] === 'H') {
        day = 'TH';
        i += 2;
      } else if (day === 'S' && dayString[i + 1] === 'A') {
        day = 'SAT';
        i += 3;
      } else if (day === 'S' && dayString[i + 1] === 'U') {
        day = 'SUN';
        i += 3;
      } else {
        i++;
      }

      if (daysOfWeek.includes(day)) {
        if (day == "M") {
          result.push('Monday');
          this.daysOftheWeek.push('Monday');
        } else if (day == "T") {
          result.push('Tuesday');
          this.daysOftheWeek.push('Tuesday');
        } else if (day == "W") {
          result.push('Wednesday');
          this.daysOftheWeek.push('Wednesday');
        } else if (day == "TH") {
          result.push('Thursday');
          this.daysOftheWeek.push('Thursday');
        } else if (day == "F") {
          result.push('Friday');
          this.daysOftheWeek.push('Friday');
        } else if (day == "SAT") {
          result.push('Saturday');
          this.daysOftheWeek.push('Saturday');
        } else if (day == "SUN") {
          result.push('Sunday');
          this.daysOftheWeek.push('Sunday');
        }

      }
    }
    this.facultySchedule[x].day = result
    // return result;
  }

  convertTo24Hour(time) {
    const [hour, minute] = time.split(":").map(Number);
    if (hour < 7) {
      // If hour is less than 7, add 12 to it
      return `${hour + 12}:${minute < 10 ? "0" + minute : minute}`;
    }
    return time;
  }

  getEmployeeLeaveManagementSchedule() {

    if (this.data.Ltype == 1 || this.data.Ltype == 4) {

      this.global.swalLoading('Loading Employee Schedule.')
      this.HrisApi.getLeaveManagementSchedule()
        .map(response => response.json())
        .subscribe(res => {
          this.empSchedule = res.data
          // console.log(res.data)
          this.daysOftheWeek = res.data.map(item => item.dayOfWeek)
          // console.log(this.daysOftheWeek)
          this.facultyType = 1
          this.preSelectedDate(this.date)
          this.global.swalClose();
        })

    }

    if (this.data.Ltype == 2) {

      this.global.swalLoading('Loading College Faculty Schedule.')
      this.HrisApi.getEmployeePortalCodeSummary(this.global.sy, this.global.empInfo.idnumber, "")
        .map(response => response.json())
        .subscribe(res => {
          this.facultySchedule = res.data

          for (var x = 0; x < this.facultySchedule.length; x++) {
            this.separateDays(this.facultySchedule[x].day, x)
          }

          // console.log(this.facultySchedule);
          const uniqueDays: string[] = Array.from(new Set(this.daysOftheWeek));
          this.daysOftheWeek = uniqueDays
          // console.log(this.daysOftheWeek);
          this.facultyType = 2
          this.preSelectedDate(this.date)
          this.global.swalClose();
        })
    }

    if (this.data.Ltype == 3) {

      this.global.swalLoading('Loading BES Faculty Schedule.')
      this.HrisApi.getEmployeePortalBasicEdFacultySchedule(this.global.sy.substring(0, 6), this.global.empInfo.idnumber)
        .map(response => response.json())
        .subscribe(res => {

          this.besFacultySchedule = res.data
          this.daysOftheWeek = res.data.map(item => item.day)

          function parseTime(timeString) {
            const [start, end] = timeString.split('-');
            const [startHour, startMinute] = start.split(':').map(Number);
            return startHour * 60 + startMinute;
          }

          // Sort the data by day and then time
          this.besFacultySchedule.sort((a, b) => {
            // First compare by day
            const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            const dayComparison = days.indexOf(a.day) - days.indexOf(b.day);
            if (dayComparison !== 0) {
              return dayComparison;
            }

            // If days are the same, compare by time
            const timeA = parseTime(a.time);
            const timeB = parseTime(b.time);
            return timeA - timeB;
          });
          this.facultyType = 3
          this.preSelectedDate(this.date)
          this.global.swalClose();

        })



    }

  }

  hourChecker(fromAmValue, toAmValue) {
    // Split the time values into hours and minutes
    const [fromAmHours, fromAmMinutes] = fromAmValue.split(":").map(Number);
    const [toAmHours, toAmMinutes] = toAmValue.split(":").map(Number);

    // Check if the minutes are either 00 or 30
    const isValidMinutes = [0, 30].includes(fromAmMinutes) && [0, 30].includes(toAmMinutes);

    // Convert the time values to minutes
    const fromAmTotalMinutes = fromAmHours * 60 + fromAmMinutes;
    const toAmTotalMinutes = toAmHours * 60 + toAmMinutes;

    // Check if there is at least a 1-hour difference
    const isOneHourDifference = Math.abs(toAmTotalMinutes - fromAmTotalMinutes) >= 60;

    // Combine the conditions

    if (isValidMinutes && isOneHourDifference) {
      return true;
    } else {
      // Log an error or handle the case where the conditions are not met
      // this.global.swalAlert("Invalid Input", "There should be at least 1 hour gap between the specified times, and minutes should be 00 or 30.", "error");
      return false;
    }
  }

  dateFormat(value) {
    const originalDate = new Date(value);
    const year = originalDate.getFullYear();
    const month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
    const day = originalDate.getDate().toString().padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;

    return formattedDate
  }

  selectedDate() {
    this.scheduleChecker = []
    if (this.facultyType == 1) {

      const dateString = this.date.toDateString();
      const firstThreeCharacters = dateString.substring(0, 3);

      for (var y = 0; y < this.empSchedule.length; y++) {

        if (firstThreeCharacters.toLowerCase() == this.empSchedule[y].dayOfWeek.toLowerCase().substring(0, 3)) {
          const timeIn = this.empSchedule[y].timeIn;
          const timeOut = this.empSchedule[y].timeOut;

          const existingEntryIndex = this.scheduleChecker.findIndex(entry => entry.date === this.dateFormat(this.date));

          if (existingEntryIndex === -1) {
            this.scheduleChecker.push({ date: this.dateFormat(this.date), amTimeIn: timeIn, amTimeOut: timeOut });
          } else {
            this.scheduleChecker[existingEntryIndex].pmTimeIn = timeIn;
            this.scheduleChecker[existingEntryIndex].pmTimeOut = timeOut;
          }
        }
      }

    }

    if (this.facultyType == 2) {

      const dateString = this.date.toDateString();
      const firstThreeCharacters = dateString.substring(0, 3);

      for (var x = 0; x < 1; x++) {
        for (var y = 0; y < this.facultySchedule.length; y++) {
          for (var z = 0; z < this.facultySchedule[y].day.length; z++) {

            if (firstThreeCharacters.toLowerCase() == this.facultySchedule[y].day[z].toLowerCase().substring(0, 3)) {

              const timeSched = this.facultySchedule[y].time

              // Split the string using the '-' delimiter and adding a space between the period
              const [startTime, endTime] = timeSched.split('-');
              const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
              const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

              this.scheduleChecker.push({
                date: this.dateFormat(this.date), timeIn: this.convertTo24HourFormat(formattedStartTime), timeOut: this.convertTo24HourFormat(formattedEndTime),
                dispalyTimeIn: this.convertTime(this.convertTo24HourFormat(formattedStartTime), 0), dispalyTimeOut: this.convertTime(this.convertTo24HourFormat(formattedEndTime), 0),
                codeNo: this.facultySchedule[y].codeNo
              });
            }
          }
        }
      }

    }

    if (this.facultyType == 3) {

      const dateString = this.date.toDateString();
      const firstThreeCharacters = dateString.substring(0, 3);

      for (var y = 0; y < this.besFacultySchedule.length; y++) {
        if (firstThreeCharacters.toLowerCase() == this.besFacultySchedule[y].day.toLowerCase().substring(0, 3)) {
          const section = this.besFacultySchedule[y].section
          const subject = this.besFacultySchedule[y].subject
          const timeSched = this.besFacultySchedule[y].time
          const [startTime, endTime] = timeSched.split('-');
          const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
          const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

          let sTimeDisplay
          let eTimeDisplay
          if (formattedStartTime < '07:00') {
            sTimeDisplay = formattedStartTime + ' PM'
            eTimeDisplay = formattedEndTime + ' PM'
          } else {
            sTimeDisplay = formattedStartTime + ' AM'
            if (formattedEndTime >= '12:00') {
              eTimeDisplay = formattedEndTime + ' PM'
            } else {
              eTimeDisplay = formattedEndTime + ' AM'
            }
          }

          let sTimeValue
          let eTimeValue
          if (formattedStartTime < '07:00') {
            sTimeValue = this.convertTo24Hour(formattedStartTime)
            eTimeValue = this.convertTo24Hour(formattedEndTime)

          } else {
            sTimeValue = formattedStartTime
            eTimeValue = formattedEndTime
          }

          this.scheduleChecker.push({
            date: this.dateFormat(this.date), timeIn: sTimeValue, timeOut: eTimeValue,
            dispalyTimeIn: sTimeDisplay, dispalyTimeOut: eTimeDisplay, section: section, subject: subject
          });

        }


      }


    }
    // console.log(this.scheduleChecker)
  }

  preSelectedDate(value) {
    // console.log(this.facultyType);

    if (this.facultyType == 1) {

      const dateString = value.toDateString();
      const firstThreeCharacters = dateString.substring(0, 3);

      for (var y = 0; y < this.empSchedule.length; y++) {

        if (firstThreeCharacters.toLowerCase() == this.empSchedule[y].dayOfWeek.toLowerCase().substring(0, 3)) {
          const timeIn = this.empSchedule[y].timeIn;
          const timeOut = this.empSchedule[y].timeOut;

          const existingEntryIndex = this.scheduleChecker.findIndex(entry => entry.date === this.dateFormat(this.date));

          if (existingEntryIndex === -1) {
            this.scheduleChecker.push({ date: this.dateFormat(this.date), amTimeIn: timeIn, amTimeOut: timeOut });
          } else {
            this.scheduleChecker[existingEntryIndex].pmTimeIn = timeIn;
            this.scheduleChecker[existingEntryIndex].pmTimeOut = timeOut;
          }
        }
      }

    }

    if (this.facultyType == 2) {

      const dateString = value.toDateString();
      const firstThreeCharacters = dateString.substring(0, 3);

      for (var x = 0; x < 1; x++) {
        for (var y = 0; y < this.facultySchedule.length; y++) {
          for (var z = 0; z < this.facultySchedule[y].day.length; z++) {

            if (firstThreeCharacters.toLowerCase() == this.facultySchedule[y].day[z].toLowerCase().substring(0, 3)) {

              const timeSched = this.facultySchedule[y].time

              // Split the string using the '-' delimiter and adding a space between the period
              const [startTime, endTime] = timeSched.split('-');
              const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
              const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

              this.scheduleChecker.push({
                date: this.dateFormat(value), timeIn: this.convertTo24HourFormat(formattedStartTime), timeOut: this.convertTo24HourFormat(formattedEndTime),
                dispalyTimeIn: this.convertTime(this.convertTo24HourFormat(formattedStartTime), 0), dispalyTimeOut: this.convertTime(this.convertTo24HourFormat(formattedEndTime), 0),
                codeNo: this.facultySchedule[y].codeNo
              });
            }
          }
        }
      }

    }

    if (this.facultyType == 3) {

      const dateString = value.toDateString();
      const firstThreeCharacters = dateString.substring(0, 3);

      for (var y = 0; y < this.besFacultySchedule.length; y++) {
        if (firstThreeCharacters.toLowerCase() == this.besFacultySchedule[y].day.toLowerCase().substring(0, 3)) {
          const section = this.besFacultySchedule[y].section
          const subject = this.besFacultySchedule[y].subject
          const timeSched = this.besFacultySchedule[y].time
          const [startTime, endTime] = timeSched.split('-');
          const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
          const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

          let sTimeDisplay
          let eTimeDisplay
          if (formattedStartTime < '07:00') {
            sTimeDisplay = formattedStartTime + ' PM'
            eTimeDisplay = formattedEndTime + ' PM'
          } else {
            sTimeDisplay = formattedStartTime + ' AM'
            if (formattedEndTime >= '12:00') {
              eTimeDisplay = formattedEndTime + ' PM'
            } else {
              eTimeDisplay = formattedEndTime + ' AM'
            }
          }

          let sTimeValue
          let eTimeValue
          if (formattedStartTime < '07:00') {
            sTimeValue = this.convertTo24Hour(formattedStartTime)
            eTimeValue = this.convertTo24Hour(formattedEndTime)

          } else {
            sTimeValue = formattedStartTime
            eTimeValue = formattedEndTime
          }

          this.scheduleChecker.push({
            date: this.dateFormat(value), timeIn: sTimeValue, timeOut: eTimeValue,
            dispalyTimeIn: sTimeDisplay, dispalyTimeOut: eTimeDisplay, section: section, subject: subject
          });

        }


      }

      // console.log(this.scheduleChecker);

    }


  }

  convertTo24HourFormat(time: string): string {
    // Split the time into hours and minutes
    const [hoursStr, minutesStr] = time.split(':');

    // Convert hours to a number
    let hours = parseInt(hoursStr, 10);

    // Extract the am/pm part
    const period = minutesStr.toLowerCase().includes('pm') ? 'pm' : 'am';

    // Adjust hours for pm
    if (period === 'pm' && hours !== 12) {
      hours += 12;
    }

    // Adjust hours for am (midnight)
    if (period === 'am' && hours === 12) {
      hours = 0;
    }

    // Format the result
    const formattedHours = hours.toString().padStart(2, '0');
    const formattedMinutes = minutesStr.replace(/[^\d]/g, '').padStart(2, '0');

    return `${formattedHours}:${formattedMinutes}`;
  }

  timeChecker(value) {

    if (value == 'from') {
      this.fromValue = this.convertTo24HourFormat(this.fromValue)
    }
    if (value == 'to') {
      this.toValue = this.convertTo24HourFormat(this.toValue)
    }

  }

  adjustTimeInDateTime(dateTimeString, timeToSet) {
    if (dateTimeString != null && dateTimeString != '' && timeToSet != null && timeToSet != '') {
      // Parse the original date-time string into a Date object
      const originalDate = new Date(dateTimeString);

      // Parse the time string "07:30" into hours and minutes
      const [hours, minutes] = timeToSet.split(':').map(Number);

      // Get the time zone offset in minutes
      const timeZoneOffset = originalDate.getTimezoneOffset();

      // Calculate the total minutes since midnight
      const totalMinutes = hours * 60 + minutes;

      // Adjust the time by subtracting the time zone offset
      const adjustedMinutes = totalMinutes - timeZoneOffset;

      // Calculate the new hours and minutes
      const newHours = Math.floor(adjustedMinutes / 60);
      const newMinutes = adjustedMinutes % 60;

      // Set the specified hours and minutes without changing the date
      originalDate.setHours(newHours);
      originalDate.setMinutes(newMinutes);
      originalDate.setSeconds(0);
      originalDate.setMilliseconds(0);

      // Format the Date object into the desired format
      const formattedDate = originalDate.toISOString();

      // console.log(formattedDate);

      return formattedDate;
    }

  }

  extractDayFromDate(dateString) {
    const date = new Date(dateString);
    const day = date.getDate();
    return day;
  }

  leaveDateDurationChecker() {

    if (this.extractDayFromDate(this.data.sdate) != this.extractDayFromDate(this.date)) {
      return this.leaveDurationArray.some(item => {
        // Convert the start date to MM-DD-YYYY format
        const startDate = new Date(item.startDate).toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' });
        return startDate === this.date.toLocaleDateString('en-US', { month: '2-digit', day: '2-digit', year: 'numeric' });
      });
    }

  }

  hasTrue(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].toLowerCase() === 'true') {
        return true;
      }
    }
    return false;
  }

  adjustMinusTime(value) {
    const inputTime = value;

    // Split the time string to get hours and minutes
    const [hours, minutes] = inputTime.split(":").map(Number);

    // Convert the time to minutes
    const totalMinutes = hours * 60 + minutes;

    // Subtract 1 minute
    const newTotalMinutes = totalMinutes - 1;

    // Convert the new total minutes back to "HH:mm" format
    const newHours = Math.floor(newTotalMinutes / 60);
    const newMinutes = newTotalMinutes % 60;

    // Format the new time as "HH:mm" string
    const newTime = `${newHours.toString().padStart(2, '0')}:${newMinutes.toString().padStart(2, '0')}`;
    // console.log(newTime)
    return newTime
  }

  adjustAddTime(value) {
    const inputTime = value;

    // Split the time string to get hours and minutes
    const [hours, minutes] = inputTime.split(":").map(Number);

    // Convert the time to minutes
    const totalMinutes = hours * 60 + minutes;

    // Subtract 1 minute
    const newTotalMinutes = totalMinutes + 1;

    // Convert the new total minutes back to "HH:mm" format
    const newHours = Math.floor(newTotalMinutes / 60);
    const newMinutes = newTotalMinutes % 60;

    // Format the new time as "HH:mm" string
    const newTime = `${newHours.toString().padStart(2, '0')}:${newMinutes.toString().padStart(2, '0')}`;
    return newTime
  }

  tempArray = []
  checker1 = []
  checker2 = []

  save() {

    if (this.facultyType == 1) {
      this.tempArray = []
      this.checker1 = []
      this.checker2 = []

      const sDateToExclude = this.data.sdate
      const eDateToExclude = this.data.edate

      for (var x = 0; x < this.data.value.leave_LeaveDurations.length; x++) {
        if (this.extractDayFromDate(this.date) == this.extractDayFromDate(this.data.value.leave_LeaveDurations[x].startDate)) {
          if (sDateToExclude == this.data.value.leave_LeaveDurations[x].startDate && eDateToExclude == this.data.value.leave_LeaveDurations[x].endDate) {
            // console.log(this.data.value.leave_LeaveDurations[x]);
          } else {
            this.tempArray.push(this.data.value.leave_LeaveDurations[x])
          }

        }
      }

      if (this.fromValue && this.toValue) {
        if (this.fromValue < this.toValue) {
          if (this.hourChecker(this.fromValue, this.toValue) == true) {
            if ((this.fromValue >= this.scheduleChecker[0].amTimeIn && this.fromValue <= this.scheduleChecker[0].amTimeOut) &&
              (this.toValue >= this.scheduleChecker[0].amTimeIn && this.toValue <= this.scheduleChecker[0].amTimeOut)) {

              for (var x = 0; x < this.tempArray.length; x++) {

                const sDate = new Date(this.tempArray[x].startDate);
                const eDate = new Date(this.tempArray[x].endDate);
                const startTime = sDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
                const endTime = eDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });

                if ((startTime >= this.scheduleChecker[0].amTimeIn && startTime <= this.scheduleChecker[0].amTimeOut) &&
                  (endTime >= this.scheduleChecker[0].amTimeIn && endTime <= this.scheduleChecker[0].amTimeOut)) {

                  if ((this.fromValue >= this.adjustAddTime(startTime) && this.fromValue <= this.adjustMinusTime(endTime)) ||
                    (this.toValue >= this.adjustAddTime(startTime) && this.toValue <= this.adjustMinusTime(endTime))) {
                    this.checker1.push('true')
                  } else {
                    this.checker1.push('false')
                  }

                  if ((startTime >= this.fromValue && startTime <= this.toValue) &&
                    (endTime >= this.fromValue && endTime <= this.toValue)) {
                    this.checker2.push('true')
                  } else {
                    this.checker2.push('false')
                  }

                }

              }

              if (this.hasTrue(this.checker1) == false && this.hasTrue(this.checker2) == false) {

                this.HrisApi.putEmployeePortalLeaveDuration(this.data.leaveId, this.data.leaveDurationId, {
                  "startDate": this.adjustTimeInDateTime(this.date, this.fromValue),
                  "endDate": this.adjustTimeInDateTime(this.date, this.toValue)
                })
                  // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {

                    this.global.swalAlert("Success", 'Leave duration updated.', 'success');
                    this.dialogRef.close({ result: 'success' });

                  }, Error => {
                    this.global.swalAlertError();
                    // console.log(Error)
                  });
              } else {
                this.global.swalAlert("Time Conflict!", "The selected time is already taken.", "error")
              }

            } else if ((this.fromValue >= this.scheduleChecker[0].pmTimeIn && this.fromValue <= this.scheduleChecker[0].pmTimeOut) &&
              (this.toValue >= this.scheduleChecker[0].pmTimeIn && this.toValue <= this.scheduleChecker[0].pmTimeOut)) {

              for (var x = 0; x < this.tempArray.length; x++) {
                const sDate = new Date(this.tempArray[x].startDate);
                const eDate = new Date(this.tempArray[x].endDate);
                const startTime = sDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
                const endTime = eDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });

                if ((startTime >= this.scheduleChecker[0].pmTimeIn && startTime <= this.scheduleChecker[0].pmTimeOut) &&
                  (endTime >= this.scheduleChecker[0].pmTimeIn && endTime <= this.scheduleChecker[0].pmTimeOut)) {

                  if ((this.fromValue >= this.adjustAddTime(startTime) && this.fromValue <= this.adjustMinusTime(endTime)) ||
                    (this.toValue >= this.adjustAddTime(startTime) && this.toValue <= this.adjustMinusTime(endTime))) {
                    this.checker1.push('true')
                  } else {
                    this.checker1.push('false')
                  }

                  if ((startTime >= this.fromValue && startTime <= this.toValue) &&
                    (endTime >= this.fromValue && endTime <= this.toValue)) {
                    this.checker2.push('true')
                  } else {
                    this.checker2.push('false')
                  }

                }

              }

              if (this.hasTrue(this.checker1) == false && this.hasTrue(this.checker2) == false) {
                // console.log(this.hasTrue(this.checker1), this.hasTrue(this.checker2))
                this.HrisApi.putEmployeePortalLeaveDuration(this.data.leaveId, this.data.leaveDurationId, {
                  "startDate": this.adjustTimeInDateTime(this.date, this.fromValue),
                  "endDate": this.adjustTimeInDateTime(this.date, this.toValue)
                })
                  // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {

                    this.global.swalAlert("Success", 'Leave duration updated.', 'success');
                    this.dialogRef.close({ result: 'success' });

                  }, Error => {
                    this.global.swalAlertError();
                    // console.log(Error)
                  });

              } else {
                this.global.swalAlert("Time  Conflict!", "The selected time is already taken.", "error")
              }

            } else {
              this.global.swalAlert("Invalid Input", "The time range is not within the valid time schedule.", "error");
            }
          } else {
            this.global.swalAlert("Invalid Time!", "There should be at least 1 hour gap between the specified times, and minutes should be 00 or 30.", "error");
          }
        } else {
          this.global.swalAlert("Invalid Input!", "The start time cannot be later than or equal to the end time.\nPlease make sure the start date comes before the end date.", "error");
        }
      }
    }

    if (this.facultyType == 2 || this.facultyType == 3) {

      this.tempArray = []
      this.checker1 = []
      this.checker2 = []

      const sDateToExclude = this.data.sdate
      const eDateToExclude = this.data.edate

      for (var x = 0; x < this.data.value.leave_LeaveDurations.length; x++) {
        if (this.extractDayFromDate(this.date) == this.extractDayFromDate(this.data.value.leave_LeaveDurations[x].startDate)) {
          if (sDateToExclude == this.data.value.leave_LeaveDurations[x].startDate && eDateToExclude == this.data.value.leave_LeaveDurations[x].endDate) {
            // console.log(this.data.value.leave_LeaveDurations[x]);
          } else {
            this.tempArray.push(this.data.value.leave_LeaveDurations[x])
          }

        }
      }

      if (this.fromValue && this.toValue) {
        if (this.fromValue < this.toValue) {
          if (this.hourChecker(this.fromValue, this.toValue) == true) {
            // console.log(this.scheduleChecker);

            for (var x = 0; x < this.scheduleChecker.length; x++) {
              if ((this.fromValue >= this.scheduleChecker[x].timeIn && this.fromValue <= this.scheduleChecker[x].timeOut) &&
                (this.toValue >= this.scheduleChecker[x].timeIn && this.toValue <= this.scheduleChecker[x].timeOut)) {
                let xCounter = x
                for (var y = 0; y < this.tempArray.length; y++) {

                  const sDate = new Date(this.tempArray[y].startDate);
                  const eDate = new Date(this.tempArray[y].endDate);
                  const startTime = sDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
                  const endTime = eDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });

                  if ((startTime >= this.scheduleChecker[x].timeIn && startTime <= this.scheduleChecker[x].timeOut) &&
                    (endTime >= this.scheduleChecker[x].timeIn && endTime <= this.scheduleChecker[x].timeOut)) {

                    if ((this.fromValue >= this.adjustAddTime(startTime) && this.fromValue <= this.adjustMinusTime(endTime)) ||
                      (this.toValue >= this.adjustAddTime(startTime) && this.toValue <= this.adjustMinusTime(endTime))) {
                      this.checker1.push('true')
                    } else {
                      this.checker1.push('false')
                    }

                    if ((startTime >= this.fromValue && startTime <= this.toValue) &&
                      (endTime >= this.fromValue && endTime <= this.toValue)) {
                      this.checker2.push('true')
                    } else {
                      this.checker2.push('false')
                    }

                  }

                }

                if (this.hasTrue(this.checker1) == false && this.hasTrue(this.checker2) == false) {

                  if (this.facultyType == 2) {
                    this.HrisApi.putEmployeePortalLeaveDuration(this.data.leaveId, this.data.leaveDurationId, {
                      "startDate": this.adjustTimeInDateTime(this.date, this.fromValue),
                      "endDate": this.adjustTimeInDateTime(this.date, this.toValue)
                    }).map(response => response.json()).subscribe(res => {

                      this.HrisApi.putEmployeePortalLeaveFacultyLeaveDetail(
                        {
                          "leaveId": this.data.leaveId,
                          "leaveDurationID": this.data.leaveDurationId,
                          "schoolYear": this.global.sy.slice(0, 6),
                          "codeNo": this.scheduleChecker[xCounter].codeNo,
                          "section": "string",
                          "subject": "string"
                        }
                      ).map(response => response.json()).subscribe(res => {

                        this.global.swalAlert("Success", 'Leave duration updated.', 'success');
                        this.dialogRef.close({ result: 'success' });

                      })

                    }, Error => {
                      this.global.swalAlertError();
                      // console.log(Error)
                    });
                  }


                  if (this.facultyType == 3) {
                    this.HrisApi.putEmployeePortalLeaveDuration(this.data.leaveId, this.data.leaveDurationId, {
                      "startDate": this.adjustTimeInDateTime(this.date, this.fromValue),
                      "endDate": this.adjustTimeInDateTime(this.date, this.toValue)
                    }).map(response => response.json()).subscribe(res => {

                      this.HrisApi.putEmployeePortalLeaveFacultyLeaveDetail(
                        {
                          "leaveId": this.data.leaveId,
                          "leaveDurationID": this.data.leaveDurationId,
                          "schoolYear":"string",
                          "codeNo": "string",
                          "section": this.scheduleChecker[xCounter].section,
                          "subject": this.scheduleChecker[xCounter].subject
                        }
                      ).map(response => response.json()).subscribe(res => {

                        this.global.swalAlert("Success", 'Leave duration updated.', 'success');
                        this.dialogRef.close({ result: 'success' });

                      })

                    }, Error => {
                      this.global.swalAlertError();
                      // console.log(Error)
                    });
                  }


                } else {
                  this.global.swalAlert("Time Conflict!", "The selected time is already taken.", "error")
                }

              }
              else {
                this.global.swalAlert("Invalid Input", "The time range is not within the valid time schedule.", "error");
              }
            }
          } else {
            this.global.swalAlert("Invalid Time!", "There should be at least 1 hour gap between the specified times, and minutes should be 00 or 30.", "error");
          }
        } else {
          this.global.swalAlert("Invalid Input!", "The start time cannot be later than or equal to the end time.\nPlease make sure the start date comes before the end date.", "error");
        }
      }
    }
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

}

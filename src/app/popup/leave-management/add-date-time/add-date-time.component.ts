import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HrisLeaveManagementApiService } from 'src/app/hris-leave-management-api.service';
import { GlobalService } from 'src/app/global.service';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { CheckScheduleComponent } from '../check-schedule/check-schedule.component';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import { Inject } from '@angular/core';



@Component({
  selector: 'app-add-date-time',
  templateUrl: './add-date-time.component.html',
  styleUrls: ['./add-date-time.component.scss']
})
export class AddDateTimeComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddDateTimeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private hrisApi: HrisLeaveManagementApiService,
    private FormBuilder: FormBuilder,
    private global: GlobalService,
    private atp: AmazingTimePickerService,
    private dialog: MatDialog
  ) { }

  form: FormGroup;
  selectedDates: Date[] = [];
  empSchedule = []
  daysOftheWeek = []

  fromAmValue: string = '';
  toAmValue: string = '';
  fromPmValue: string = '';
  toPmValue: string = '';
  displayArr = []

  fromAmPassValue = ''
  toAmPassValue = ''
  fromPmPassValue = ''
  toPmPassValue = ''
  passDisplayArr = []

  scheduleChecker = []

  timeCheck: boolean = true

  timeToBeUpdated = ''
  ntpTempArray = []

  leaveCategory = []
  facultyLeaveType = []

  facultyType = 0

  totalLeaveTimeSched = 0

  ngOnInit() {
    this.getEmployeeLeaveManagementSchedule()

    this.form = this.FormBuilder.group({
      sdate: ['', Validators.required],
      edate: ['', Validators.required]
    });

    if (this.data.type == "editTime") {
      this.timeToBeUpdated = this.data.value.displayDate
    }

    this.facultyType = this.data.leaveType

    if (this.facultyType == 1) {
      this.leaveCategory.push({ id: 1, category: 'NTP' });
    }

    if (this.facultyType == 4) {
      this.leaveCategory.push({ id: 4, category: 'EAB' });
    }
    // console.log(this.data.typeOfLeave);


  }




  calculateTotalScheduledLeaveHours(data: any[]): number {
    let totalHours = 0;

    // Loop through each object in the array up to the first two elements
    for (let i = 0; i < Math.min(data.length, 2); i++) {
      const entry = data[i];
      // Extract time values from each object
      const amTimeIn = new Date(`2000-01-01T${entry.amTimeIn}`);
      const amTimeOut = new Date(`2000-01-01T${entry.amTimeOut}`);
      const pmTimeIn = new Date(`2000-01-01T${entry.pmTimeIn}`);
      const pmTimeOut = new Date(`2000-01-01T${entry.pmTimeOut}`);

      // Calculate hours for each period (AM and PM) and add to total
      totalHours += (amTimeOut.getTime() - amTimeIn.getTime()) / (1000 * 60 * 60); // Convert milliseconds to hours
      totalHours += (pmTimeOut.getTime() - pmTimeIn.getTime()) / (1000 * 60 * 60); // Convert milliseconds to hours
    }

    return totalHours;
  }

  calculateTotaLeaveHours(data: any[]): number {
    let totalHours = 0;

    // Loop through each object in the array
    data.forEach(entry => {
      // Split the hours and minutes from the hrmins string
      const [hoursStr, minutesStr] = entry.hrmins.split(' & ');

      // Extract hours and minutes as numbers
      const hours = parseInt(hoursStr, 10);
      const minutes = parseInt(minutesStr.replace(' mins', ''), 10);

      // Convert minutes to fraction of an hour (divide by 60)
      const fractionalHours = minutes / 60;

      // Add total hours
      totalHours += hours + fractionalHours;
    });

    return totalHours;
  }


  checkSchedule(param) {
    // console.log(this.leavetoUpdate);
    const dialogRef = this.dialog.open(CheckScheduleComponent, {
      width: '1200px',
      disableClose: false,
      autoFocus: false,
      data: {
        lType: this.leaveCategory,
        fType: this.facultyLeaveType
      }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  async loadSchedule() {

    try {
      const response = await this.hrisApi.getEmployeePortalEmployeeSchedule().toPromise();
      const result = response.json();
      this.daysOftheWeek = result.data.map(item => item.dayOfWeek)
      // console.log(result,'EmpSched');
      
      if (result != '') {
        // console.log(this.daysOftheWeek)
        this.addTime(new Date(this.data.value.displayDate), new Date(this.data.value.displayDate), 2)
      }
    } catch (error) {
      // console.error(error);
    }
  }

  convertTo24HourFormat(time: string): string {
    const [timePart, meridiem] = time.split(' ');
    const [hours, minutes] = timePart.split(':');
    let hour = parseInt(hours, 10);

    if (meridiem === 'PM' && hour < 12) {
      hour += 12;
    } else if (meridiem === 'AM' && hour === 12) {
      hour = 0;
    }
    // console.log(`${hour.toString().padStart(2, '0')}:${minutes}`);

    return `${hour.toString().padStart(2, '0')}:${minutes}`;
  }

  getEmployeeLeaveManagementSchedule() {
    this.global.swalLoading('Loading Employee Schedule.')
    this.hrisApi.getEmployeePortalEmployeeSchedule()
      .map(response => response.json())
      .subscribe(res => {
        // console.log("Get leaveManagement Schedule : ", res);
        this.empSchedule = res.data
        // console.log(res.data)
        this.daysOftheWeek = res.data.map(item => item.dayOfWeek)
        // console.log(this.daysOftheWeek)
        // console.log(this.data.type);
        
        if (this.data.type == "editTime") {
          this.loadSchedule()
        }
        this.global.swalClose();
      })
  }
sd
  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel', data: this.passDisplayArr });
  }

  myFilter = (d: Date): boolean => {

    if (this.data.typeOfLeave !== 0) {
      return true
    } else {
      const day = d.getDay();
      // Check if the day of the week is in the array. If not, disable the date.
      return this.daysOftheWeek.includes(this.getDayName(day));
    }

  }

  getDayName(day: number): string {
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return weekdays[day];
  }

  calculateHoursAndMinutes(startTime: string, endTime: string): string {
    let startTimeDate = moment(startTime, 'h:mm A').toDate();
    let endTimeDate = moment(endTime, 'h:mm A').toDate();

    // Check if the start time is greater than the end time.
    if (startTimeDate > endTimeDate) {
      // Swap the start and end times.
      const temp = startTimeDate;
      startTimeDate = endTimeDate;
      endTimeDate = temp;
    }

    const minutes = (endTimeDate.getTime() - startTimeDate.getTime()) / (1000 * 60);
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;

    // console.log(remainingMinutes)

    if (hours === 1) {
      var hourText = "hr";
    } else {
      var hourText = "hrs";
    }

    if (remainingMinutes === 1) {
      var minsText = "min";
    } else {
      var minsText = "mins";
    }

    if (hours > 0 && remainingMinutes > 0) {
      return `${hours} ${hourText} & ${remainingMinutes} ${minsText}`;
    } else if (hours > 0) {
      return `${hours} ${hourText}`;
    } else {
      return `${remainingMinutes} ${minsText}`;
    }
  }

  calculateTotalHoursAndMinutes(amStartTime: string, amEndTime: string, pmStartTime: string, pmEndTime: string): string {
    let amStartTimeDate = moment(amStartTime, 'h:mm A').toDate();
    let amEndTimeDate = moment(amEndTime, 'h:mm A').toDate();

    // Check if the AM start time is greater than the AM end time.
    if (amStartTimeDate > amEndTimeDate) {
      // Swap the AM start and end times.
      const temp = amStartTimeDate;
      amStartTimeDate = amEndTimeDate;
      amEndTimeDate = temp;
    }

    let pmStartTimeDate = moment(pmStartTime, 'h:mm A').toDate();
    let pmEndTimeDate = moment(pmEndTime, 'h:mm A').toDate();

    // Check if the PM start time is greater than the PM end time.
    if (pmStartTimeDate > pmEndTimeDate) {
      // Swap the PM start and end times.
      const temp = pmStartTimeDate;
      pmStartTimeDate = pmEndTimeDate;
      pmEndTimeDate = temp;
    }

    // **If neither of the PM start and end time values are present, calculate the total hours and minutes for the AM start and end time only.**
    if (!pmStartTime && !pmEndTime) {
      // Calculate the total minutes for the AM duration.
      const amMinutes = (amEndTimeDate.getTime() - amStartTimeDate.getTime()) / (1000 * 60);

      // Calculate the total hours and remaining minutes.
      const totalMinutes = amMinutes;
      const hours = Math.floor(totalMinutes / 60);
      const remainingMinutes = totalMinutes % 60;


      if (hours === 1) {
        var hourText = "hr";
      } else {
        var hourText = "hrs";
      }

      if (hours > 0 && remainingMinutes > 0) {
        return `${hours} ${hourText} & ${remainingMinutes} mins`;
      } else if (hours > 0) {
        return `${hours} ${hourText}`;
      } else {
        return `${remainingMinutes} mins`;
      }
    }

    if (!amStartTime && !amEndTime) {
      // Calculate the total minutes for the AM duration.
      const amMinutes = (pmEndTimeDate.getTime() - pmStartTimeDate.getTime()) / (1000 * 60);

      // Calculate the total hours and remaining minutes.
      const totalMinutes = amMinutes;
      const hours = Math.floor(totalMinutes / 60);
      const remainingMinutes = totalMinutes % 60;

      if (hours > 0 && remainingMinutes > 0) {
        return `${hours} hrs & ${remainingMinutes} mins`;
      } else if (hours > 0) {
        return `${hours} hrs`;
      } else {
        return `${remainingMinutes} mins`;
      }
    }

    // Otherwise, calculate the total hours and minutes for both AM and PM durations.
    const amMinutes = (amEndTimeDate.getTime() - amStartTimeDate.getTime()) / (1000 * 60);
    const pmMinutes = (pmEndTimeDate.getTime() - pmStartTimeDate.getTime()) / (1000 * 60);

    const totalMinutes = amMinutes + pmMinutes;
    const hours = Math.floor(totalMinutes / 60);
    const remainingMinutes = totalMinutes % 60;
    // console.log(hours);

    if (hours === 1) {
      var hourText = "hr";
    } else {
      var hourText = "hrs";
    }

    if (hours > 0 && remainingMinutes > 0) {
      return `${hours} ${hourText} & ${remainingMinutes} mins`;
    } else if (hours > 0) {
      return `${hours} ${hourText}`;
    } else {
      return `${remainingMinutes} mins`;
    }
  }

  hourChecker(fromAmValue, toAmValue, fromPmValue, toPmValue) {
    // Function to check if the minutes are either 00 or 30
    const isValidMinutes = (hours, minutes) => [0, 30].includes(minutes);

    // Function to calculate total minutes from time values
    const calculateTotalMinutes = (hours, minutes) => hours * 60 + minutes;

    // Split the time values into hours and minutes for AM
    const [fromAmHours, fromAmMinutes] = fromAmValue ? fromAmValue.split(":").map(Number) : [0, 0];
    const [toAmHours, toAmMinutes] = toAmValue ? toAmValue.split(":").map(Number) : [0, 0];

    // Split the time values into hours and minutes for PM
    const [fromPmHours, fromPmMinutes] = fromPmValue ? fromPmValue.split(":").map(Number) : [0, 0];
    const [toPmHours, toPmMinutes] = toPmValue ? toPmValue.split(":").map(Number) : [0, 0];

    // Convert AM time values to total minutes
    const fromAmTotalMinutes = calculateTotalMinutes(fromAmHours, fromAmMinutes);
    const toAmTotalMinutes = calculateTotalMinutes(toAmHours, toAmMinutes);

    // Convert PM time values to total minutes
    const fromPmTotalMinutes = calculateTotalMinutes(fromPmHours, fromPmMinutes);
    const toPmTotalMinutes = calculateTotalMinutes(toPmHours, toPmMinutes);

    // Calculate the total minutes for both AM and PM
    const totalMinutes = Math.abs(toAmTotalMinutes - fromAmTotalMinutes) + Math.abs(toPmTotalMinutes - fromPmTotalMinutes);

    // Calculate the total hours
    const totalHours = totalMinutes / 60;

    // Check if the total hours are greater than or equal to 1 and if the minutes are either 00 or 30
    return totalHours >= 1 &&
      isValidMinutes(fromAmHours, fromAmMinutes) &&
      isValidMinutes(toAmHours, toAmMinutes) &&
      isValidMinutes(fromPmHours, fromPmMinutes) &&
      isValidMinutes(toPmHours, toPmMinutes);
  }

  hourGetter(value) {
    const dateObject = new Date(value);

    // Extracting hours and minutes
    const hours = dateObject.getUTCHours();
    const minutes = dateObject.getUTCMinutes();

    // Formatting the time as HH:mm
    const formattedTime = `${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
    return formattedTime
  }

  timeChecker(value, num) {

    if (num == 1) {
      this.fromAmValue = this.convertTo24HourFormat(this.fromAmValue)
    }

    if (num == 2) {
      this.toAmValue = this.convertTo24HourFormat(this.toAmValue)
    }

    if (num == 3) {
      this.fromPmValue = this.convertTo24HourFormat(this.fromPmValue)
    }

    if (num == 4) {
      this.toPmValue = this.convertTo24HourFormat(this.toPmValue)
    }

    if (this.fromAmValue && this.toAmValue) {

      for (var x = 0; x < this.scheduleChecker.length; x++) {

        if (num === 1 && this.fromAmValue >= this.scheduleChecker[x].amTimeIn && this.fromAmValue <= this.scheduleChecker[x].amTimeOut) {
          this.convertTime(value, num);
        }

        if (num === 2 && this.toAmValue >= this.scheduleChecker[x].amTimeIn && this.toAmValue <= this.scheduleChecker[x].amTimeOut) {
          this.convertTime(value, num);
        }

      }
    }

    if (this.fromPmValue && this.toPmValue) {

      for (var x = 0; x < this.scheduleChecker.length; x++) {

        if (num === 3 && this.fromPmValue >= this.scheduleChecker[x].pmTimeIn && this.fromPmValue <= this.scheduleChecker[x].pmTimeOut) {
          this.convertTime(value, num);
        }

        if (num === 4 && this.toPmValue >= this.scheduleChecker[x].pmTimeIn && this.toPmValue <= this.scheduleChecker[x].pmTimeOut) {
          this.convertTime(value, num);
        }

      }


    }

  }

  convertTime(value, num) {

    if (value != null && value != '') {
      // Split the input time into hours and minutes
      const [hours, minutes] = value.split(':');

      // Parse hours and minutes as integers
      const hoursInt = parseInt(hours, 10);
      const minutesInt = parseInt(minutes, 10);

      // Determine the period (AM or PM)
      const period = hoursInt >= 12 ? 'PM' : 'AM';

      // Convert hours to 12-hour format
      const formattedHours = hoursInt % 12 || 12;

      // Format the time as "h:mm a" (e.g., "3:43 PM")
      const formattedTime = `${formattedHours}:${minutesInt.toString().padStart(2, '0')} ${period}`;

      // Add a console.log statement for debugging
      // console.log(`Input Time: ${value}, Formatted Time: ${formattedTime}`);

      if (num == 1) { this.fromAmPassValue = formattedTime }
      else if (num == 2) { this.toAmPassValue = formattedTime }
      else if (num == 3) { this.fromPmPassValue = formattedTime }
      else if (num == 4) { this.toPmPassValue = formattedTime }

      return formattedTime;
    }
    else { return null }

  }

  adjustTimeInDateTime(dateTimeString, timeToSet) {
    if (dateTimeString != null && dateTimeString != '' && timeToSet != null && timeToSet != '') {
      // Parse the original date-time string into a Date object
      const originalDate = new Date(dateTimeString);

      // Parse the time string "07:30" into hours and minutes
      const [hours, minutes] = timeToSet.split(':').map(Number);

      // Get the time zone offset in minutes
      const timeZoneOffset = originalDate.getTimezoneOffset();

      // Calculate the total minutes since midnight
      const totalMinutes = hours * 60 + minutes;

      // Adjust the time by subtracting the time zone offset
      const adjustedMinutes = totalMinutes - timeZoneOffset;

      // Calculate the new hours and minutes
      const newHours = Math.floor(adjustedMinutes / 60);
      const newMinutes = adjustedMinutes % 60;

      // Set the specified hours and minutes without changing the date
      originalDate.setHours(newHours);
      originalDate.setMinutes(newMinutes);
      originalDate.setSeconds(0);
      originalDate.setMilliseconds(0);

      // Format the Date object into the desired format
      const formattedDate = originalDate.toISOString();

      // console.log(formattedDate);

      return formattedDate;
    }

  }

  dateFormat(value) {
    const originalDate = new Date(value);
    const year = originalDate.getFullYear();
    const month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
    const day = originalDate.getDate().toString().padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;

    return formattedDate
  }

  delete(date, from, to) {
    // console.log(date, from, to)
    for (var x = 0; x < this.displayArr.length; x++) {
      if (date == this.displayArr[x].date && from == this.displayArr[x].from && to == this.displayArr[x].to) {
        // Remove the item from the array using splice
        this.displayArr.splice(x, 1);

        break; // Exit the loop after removing the item
      }

    }

    // console.log(this.displayArr)
  }

  addTime(startDateValue, endDateValue, value) {
    // console.log(startDateValue, endDateValue, value)
    this.displayArr = []
    if (startDateValue && endDateValue) {
      if (startDateValue <= endDateValue) {
        // Clear the array before populating it with new dates.
        this.selectedDates = [];

        // Clone the start date to avoid modifying the original date object.
        let currentDate = new Date(startDateValue);

        if (this.data.typeOfLeave != 0) {
          while (currentDate <= endDateValue) {
            // Check if the day of the week is in the 'weekdays' array before pushing it.
            // console.log(this.daysOftheWeek)
            if (true) {
              this.selectedDates.push(new Date(currentDate));
            }
            currentDate.setDate(currentDate.getDate() + 1);
          }
        } else {
          while (currentDate <= endDateValue) {
            // Check if the day of the week is in the 'weekdays' array before pushing it.
            // console.log(this.daysOftheWeek)
            if (this.daysOftheWeek.includes(this.getDayName(currentDate.getDay()))) {
              this.selectedDates.push(new Date(currentDate));
            }
            currentDate.setDate(currentDate.getDate() + 1);
          }

        }


        // Iterate through the dates and add them to the array until the end date is reached.
        this.schedChecker()
      } else {
        // Handle the case where startDate is greater than endDate.
        Swal({
          title: 'Invalid Date Range!',
          text: 'The start date cannot be later than the end date.\nPlease make sure the start date comes before the end date.',
          type: 'warning',
        });

        if (value == '1') {
          this.form.get('sdate').patchValue('')
          this.displayArr = [];
        }
        else if (value == '0') {
          this.form.get('edate').patchValue('')
          this.displayArr = [];
        }

        return
      }
    }

    // console.log("this.selectedDates:", this.selectedDates); // Debugging statement
  }

  sDateMLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + 104);
    this.form.get('edate').patchValue(newDate);
    return newDate;
  }

  eDateMLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() - 104);
    this.form.get('sdate').patchValue(newDate);
    return newDate;
  }

  sDatePLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + 6);
    this.form.get('edate').patchValue(newDate);
    return newDate;
  }

  eDatePLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() - 6);
    this.form.get('sdate').patchValue(newDate);
    return newDate;
  }

  // sDateBerlLeaveFunction(date: Date): Date {
  //   const newDate = new Date(date);
  //   newDate.setDate(newDate.getDate() + 2);
  //   this.form.get('edate').patchValue(newDate);
  //   return newDate;
  // }

  // eDateBerlLeaveFunction(date: Date): Date {
  //   const newDate = new Date(date);
  //   newDate.setDate(newDate.getDate() - 2);
  //   this.form.get('sdate').patchValue(newDate);
  //   return newDate;
  // }

  onDateSelected(value) {

    // if (this.data.typeOfLeave == 7) {
    //   this.eDateBerlLeaveFunction(this.form.get('edate').value)
    // } else
    if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'M') {
      this.eDatePLLeaveFunction(this.form.get('edate').value)
    } else if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'F') {
      this.eDateMLLeaveFunction(this.form.get('edate').value)
    } else if (this.data.typeOfLeave == 6) {
      this.form.get('sdate').patchValue(this.form.get('edate').value);
    }

    if (this.data.type === "editTime") {
      this.form.get('sdate').patchValue(this.form.get('edate').value);
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    } else {
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    }

  }

  onDateSelected1(value) {

    if (this.data.typeOfLeave == 7) {
      // console.log(this.form.get('sdate').value,);
    } else if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'M') {
      this.sDatePLLeaveFunction(this.form.get('sdate').value)
    } else if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'F') {
      this.sDateMLLeaveFunction(this.form.get('sdate').value)
    } else if (this.data.typeOfLeave == 6) {
      this.form.get('edate').patchValue(this.form.get('sdate').value);
    }


    if (this.data.type === "editTime") {
      this.form.get('edate').patchValue(this.form.get('sdate').value);
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    } else {
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    }

  }

  calculateDates() {
    // console.log(this.selectedDates)

    if (this.selectedDates.length >= 1) {
      for (var x = 0; x < this.selectedDates.length; x++) {
        // Check if the date already exists in the scheduleChecker array
        const isDateDuplicate = this.displayArr.some(schedule => {
          return schedule.date === this.dateFormat(this.selectedDates[x]);
        });

        if (isDateDuplicate) {
          this.global.swalAlert(
            "Duplicate Date!",
            "The date you selected already exists in the leave schedule. Please choose a different date.",
            "error"
          );
        } else {
          // console.log(this.fromAmValue && this.toAmValue && this.fromPmValue && this.toPmValue);
          // If no duplicates found, continue with your existing code to add the date
          if ((this.fromAmValue && this.toAmValue) && this.fromAmValue < this.toAmValue) {
            if ((this.fromAmValue >= this.scheduleChecker[x].amTimeIn && this.fromAmValue <= this.scheduleChecker[x].amTimeOut) &&
              (this.toAmValue >= this.scheduleChecker[x].amTimeIn && this.toAmValue <= this.scheduleChecker[x].amTimeOut)) {
              this.convertTime(this.fromAmValue, 1)
              this.convertTime(this.toAmValue, 2)
              this.displayArr.push({
                date: this.dateFormat(this.selectedDates[x]), from: this.fromAmPassValue, to: this.toAmPassValue,
                fromValue: this.fromAmValue, toValue: this.toAmValue, hrmins: this.calculateHoursAndMinutes(this.fromAmPassValue, this.toAmPassValue)
              });
            } else {
              this.global.swalAlert("Invalid Input!", "The AM time is not within the valid time schedule.", "error")
            }
          }

          if ((this.fromPmValue && this.toPmValue) && this.fromPmValue < this.toPmValue) {
            if ((this.fromPmValue >= this.scheduleChecker[x].pmTimeIn && this.fromPmValue <= this.scheduleChecker[x].pmTimeOut) &&
              (this.scheduleChecker[x].pmTimeIn && this.toPmValue <= this.scheduleChecker[x].pmTimeOut)) {
              this.convertTime(this.fromPmValue, 3)
              this.convertTime(this.toPmValue, 4)
              // console.log(this.fromPmValue, this.toPmValue)
              this.displayArr.push({
                date: this.dateFormat(this.selectedDates[x]), from: this.fromPmPassValue, to: this.toPmPassValue,
                fromValue: this.fromPmValue, toValue: this.toPmValue, hrmins: this.calculateHoursAndMinutes(this.fromPmPassValue, this.toPmPassValue)
              });
            } else {
              // console.log('here')
              this.global.swalAlert("Invalid Input!", "The PM time is not within the valid time schedule.", "error")
            }
          }

        }
      }
    }


  }

  autoCalculateDates() {

    if (this.selectedDates.length >= 1) {

      if (this.data.type === 'editTime' && this.ntpTempArray.length == 0) {
        this.ntpTempArray.push(this.data.value)

        if (this.data.value.amFromDateTime && this.data.value.amToDateTime !== undefined) {
          const amTimeSched = this.data.value.amTime
          const [amStartTime, amEndTime] = amTimeSched.split('-');
          const amFromTime = this.data.value.amFromDateTime
          const amToTime = this.data.value.amToDateTime
          // console.log(amStartTime, amEndTime, this.hourGetter(amFromTime), this.hourGetter(amToTime));

          this.displayArr.push({
            date: this.dateFormat(this.selectedDates[0]), from: amStartTime.trim(), to: amEndTime.trim(), fromValue: this.hourGetter(amFromTime),
            toValue: this.hourGetter(amToTime), hrmins: this.calculateHoursAndMinutes(amStartTime, amEndTime)
          });
        }

        if (this.data.value.pmFromDateTime && this.data.value.pmToDateTime !== undefined) {

          const pmTimeSched = this.data.value.pmTime
          const [pmStartTime, pmEndTime] = pmTimeSched.split('-');
          const pmFromTime = this.data.value.pmFromDateTime
          const pmToTime = this.data.value.pmToDateTime

          this.displayArr.push({
            date: this.dateFormat(this.selectedDates[0]), from: pmStartTime.trim(), to: pmEndTime.trim(), fromValue: this.hourGetter(pmFromTime),
            toValue: this.hourGetter(pmToTime), hrmins: this.calculateHoursAndMinutes(pmStartTime, pmEndTime)
          });

        }

      } else {

        for (var x = 0; x < this.scheduleChecker.length; x++) {

          this.convertTime(this.scheduleChecker[x].amTimeIn, 1)
          this.convertTime(this.scheduleChecker[x].amTimeOut, 2)
          this.convertTime(this.scheduleChecker[x].pmTimeIn, 3)
          this.convertTime(this.scheduleChecker[x].pmTimeOut, 4)

          const isDateDuplicate = this.displayArr.some(schedule => {
            return schedule.date === this.dateFormat(this.selectedDates[x]);
          });

          if (isDateDuplicate) {
            // console.log('There are duplicates.');
            // Handle the case where there are duplicates, e.g., show an error message
          } else {

            if ((this.fromAmValue && this.toAmValue) && this.fromAmValue < this.toAmValue) {
              this.displayArr.push({
                date: this.dateFormat(this.selectedDates[x]), from: this.fromAmPassValue, to: this.toAmPassValue, fromValue: this.scheduleChecker[x].amTimeIn,
                toValue: this.scheduleChecker[x].amTimeOut, hrmins: this.calculateHoursAndMinutes(this.fromAmPassValue, this.toAmPassValue)
              });
            }

            if ((this.fromPmValue && this.toPmValue) && this.fromPmValue < this.toPmValue) {
              this.displayArr.push({
                date: this.dateFormat(this.selectedDates[x]), from: this.fromPmPassValue, to: this.toPmPassValue, fromValue: this.scheduleChecker[x].pmTimeIn,
                toValue: this.scheduleChecker[x].pmTimeOut, hrmins: this.calculateHoursAndMinutes(this.fromPmPassValue, this.toPmPassValue)
              });
            }
          }
        }

      }
    }
    // console.log(this.displayArr);

    const totalleaveHours = this.calculateTotaLeaveHours(this.displayArr);
    // console.log("Total Hours:", totalleaveHours);

    if (this.selectedDates.length >= 3 && this.data.typeOfLeave == 0) {
      //   const totalScheduleHours = this.calculateTotalScheduledLeaveHours(this.scheduleChecker);
      //   // console.log("Total Hours:", totalScheduleHours);
      //   if (totalleaveHours > totalScheduleHours) {
      // }
      this.global.swalAlert("Warning!", "You are required to submit a leave request letter for a leave duration of three consecutive days or longer.", "warning");

    }

  }

  schedChecker() {
    // console.log( this.empSchedule)
    this.scheduleChecker = []

    if (this.selectedDates.length >= 1) {
      // console.log('here')

      if (this.data.typeOfLeave == 0) {

        for (var x = 0; x < this.selectedDates.length; x++) {
          const dateString = this.selectedDates[x].toDateString();
          const firstThreeCharacters = dateString.substring(0, 3);

          for (var y = 0; y < this.empSchedule.length; y++) {
            if (firstThreeCharacters.toLowerCase() == this.empSchedule[y].dayOfWeek.toLowerCase().substring(0, 3)) {
              const timeIn = this.empSchedule[y].timeIn;
              const timeOut = this.empSchedule[y].timeOut;

              const existingEntryIndex = this.scheduleChecker.findIndex(entry => entry.date === this.dateFormat(this.selectedDates[x]));

              if (existingEntryIndex === -1) {
                this.scheduleChecker.push({ date: this.dateFormat(this.selectedDates[x]), amTimeIn: timeIn, amTimeOut: timeOut });
              } else {
                this.scheduleChecker[existingEntryIndex].pmTimeIn = timeIn;
                this.scheduleChecker[existingEntryIndex].pmTimeOut = timeOut;
              }
            }
          }

        }
      } else {
        var datesWithoutSchedule = [];

        var tempEmpSched = this.empSchedule

        interface Schedule {
          day: string;
          amTimeIn: string;
          amTimeOut: string;
          pmTimeIn: string;
          pmTimeOut: string;
          hours: number;
        }

        function mergeSchedules(schedules) {
          const mergedSchedules: { [dayOfWeek: string]: Schedule } = {};

          // Iterate through each schedule
          schedules.forEach(schedule => {
            const dayOfWeek = schedule.dayOfWeek;

            // If the day already exists in mergedSchedules, update the times and hours
            if (mergedSchedules[dayOfWeek]) {
              const mergedSchedule = mergedSchedules[dayOfWeek];

              // Check if the current schedule is in the morning or afternoon
              if (schedule.timeIn === '07:30' && schedule.timeOut === '12:00') {
                mergedSchedule.amTimeIn = schedule.timeIn;
                mergedSchedule.amTimeOut = schedule.timeOut;
              } else {
                mergedSchedule.pmTimeIn = schedule.timeIn;
                mergedSchedule.pmTimeOut = schedule.timeOut;
              }

              mergedSchedule.hours += schedule.hours;
            } else {
              // If it's a new day, create a new entry
              mergedSchedules[dayOfWeek] = {
                day: dayOfWeek,
                amTimeIn: schedule.timeIn,
                amTimeOut: schedule.timeOut,
                pmTimeIn: '',
                pmTimeOut: '',
                hours: schedule.hours
              };

              // Check if the current schedule is in the morning or afternoon
              if (schedule.timeIn !== '07:30' || schedule.timeOut !== '12:00') {
                mergedSchedules[dayOfWeek].pmTimeIn = schedule.timeIn;
                mergedSchedules[dayOfWeek].pmTimeOut = schedule.timeOut;
              }
            }
          });

          // Convert mergedSchedules object back to array and sort by day of the week
          const sortedMergedArray = Object.values(mergedSchedules).sort((a, b) => {
            const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            return daysOfWeek.indexOf(a.day) - daysOfWeek.indexOf(b.day);
          });

          return sortedMergedArray;
        }

        function findScheduleWithLongestDuration(schedules) {
          let longestDuration = 0;
          let scheduleWithLongestDuration = null;

          schedules.forEach(schedule => {
            const totalDuration = schedule.hours;
            if (totalDuration > longestDuration) {
              longestDuration = totalDuration;
              scheduleWithLongestDuration = schedule;
            }
          });

          return scheduleWithLongestDuration;
        }

        // Array to store dates without a schedule
        datesWithoutSchedule = [];

        for (var x = 0; x < this.selectedDates.length; x++) {
          const dateString = this.selectedDates[x].toDateString();
          const firstThreeCharacters = dateString.substring(0, 3);

          // Flag to track if a match is found for the current date
          let hasMatch = false;

          for (var y = 0; y < this.empSchedule.length; y++) {
            if (firstThreeCharacters.toLowerCase() === this.empSchedule[y].dayOfWeek.toLowerCase().substring(0, 3)) {
              const timeIn = this.empSchedule[y].timeIn;
              const timeOut = this.empSchedule[y].timeOut;

              const existingEntryIndex = this.scheduleChecker.findIndex(entry => entry.date === this.dateFormat(this.selectedDates[x]));

              if (existingEntryIndex === -1) {
                this.scheduleChecker.push({ date: this.dateFormat(this.selectedDates[x]), amTimeIn: timeIn, amTimeOut: timeOut });
              } else {
                this.scheduleChecker[existingEntryIndex].pmTimeIn = timeIn;
                this.scheduleChecker[existingEntryIndex].pmTimeOut = timeOut;
              }

              hasMatch = true; // Set to true when a match is found
            }
          }

          // If no match was found, add the date to the datesWithoutSchedule array
          if (!hasMatch) {
            datesWithoutSchedule.push(this.dateFormat(this.selectedDates[x]));
            // console.log(this.scheduleChecker, datesWithoutSchedule);
          }
        }

        const mergedResult = mergeSchedules(tempEmpSched);
        const schedulesWithLongestDuration = findScheduleWithLongestDuration(mergedResult);

        for (var i = 0; i < datesWithoutSchedule.length; i++) {
          this.scheduleChecker.push({
            date: this.dateFormat(datesWithoutSchedule[i]), amTimeIn: schedulesWithLongestDuration.amTimeIn, amTimeOut: schedulesWithLongestDuration.amTimeOut,
            pmTimeIn: schedulesWithLongestDuration.pmTimeIn, pmTimeOut: schedulesWithLongestDuration.pmTimeOut
          });
        }

      }

      // console.log(this.scheduleChecker)
      if (this.data.type == "editTime") {
        this.form.get('sdate').patchValue(this.selectedDates[0]);
        this.form.get('edate').patchValue(this.selectedDates[0]);
      }

      this.fromAmValue = this.scheduleChecker[0].amTimeIn
      this.toAmValue = this.scheduleChecker[0].amTimeOut
      this.fromPmValue = this.scheduleChecker[0].pmTimeIn
      this.toPmValue = this.scheduleChecker[0].pmTimeOut

      this.autoCalculateDates()
    }

  }

  clear() {
    this.fromAmValue = ''
    this.toAmValue = ''
    this.fromPmValue = ''
    this.toPmValue = ''
    this.displayArr = []
  }

  tempArray = []
  checker = false
  
  testSave() {
    // console.log('displayArr', this.displayArr)
    this.tempArray = []
    this.fromAmPassValue = null
    this.toAmPassValue = null
    this.fromPmPassValue = null
    this.toPmPassValue = null

    this.displayArr.forEach(item => {
      const date = item.date;
      const from = item.from;
      const to = item.to;
      const fromValue = item.fromValue
      const toValue = item.toValue

      const existingEntry = this.tempArray.find(entry => entry.date === date);

      if (!existingEntry) {
        this.tempArray.push({
          "date": date,
          "amTimeIn": null,
          "amTimeOut": null,
          "pmTimeIn": null,
          "pmTimeOut": null
        });
      }

      if (from.includes("AM")) {
        this.tempArray.find(entry => entry.date === date).amTimeIn = fromValue;
        this.tempArray.find(entry => entry.date === date).amTimeOut = toValue;
      } else {
        this.tempArray.find(entry => entry.date === date).pmTimeIn = fromValue;
        this.tempArray.find(entry => entry.date === date).pmTimeOut = toValue;
      }
    });

    // console.log(this.tempArray)
    for (var x = 0; x < this.tempArray.length; x++) {
      const date = new Date(this.tempArray[x].date)
      // console.log(date)

      if (this.tempArray[x] != undefined) {

        if (this.hourChecker(this.tempArray[x].amTimeIn, this.tempArray[x].amTimeOut, this.tempArray[x].pmTimeIn, this.tempArray[x].pmTimeOut) == true) {

          this.passDisplayArr.push({
            displayDate: this.dateFormat(date),
            amTime: this.convertTime(this.tempArray[x].amTimeIn, 1) + ' - ' + this.convertTime(this.tempArray[x].amTimeOut, 2),
            pmTime: this.convertTime(this.tempArray[x].pmTimeIn, 3) + ' - ' + this.convertTime(this.tempArray[x].pmTimeOut, 4),
            hrs: this.calculateTotalHoursAndMinutes(this.tempArray[x].amTimeIn, this.tempArray[x].amTimeOut, this.tempArray[x].pmTimeIn, this.tempArray[x].pmTimeOut),
            amFromDateTime: this.adjustTimeInDateTime(date, this.tempArray[x].amTimeIn),
            amToDateTime: this.adjustTimeInDateTime(date, this.tempArray[x].amTimeOut),
            pmFromDateTime: this.adjustTimeInDateTime(date, this.tempArray[x].pmTimeIn),
            pmToDateTime: this.adjustTimeInDateTime(date, this.tempArray[x].pmTimeOut),
            dateToBeUpdated: this.timeToBeUpdated
          })

          this.checker = true
        } else {
          this.global.swalAlert("Invalid Time!", "There should be at least 1 hour gap between the specified time, and minutes should be 00 or 30.", "error");
          this.checker = false
          break
        }

      }


    }

    if (this.checker == true) {
      if (this.data.typeOfLeave == 7) {
        // console.log(this.passDisplayArr.length);
        
        if (this.passDisplayArr.length <= 5) {
          this.dialogRef.close({ result: 'success', data: this.passDisplayArr });
        } else {
          this.passDisplayArr = []
          this.global.swalAlert("Invalid Time!", "Bereavement leave cannot exceed 5 days.", "error");
        }

      } else {
        this.dialogRef.close({ result: 'success', data: this.passDisplayArr });
      }

    }

  }

}

import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/global.service';
import { HrisLeaveManagementApiService } from 'src/app/hris-leave-management-api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-check-schedule',
  templateUrl: './check-schedule.component.html',
  styleUrls: ['./check-schedule.component.scss']
})
export class CheckScheduleComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CheckScheduleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private HrisApi: HrisLeaveManagementApiService,
    private global: GlobalService,) { }

  empSchedule = []
  tempArray = []
  daysOftheWeek = []
  facultyType = []
  leaveCategory = []
  facultySchedule = []
  besSchedule = []
  daysOfTheWeekCF = []
  daysOfTheWeekBF = []
  collegeFacultySchedule = []
  besFacultySchedule = []
  displayTimeSchedule = []
  displayBesTimeSchedule = []
  lType = 0
  fType = 0

  typeChecker = []

  ngOnInit() {
    
    this.leaveCategory = this.data.lType
    this.facultyType = this.data.fType
    if(this.data.lType.length == 1){
      this.lType = this.data.lType[0].id
    }
    if(this.data.fType.length == 1){
      this.fType = this.data.fType[0].id
    }
    this.getEmployeeLeaveManagementSchedule()
  }

  getEmployeeLeaveManagementSchedule() {

    const hasIdOneOrFour = this.data.lType.some(item => item.id === 1 || item.id === 4);
    const hasTwoOrThree = this.data.lType.some(item => item.id === 2);
    const collegeFaculty = this.data.fType.some(item => item.id === 2);
    const besFaculty = this.data.fType.some(item => item.id === 3);

    if (hasIdOneOrFour) {
      this.HrisApi.getEmployeePortalEmployeeSchedule()
        .map(response => response.json())
        .subscribe(res => {
          this.empSchedule = res.data
          
          // console.log(this.empSchedule, 'EMP portal leave');

          this.empSchedule.sort((a, b) => {
            const dayComparison = a.dayOfWeek.localeCompare(b.dayOfWeek);
            if (dayComparison === 0) {
              const timeInComparison = a.timeIn.localeCompare(b.timeIn);
              return timeInComparison !== 0 ? timeInComparison : a.timeOut.localeCompare(b.timeOut);
            }
            return dayComparison;
          });

          // sorts the days of the week 
          const uniqueDaysOfWeek = new Set(res.data.map(item => item.dayOfWeek));
          const daysOfWeek = Array.from(uniqueDaysOfWeek);
          daysOfWeek.sort((a: string, b: string) => {
            const order = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
            return order.indexOf(a) - order.indexOf(b);
          });

          this.daysOftheWeek = daysOfWeek

          for (var x = 0; x < this.daysOftheWeek.length; x++) {
            const dateString = this.daysOftheWeek[x]
            const firstThreeCharacters = dateString.substring(0, 3);

            for (var y = 0; y < this.empSchedule.length; y++) {

              if (firstThreeCharacters.toLowerCase() == this.empSchedule[y].dayOfWeek.toLowerCase().substring(0, 3)) {
                const timeIn = this.empSchedule[y].timeIn;
                const timeOut = this.empSchedule[y].timeOut;
                const totalHrs = this.empSchedule[y].hours;
                const roundedTotalHrs = Number.isInteger(totalHrs) ? totalHrs : totalHrs.toFixed(1);

                const existingEntryIndex = this.tempArray.findIndex(entry => entry.day === this.daysOftheWeek[x]);

                if (existingEntryIndex === -1) {
                  this.tempArray.push({ day: this.daysOftheWeek[x], amTimeIn: timeIn, amTimeOut: timeOut, amTotalHrs: roundedTotalHrs });
                } else {
                  this.tempArray[existingEntryIndex].pmTimeIn = timeIn;
                  this.tempArray[existingEntryIndex].pmTimeOut = timeOut;
                  this.tempArray[existingEntryIndex].pmTotalHrs = roundedTotalHrs;
                }
              }
            }
          }


        })
    }

    if (hasTwoOrThree && collegeFaculty) {

      this.global.swalLoading('Loading College Faculty Schedule.')
      this.HrisApi.getEmployeePortalCodeSummary(this.global.sy, this.global.empInfo.idnumber, "")
        .map(response => response.json())
        .subscribe(res => {
        
          
          this.facultySchedule = res.data

          for (var x = 0; x < this.facultySchedule.length; x++) {
            this.separateDays(this.facultySchedule[x].day, x)
          }

          const uniqueDays: string[] = Array.from(new Set(this.daysOfTheWeekCF));
          this.daysOfTheWeekCF = uniqueDays

          this.daysOfTheWeekCF.sort((a, b) => {
            // Convert day names to their corresponding numerical values
            const dayValues = {
              "Sunday": 0,
              "Monday": 1,
              "Tuesday": 2,
              "Wednesday": 3,
              "Thursday": 4,
              "Friday": 5,
              "Saturday": 6
            };

            // Compare the numerical values of the days
            return dayValues[a] - dayValues[b];
          });

          // console.log('College Faculty', this.facultySchedule);
          // console.log('College Faculty Schedule', this.daysOfTheWeekCF);

          this.schedChecker(2)
          this.global.swalClose();
        })


    }

    if (hasTwoOrThree && besFaculty) {

      this.global.swalLoading('Loading BES Faculty Schedule.')
      this.HrisApi.getEmployeePortalBasicEdFacultySchedule(this.global.sy.substring(0, 6), this.global.empInfo.idnumber)
        .map(response => response.json())
        .subscribe(res => {
          // console.log("Get BasicEdFaculty  Schedule : ", res)

          this.besSchedule = res.data
          this.daysOfTheWeekBF = res.data.map(item => item.day)

          const uniqueDays: string[] = Array.from(new Set(this.daysOfTheWeekBF));
          this.daysOfTheWeekBF = uniqueDays

          // console.log(this.daysOftheWeek);
          function parseTime(timeString) {
            const [start, end] = timeString.split('-');
            const [startHour, startMinute] = start.split(':').map(Number);
            return startHour * 60 + startMinute;
          }

          // Sort the data by day and then time
          this.besSchedule.sort((a, b) => {
            // First compare by day
            const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            const dayComparison = days.indexOf(a.day) - days.indexOf(b.day);
            if (dayComparison !== 0) {
              return dayComparison;
            }

            // If days are the same, compare by time
            const timeA = parseTime(a.time);
            const timeB = parseTime(b.time);
            return timeA - timeB;
          });

          // console.log(this.besSchedule);
          this.schedChecker(3)
          this.global.swalClose();
        })


    }

  }

  schedChecker(value) {
    this.collegeFacultySchedule = []
    this.besFacultySchedule = []

    if (value == 2) {
      for (var x = 0; x < this.daysOfTheWeekCF.length; x++) {
        const firstThreeCharacters = this.daysOfTheWeekCF[x].substring(0, 3);

        for (var y = 0; y < this.facultySchedule.length; y++) {
          for (var z = 0; z < this.facultySchedule[y].day.length; z++) {

            if (firstThreeCharacters.toLowerCase() == this.facultySchedule[y].day[z].toLowerCase().substring(0, 3)) {

              const timeSched = this.facultySchedule[y].time

              // Split the string using the '-' delimiter and adding a space between the period
              const [startTime, endTime] = timeSched.split('-');
              const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
              const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');
              const fromValue = this.convertTo24HourFormat(formattedStartTime)
              const toValue = this.convertTo24HourFormat(formattedEndTime.trim())

              this.collegeFacultySchedule.push({
                day: this.daysOfTheWeekCF[x], timeIn: this.convertTo24HourFormat(formattedStartTime), timeOut: this.convertTo24HourFormat(formattedEndTime),
                dispalyTimeIn: this.convertTime(this.convertTo24HourFormat(formattedStartTime)), dispalyTimeOut: this.convertTime(this.convertTo24HourFormat(formattedEndTime)),
                codeNo: this.facultySchedule[y].codeNo, subjectTitle: this.facultySchedule[y].subjectTitle, hrmins: this.calculateHoursAndMinutes2(fromValue, toValue)
              });
            }
          }
        }
      }

      this.displayData(2)
    } else if (value == 3) {

      for (var x = 0; x < this.daysOfTheWeekBF.length; x++) {

        const firstThreeCharacters = this.daysOfTheWeekBF[x].substring(0, 3);

        for (var y = 0; y < this.besSchedule.length; y++) {
          if (firstThreeCharacters.toLowerCase() == this.besSchedule[y].day.toLowerCase().substring(0, 3)) {
            const section = this.besSchedule[y].section
            const subject = this.besSchedule[y].subject
            const gradeLevel = this.besSchedule[y].gradeLevel
            const timeSched = this.besSchedule[y].time
            const [startTime, endTime] = timeSched.split('-');
            const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
            const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

            let sTimeDisplay
            let eTimeDisplay
            if (formattedStartTime < '07:00') {
              sTimeDisplay = formattedStartTime + ' PM'
              eTimeDisplay = formattedEndTime + ' PM'
            } else {
              sTimeDisplay = formattedStartTime + ' AM'
              if (formattedEndTime >= '12:00') {
                eTimeDisplay = formattedEndTime + ' PM'
              } else {
                eTimeDisplay = formattedEndTime + ' AM'
              }
            }

            let sTimeValue
            let eTimeValue
            if (formattedStartTime < '07:00') {
              sTimeValue = this.convertTo24Hour(formattedStartTime)
              eTimeValue = this.convertTo24Hour(formattedEndTime)

            } else {
              sTimeValue = formattedStartTime
              eTimeValue = formattedEndTime
            }

            this.besFacultySchedule.push({
              day: this.daysOfTheWeekBF[x], timeIn: sTimeValue, timeOut: eTimeValue,
              dispalyTimeIn: sTimeDisplay, dispalyTimeOut: eTimeDisplay, section: gradeLevel + ' - ' + section, subject: subject,
              hrmins: this.calculateHoursAndMinutes2(sTimeValue, eTimeValue)
            });

          }
        }

      }

      this.displayData(3)
    }

  }

  displayData(value) {

    const consolidatedDisplay = {};
    let consolidatedArray = {}
    let tempFacultyArray = []
    let besTempFacultyArray = []

    if (value == 2) {
      // console.log('colFaculty');

      for (var x = 0; x < this.collegeFacultySchedule.length; x++) {

        tempFacultyArray.push({
          displayDay: this.collegeFacultySchedule[x].day,
          time: this.convertTime(this.collegeFacultySchedule[x].timeIn) + ' - ' + this.convertTime(this.collegeFacultySchedule[x].timeOut),
          hrs: this.collegeFacultySchedule[x].hrmins,
          codeNo: this.collegeFacultySchedule[x].codeNo,
          subjectTitle: this.collegeFacultySchedule[x].subjectTitle,
          schoolYear: this.global.sy.slice(0, 6),
        })

      }

      tempFacultyArray.sort((a, b) => {
        // Compare the displayDay values
        if (a.displayDay < b.displayDay) return -1;
        if (a.displayDay > b.displayDay) return 1;

        // If displayDay values are equal, compare the time values within the same displayDay
        const timeA = convertTimeToMinutes(a.time);
        const timeB = convertTimeToMinutes(b.time);

        return timeA - timeB;
      });

      // Function to convert time to minutes for comparison
      function convertTimeToMinutes(timeString) {
        const [time, period] = timeString.split(' ');
        const [hours, minutes] = time.split(':');
        let totalMinutes = parseInt(hours) * 60 + parseInt(minutes);
        if (period.toUpperCase() === 'PM' && parseInt(hours) !== 12) totalMinutes += 12 * 60; // Convert PM time to 24-hour format
        return totalMinutes;
      }


      for (let x = 0; x < tempFacultyArray.length; x++) {

        const displayDay = tempFacultyArray[x].displayDay;

        if (consolidatedDisplay[displayDay]) {
          consolidatedDisplay[displayDay].time.push(tempFacultyArray[x].time);
          consolidatedDisplay[displayDay].hrs.push(tempFacultyArray[x].hrs);
          consolidatedDisplay[displayDay].codeNo.push(tempFacultyArray[x].codeNo);
          consolidatedDisplay[displayDay].subjectTitle.push(tempFacultyArray[x].subjectTitle);
          // consolidatedDisplay[displayDate].schoolYear.push(this.cFacultyDisplay[x].schoolYear); // Add schoolYear

        } else {
          consolidatedDisplay[displayDay] = {
            displayDay: displayDay,
            time: [tempFacultyArray[x].time],
            hrs: [tempFacultyArray[x].hrs],
            codeNo: [tempFacultyArray[x].codeNo],
            subjectTitle: [tempFacultyArray[x].subjectTitle], // Add codeNo
            schoolYear: tempFacultyArray[x].schoolYear,

          };
        }

        // Convert consolidatedDisplay to an array
        this.displayTimeSchedule = Object.values(consolidatedDisplay);

      }

      // console.log(this.displayTimeSchedule);
    } else if (value == 3) {

      for (var x = 0; x < this.besFacultySchedule.length; x++) {
        // console.log(this.displayArr)

        besTempFacultyArray.push({
          displayDay: this.besFacultySchedule[x].day,
          time: this.convertTime(this.besFacultySchedule[x].timeIn) + ' - ' + this.convertTime(this.besFacultySchedule[x].timeOut),
          hrs: this.besFacultySchedule[x].hrmins,
          // fromDateTime: this.adjustTimeInDateTime(date, this.displayArr[x].from),
          // toDateTime: this.adjustTimeInDateTime(date, this.displayArr[x].to),
          section: this.besFacultySchedule[x].section,
          subject: this.besFacultySchedule[x].subject,
          dateToBeUpdated: this.besFacultySchedule
        })

      }

      besTempFacultyArray.sort((a, b) => {
        // Compare the displayDay values
        if (a.displayDay < b.displayDay) return -1;
        if (a.displayDay > b.displayDay) return 1;

        // If displayDay values are equal, compare the time values within the same displayDay
        const timeA = convertTimeToMinutes(a.time);
        const timeB = convertTimeToMinutes(b.time);

        return timeA - timeB;
      });

      // Function to convert time to minutes for comparison
      function convertTimeToMinutes(timeString) {
        const [time, period] = timeString.split(' ');
        const [hours, minutes] = time.split(':');
        let totalMinutes = parseInt(hours) * 60 + parseInt(minutes);
        if (period.toUpperCase() === 'PM' && parseInt(hours) !== 12) totalMinutes += 12 * 60; // Convert PM time to 24-hour format
        return totalMinutes;
      }


      for (let x = 0; x < besTempFacultyArray.length; x++) {

        const displayDay = besTempFacultyArray[x].displayDay;

        if (consolidatedDisplay[displayDay]) {
          consolidatedDisplay[displayDay].time.push(besTempFacultyArray[x].time);
          // consolidatedDisplay[displayDay].fromDateTime.push(besTempFacultyArray[x].fromDateTime);
          // consolidatedDisplay[displayDay].toDateTime.push(besTempFacultyArray[x].toDateTime);
          consolidatedDisplay[displayDay].hrs.push(besTempFacultyArray[x].hrs);
          consolidatedDisplay[displayDay].section.push(besTempFacultyArray[x].section); 
          consolidatedDisplay[displayDay].subject.push(besTempFacultyArray[x].subject); 
        } else {
          consolidatedDisplay[displayDay] = {
            displayDay: displayDay,
            time: [besTempFacultyArray[x].time],
            hrs: [besTempFacultyArray[x].hrs],
            section: [besTempFacultyArray[x].section], // Add codeNo
            subject: [besTempFacultyArray[x].subject], // Add schoolYear
   
          };
        }

        // Convert consolidatedDisplay to an array
        this.displayBesTimeSchedule = Object.values(consolidatedDisplay);

      }

      // console.log(this.displayBesTimeSchedule);


    }

    // Convert day names to their corresponding numerical values
    const dayValues = {
      "Sunday": 0,
      "Monday": 1,
      "Tuesday": 2,
      "Wednesday": 3,
      "Thursday": 4,
      "Friday": 5,
      "Saturday": 6
    };

    this.displayBesTimeSchedule.sort((a, b) => {
      const dayA = a.displayDay;
      const dayB = b.displayDay;

      // Compare the numerical values of the days
      const dayComparison = dayValues[dayA] - dayValues[dayB];

      // If the days are the same, compare by other criteria (e.g., time)
      if (dayComparison === 0) {
        // Here you can compare by other criteria if needed
        // For example, if you want to sort by time within the same day:
        const timeA = a.time[0]; // Assuming time is an array and we compare the first time slot
        const timeB = b.time[0];
        return timeA.localeCompare(timeB); // Sort alphabetically, adjust as needed
      }

      return dayComparison;
    });

    // Sort the array by dayValues and then by other criteria if the days are the same
    this.displayTimeSchedule.sort((a, b) => {
      const dayA = a.displayDay;
      const dayB = b.displayDay;

      // Compare the numerical values of the days
      const dayComparison = dayValues[dayA] - dayValues[dayB];

      // If the days are the same, compare by other criteria (e.g., time)
      if (dayComparison === 0) {
        // Here you can compare by other criteria if needed
        // For example, if you want to sort by time within the same day:
        const timeA = a.time[0]; // Assuming time is an array and we compare the first time slot
        const timeB = b.time[0];
        return timeA.localeCompare(timeB); // Sort alphabetically, adjust as needed
      }

      return dayComparison;
    });

  }

  convertTo24Hour(time) {
    const [hour, minute] = time.split(":").map(Number);
    if (hour < 7) {
      // If hour is less than 7, add 12 to it
      return `${hour + 12}:${minute < 10 ? "0" + minute : minute}`;
    }
    return time;
  }

  adjustTimeInDateTime(dateTimeString, timeToSet) {
    if (dateTimeString != null && dateTimeString != '' && timeToSet != null && timeToSet != '') {
      // Parse the original date-time string into a Date object
      const originalDate = new Date(dateTimeString);

      // Parse the time string "07:30" into hours and minutes
      const [hours, minutes] = timeToSet.split(':').map(Number);

      // Get the time zone offset in minutes
      const timeZoneOffset = originalDate.getTimezoneOffset();

      // Calculate the total minutes since midnight
      const totalMinutes = hours * 60 + minutes;

      // Adjust the time by subtracting the time zone offset
      const adjustedMinutes = totalMinutes - timeZoneOffset;

      // Calculate the new hours and minutes
      const newHours = Math.floor(adjustedMinutes / 60);
      const newMinutes = adjustedMinutes % 60;

      // Set the specified hours and minutes without changing the date
      originalDate.setHours(newHours);
      originalDate.setMinutes(newMinutes);
      originalDate.setSeconds(0);
      originalDate.setMilliseconds(0);

      // Format the Date object into the desired format
      const formattedDate = originalDate.toISOString();

      // console.log(formattedDate);

      return formattedDate;
    }

  }

  convertTime(value) {

    if (value != null && value != '') {
      // Split the input time into hours and minutes
      const [hours, minutes] = value.split(':');

      // Parse hours and minutes as integers
      const hoursInt = parseInt(hours, 10);
      const minutesInt = parseInt(minutes, 10);

      // Determine the period (AM or PM)
      const period = hoursInt >= 12 ? 'PM' : 'AM';

      // Convert hours to 12-hour format
      const formattedHours = hoursInt % 12 || 12;

      // Format the time as "h:mm a" (e.g., "3:43 PM")
      const formattedTime = `${formattedHours}:${minutesInt.toString().padStart(2, '0')} ${period}`;

      // Add a console.log statement for debugging
      // console.log(`Input Time: ${value}, Formatted Time: ${formattedTime}`);

      return formattedTime;
    }
    else { return null }

  }

  calculateHoursAndMinutes2(startTime: string, endTime: string): string {
    let startTimeDate = moment(startTime, 'h:mm A').toDate();
    let endTimeDate = moment(endTime, 'h:mm A').toDate();

    // Check if the start time is greater than the end time.
    if (startTimeDate > endTimeDate) {
      // Swap the start and end times.
      const temp = startTimeDate;
      startTimeDate = endTimeDate;
      endTimeDate = temp;
    }

    const minutes = (endTimeDate.getTime() - startTimeDate.getTime()) / (1000 * 60);
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;

    // console.log(remainingMinutes)

    if (hours === 1) {
      var hourText = "hr";
    } else {
      var hourText = "hrs";
    }

    if (remainingMinutes === 1) {
      var minsText = "min";
    } else {
      var minsText = "mins";
    }

    if (hours > 0 && remainingMinutes > 0) {
      return `${hours} ${hourText} & ${remainingMinutes} ${minsText}`;
    } else if (hours > 0) {
      return `${hours} ${hourText}`;
    } else {
      return `${remainingMinutes} ${minsText}`;
    }
  }

  convertTo24HourFormat(time: string): string {
    const [timePart, meridiem] = time.split(' ');
    const [hours, minutes] = timePart.split(':');
    let hour = parseInt(hours, 10);

    if (meridiem === 'PM' && hour < 12) {
      hour += 12;
    } else if (meridiem === 'AM' && hour === 12) {
      hour = 0;
    }
    // console.log(`${hour.toString().padStart(2, '0')}:${minutes}`);

    return `${hour.toString().padStart(2, '0')}:${minutes}`;
  }

  separateDays(dayString, x) {
    const daysOfWeek = ['M', 'T', 'W', 'TH', 'F', 'SAT', 'SUN'];
    const result = [];
    let i = 0;

    while (i < dayString.length) {
      let day = dayString[i];
      if (day === 'T' && dayString[i + 1] === 'H') {
        day = 'TH';
        i += 2;
      } else if (day === 'S' && dayString[i + 1] === 'A') {
        day = 'SAT';
        i += 3;
      } else if (day === 'S' && dayString[i + 1] === 'U') {
        day = 'SUN';
        i += 3;
      } else {
        i++;
      }

      if (daysOfWeek.includes(day)) {
        if (day == "M") {
          result.push('Monday');
          this.daysOfTheWeekCF.push('Monday');
        } else if (day == "T") {
          result.push('Tuesday');
          this.daysOfTheWeekCF.push('Tuesday');
        } else if (day == "W") {
          result.push('Wednesday');
          this.daysOfTheWeekCF.push('Wednesday');
        } else if (day == "TH") {
          result.push('Thursday');
          this.daysOfTheWeekCF.push('Thursday');
        } else if (day == "F") {
          result.push('Friday');
          this.daysOfTheWeekCF.push('Friday');
        } else if (day == "SAT") {
          result.push('Saturday');
          this.daysOfTheWeekCF.push('Saturday');
        } else if (day == "SUN") {
          result.push('Sunday');
          this.daysOfTheWeekCF.push('Sunday');
        }

      }
    }
    this.facultySchedule[x].day = result

  }

  convertTo12HourFormat(time24) {
    // Split the input time into hours and minutes
    const [hours, minutes] = time24.split(":");

    // Determine whether it's in the morning (a.m.) or the afternoon (p.m.)
    const period = hours >= 12 ? "PM" : "AM";

    // If the hour is greater than 12, subtract 12 from it
    const hours12 = (hours % 12) || 12;

    // Format the time in the 12-hour format
    const time12 = `${hours12}:${minutes} ${period}`;

    return time12;
  }

  calculateTotalHoursAndMinutes(amTotalHrs, pmTotalHrs) {
    const totalHours = Math.floor(amTotalHrs + pmTotalHrs);
    const totalMinutes = Math.round((amTotalHrs + pmTotalHrs - totalHours) * 60);

    let hoursText = totalHours === 1 ? 'hr' : 'hrs';
    let minutesText = totalMinutes === 1 ? 'min' : 'mins';

    if (totalHours === 1) {
      hoursText = 'hr';
    } else if (totalHours > 1) {
      hoursText = 'hrs';
    }

    if (totalMinutes === 1) {
      minutesText = 'min';
    } else if (totalMinutes > 1) {
      minutesText = 'mins';
    }

    if (totalHours > 0 && totalMinutes > 0) {
      return `${totalHours} ${hoursText} and ${totalMinutes} ${minutesText}`;
    } else if (totalHours > 0) {
      return `${totalHours} ${hoursText}`;
    } else if (totalMinutes > 0) {
      return `${totalMinutes} ${minutesText}`;
    } else {
      return '0 min';
    }
  }




}

import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { HrisLeaveManagementApiService } from 'src/app/hris-leave-management-api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { OfficialBusinessComponent } from './../../popup/leave/official-business/official-business.component';
import { AddDateTimeComponent } from './add-date-time/add-date-time.component';
import { AddDateTimeFacultyComponent } from './add-date-time-faculty/add-date-time-faculty.component';
import { Inject } from '@angular/core';
import { StudentApiService } from 'src/app/student-api.service';
import { forkJoin } from 'rxjs';
import * as moment from 'moment';


@Component({
  selector: 'app-leave-management',
  templateUrl: './leave-management.component.html',
  styleUrls: ['./leave-management.component.css']
})
export class LeaveManagementComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<LeaveManagementComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private HrisApi: HrisLeaveManagementApiService,
    public global: GlobalService,
    private FormBuilder: FormBuilder,
    public dialog: MatDialog,
    public api: StudentApiService
  ) { }

  form: FormGroup;

  AID
  lType = 0
  appointmentList = []

  currentDate
  appValue = 0
  department = ''
  departmentID = ''

  venue = ''
  companySponsor = ''

  leaveCategory = [];
  facultyType = [];
  dateTimeDisplayArr = []
  dateTimeValueArr = []
  tempdata

  ctr = ''
  leaveId = ''

  amFromTime = ''
  amToTime = ''
  pmFromTime = ''
  pmToTime = ''

  facultyDateTimeDisplayArr = []
  checker: boolean = false

  facultyTempData
  fType = 0

  typeChecker = []

  additionalLeaveType = [
    {
      "leaveTypeID": 0,
      "leaveTypeInitials": "VL/SL ",
      "leaveTypeDescription": "Regular (VL/SL)"
    },

    {
      "leaveTypeID": 3,
      "leaveTypeInitials": "ML/PL",
      "leaveTypeDescription": "Maternity/Paternity Leave"
    },
    {
      "leaveTypeID": 4,
      "leaveTypeInitials": "OB",
      "leaveTypeDescription": "Official Business"
    },
    {
      "leaveTypeID": 6,
      "leaveTypeInitials": "BL",
      "leaveTypeDescription": "Birthday Leave",
      "status": false
    },
    {
      "leaveTypeID": 7,
      "leaveTypeInitials": "BerL",
      "leaveTypeDescription": "Bereavement Leave"
    },
    {
      "leaveTypeID": 8,
      "leaveTypeInitials": "SPL",
      "leaveTypeDescription": "Solo Parent Leave",
      "status": false
    }
  ]

  typeOfLeave
  gender = ''

  pinfo
  personNameInfo

  splChecker: boolean = false
  blChecker: boolean = true

  leaveDetailArray
  linfo = ''
  sickleave = 0
  slDay = 0
  slHour = 0
  slMin = 0
  vacationleave = 0
  vlDay = 0
  vlHour = 0
  vlMin = 0
  matrix = ''

  ngOnInit() {


    this.form = this.FormBuilder.group({
      reason: ['', Validators.required],
    });

    this.getAppointment()
    this.getEmployeePerson()
    this.getLeaveHistory()
    this.getLeaveCredit()
    this.getCurrentServerTime()
    this.ctr = this.data.type


    if (this.ctr == 'updateLeave') {
      this.lType = this.data.value.leaveCategoryID


      if (this.data.value.leaveCategoryID == 2) {
        this.lType = 2
        this.fType = 2
      } else if (this.data.value.leaveCategoryID == 3) {
        this.lType = 2
        this.fType = 3
      }

      this.form.get('reason').patchValue(this.data.value.reason)
      this.venue = this.data.value.venue
      this.currentDate = new Date(this.data.value.dateFiled)

    }

    if (this.ctr == 'addLeaveTimeDuration') {
      this.typeOfLeave = this.data.leaveTypeChecker
      this.lType = this.data.Ltype
      this.fType = this.data.value.leaveCategoryID
      // console.log(this.lType, this.fType);
      // console.log(this.data);

      let tempArr = []
      tempArr = []
      this.HrisApi.getEmployeePortalLeave().map(response => response.json())
        .subscribe(res => {
          tempArr = res.data
          // console.log(tempArr.length);
          for (var x = 0; x < tempArr.length; x++) {
            // console.log(tempArr[x]);
            if (this.data.leaveId == tempArr[x].leaveID) {
              // console.log(tempArr[x].leave_LeaveDurations[0].leave_LeaveTypes[0].leaveTypeID);
              this.typeOfLeave = tempArr[x].leave_LeaveDurations[0].leave_LeaveTypes[0].leaveTypeID
            }
          }
        })
    }
    // console.log(this.data);
  }

  getCurrentServerTime() {
    this.global.getapiPublicAPICurrentServerTime().map(response => response.json())
      .subscribe(res => {
  
        var today: any = new Date(res.data);
        // var dd = String(today.getDate() - 1).padStart(2, '0');
        // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        // var yyyy = today.getFullYear();
        // today = mm + '/' + dd + '/' + yyyy;
        // console.log(today);
        this.currentDate = today
  
       
      })


  }
  fucnTypeOfLeave() {
    // console.log(this.typeOfLeave);
    this.dateTimeDisplayArr = []
    if (this.typeOfLeave == 3) {

      if (this.personNameInfo.gender == 'M') {
        this.gender = 'M'
        // console.log('7 days paternity leave');
      } else if (this.personNameInfo.gender == 'F') {
        // console.log('105 days maternity leave');
        this.gender = 'F'
      }
      // if (this.personNameInfo.civilStatus == 'M') {

      // } else {
      //   this.global.swalAlert(
      //     'Invalid Civil Status.',
      //     'Only married employees are eligible for Maternity/Paternity Leave.',
      //     'warning'
      //   );

      // }
    }
  }

  clearSelection() {
    this.typeOfLeave = 0; // or whatever initial value you want
  }

  calculateTotaLeaveHours(data: any[]): number {
    let totalHours = 0;

    // Loop through each object in the array
    data.forEach(entry => {
      // Extract hours from each object and convert to number
      const hours = parseFloat(entry.hrs.replace(' hrs', ''));
      // Add to total hours
      totalHours += hours;
    });

    return totalHours;
  }

  getLeaveCategory() {
    this.HrisApi.getOnlineLeavemanagementLeaveCategory()
      .map(response => response.json())
      .subscribe(res => {
        this.leaveCategory = res.data
      })
  }

  getEmployeePerson() {
    this.HrisApi.getOLMEmployeePerson()
      .map(response => response.json())
      .subscribe(res => {
        this.personNameInfo = res.data
      })
  }

  getLeaveCredit() {

    this.HrisApi.getLeaveCredit(this.global.empInfo.idnumber)
      .map(response => response.json())
      .subscribe(res => {
        this.leaveDetailArray = res.data
        // console.log(res.data);

        if (res.data != null) {
          this.linfo = this.leaveDetailArray;
          this.sickleave = this.leaveDetailArray.sickleave;
          this.slDay = this.leaveDetailArray.slDay;
          this.slHour = this.leaveDetailArray.slHour;
          this.slMin = this.leaveDetailArray.slMin;
          this.vacationleave = this.leaveDetailArray.vacationleave;
          this.vlDay = this.leaveDetailArray.vlDay;
          this.vlHour = this.leaveDetailArray.vlHour;
          this.vlMin = this.leaveDetailArray.vlMin;
          this.matrix = this.leaveDetailArray.matrix;
        } else {
          this.leaveDetailArray = []
          this.linfo = this.leaveDetailArray;
          this.sickleave = this.leaveDetailArray.sickleave;
          this.slDay = this.leaveDetailArray.slDay;
          this.slHour = this.leaveDetailArray.slHour;
          this.slMin = this.leaveDetailArray.slMin;
          this.vacationleave = this.leaveDetailArray.vacationleave;
          this.vlDay = this.leaveDetailArray.vlDay;
          this.vlHour = this.leaveDetailArray.vlHour;
          this.vlMin = this.leaveDetailArray.vlMin;
          this.matrix = this.leaveDetailArray.matrix;
        }

        if (this.vacationleave <= 0) {
          this.additionalLeaveType[5].status = false
          this.splChecker = false
        } else {
          this.additionalLeaveType[5].status = true
          this.splChecker = true
        }
        // console.log( this.additionalLeaveType[5].status)
      }, Error => {
        //console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });
  }

  facultyTotalHrsMins(timeArray) {
    let totalHours = 0;
    let totalMinutes = 0;

    timeArray.forEach((timeString) => {
      const hoursAndMinutes = timeString.match(/(\d+)\s*hr\s*(?:&\s*(\d+)\s*mins)?/);

      if (hoursAndMinutes) {
        const hours = parseInt(hoursAndMinutes[1], 10) || 0;
        const minutes = parseInt(hoursAndMinutes[2], 10) || 0;
        totalHours += hours;
        totalMinutes += minutes;
      }
    });

    // Convert excess minutes to hours
    totalHours += Math.floor(totalMinutes / 60);
    totalMinutes %= 60;

    if (totalHours === 1) {
      var hourText = "hr";
    } else {
      var hourText = "hrs";
    }

    if (totalHours > 0 && totalMinutes > 0) {
      return `${totalHours} ${hourText} & ${totalMinutes} mins`;
    } else if (totalHours > 0) {
      return `${totalHours} ${hourText}`;
    } else {
      return `${totalMinutes} mins`;
    }
  }

  currentDateFormat(value) {
    const originalDate = new Date(value);

    const day = originalDate.getDate();
    const month = originalDate.getMonth() + 1;
    const year = originalDate.getFullYear();

    return `${month}/${day}/${year}`;
  }

  leaveHistory = []
  getLeaveHistory() {

    // console.log(this.global.sy, this.global.empInfo.idnumber);


    function transformYearRange(yearRange) {
      // Extract the start and end years from the year range
      let startYear = parseInt(yearRange.slice(0, 4));
      let endYear = parseInt(yearRange.slice(4, 6)); // Using let to allow reassignment

      // Adjust the years by subtracting 1
      startYear -= 1;
      endYear -= 1;

      // Convert the adjusted years back to a string and concatenate them
      return `${startYear}${endYear < 10 ? '0' + endYear : endYear}`;
    }

    this.HrisApi.getEmployeePortalLeaveHistory(this.global.empInfo.idnumber, this.global.sy)
      .map(response => response.json())
      .subscribe(res => {
        this.leaveHistory = res.data;

        // Check for leave type ID 6
        // Fetch leave history for the adjusted year range
        this.HrisApi.getEmployeePortalLeaveHistory(this.global.empInfo.idnumber, transformYearRange(this.global.sy))
          .map(response => response.json())
          .subscribe(res => {
            // Check if any leave history matches the current date
            for (var x = 0; x < res.data.length; x++) {
              if (res.data[x].dateFiled)
                this.leaveHistory.push(res.data[x])
            }

            const currentYear = new Date().getFullYear();

            // Filter out entries for the current year
            const filteredData = this.leaveHistory.filter(entry => {
              const year = new Date(entry.dateFiled).getFullYear();
              return year === currentYear;
            });

            for (var y in filteredData) {
              if (filteredData[y].leaveTypeId == 6) {
                this.additionalLeaveType[3].status = true
                this.blChecker = true;
                // console.log( this.additionalLeaveType[3].status);
                break; // Exit loop once found
              }
            }

            let splCheckerctr = 0
            for (var i in filteredData) {
              if (filteredData[i].leaveTypeId == 8) {
                splCheckerctr += filteredData[i].totalMinutes
                if (splCheckerctr > 3360) {
                  this.additionalLeaveType[5].status = true
                  this.splChecker = true
                }
              }
            }

          });
      });


  }

  getDayOfWeek(value) {
    const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const dayIndex = new Date(value).getDay();
    const day = daysOfWeek[dayIndex];
    const abbreviatedDay = day.substring(0, 3).toUpperCase(); // Returns the first three letters of the day in uppercase
    return abbreviatedDay;
  }

  clearDislay() {
    this.facultyDateTimeDisplayArr = []
  }

  calculateTotalHoursAndMinutes(amStartTime: string, amEndTime: string, pmStartTime: string, pmEndTime: string): string {
    let amStartTimeDate = moment(amStartTime, 'h:mm A').toDate();
    let amEndTimeDate = moment(amEndTime, 'h:mm A').toDate();

    // Check if the AM start time is greater than the AM end time.
    if (amStartTimeDate > amEndTimeDate) {
      // Swap the AM start and end times.
      const temp = amStartTimeDate;
      amStartTimeDate = amEndTimeDate;
      amEndTimeDate = temp;
    }

    let pmStartTimeDate = moment(pmStartTime, 'h:mm A').toDate();
    let pmEndTimeDate = moment(pmEndTime, 'h:mm A').toDate();

    // Check if the PM start time is greater than the PM end time.
    if (pmStartTimeDate > pmEndTimeDate) {
      // Swap the PM start and end times.
      const temp = pmStartTimeDate;
      pmStartTimeDate = pmEndTimeDate;
      pmEndTimeDate = temp;
    }

    // **If neither of the PM start and end time values are present, calculate the total hours and minutes for the AM start and end time only.**
    if (!pmStartTime && !pmEndTime) {
      // Calculate the total minutes for the AM duration.
      const amMinutes = (amEndTimeDate.getTime() - amStartTimeDate.getTime()) / (1000 * 60);

      // Calculate the total hours and remaining minutes.
      const totalMinutes = amMinutes;
      const hours = Math.floor(totalMinutes / 60);
      const remainingMinutes = totalMinutes % 60;

      if (hours === 1) {
        var hourText = "hr";
      } else {
        var hourText = "hrs";
      }

      if (hours > 0 && remainingMinutes > 0) {
        return `${hours} ${hourText} & ${remainingMinutes} mins`;
      } else if (hours > 0) {
        return `${hours} ${hourText}`;
      } else {
        return `${remainingMinutes} mins`;
      }
    }

    if (!amStartTime && !amEndTime) {
      // Calculate the total minutes for the AM duration.
      const amMinutes = (pmEndTimeDate.getTime() - pmStartTimeDate.getTime()) / (1000 * 60);

      // Calculate the total hours and remaining minutes.
      const totalMinutes = amMinutes;
      const hours = Math.floor(totalMinutes / 60);
      const remainingMinutes = totalMinutes % 60;

      if (hours > 0 && remainingMinutes > 0) {
        return `${hours} hrs & ${remainingMinutes} mins`;
      } else if (hours > 0) {
        return `${hours} hrs`;
      } else {
        return `${remainingMinutes} mins`;
      }
    }

    // Otherwise, calculate the total hours and minutes for both AM and PM durations.
    const amMinutes = (amEndTimeDate.getTime() - amStartTimeDate.getTime()) / (1000 * 60);
    const pmMinutes = (pmEndTimeDate.getTime() - pmStartTimeDate.getTime()) / (1000 * 60);

    const totalMinutes = amMinutes + pmMinutes;
    const hours = Math.floor(totalMinutes / 60);
    const remainingMinutes = totalMinutes % 60;

    if (hours > 0 && remainingMinutes > 0) {
      return `${hours} hrs & ${remainingMinutes} mins`;
    } else if (hours > 0) {
      return `${hours} hrs`;
    } else {
      return `${remainingMinutes} mins`;
    }
  }

  formatTime(dateTimeStr: string) {
    const date = new Date(dateTimeStr);
    const hours = date.getUTCHours();
    const minutes = date.getUTCMinutes();
    return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
  }

  getTime(amFromDateTime: string, amToDateTime: string, pmFromDateTime: string, pmToDateTime: string) {

    this.amFromTime = this.formatTime(amFromDateTime);
    this.amToTime = this.formatTime(amToDateTime);
    this.pmFromTime = this.formatTime(pmFromDateTime);
    this.pmToTime = this.formatTime(pmToDateTime);
  }

  getAppointment() {
    let uniqueArrayFT
    this.appointmentList = [];
    this.global.swalLoading('Loading Appointment.')
    this.HrisApi.getEmployeePortalAppointmentbyActive(this.global.empInfo.idnumber, 1)
      .map(response => response.json())
      .subscribe(res => {
        this.appointmentList = res.data.filter(appointment => appointment.active === 1); // Filter active appointments only
        // console.log(this.appointmentList);

        if (this.appointmentList.length > 0) {

          // Initialize leaveCategory array
          let leaveCategory = [];

          // Iterate over appointmentList and check appointmentTypeID
          this.appointmentList.forEach(appointment => {
            // console.log(appointment.appointmentCategoryID);

            if (appointment.appointmentCategoryID === 2 || appointment.appointmentCategoryID === 3 || appointment.appointmentCategoryID === 4 ||
              appointment.appointmentCategoryID === 5 || appointment.appointmentCategoryID === 8 || appointment.appointmentCategoryID === 9
            ) {

              this.global.swalLoading('Loading Employee Schedule.')
              this.HrisApi.getEmployeePortalEmployeeSchedule()
                .map(response => response.json()).subscribe(res => {
                  this.typeChecker = res.data

                  this.global.swalClose();
                })

              leaveCategory.push({ id: 1, category: 'NTP' });

            }

            // if (appointment.appointmentCategoryID === 1) {
            //   leaveCategory.push({ id: 2, category: 'FACULTY' });

            //   this.global.swalLoading('Loading College Faculty Schedule.')
            //   this.HrisApi.getEmployeePortalCodeSummary(this.global.sy, this.global.empInfo.idnumber, "")
            //     .map(response => response.json())
            //     .subscribe(res => {
            //       // console.log(res.data);
            //       if (res.data.length > 0) {
            //         this.facultyType.push({ id: 2, category: 'COLLEGE FACULTY', })
            //         uniqueArrayFT = this.facultyType.filter((obj, index, self) =>
            //           index === self.findIndex((o) => (
            //             o.id === obj.id && o.category === obj.category
            //           ))
            //         );

            //         this.facultyType = uniqueArrayFT
            //         if (this.facultyType.length == 1) {
            //           this.fType = this.facultyType[0].id
            //         }

            //       }
            //       this.global.swalClose();
            //     })


            //   this.global.swalLoading('Loading BES Faculty Schedule.')
            //   this.HrisApi.getEmployeePortalBasicEdFacultySchedule(this.global.sy.substring(0, 6), this.global.empInfo.idnumber)
            //     .map(response => response.json())
            //     .subscribe(res => {
            //       // console.log(res.data);
            //       if (res.data.length > 0) {
            //         this.facultyType.push({ id: 3, category: 'BES FACULTY', })
            //         uniqueArrayFT = this.facultyType.filter((obj, index, self) =>
            //           index === self.findIndex((o) => (
            //             o.id === obj.id && o.category === obj.category
            //           ))
            //         );

            //         this.facultyType = uniqueArrayFT
            //         if (this.facultyType.length == 1) {
            //           this.fType = this.facultyType[0].id
            //         }

            //       }
            //       this.global.swalClose();
            //     })

            // }

            // if (appointment.appointmentCategoryID === 6 || appointment.appointmentCategoryID === 7) {

            //   this.global.swalLoading('Loading Employee Schedule.')
            //   this.HrisApi.getEmployeePortalEmployeeSchedule()
            //     .map(response => response.json()).subscribe(res => {
            //       this.typeChecker = res.data
            //       this.global.swalClose();
            //     })

            //   leaveCategory.push({ id: 4, category: 'EAB' });
            // }
          });

          let uniqueArray = leaveCategory.filter((obj, index, self) =>
            index === self.findIndex((o) => (
              o.id === obj.id && o.category === obj.category
            ))
          );

          leaveCategory = uniqueArray

          if (leaveCategory.length > 0) {
            this.leaveCategory = leaveCategory
          }

          if (leaveCategory.length == 1 && this.leaveCategory.length == 0) {
            this.lType = leaveCategory[0].id
          }

          this.global.swalClose();
        }

        for (var x = 0; x < this.appointmentList.length; x++) {
          if (this.appointmentList[x].appointmentTypeID == 3 && this.appointmentList[x].active == 1) {
            if (this.appointmentList[x].empStatusID == 12 || this.appointmentList[x].empStatusID == 2) {
              // console.log(this.appointmentList[x].empStatusID);

              this.blChecker = false
            }
          }
        }

      }, error => {
        this.global.swalAlertError();
      });
  }

  // recordDeptName(dept) {
  //   // // this.selectedDept = param
  //   // console.log(dept)
  //   this.HrisApi.getLeaveManagementDepartment(dept).map(response => response.json()).subscribe(res => {
  //     // console.log(res.data)
  //     this.department = res.data[0].departmentName;
  //     this.departmentID = res.data[0].departmentID
  //   }, Error => {
  //     this.global.swalAlertError();
  //   })
  //   // console.log(this.department, this.departmentID)
  // }

  addLeave() {
    // console.log(this.typeOfLeave, this.venue);
    if (this.typeOfLeave != undefined) {
      if ((this.form.get('reason').value != null && this.form.get('reason').value != '')) {



        if (this.typeOfLeave == 4) {
          if (this.venue != '') {
            let lType
            if (this.lType == 2) {
              lType = this.fType
            } else {
              lType = this.lType
            }

            this.HrisApi.postEmployeePortalLeave({
              "dateFiled": this.currentDate,
              "reason": this.form.get('reason').value,
              "venue": this.venue,
              "leaveCategoryID": lType
            })
              // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
              .map(response => response.json())
              .subscribe(res => {

                this.leaveId = res.data[0].leaveID

                if (this.lType == 1 || this.lType == 4) {
                  this.addLeaveTime()
                } else if (this.lType == 2) {
                  // console.log('type2');
                  this.addFacultyLeaveTime()
                }

              }, Error => {
                this.global.swalAlertError();
                // console.log(Error)
              });


          } else {
            this.global.swalAlert('Missing Information', 'Please provide a venue for your Official Business Leave.', 'warning')
          }

        } else {

          this.global.swalLoading('Filling Leave.')
          if (this.dateTimeDisplayArr.length != 0 || this.facultyDateTimeDisplayArr.length != 0) {
            let lType
            if (this.lType == 2) {
              lType = this.fType
            } else {
              lType = this.lType
            }


            // console.log(this.typeOfLeave);

            if (this.typeOfLeave == 7) {
              // console.log('here');
              // console.log(this.dateTimeDisplayArr.length == 1);
              // console.log(this.facultyDateTimeDisplayArr.length == 1);
              if (this.dateTimeDisplayArr.length <= 5 && (this.lType == 1 || this.lType == 4)) {
                this.HrisApi.postEmployeePortalLeave({
                  "dateFiled": this.currentDate,
                  "reason": this.form.get('reason').value,
                  "venue": this.venue,
                  "leaveCategoryID": lType
                })
                  // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {

                    this.leaveId = res.data[0].leaveID

                    if (this.lType == 1 || this.lType == 4) {
                      this.addLeaveTime()
                    }
                    // else if (this.lType == 2) {
                    //   // console.log('type2');
                    //   this.addFacultyLeaveTime()
                    // }

                  }, Error => {
                    this.global.swalAlertError();
                    // console.log(Error)
                  });
              } else if (this.facultyDateTimeDisplayArr.length <= 5 && (this.lType == 2)) {
                this.HrisApi.postEmployeePortalLeave({
                  "dateFiled": this.currentDate,
                  "reason": this.form.get('reason').value,
                  "venue": this.venue,
                  "leaveCategoryID": lType
                })
                  // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {

                    this.leaveId = res.data[0].leaveID

                    if (this.lType == 1 || this.lType == 4) {
                      // this.addLeaveTime()
                    } else if (this.lType == 2) {
                      // console.log('type2');
                      this.addFacultyLeaveTime()
                    }

                  }, Error => {
                    this.global.swalAlertError();
                    // console.log(Error)
                  });
              } else {
                // console.log('here2');
                this.global.swalClose()
                this.global.swalAlert("Invalid Time!", "Bereavement leave cannot exceed 5 days.", "error");

              }
            } else if (this.typeOfLeave == 6) {
              if (this.dateTimeDisplayArr.length == 1 && (this.lType == 1 || this.lType == 4)) {
                this.HrisApi.postEmployeePortalLeave({
                  "dateFiled": this.currentDate,
                  "reason": this.form.get('reason').value,
                  "venue": this.venue,
                  "leaveCategoryID": lType
                })
                  // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {

                    this.leaveId = res.data[0].leaveID

                    if (this.lType == 1 || this.lType == 4) {
                      this.addLeaveTime()
                    }
                    // else if (this.lType == 2) {
                    //   // console.log('type2');
                    //   this.addFacultyLeaveTime()
                    // }

                  }, Error => {
                    this.global.swalAlertError();
                    // console.log(Error)
                  });
              } else if (this.facultyDateTimeDisplayArr.length == 1 && (this.lType == 2)) {
                this.HrisApi.postEmployeePortalLeave({
                  "dateFiled": this.currentDate,
                  "reason": this.form.get('reason').value,
                  "venue": this.venue,
                  "leaveCategoryID": lType
                })
                  // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {

                    this.leaveId = res.data[0].leaveID

                    if (this.lType == 1 || this.lType == 4) {
                      // this.addLeaveTime()
                    } else if (this.lType == 2) {
                      // console.log('type2');
                      this.addFacultyLeaveTime()
                    }

                  }, Error => {
                    this.global.swalAlertError();
                    // console.log(Error)
                  });
              } else {
                // console.log('here2');
                this.global.swalClose()
                this.global.swalAlert("Invalid Time!", "Birthday leave cannot exceed 1 day.", "error");

              }
            } else {
              this.HrisApi.postEmployeePortalLeave({
                "dateFiled": this.currentDate,
                "reason": this.form.get('reason').value,
                "venue": this.venue,
                "leaveCategoryID": lType
              })
                // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
                .map(response => response.json())
                .subscribe(res => {
                  // console.log(res.data);

                  this.leaveId = res.data[0].leaveID

                  if (this.lType == 1 || this.lType == 4) {
                    this.addLeaveTime()
                  } else if (this.lType == 2) {
                    this.addFacultyLeaveTime()
                  }

                }, Error => {
                  this.global.swalAlertError();
                  // console.log(Error)
                });

            }

          } else {
            this.global.swalAlert('Missing Information!', 'Please provide the correct date and time.', 'warning');
          }

        }

      }
      else {
        this.global.swalAlert('Missing Information!', 'Please provide a reason for your leave.', 'warning');
      }
    } else {
      this.global.swalAlert('Missing Information!', 'Please select the type of leave to be filed.', 'warning');
    }

  }

  addFacultyLeaveTime() {

    if (this.facultyDateTimeDisplayArr.length != 0) {
      if (this.fType == 2) {
        for (var x = 0; x < this.facultyDateTimeDisplayArr.length; x++) {
          for (var y = 0; y < this.facultyDateTimeDisplayArr[x].fromDateTime.length; y++) {
            let xCounter = x
            let yCounter = y
            // this.global.swalLoading('Posting Data...')
            // observables.push(xCounter)
            this.HrisApi.postEmployeePortalLeaveDuration(this.leaveId,
              {
                "startDate": this.facultyDateTimeDisplayArr[x].fromDateTime[y],
                "endDate": this.facultyDateTimeDisplayArr[x].toDateTime[y]
              }
            ).map(response => response.json()).subscribe(res => {
              const leaveDurationID = res.data[0].id
              this.HrisApi.postEmployeePortalLeaveFacultyLeaveDetail(
                {
                  "leaveId": this.leaveId,
                  "leaveDurationID": leaveDurationID,
                  "schoolYear": this.facultyDateTimeDisplayArr[xCounter].schoolYear,
                  "codeNo": this.facultyDateTimeDisplayArr[xCounter].codeNo[yCounter],
                  "section": "string",
                  "subject": "string"
                }

              ).map(response => response.json()).subscribe(res => {
                // this.global.swalClose();
              })

            })

          }
        }

        let leaveDuationArray = [];
        this.HrisApi.getOnlineLeaveManagement().map(response => response.json())
          .subscribe(res => {
            // console.log('here');
            for (var x = 0; x < res.data.length; x++) {
              if (this.leaveId == res.data[x].leaveID) {
                leaveDuationArray = res.data[x].leave_LeaveDurations;
              }
            }

            for (var y = 0; y < leaveDuationArray.length; y++) {
              this.HrisApi.postOLMOnlineLeaveType(
                {
                  "leaveDurationID": leaveDuationArray[y].id,
                  "leaveTypeID": this.typeOfLeave,
                  "totalMinutes": 0
                }
              )
                .map(response => response.json())
                .subscribe(res => {
                  // Handle response if needed
                });
            }
          });

        if (this.ctr == 'addLeave') {
          this.global.swalAlert("Leave created successfully.", '', 'success');
        } else if (this.ctr == 'addLeaveTimeDuration') {
          this.global.swalAlert("Leave duration added successfully.", '', 'success');
        }
        this.dialogRef.close({ result: 'success' });

      } else if (this.fType == 3) {
        // console.log('here');

        for (var x = 0; x < this.facultyDateTimeDisplayArr.length; x++) {
          for (var y = 0; y < this.facultyDateTimeDisplayArr[x].fromDateTime.length; y++) {
            let xCounter = x
            let yCounter = y
            // this.global.swalLoading('Posting Data...')
            // observables.push( 'observables'  )
            this.HrisApi.postEmployeePortalLeaveDuration(this.leaveId,
              {
                "startDate": this.facultyDateTimeDisplayArr[x].fromDateTime[y],
                "endDate": this.facultyDateTimeDisplayArr[x].toDateTime[y]
              }
            ).map(response => response.json()).subscribe(res => {
              const section = this.facultyDateTimeDisplayArr[xCounter].section[yCounter]
              const subject = this.facultyDateTimeDisplayArr[xCounter].subject[yCounter]
              const leaveDurationID = res.data[0].id
              // console.log(section, subject);

              this.HrisApi.postEmployeePortalLeaveFacultyLeaveDetail(
                {
                  "leaveId": this.leaveId,
                  "leaveDurationID": leaveDurationID,
                  "schoolYear": "string",
                  "codeNo": "string",
                  "section": section,
                  "subject": subject
                }
              ).map(response => response.json()).subscribe(res => {
                // this.global.swalClose();
              })


            })
          }
        }

        let leaveDuationArray = [];
        this.HrisApi.getOnlineLeaveManagement().map(response => response.json())
          .subscribe(res => {

            for (var x = 0; x < res.data.length; x++) {
              if (this.leaveId == res.data[x].leaveID) {
                leaveDuationArray = res.data[x].leave_LeaveDurations;
              }
            }

            for (var y = 0; y < leaveDuationArray.length; y++) {
              this.HrisApi.postOLMOnlineLeaveType(
                {
                  "leaveDurationID": leaveDuationArray[y].id,
                  "leaveTypeID": this.typeOfLeave,
                  "totalMinutes": 0
                }
              )
                .map(response => response.json())
                .subscribe(res => {
                  // console.log('success');

                  // Handle response if needed
                });
            }
          });

        if (this.ctr == 'addLeave') {
          this.global.swalAlert("Leave created successfully.", '', 'success');
        } else if (this.ctr == 'addLeaveTimeDuration') {
          this.global.swalAlert("Leave duration added successfully.", '', 'success');
        }
        this.dialogRef.close({ result: 'success' });
      }
    } else {
      this.global.swalAlert('Missing Information!', 'Please provide the correct date and time.', 'warning');
    }

  }

  addLeaveTime() {

    if (this.dateTimeDisplayArr.length != 0) {

      let observables = [];

      for (var x = 0; x < this.dateTimeDisplayArr.length; x++) {
        if ((this.dateTimeDisplayArr[x].amFromDateTime != null && this.dateTimeDisplayArr[x].amFromDateTime != '') &&
          (this.dateTimeDisplayArr[x].amToDateTime != null && this.dateTimeDisplayArr[x].amToDateTime != '')) {
          observables.push(
            this.HrisApi.postEmployeePortalLeaveDuration(this.leaveId,
              {
                "startDate": this.dateTimeDisplayArr[x].amFromDateTime,
                "endDate": this.dateTimeDisplayArr[x].amToDateTime
              }
            ).map(response => response.json())
          );
        }

        if ((this.dateTimeDisplayArr[x].pmFromDateTime != null && this.dateTimeDisplayArr[x].pmFromDateTime != '') &&
          (this.dateTimeDisplayArr[x].pmToDateTime != null && this.dateTimeDisplayArr[x].pmToDateTime != '')) {
          observables.push(
            this.HrisApi.postEmployeePortalLeaveDuration(this.leaveId,
              {
                "startDate": this.dateTimeDisplayArr[x].pmFromDateTime,
                "endDate": this.dateTimeDisplayArr[x].pmToDateTime
              }
            ).map(response => response.json())
          );
        }
      }

      forkJoin(observables).subscribe(responses => {
        // Handle responses here
        // console.log('All API calls completed');

        // Continue with other operations after all API calls are done
        let leaveDuationArray = [];
        this.HrisApi.getOnlineLeaveManagement().map(response => response.json())
          .subscribe(res => {
            // console.log(res.data, this.leaveId);

            for (var x = 0; x < res.data.length; x++) {
              if (this.leaveId == res.data[x].leaveID) {
                leaveDuationArray = res.data[x].leave_LeaveDurations;
              }
            }

            for (var y = 0; y < leaveDuationArray.length; y++) {
              this.HrisApi.postOLMOnlineLeaveType(
                {
                  "leaveDurationID": leaveDuationArray[y].id,
                  "leaveTypeID": this.typeOfLeave,
                  "totalMinutes": 0
                }
              )
                .map(response => response.json())
                .subscribe(res => {
                  // Handle response if needed
                });
            }
          });
      });

      this.global.swalClose();
      this.dialogRef.close({ result: 'success' });
      if (this.ctr == 'addLeave') {
        this.global.swalAlert("Leave created successfully.", '', 'success');
      } else if (this.ctr == 'addLeaveTimeDuration') {
        this.global.swalAlert("Leave duration added successfully.", '', 'success');
      }

    } else {
      this.global.swalAlert('Missing Information!', 'Please provide the correct date and time.', 'warning');
    }

  }

  delete(value) {
    // console.log(value);
    if (value == 'NTP') {
      for (var x = 0; x < this.dateTimeDisplayArr.length; x++) {
        if (this.tempdata.displayDate == this.dateTimeDisplayArr[x].displayDate &&
          this.tempdata.amTime == this.dateTimeDisplayArr[x].amTime &&
          this.tempdata.pmTime == this.dateTimeDisplayArr[x].pmTime &&
          this.tempdata.hrs == this.dateTimeDisplayArr[x].hrs) {
          this.dateTimeDisplayArr.splice(x, 1);
          break; // Exit the loop after removing the item
        }
      }
    } else {
      // console.log(this.facultyTempData, this.facultyDateTimeDisplayArr);
      for (var x = 0; x < this.facultyDateTimeDisplayArr.length; x++) {
        if (this.facultyTempData.displayDate == this.facultyDateTimeDisplayArr[x].displayDate &&
          this.facultyTempData.fromDateTime == this.facultyDateTimeDisplayArr[x].fromDateTime &&
          this.facultyTempData.toDateTime == this.facultyDateTimeDisplayArr[x].toDateTime &&
          this.facultyTempData.hrs == this.facultyDateTimeDisplayArr[x].hrs) {
          this.facultyDateTimeDisplayArr.splice(x, 1);
          break; // Exit the loop after removing the item
        }
      }

    }

    // console.log(this.dateTimeDisplayArr)
  }

  setData(param) {
    this.tempdata = param
  }

  setFacultyData(param) {
    this.facultyTempData = param
  }

  updateLeave() {
    let fTypeVal = 0
    if (this.ctr == 'updateLeave' && (this.form.get('reason').value != null && this.form.get('reason').value != '')) {

      if (this.lType == 2) {
        fTypeVal = this.fType
      } else {
        fTypeVal = this.lType
      }

      this.HrisApi.putEmployeePortalLeave(this.data.value.leaveID, {
        "dateFiled": this.currentDate,
        "reason": this.form.get('reason').value,
        "venue": this.venue,
        "leaveCategoryID": fTypeVal
      })
        // this.http.post(this.global.api+'Employee/Rank/'+,this.global.option)
        .map(response => response.json())
        .subscribe(res => {

          this.global.swalAlert("Success", 'Leave updated.', 'success');
        }, Error => {
          this.global.swalAlertError();
          // console.log(Error)
        });


      this.dialogRef.close({ result: 'success', value: fTypeVal });

    } else {
      this.global.swalAlert('Missing Information!', 'Please provide a reason for your leave .', 'warning');

    }
  }

  //date time module start
  addDateAndTime() {
    // console.log(this.lType);
    if (this.lType == 1 || this.lType == 4) {

      const dialogRef = this.dialog.open(AddDateTimeComponent, {
        width: 'px', disableClose: true, autoFocus: false, data: {
          leaveType: this.lType,
          typeOfLeave: this.typeOfLeave,
          employeeGender: this.gender
        }
      });

      dialogRef.afterClosed().subscribe(result => {

        for (var x = 0; x < result.data.length; x++) {
          const newItem = result.data[x];
          const newItemDate = newItem.displayDate;

          // Check if an item with the same displayDate already exists in the dateTimeDisplayArr array
          const isItemDuplicate = this.dateTimeDisplayArr.some(existingItem => {
            return existingItem.displayDate === newItemDate;
          });

          if (!isItemDuplicate) {
            // If no duplicates found, add the item to the dateTimeDisplayArr
            this.dateTimeDisplayArr.push(newItem);

          } else {
            this.updateSameDateNTP(newItemDate, newItem)

          }

        }

        this.dateTimeDisplayArr.sort((b, a) => {
          const dateA: Date = new Date(a.displayDate);
          const dateB: Date = new Date(b.displayDate);
          return dateB.getTime() - dateA.getTime();
        });

      });

    } else if (this.lType == 2 || this.lType == 3) {

      const dialogRef = this.dialog.open(AddDateTimeFacultyComponent, {
        width: '1000px', disableClose: true, autoFocus: false, data: {
          leaveType: this.fType,
          typeOfLeave: this.typeOfLeave,
          employeeGender: this.gender
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        // this.facultyDateTimeDisplayArr = result.data
        // console.log(result);
        if (result.result !== 'cancel') {
          this.fType = result.fType
          if (result.fType == 2) {

            for (var x = 0; x < result.data.length; x++) {
              const newItem = result.data[x];
              const newItemDate = newItem.displayDate;
              result.data[x].hrs = this.facultyTotalHrsMins(result.data[x].hrs)
              // Check if an item with the same displayDate already exists in the dateTimeDisplayArr array
              const isItemDuplicate = this.facultyDateTimeDisplayArr.some(existingItem => {
                return existingItem.displayDate === newItemDate;
              });

              if (!isItemDuplicate) {
                // If no duplicates found, add the item to the dateTimeDisplayArr
                this.facultyDateTimeDisplayArr.push(newItem);

              } else {
                this.global.swalAlert('Time Conflict', 'The Date and time has already been set.', 'error');
              }

            }

          }

          else if (result.fType == 3) {

            for (var x = 0; x < result.data.length; x++) {
              const newItem = result.data[x];
              const newItemDate = newItem.displayDate;
              result.data[x].hrs = this.facultyTotalHrsMins(result.data[x].hrs)
              // Check if an item with the same displayDate already exists in the dateTimeDisplayArr array
              const isItemDuplicate = this.facultyDateTimeDisplayArr.some(existingItem => {
                return existingItem.displayDate === newItemDate;
              });

              if (!isItemDuplicate) {
                // If no duplicates found, add the item to the dateTimeDisplayArr
                this.facultyDateTimeDisplayArr.push(newItem);

              } else {
                this.global.swalAlert('Time Conflict', 'The Date and time has already been set.', 'error');
              }

            }

          }

          this.facultyDateTimeDisplayArr.sort((b, a) => {
            const dateA: Date = new Date(a.displayDate);
            const dateB: Date = new Date(b.displayDate);
            return dateB.getTime() - dateA.getTime();
          });
        }
      });

    }

  }

  updateSameDateFaculty(date, data) {

    this.amFromTime = ''
    this.amToTime = ''
    this.pmFromTime = ''
    this.pmToTime = ''

    for (var x = 0; x < this.facultyDateTimeDisplayArr.length; x++) {

      if (date == this.facultyDateTimeDisplayArr[x].displayDate) {

        if (this.facultyDateTimeDisplayArr[x].amFromDateTime === undefined && this.facultyDateTimeDisplayArr[x].amToDateTime === undefined) {
          // Check if 'pmFromDateTime' and 'pmToDateTime' are defined in data
          if (data.pmFromDateTime != undefined && data.pmToDateTime != undefined) {
            this.global.swalAlert('Time Conflict', 'The afternoon time schedule has already been set.', 'error');
          } else {
            // If 'amFromDateTime' and 'amToDateTime' are undefined in dateTimeDisplayArr[x], set them from data
            this.facultyDateTimeDisplayArr[x].amFromDateTime = data.amFromDateTime;
            this.facultyDateTimeDisplayArr[x].amToDateTime = data.amToDateTime;
            this.facultyDateTimeDisplayArr[x].amTime = data.amTime;
            this.getTime(this.facultyDateTimeDisplayArr[x].amFromDateTime, this.facultyDateTimeDisplayArr[x].amToDateTime,
              this.facultyDateTimeDisplayArr[x].pmFromDateTime, this.facultyDateTimeDisplayArr[x].pmToDateTime)

            this.facultyDateTimeDisplayArr[x].hrs = this.calculateTotalHoursAndMinutes(this.amFromTime, this.amToTime, this.pmFromTime, this.pmToTime)

            this.global.swalAlert('Success', 'The morning time schedule has set.', 'success');

          }
        } else if (this.facultyDateTimeDisplayArr[x].pmFromDateTime === undefined && this.facultyDateTimeDisplayArr[x].pmToDateTime === undefined) {
          // Check if 'amFromDateTime' and 'amToDateTime' are defined in data
          if (data.amFromDateTime != undefined && data.amToDateTime != undefined) {
            this.global.swalAlert('Time Conflict', 'The morning time schedule has already been set.', 'error');
          } else {
            // If 'pmFromDateTime' and 'pmToDateTime' are undefined in dateTimeDisplayArr[x], set them from data

            this.facultyDateTimeDisplayArr[x].pmFromDateTime = data.pmFromDateTime;
            this.facultyDateTimeDisplayArr[x].pmToDateTime = data.pmToDateTime;
            this.facultyDateTimeDisplayArr[x].pmTime = data.pmTime;
            this.getTime(this.facultyDateTimeDisplayArr[x].amFromDateTime, this.facultyDateTimeDisplayArr[x].amToDateTime,
              this.facultyDateTimeDisplayArr[x].pmFromDateTime, this.facultyDateTimeDisplayArr[x].pmToDateTime)
            this.global.swalAlert('Success', 'The afternoon time schedule has set.', 'success');
            this.facultyDateTimeDisplayArr[x].hrs = this.calculateTotalHoursAndMinutes(this.amFromTime, this.amToTime, this.pmFromTime, this.pmToTime)

          }
        } else {
          this.global.swalAlert('Time Conflict', 'The Date and time has already been set.', 'error');
        }

      }
    }


  }

  updateSameDateNTP(date, data) {

    this.amFromTime = ''
    this.amToTime = ''
    this.pmFromTime = ''
    this.pmToTime = ''

    for (var x = 0; x < this.dateTimeDisplayArr.length; x++) {

      if (date == this.dateTimeDisplayArr[x].displayDate) {

        if (this.dateTimeDisplayArr[x].amFromDateTime === undefined && this.dateTimeDisplayArr[x].amToDateTime === undefined) {
          // Check if 'pmFromDateTime' and 'pmToDateTime' are defined in data
          if (data.pmFromDateTime != undefined && data.pmToDateTime != undefined) {
            this.global.swalAlert('Time Conflict', 'The afternoon time schedule has already been set.', 'error');
          } else {
            // If 'amFromDateTime' and 'amToDateTime' are undefined in dateTimeDisplayArr[x], set them from data
            this.dateTimeDisplayArr[x].amFromDateTime = data.amFromDateTime;
            this.dateTimeDisplayArr[x].amToDateTime = data.amToDateTime;
            this.dateTimeDisplayArr[x].amTime = data.amTime;
            this.getTime(this.dateTimeDisplayArr[x].amFromDateTime, this.dateTimeDisplayArr[x].amToDateTime,
              this.dateTimeDisplayArr[x].pmFromDateTime, this.dateTimeDisplayArr[x].pmToDateTime)

            this.dateTimeDisplayArr[x].hrs = this.calculateTotalHoursAndMinutes(this.amFromTime, this.amToTime, this.pmFromTime, this.pmToTime)

            this.global.swalAlert('Success', 'The morning time schedule has set.', 'success');

          }
        } else if (this.dateTimeDisplayArr[x].pmFromDateTime === undefined && this.dateTimeDisplayArr[x].pmToDateTime === undefined) {
          // Check if 'amFromDateTime' and 'amToDateTime' are defined in data
          if (data.amFromDateTime != undefined && data.amToDateTime != undefined) {
            this.global.swalAlert('Time Conflict', 'The morning time schedule has already been set.', 'error');
          } else {
            // If 'pmFromDateTime' and 'pmToDateTime' are undefined in dateTimeDisplayArr[x], set them from data

            this.dateTimeDisplayArr[x].pmFromDateTime = data.pmFromDateTime;
            this.dateTimeDisplayArr[x].pmToDateTime = data.pmToDateTime;
            this.dateTimeDisplayArr[x].pmTime = data.pmTime;
            this.getTime(this.dateTimeDisplayArr[x].amFromDateTime, this.dateTimeDisplayArr[x].amToDateTime,
              this.dateTimeDisplayArr[x].pmFromDateTime, this.dateTimeDisplayArr[x].pmToDateTime)
            this.global.swalAlert('Success', 'The afternoon time schedule has set.', 'success');
            this.dateTimeDisplayArr[x].hrs = this.calculateTotalHoursAndMinutes(this.amFromTime, this.amToTime, this.pmFromTime, this.pmToTime)

          }
        } else {
          this.global.swalAlert('Time Conflict', 'The Date and time has already been set.', 'error');

        }

      }
    }

    // console.log(this.dateTimeDisplayArr)
  }

  editTime(editTime, type) {

    if (type == 'NTP') {
      const dialogRef = this.dialog.open(AddDateTimeComponent, {
        width: '750px', disableClose: true, autoFocus: false, data: {
          value: this.tempdata,
          type: editTime
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        // console.log('DisplayArray', this.dateTimeDisplayArr)
        // console.log('new date', result.data)

        if (result.data.length !== 0) {
          const dateToBeUpdated = result.data[0].dateToBeUpdated;
          const newItemDate = result.data[0].displayDate;

          // Check if the new date already exists in dateTimeDisplayArr
          const indexToUpdate = this.dateTimeDisplayArr.findIndex(item => item.displayDate === newItemDate);
          // console.log(this.dateTimeDisplayArr[indexToUpdate].displayDate, dateToBeUpdated);
          if (indexToUpdate !== -1) {
            // If a matching date is found, update dateToBeUpdated
            if (this.dateTimeDisplayArr[indexToUpdate].displayDate == dateToBeUpdated) {
              this.dateTimeDisplayArr[indexToUpdate] = result.data[0];
              this.dateTimeDisplayArr[indexToUpdate].dateToBeUpdated = ''
              this.global.swalAlert("Success!", "The date and time has been updated.", "success");
            } else {
              this.global.swalAlert("Duplicate Date!", "The date you selected already exists in the schedule. Please choose a different date.", "error");
            }

          } else {
            const indexOfDateToBeUpdated = this.dateTimeDisplayArr.findIndex(existingItem => existingItem.displayDate === dateToBeUpdated);

            // Check if an item with the matching dateToBeUpdated is found
            if (indexOfDateToBeUpdated !== -1) {
              // Update the item in dateTimeDisplayArr with values from result.data
              this.dateTimeDisplayArr[indexOfDateToBeUpdated] = result.data[0];
              this.dateTimeDisplayArr[indexOfDateToBeUpdated].dateToBeUpdated = ''

              // console.log('Item updated with result.data values:', this.dateTimeDisplayArr[indexOfDateToBeUpdated]);
              this.global.swalAlert("Success!", "The date and time has been updated.", "success");
            }
          }
        }

        this.dateTimeDisplayArr.sort((b, a) => {
          const dateA: Date = new Date(a.displayDate);
          const dateB: Date = new Date(b.displayDate);
          return dateB.getTime() - dateA.getTime();
        });


      });
    } else {
      // console.log(editTime);

      // console.log('update type', type);
      const dialogRef = this.dialog.open(AddDateTimeFacultyComponent, {
        width: '1000px', disableClose: true, autoFocus: false, data: {
          value: this.facultyTempData,
          type: editTime,
          facultyType: this.fType,
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        // console.log(result);

        if (result.data.length !== 0) {
          if (result.fType == 1 || result.fType == 4) {

            const dateToBeUpdated = result.data[0].dateToBeUpdated;
            const newItemDate = result.data[0].displayDate;

            // Check if the new date already exists in dateTimeDisplayArr
            const indexToUpdate = this.facultyDateTimeDisplayArr.findIndex(item => item.displayDate === newItemDate);
            // console.log(this.dateTimeDisplayArr[indexToUpdate].displayDate, dateToBeUpdated);

            if (indexToUpdate !== -1) {
              // If a matching date is found, update dateToBeUpdated
              if (this.facultyDateTimeDisplayArr[indexToUpdate].displayDate == dateToBeUpdated) {
                this.facultyDateTimeDisplayArr[indexToUpdate] = result.data[0];
                this.facultyDateTimeDisplayArr[indexToUpdate].dateToBeUpdated = ''
                this.global.swalAlert("Success!", "The date and time has been updated.", "success");
              } else {
                this.global.swalAlert("Duplicate Date!", "The date you selected already exists in the schedule. Please choose a different date.", "error");
              }

            } else {
              const indexOfDateToBeUpdated = this.facultyDateTimeDisplayArr.findIndex(existingItem => existingItem.displayDate === dateToBeUpdated);

              // Check if an item with the matching dateToBeUpdated is found
              if (indexOfDateToBeUpdated !== -1) {
                // Update the item in dateTimeDisplayArr with values from result.data
                this.facultyDateTimeDisplayArr[indexOfDateToBeUpdated] = result.data[0];
                this.facultyDateTimeDisplayArr[indexOfDateToBeUpdated].dateToBeUpdated = ''

                // console.log('Item updated with result.data values:', this.dateTimeDisplayArr[indexOfDateToBeUpdated]);
                this.global.swalAlert("Success!", "The date and time has been updated.", "success");
              }
            }

          } else if (result.fType == 2 || result.fType == 3) {
            result.data[0].hrs = this.facultyTotalHrsMins(result.data[0].hrs)
            const dateToBeUpdated = result.data[0].dateToBeUpdated;
            const newItemDate = result.data[0].displayDate;

            const indexToUpdate = this.facultyDateTimeDisplayArr.findIndex(item => item.displayDate === newItemDate);
            // console.log(indexToUpdate, dateToBeUpdated);

            if (indexToUpdate !== -1) {
              // If a matching date is found, update dateToBeUpdated
              if (this.facultyDateTimeDisplayArr[indexToUpdate].displayDate == dateToBeUpdated) {
                this.facultyDateTimeDisplayArr[indexToUpdate] = result.data[0];
                this.facultyDateTimeDisplayArr[indexToUpdate].dateToBeUpdated = ''
                this.global.swalAlert("Success!", "The date has been updated.", "success");
              } else {
                this.global.swalAlert("Duplicate Date!", "The date you selected already exists in the schedule. Please choose a different date.", "error");
              }

            } else {
              const indexToUpdate = this.facultyDateTimeDisplayArr.findIndex(existingItem => existingItem.displayDate === dateToBeUpdated);

              // Check if an item with the matching dateToBeUpdated is found
              if (indexToUpdate !== -1) {
                // Update the item in dateTimeDisplayArr with values from result.data
                this.facultyDateTimeDisplayArr[indexToUpdate] = result.data[0];
                this.facultyDateTimeDisplayArr[indexToUpdate].dateToBeUpdated = ''

                // console.log('Item updated with result.data values:', this.dateTimeDisplayArr[indexOfDateToBeUpdated]);
                this.global.swalAlert("Success!", "The date has been updated.", "success");
              }
            }

          }

          this.facultyDateTimeDisplayArr.sort((b, a) => {
            const dateA: Date = new Date(a.displayDate);
            const dateB: Date = new Date(b.displayDate);
            return dateB.getTime() - dateA.getTime();
          });
        }


      });
    }



  }

  employeeLeaveType() {
    this.form.get('reason').patchValue('')
    this.venue = ''
    this.dateTimeDisplayArr = []
    this.facultyDateTimeDisplayArr = []
  }

  save() {
    console.log(this.typeOfLeave);

    // Calculate total hours
    // const totalHours = this.calculateTotaLeaveHours(this.dateTimeDisplayArr);
    // console.log("Total Hours:", totalHours);

    if (this.ctr == 'addLeave') {
      this.addLeave();
    }

    if (this.ctr == 'updateLeave') {
      this.updateLeave()
    }

    if (this.ctr == 'addLeaveTimeDuration') {

      let blChecker = false
      let berlChecker = false

      // console.log(this.typeOfLeave);
      // console.log(this.data.value);
      // console.log(this.dateTimeDisplayArr,'datetimedisplayarr');

      const uniqueDates = [...new Set(this.data.value.leave_LeaveDurations.map(item => item.startDate.split('T')[0]))];

      // Output the unique dates array
      // console.log(uniqueDates);
      const transformedArray = this.data.value.leave_LeaveDurations.map(item => {
        return {
          id: item.id, displayDate: item.startDate, startDate: item.startDate, endDate: item.endDate
        };
      });

      const matchingDates = [];
      if (this.lType == 1 || this.lType == 4) {


        if (this.typeOfLeave == 6 && uniqueDates.length == 1) {
          this.global.swalAlert("Invalid Time!", "Birthday leave cannot exceed 1 day.", "error");
          blChecker = true
        } else if (this.typeOfLeave == 7 && (this.dateTimeDisplayArr.length + uniqueDates.length > 5)) {
          this.global.swalAlert("Invalid Time!", "Bereavement leave cannot exceed 5 days.", "error");
          berlChecker = true
        } else {

          this.dateTimeDisplayArr.forEach(newItem => {
            transformedArray.forEach(oldItem => {
              // console.log(newItem.displayDate, oldItem.startDate.substring(0, 10));
              if (newItem.displayDate === oldItem.startDate.substring(0, 10)) {
                matchingDates.push(newItem.displayDate);
              }
            });
          });

        }

        if (matchingDates.length > 0) {
          this.global.swalAlert("Duplicate Date!", "The date you selected already exists in the schedule. Please choose a different date.", "error");
        } else {
          if (blChecker == false && berlChecker == false) {
            this.leaveId = this.data.leaveId
            this.addLeaveTime()
          }

        }

      } else if (this.lType == 2 || this.lType == 3) {

        if (this.typeOfLeave == 6 && uniqueDates.length == 1) {
          this.global.swalAlert("Invalid Time!", "Birthday leave cannot exceed 1 day.", "error");
          blChecker = true
        } else if (this.typeOfLeave == 7 && (this.facultyDateTimeDisplayArr.length + uniqueDates.length > 5)) {
          this.global.swalAlert("Invalid Time!", "Bereavement leave cannot exceed 5 days.", "error");
          berlChecker = true
        } else {
          this.facultyDateTimeDisplayArr.forEach(newItem => {
            transformedArray.forEach(oldItem => {
              if (newItem.displayDate === oldItem.startDate.substring(0, 10)) {
                matchingDates.push(newItem.displayDate);
              }
            });
          });

        }





        if (matchingDates.length > 0) {
          this.global.swalAlert("Duplicate Date!", "The date you selected already exists in the schedule. Please choose a different date.", "error");
        } else {
          if (blChecker == false && berlChecker == false) {
            this.leaveId = this.data.leaveId
            this.addFacultyLeaveTime()
          }
        }

      }

    }
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel' });
  }

}

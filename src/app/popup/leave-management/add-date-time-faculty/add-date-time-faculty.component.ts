import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HrisLeaveManagementApiService } from 'src/app/hris-leave-management-api.service';
import { GlobalService } from 'src/app/global.service';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { CheckScheduleComponent } from '../check-schedule/check-schedule.component';
import * as moment from 'moment';
import Swal from 'sweetalert2'
import { Inject } from '@angular/core';


@Component({
  selector: 'app-add-date-time-faculty',
  templateUrl: './add-date-time-faculty.component.html',
  styleUrls: ['./add-date-time-faculty.component.scss']
})
export class AddDateTimeFacultyComponent implements OnInit {


  constructor(public dialogRef: MatDialogRef<AddDateTimeFacultyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private hrisApi: HrisLeaveManagementApiService,
    private FormBuilder: FormBuilder,
    private global: GlobalService,
    private atp: AmazingTimePickerService,
    public dialog: MatDialog,
  ) { }

  form: FormGroup;
  daysOftheWeek = []
  displayArr = []
  selectedDates = []
  scheduleChecker = []
  facultySchedule = []

  fromValue: string = '';
  toValue: string = '';
  fromValueNTime = ''
  toValueNTime = '';
  dupTimeChker: boolean = false

  cFacultyDisplay = []
  timeToBeUpdated = ''

  facultyType

  fromAmValue: string = '';
  toAmValue: string = '';
  fromAmPassValue = ''
  toAmPassValue = ''

  colUpdateArray = []

  besFacultySchedule = []
  besTempArray = []

  tempArray

  leaveCategory = []
  facultyLeaveType = []

  dayArray = []
  day
  facultyCodeTimeArray = []
  timeAndCode
  tempfacultyCodeTimeArray = []

  besDayArray = []
  besDay
  besFacultyCodeTimeArray = []
  timeAndSubject
  besTempfacultyCodeTimeArray = []

  timeArrayDisplay = [
    { time: 'WHOLE DAY', timeId: 1 },
    { time: 'AM', timeId: 2 },
    { time: 'PM', timeId: 3 },
  ];

  timeArrayValue

  tempDisplayArray = []

  ngOnInit() {

    this.form = this.FormBuilder.group({
      sdate: ['', Validators.required],
      edate: ['', Validators.required]
    });

    if (this.data.type == "editTime") {
      this.timeToBeUpdated = this.data.value.displayDate
      this.facultyType = parseInt(this.data.facultyType);
    } else {
      this.facultyType = this.data.leaveType
    }

    if (this.facultyType == 2) {
      this.leaveCategory.push({ id: 2, category: 'FACULTY' });
      this.facultyLeaveType.push({ id: 2, category: 'COLLEGE FACULTY', })
    }

    if (this.facultyType == 3) {
      this.leaveCategory.push({ id: 2, category: 'FACULTY' });
      this.facultyLeaveType.push({ id: 3, category: 'BES FACULTY', })
    }

    this.getFacultySchedule()
  }

  myFilter = (d: Date): boolean => {
    if (this.data.typeOfLeave !== 0) {
      return true
    } else {
      const day = d.getDay();
      // Check if the day of the week is in the array. If not, disable the date.
      return this.daysOftheWeek.includes(this.getDayName(day));
    }

  }

  getDayName(day: number): string {
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    return weekdays[day];
  }

  convertTo24Hour(time) {
    const [hour, minute] = time.split(":").map(Number);
    if (hour < 7) {
      // If hour is less than 7, add 12 to it
      return `${hour + 12}:${minute < 10 ? "0" + minute : minute}`;
    }
    return time;
  }

  getFacultySchedule() {
    const uniqueDays: string[] = Array.from(new Set(this.daysOftheWeek));
    this.daysOftheWeek = uniqueDays

    if (this.facultyType == 2) {

      this.global.swalLoading('Loading College Faculty Schedule.')
      this.hrisApi.getEmployeePortalCodeSummary(this.global.sy, this.global.empInfo.idnumber, "")
        .map(response => response.json())
        .subscribe(res => {
          this.facultySchedule = res.data
          // console.log(this.facultySchedule);

          for (var x = 0; x < this.facultySchedule.length; x++) {
            this.separateDays(this.facultySchedule[x].day, x)
          }

          // this.facultyType = 2
          if (this.data.type == 'editTime') {
            const date = new Date(this.data.value.displayDate)
            this.form.get('sdate').patchValue(date);
            this.form.get('edate').patchValue(date);
            this.addTime(date, date, 3);
          }
          this.global.swalClose();
        })

    } else if (this.facultyType == 3) {

      this.global.swalLoading('Loading BES Faculty Schedule.')
      this.hrisApi.getEmployeePortalBasicEdFacultySchedule(this.global.sy.substring(0, 6), this.global.empInfo.idnumber)
        .map(response => response.json())
        .subscribe(res => {
          // console.log("Get BasicEdFaculty  Schedule : ", res)

          this.besFacultySchedule = res.data
          this.daysOftheWeek = res.data.map(item => item.day)

          // console.log(this.daysOftheWeek);

          // this.facultyType = 3
          if (this.data.type == 'editTime') {
            const date = new Date(this.data.value.displayDate)
            this.form.get('sdate').patchValue(date);
            this.form.get('edate').patchValue(date);
            this.addTime(date, date, 3);
          }

          function parseTime(timeString) {
            const [start, end] = timeString.split('-');
            const [startHour, startMinute] = start.split(':').map(Number);
            return startHour * 60 + startMinute;
          }

          // Sort the data by day and then time q
          this.besFacultySchedule.sort((a, b) => {
            // First compare by day
            const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            const dayComparison = days.indexOf(a.day) - days.indexOf(b.day);
            if (dayComparison !== 0) {
              return dayComparison;
            }

            // If days are the same, compare by time
            const timeA = parseTime(a.time);
            const timeB = parseTime(b.time);
            return timeA - timeB;
          });
          // console.log(this.besFacultySchedule);
          this.global.swalClose();
        })

    }

  }

  separateDays(dayString, x) {
    const daysOfWeek = ['M', 'T', 'W', 'TH', 'F', 'SAT', 'SUN'];
    const result = [];
    let i = 0;

    while (i < dayString.length) {
      let day = dayString[i];
      if (day === 'T' && dayString[i + 1] === 'H') {
        day = 'TH';
        i += 2;
      } else if (day === 'S' && dayString[i + 1] === 'A') {
        day = 'SAT';
        i += 3;
      } else if (day === 'S' && dayString[i + 1] === 'U') {
        day = 'SUN';
        i += 3;
      } else {
        i++;
      }

      if (daysOfWeek.includes(day)) {
        if (day == "M") {
          result.push('Monday');
          this.daysOftheWeek.push('Monday');
        } else if (day == "T") {
          result.push('Tuesday');
          this.daysOftheWeek.push('Tuesday');
        } else if (day == "W") {
          result.push('Wednesday');
          this.daysOftheWeek.push('Wednesday');
        } else if (day == "TH") {
          result.push('Thursday');
          this.daysOftheWeek.push('Thursday');
        } else if (day == "F") {
          result.push('Friday');
          this.daysOftheWeek.push('Friday');
        } else if (day == "SAT") {
          result.push('Saturday');
          this.daysOftheWeek.push('Saturday');
        } else if (day == "SUN") {
          result.push('Sunday');
          this.daysOftheWeek.push('Sunday');
        }

      }
    }
    this.facultySchedule[x].day = result
    // return result;
  }

  // Function to convert AM/PM time to military time
  convertTo24HourFormat(time: string): string {
    const [timePart, meridiem] = time.split(' ');
    const [hours, minutes] = timePart.split(':');
    let hour = parseInt(hours, 10);

    if (meridiem === 'PM' && hour < 12) {
      hour += 12;
    } else if (meridiem === 'AM' && hour === 12) {
      hour = 0;
    }
    // console.log(`${hour.toString().padStart(2, '0')}:${minutes}`);

    return `${hour.toString().padStart(2, '0')}:${minutes}`;
  }

  dateFormat(value) {
    const originalDate = new Date(value);
    const year = originalDate.getFullYear();
    const month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
    const day = originalDate.getDate().toString().padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;

    return formattedDate
  }

  hourGetter(value) {
    const dateObject = new Date(value);

    // Extracting hours and minutes
    const hours = dateObject.getUTCHours();
    const minutes = dateObject.getUTCMinutes();

    // Formatting the time as HH:mm
    const formattedTime = `${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
    return formattedTime
  }

  extractHoursAndMinutes(dateString: string) {
    // Create a new Date object from the date string
    const dateObject = new Date(dateString);

    // Specify the options for formatting (exclude seconds)
    const options: Intl.DateTimeFormatOptions = { hour: '2-digit', minute: '2-digit', hour12: true };

    // Get the formatted time string with hours and minutes only
    const hoursAndMinutes = dateObject.toLocaleTimeString('en-US', options);

    return hoursAndMinutes;
  }

  calculateHoursAndMinutes2(startTime: string, endTime: string): string {
    let startTimeDate = moment(startTime, 'h:mm A').toDate();
    let endTimeDate = moment(endTime, 'h:mm A').toDate();

    // Check if the start time is greater than the end time.
    if (startTimeDate > endTimeDate) {
      // Swap the start and end times.
      const temp = startTimeDate;
      startTimeDate = endTimeDate;
      endTimeDate = temp;
    }

    const minutes = (endTimeDate.getTime() - startTimeDate.getTime()) / (1000 * 60);
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;

    // console.log(remainingMinutes)

    if (hours === 1) {
      var hourText = "hr";
    } else {
      var hourText = "hrs";
    }

    if (remainingMinutes === 1) {
      var minsText = "min";
    } else {
      var minsText = "mins";
    }

    if (hours > 0 && remainingMinutes > 0) {
      return `${hours} ${hourText} & ${remainingMinutes} ${minsText}`;
    } else if (hours > 0) {
      return `${hours} ${hourText}`;
    } else {
      return `${remainingMinutes} ${minsText}`;
    }
  }

  calculateTotalHoursAndMinutes4(amStartTime: string, amEndTime: string, pmStartTime: string, pmEndTime: string): string {
    let amStartTimeDate = moment(amStartTime, 'h:mm A').toDate();
    let amEndTimeDate = moment(amEndTime, 'h:mm A').toDate();

    // Check if the AM start time is greater than the AM end time.
    if (amStartTimeDate > amEndTimeDate) {
      // Swap the AM start and end times.
      const temp = amStartTimeDate;
      amStartTimeDate = amEndTimeDate;
      amEndTimeDate = temp;
    }

    let pmStartTimeDate = moment(pmStartTime, 'h:mm A').toDate();
    let pmEndTimeDate = moment(pmEndTime, 'h:mm A').toDate();

    // Check if the PM start time is greater than the PM end time.
    if (pmStartTimeDate > pmEndTimeDate) {
      // Swap the PM start and end times.
      const temp = pmStartTimeDate;
      pmStartTimeDate = pmEndTimeDate;
      pmEndTimeDate = temp;
    }

    // **If neither of the PM start and end time values are present, calculate the total hours and minutes for the AM start and end time only.**
    if (!pmStartTime && !pmEndTime) {
      // Calculate the total minutes for the AM duration.
      const amMinutes = (amEndTimeDate.getTime() - amStartTimeDate.getTime()) / (1000 * 60);

      // Calculate the total hours and remaining minutes.
      const totalMinutes = amMinutes;
      const hours = Math.floor(totalMinutes / 60);
      const remainingMinutes = totalMinutes % 60;


      if (hours === 1) {
        var hourText = "hr";
      } else {
        var hourText = "hrs";
      }

      if (hours > 0 && remainingMinutes > 0) {
        return `${hours} ${hourText} & ${remainingMinutes} mins`;
      } else if (hours > 0) {
        return `${hours} ${hourText}`;
      } else {
        return `${remainingMinutes} mins`;
      }
    }

    if (!amStartTime && !amEndTime) {
      // Calculate the total minutes for the AM duration.
      const amMinutes = (pmEndTimeDate.getTime() - pmStartTimeDate.getTime()) / (1000 * 60);

      // Calculate the total hours and remaining minutes.
      const totalMinutes = amMinutes;
      const hours = Math.floor(totalMinutes / 60);
      const remainingMinutes = totalMinutes % 60;

      if (hours > 0 && remainingMinutes > 0) {
        return `${hours} hrs & ${remainingMinutes} mins`;
      } else if (hours > 0) {
        return `${hours} hrs`;
      } else {
        return `${remainingMinutes} mins`;
      }
    }

    // Otherwise, calculate the total hours and minutes for both AM and PM durations.
    const amMinutes = (amEndTimeDate.getTime() - amStartTimeDate.getTime()) / (1000 * 60);
    const pmMinutes = (pmEndTimeDate.getTime() - pmStartTimeDate.getTime()) / (1000 * 60);

    const totalMinutes = amMinutes + pmMinutes;
    const hours = Math.floor(totalMinutes / 60);
    const remainingMinutes = totalMinutes % 60;
    // console.log(hours);

    if (hours === 1) {
      var hourText = "hr";
    } else {
      var hourText = "hrs";
    }

    if (hours > 0 && remainingMinutes > 0) {
      return `${hours} ${hourText} & ${remainingMinutes} mins`;
    } else if (hours > 0) {
      return `${hours} ${hourText}`;
    } else {
      return `${remainingMinutes} mins`;
    }
  }

  sortTimeSched() {
    // Sorting displayArr by date and start time
    this.displayArr.sort((a, b) => {
      // Compare dates first
      const dateA: Date = new Date(`${a.date} ${a.from}`);
      const dateB: Date = new Date(`${b.date} ${b.from}`);
      const dateComparison: number = dateA.getTime() - dateB.getTime();

      // If dates are the same, compare start times
      if (dateComparison === 0) {
        return dateA.getTime() - dateB.getTime();
      }

      return dateComparison;
    });



    // Now displayArr is sorted by date and start time
  }

  sortCodeTimeArr(value) {
    value.sort((a, b) => {
      // Extract the hours and minutes from the "from" time field
      const [aHours, aMinutes] = a.from.split(':').map(Number);
      const [bHours, bMinutes] = b.from.split(':').map(Number);

      // Compare the hours
      if (aHours !== bHours) {
        return aHours - bHours;
      }

      // If hours are equal, compare the minutes
      return aMinutes - bMinutes;
    });
  }

  sortDayArray() {
    const dayOrder = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    // Sort the array by the "day" field
    this.dayArray.sort((a, b) => {
      const dayAIndex = dayOrder.indexOf(a.day);
      const dayBIndex = dayOrder.indexOf(b.day);

      // If both days are found in the order array, compare their indexes
      if (dayAIndex !== -1 && dayBIndex !== -1) {
        return dayAIndex - dayBIndex;
      }

      // If one of the days is not found in the order array, move it to the end
      if (dayAIndex === -1 && dayBIndex === -1) {
        // If both days are not found in the order array, compare their names
        return a.day.localeCompare(b.day);
      } else if (dayAIndex === -1) {
        return 1; // Move day A to the end
      } else {
        return -1; // Move day B to the end
      }
    });

  }

  sortBesDayArray() {
    const dayOrder = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    // Sort the array by the "day" field
    this.besDayArray.sort((a, b) => {
      const dayAIndex = dayOrder.indexOf(a.day);
      const dayBIndex = dayOrder.indexOf(b.day);

      // If both days are found in the order array, compare their indexes
      if (dayAIndex !== -1 && dayBIndex !== -1) {
        return dayAIndex - dayBIndex;
      }

      // If one of the days is not found in the order array
      if (dayAIndex === -1 && dayBIndex === -1) {
        // Compare their names directly
        return a.day.localeCompare(b.day);
      } else if (dayAIndex === -1) {
        return 1; // Move day A to the end
      } else {
        return -1; // Move day B to the end
      }
    });

    // Debugging: Output the sorted array to check if all elements are present
    // console.log(this.besDayArray);
  }

  hourChecker(fromAmValue, toAmValue) {
    // Function to check if the minutes are either 00 or 30
    const isValidMinutes = (hours, minutes) => [0, 15, 30, 45].includes(minutes);

    // Function to calculate total minutes from time values
    const calculateTotalMinutes = (hours, minutes) => hours * 60 + minutes;

    // Split the time values into hours and minutes for AM
    const [fromAmHours, fromAmMinutes] = fromAmValue ? fromAmValue.split(":").map(Number) : [0, 0];
    const [toAmHours, toAmMinutes] = toAmValue ? toAmValue.split(":").map(Number) : [0, 0];

    // Convert AM time values to total minutes
    const fromAmTotalMinutes = calculateTotalMinutes(fromAmHours, fromAmMinutes);
    const toAmTotalMinutes = calculateTotalMinutes(toAmHours, toAmMinutes);

    // Calculate the total minutes for both AM and PM
    const totalMinutes = Math.abs(toAmTotalMinutes - fromAmTotalMinutes)

    // Calculate the total hours
    const totalHours = totalMinutes / 60;

    // Check if the total hours are greater than or equal to 1 and if the minutes are either 00 or 30
    return totalHours >= 1 && isValidMinutes(fromAmHours, fromAmMinutes) && isValidMinutes(toAmHours, toAmMinutes)

  }

  adjustTimeInDateTime(dateTimeString, timeToSet) {
    if (dateTimeString != null && dateTimeString != '' && timeToSet != null && timeToSet != '') {
      // Parse the original date-time string into a Date object
      const originalDate = new Date(dateTimeString);

      // Parse the time string "07:30" into hours and minutes
      const [hours, minutes] = timeToSet.split(':').map(Number);

      // Get the time zone offset in minutes
      const timeZoneOffset = originalDate.getTimezoneOffset();

      // Calculate the total minutes since midnight
      const totalMinutes = hours * 60 + minutes;

      // Adjust the time by subtracting the time zone offset
      const adjustedMinutes = totalMinutes - timeZoneOffset;

      // Calculate the new hours and minutes
      const newHours = Math.floor(adjustedMinutes / 60);
      const newMinutes = adjustedMinutes % 60;

      // Set the specified hours and minutes without changing the date
      originalDate.setHours(newHours);
      originalDate.setMinutes(newMinutes);
      originalDate.setSeconds(0);
      originalDate.setMilliseconds(0);

      // Format the Date object into the desired format
      const formattedDate = originalDate.toISOString();

      // console.log(formattedDate);

      return formattedDate;
    }

  }

  convertTime(value, num) {

    if (this.facultyType == 3) {

      if (value != null && value != '') {
        // Split the input time into hours and minutes
        const [hours, minutes] = value.split(':');

        // Parse hours and minutes as integers
        const hoursInt = parseInt(hours, 10);
        const minutesInt = parseInt(minutes, 10);

        // Determine the period (AM or PM)
        const period = hoursInt >= 12 ? 'PM' : 'AM';

        // Convert hours to 12-hour format
        const formattedHours = hoursInt % 12 || 12;

        // Format the time as "h:mm a" (e.g., "3:43 PM")
        const formattedTime = `${formattedHours}:${minutesInt.toString().padStart(2, '0')} ${period}`;

        // Add a console.log statement for debugging
        // console.log(`Input Time: ${value}, Formatted Time: ${formattedTime}`);

        if (num == 1) { this.fromAmPassValue = formattedTime }
        else if (num == 2) { this.toAmPassValue = formattedTime }
        return formattedTime;
      }
      else { return null }

    } else if (this.facultyType == 2) {
      if (value != null && value != '') {
        // Split the input time into hours and minutes
        const [hours, minutes] = value.split(':');

        // Parse hours and minutes as integers
        const hoursInt = parseInt(hours, 10);
        const minutesInt = parseInt(minutes, 10);

        // Determine the period (AM or PM)
        const period = hoursInt >= 12 ? 'PM' : 'AM';

        // Convert hours to 12-hour format
        const formattedHours = hoursInt % 12 || 12;

        // Format the time as "h:mm a" (e.g., "3:43 PM")
        const formattedTime = `${formattedHours}:${minutesInt.toString().padStart(2, '0')} ${period}`;

        // Add a console.log statement for debugging
        // console.log(`Input Time: ${value}, Formatted Time: ${formattedTime}`);

        if (num == 1) { this.fromValueNTime = formattedTime }
        else if (num == 2) { this.toValueNTime = formattedTime }


        return formattedTime;
      }
      else { return null }
    }
  }

  sDateMLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + 104);
    this.form.get('edate').patchValue(newDate);
    return newDate;
  }

  eDateMLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() - 104);
    this.form.get('sdate').patchValue(newDate);
    return newDate;
  }

  sDatePLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + 6);
    this.form.get('edate').patchValue(newDate);
    return newDate;
  }

  eDatePLLeaveFunction(date: Date): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() - 6);
    this.form.get('sdate').patchValue(newDate);
    return newDate;
  }

  // sDateBerlLeaveFunction(date: Date): Date {
  //   const newDate = new Date(date);
  //   newDate.setDate(newDate.getDate() + 2);
  //   this.form.get('edate').patchValue(newDate);
  //   return newDate;
  // }

  // eDateBerlLeaveFunction(date: Date): Date {
  //   const newDate = new Date(date);
  //   newDate.setDate(newDate.getDate() - 2);
  //   this.form.get('sdate').patchValue(newDate);
  //   return newDate;
  // }

  onDateSelected(value) {

    // if (this.data.typeOfLeave == 7) {
    //   this.eDateBerlLeaveFunction(this.form.get('edate').value)
    // } else
    if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'M') {
      this.eDatePLLeaveFunction(this.form.get('edate').value)
    } else if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'F') {
      this.eDateMLLeaveFunction(this.form.get('edate').value)
    } else if (this.data.typeOfLeave == 6) {
      this.form.get('sdate').patchValue(this.form.get('edate').value);
    }

    if (this.data.type === "editTime") {
      this.form.get('sdate').patchValue(this.form.get('edate').value);
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    } else {
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    }

  }

  onDateSelected1(value) {

    if (this.data.typeOfLeave == 7) {
      // console.log(this.form.get('sdate').value,);
    } else if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'M') {
      this.sDatePLLeaveFunction(this.form.get('sdate').value)
    } else if (this.data.typeOfLeave == 3 && this.data.employeeGender == 'F') {
      this.sDateMLLeaveFunction(this.form.get('sdate').value)
    } else if (this.data.typeOfLeave == 6) {
      this.form.get('edate').patchValue(this.form.get('sdate').value);
    }


    if (this.data.type === "editTime") {
      this.form.get('edate').patchValue(this.form.get('sdate').value);
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    } else {
      const startDate = this.form.get('sdate').value;
      const endDate = this.form.get('edate').value;
      this.addTime(startDate, endDate, value);
    }

  }

  addTime(startDateValue, endDateValue, value) {
    // console.log(startDateValue, endDateValue, value)
    this.displayArr = []
    if (startDateValue && endDateValue) {
      if (startDateValue <= endDateValue) {

        // Clear the array before populating it with new dates.
        this.selectedDates = [];

        // Clone the start date to avoid modifying the original date object.
        let currentDate = new Date(startDateValue);

        if (this.data.typeOfLeave != 0) {
          while (currentDate <= endDateValue) {
            // Check if the day of the week is in the 'weekdays' array before pushing it.
            // console.log(this.daysOftheWeek)
            if (true) {
              this.selectedDates.push(new Date(currentDate));
            }
            currentDate.setDate(currentDate.getDate() + 1);
          }
        } else {
          while (currentDate <= endDateValue) {
            // Check if the day of the week is in the 'weekdays' array before pushing it.
            // console.log(this.daysOftheWeek)
            if (this.daysOftheWeek.includes(this.getDayName(currentDate.getDay()))) {
              this.selectedDates.push(new Date(currentDate));
            }
            currentDate.setDate(currentDate.getDate() + 1);
          }

        }

        // this.selectedDates = []
        // this.displayArr = [];
        this.facultyCodeTimeArray = []
        this.tempfacultyCodeTimeArray = []
        this.day = undefined
        this.timeAndCode = undefined

        this.besFacultyCodeTimeArray = []
        this.besTempfacultyCodeTimeArray = []
        this.besDay = undefined
        this.timeAndSubject = undefined

        this.timeArrayValue = ''
        this.besDayArray = []
        this.dayArray = []

        // Iterate through the dates and add them to the array until the end date is reached.
        this.schedChecker()
      } else {
        // Handle the case where startDate is greater than endDate.
        Swal({
          title: 'Invalid Date Range!',
          text: 'The start time cannot be later than or equal to the end time.\nPlease make sure the start date comes before the end date.',
          type: 'warning',
        });

        if (value == '1') {
          this.form.get('sdate').patchValue('')

        }
        else if (value == '0') {
          this.form.get('edate').patchValue('')
        }

        this.displayArr = [];
        this.selectedDates = []
        this.facultyCodeTimeArray = []
        this.tempfacultyCodeTimeArray = []
        this.day = undefined
        this.timeAndCode = undefined

        this.besFacultyCodeTimeArray = []
        this.besTempfacultyCodeTimeArray = []
        this.besDay = undefined
        this.timeAndSubject = undefined
        this.timeArrayValue = ''

        this.dayArray = []
        this.besDayArray = []
        return
      }
    }

    // console.log("this.selectedDates:", this.selectedDates); // Debugging statement

  }

  schedChecker() {
    this.scheduleChecker = []
    this.dayArray = []
    var datesWithoutSchedule = [];
    var tempSchedColArr = []
    var tempSchedBesArr = []
    if (this.selectedDates.length >= 1) {
      // console.log('here')
      if (this.data.typeOfLeave == 0) {
        if (this.facultyType == 3) {

          for (var x = 0; x < this.selectedDates.length; x++) {
            const dateString = this.selectedDates[x].toDateString();
            const firstThreeCharacters = dateString.substring(0, 3);

            for (var y = 0; y < this.besFacultySchedule.length; y++) {
              if (firstThreeCharacters.toLowerCase() == this.besFacultySchedule[y].day.toLowerCase().substring(0, 3)) {
                const section = this.besFacultySchedule[y].section
                const subject = this.besFacultySchedule[y].subject
                const timeSched = this.besFacultySchedule[y].time
                const [startTime, endTime] = timeSched.split('-');
                const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
                const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

                let sTimeDisplay
                let eTimeDisplay
                if (formattedStartTime < '07:00') {
                  sTimeDisplay = formattedStartTime + ' PM'
                  eTimeDisplay = formattedEndTime + ' PM'
                } else {
                  sTimeDisplay = formattedStartTime + ' AM'
                  if (formattedEndTime >= '12:00') {
                    eTimeDisplay = formattedEndTime + ' PM'
                  } else {
                    eTimeDisplay = formattedEndTime + ' AM'
                  }
                }

                let sTimeValue
                let eTimeValue
                if (formattedStartTime < '07:00') {
                  sTimeValue = this.convertTo24Hour(formattedStartTime)
                  eTimeValue = this.convertTo24Hour(formattedEndTime)

                } else {
                  sTimeValue = formattedStartTime
                  eTimeValue = formattedEndTime
                }

                this.scheduleChecker.push({
                  day: this.besFacultySchedule[y].day, date: this.dateFormat(this.selectedDates[x]), timeIn: sTimeValue, timeOut: eTimeValue,
                  dispalyTimeIn: sTimeDisplay, dispalyTimeOut: eTimeDisplay, section: section.toUpperCase(), subject: subject.toUpperCase()
                });

              }
            }

          }

        } else if (this.facultyType == 2) {

          for (var x = 0; x < this.selectedDates.length; x++) {
            const dateString = this.selectedDates[x].toDateString();
            const firstThreeCharacters = dateString.substring(0, 3);

            for (var y = 0; y < this.facultySchedule.length; y++) {
              for (var z = 0; z < this.facultySchedule[y].day.length; z++) {

                if (firstThreeCharacters.toLowerCase() == this.facultySchedule[y].day[z].toLowerCase().substring(0, 3)) {

                  const timeSched = this.facultySchedule[y].time

                  // Split the string using the '-' delimiter and adding a space between the period
                  const [startTime, endTime] = timeSched.split('-');
                  const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
                  const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

                  this.scheduleChecker.push({
                    day: this.facultySchedule[y].day[z], date: this.dateFormat(this.selectedDates[x]), timeIn: this.convertTo24HourFormat(formattedStartTime), timeOut: this.convertTo24HourFormat(formattedEndTime),
                    dispalyTimeIn: this.convertTime(this.convertTo24HourFormat(formattedStartTime), 0), dispalyTimeOut: this.convertTime(this.convertTo24HourFormat(formattedEndTime), 0),
                    codeNo: this.facultySchedule[y].codeNo
                  });
                }
              }
            }
          }
          this.sortDayArray()
        }
      } else {
        tempSchedColArr = []
        tempSchedBesArr = []
        datesWithoutSchedule = [];

        const daysArray = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

        if (this.facultyType == 3) {

          for (var z = 0; z < daysArray.length; z++) {

            const firstThreeCharacters = daysArray[z]

            for (var y = 0; y < this.besFacultySchedule.length; y++) {
              if (firstThreeCharacters.toLowerCase().substring(0, 3) == this.besFacultySchedule[y].day.toLowerCase().substring(0, 3)) {
                const section = this.besFacultySchedule[y].section
                const subject = this.besFacultySchedule[y].subject
                const timeSched = this.besFacultySchedule[y].time
                const [startTime, endTime] = timeSched.split('-');
                const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
                const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

                let sTimeDisplay
                let eTimeDisplay
                if (formattedStartTime < '07:00') {
                  sTimeDisplay = formattedStartTime + ' PM'
                  eTimeDisplay = formattedEndTime + ' PM'
                } else {
                  sTimeDisplay = formattedStartTime + ' AM'
                  if (formattedEndTime >= '12:00') {
                    eTimeDisplay = formattedEndTime + ' PM'
                  } else {
                    eTimeDisplay = formattedEndTime + ' AM'
                  }
                }

                let sTimeValue
                let eTimeValue
                if (formattedStartTime < '07:00') {
                  sTimeValue = this.convertTo24Hour(formattedStartTime)
                  eTimeValue = this.convertTo24Hour(formattedEndTime)

                } else {
                  sTimeValue = formattedStartTime
                  eTimeValue = formattedEndTime
                }

                tempSchedColArr.push({
                  day: this.besFacultySchedule[y].day, timeIn: sTimeValue, timeOut: eTimeValue,
                  dispalyTimeIn: sTimeDisplay, dispalyTimeOut: eTimeDisplay, section: section.toUpperCase(), subject: subject.toUpperCase()
                });

                // hasMatch = true;
              }
            }

          }


          for (var k = 0; k < this.selectedDates.length; k++) {
            const dateString = this.selectedDates[k].toDateString();
            const firstThreeCharacters = dateString.substring(0, 3);
            let hasMatch = false;
            for (var l = 0; l < tempSchedColArr.length; l++) {
              if (firstThreeCharacters.toLowerCase() == tempSchedColArr[l].day.toLowerCase().substring(0, 3)) {

                this.scheduleChecker.push({
                  day: tempSchedColArr[l].day, date: this.dateFormat(this.selectedDates[k]), timeIn: tempSchedColArr[l].timeIn, timeOut: tempSchedColArr[l].timeOut,
                  dispalyTimeIn: tempSchedColArr[l].dispalyTimeIn, dispalyTimeOut: tempSchedColArr[l].dispalyTimeOut, section: tempSchedColArr[l].section, subject: tempSchedColArr[l].subject
                });

                hasMatch = true;
              }

            }

            if (!hasMatch) {
              datesWithoutSchedule.push(this.dateFormat(this.selectedDates[k]));
            }
          }


          let tempArray = []
          const totalHoursPerDay = {};
          tempArray = tempSchedColArr

          // Iterate through the data
          tempArray.forEach(entry => {
            // Calculate duration for each entry
            const timeIn = new Date(`1970-01-01T${entry.timeIn}`);
            const timeOut = new Date(`1970-01-01T${entry.timeOut}`);
            const duration = (timeOut.getTime() - timeIn.getTime()) / (1000 * 60 * 60); // Calculate duration in hours

            // Get the day from the entry
            const day = entry.day;

            // Update the total hours for the day
            if (totalHoursPerDay[day]) {
              totalHoursPerDay[day] += duration;
            } else {
              totalHoursPerDay[day] = duration;
            }
          });

          // Find the day with the maximum total hours
          let maxTotalHoursDay = Object.keys(totalHoursPerDay).reduce((a, b) => {
            // If durations are the same, prioritize days according to your preference
            if (totalHoursPerDay[a] === totalHoursPerDay[b]) {
              const priorityOrder = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
              return priorityOrder.indexOf(a) < priorityOrder.indexOf(b) ? a : b;
            }
            return totalHoursPerDay[a] > totalHoursPerDay[b] ? a : b;
          });

          // Filter the original data to include only entries for the day with the longest total duration
          const longestDayEntries = tempArray.filter(entry => entry.day === maxTotalHoursDay);

          for (var j = 0; j < datesWithoutSchedule.length; j++) {
            for (var i = 0; i < longestDayEntries.length; i++) {

              this.scheduleChecker.push({
                day: longestDayEntries[i].day, date: this.dateFormat(datesWithoutSchedule[j]), timeIn: longestDayEntries[i].timeIn, timeOut: longestDayEntries[i].timeOut,
                dispalyTimeIn: longestDayEntries[i].dispalyTimeIn, dispalyTimeOut: longestDayEntries[i].dispalyTimeIn, section: longestDayEntries[i].section, subject: longestDayEntries[i].subject
              });
            }
          }

        } else if (this.facultyType == 2) {

          // for (var x = 0; x < this.selectedDates.length; x++) {
          //   const dateString = this.selectedDates[x].toDateString();
          //   const firstThreeCharacters = dateString.substring(0, 3);
          //   let hasMatch = false;
          //   for (var y = 0; y < this.facultySchedule.length; y++) {
          //     for (var z = 0; z < this.facultySchedule[y].day.length; z++) {

          //       if (firstThreeCharacters.toLowerCase() == this.facultySchedule[y].day[z].toLowerCase().substring(0, 3)) {

          //         const timeSched = this.facultySchedule[y].time

          //         // Split the string using the '-' delimiter and adding a space between the period
          //         const [startTime, endTime] = timeSched.split('-');
          //         const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
          //         const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

          //         this.scheduleChecker.push({
          //           day: this.facultySchedule[y].day[z], date: this.dateFormat(this.selectedDates[x]), timeIn: this.convertTo24HourFormat(formattedStartTime), timeOut: this.convertTo24HourFormat(formattedEndTime),
          //           dispalyTimeIn: this.convertTime(this.convertTo24HourFormat(formattedStartTime), 0), dispalyTimeOut: this.convertTime(this.convertTo24HourFormat(formattedEndTime), 0),
          //           codeNo: this.facultySchedule[y].codeNo
          //         });


          //         hasMatch = true; // Set to true when a match is found
          //       }
          //     }
          //   }

          //   if (!hasMatch) {
          //     datesWithoutSchedule.push(this.dateFormat(this.selectedDates[x]));
          //   }
          // }

          for (var x = 0; x < daysArray.length; x++) {
            const firstThreeCharacters = daysArray[x].substring(0, 3)

            for (var y = 0; y < this.facultySchedule.length; y++) {
              for (var z = 0; z < this.facultySchedule[y].day.length; z++) {

                if (firstThreeCharacters.toLowerCase() == this.facultySchedule[y].day[z].toLowerCase().substring(0, 3)) {

                  const timeSched = this.facultySchedule[y].time

                  // Split the string using the '-' delimiter and adding a space between the period
                  const [startTime, endTime] = timeSched.split('-');
                  const formattedStartTime = startTime.replace(/(P|A)M$/, ' $1M');
                  const formattedEndTime = endTime.replace(/(P|A)M$/, ' $1M');

                  tempSchedBesArr.push({
                    day: this.facultySchedule[y].day[z], timeIn: this.convertTo24HourFormat(formattedStartTime), timeOut: this.convertTo24HourFormat(formattedEndTime),
                    dispalyTimeIn: this.convertTime(this.convertTo24HourFormat(formattedStartTime), 0), dispalyTimeOut: this.convertTime(this.convertTo24HourFormat(formattedEndTime), 0),
                    codeNo: this.facultySchedule[y].codeNo
                  });
                }
              }
            }
          }

          for (var x = 0; x < this.selectedDates.length; x++) {
            const dateString = this.selectedDates[x].toDateString();
            const firstThreeCharacters = dateString.substring(0, 3);
            let hasMatch = false;
            for (var y = 0; y < tempSchedColArr.length; y++) {
              if (firstThreeCharacters.toLowerCase() == tempSchedColArr[y].day.toLowerCase().substring(0, 3)) {

                this.scheduleChecker.push({
                  day: tempSchedColArr[y].day, timeIn: tempSchedColArr[y].timeIn, timeOut: tempSchedColArr[y].timeOut,
                  dispalyTimeIn: tempSchedColArr[y].dispalyTimeIn, dispalyTimeOut: tempSchedColArr[y].dispalyTimeOut,
                  codeNo: tempSchedColArr[y].codeNo
                });

              }
            }
            if (!hasMatch) {
              datesWithoutSchedule.push(this.dateFormat(this.selectedDates[x]));
            }
          }
          // console.log(tempSchedBesArr);


          this.sortDayArray()

          let tempArray = []
          const totalHoursPerDay = {};
          tempArray = tempSchedBesArr
          // Iterate through the data
          tempArray.forEach(entry => {
            // Calculate duration for each entry
            const timeIn = new Date(`1970-01-01T${entry.timeIn}`);
            const timeOut = new Date(`1970-01-01T${entry.timeOut}`);
            const duration = (timeOut.getTime() - timeIn.getTime()) / (1000 * 60 * 60); // Calculate duration in hours

            // Get the day from the entry
            const day = entry.day;

            // Update the total hours for the day
            if (totalHoursPerDay[day]) {
              totalHoursPerDay[day] += duration;
            } else {
              totalHoursPerDay[day] = duration;
            }
          });

          const sortedDaysByDuration = Object.keys(totalHoursPerDay).sort((a, b) => totalHoursPerDay[b] - totalHoursPerDay[a]);
          let maxTotalHoursDay = sortedDaysByDuration[0];

          // Prioritize certain days if they have the same duration
          const priorityDays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
          for (let day of priorityDays) {
            if (sortedDaysByDuration.includes(day)) {
              maxTotalHoursDay = day;
              break;
            }
          }

          // Filter the original data to include only entries for the day with the longest total duration
          const longestDayEntries = tempArray.filter(entry => entry.day === maxTotalHoursDay);

          for (var j = 0; j < datesWithoutSchedule.length; j++) {
            for (var i = 0; i < longestDayEntries.length; i++) {

              this.scheduleChecker.push({
                day: longestDayEntries[i].day, date: this.dateFormat(datesWithoutSchedule[j]), timeIn: longestDayEntries[i].timeIn, timeOut: longestDayEntries[i].timeOut,
                dispalyTimeIn: longestDayEntries[i].dispalyTimeIn, dispalyTimeOut: longestDayEntries[i].dispalyTimeIn,
                codeNo: longestDayEntries[i].codeNo
              });

            }
          }

        }
      }
    }

    this.autoCalculateDates()
  }

  autoCalculateDates() {

    this.displayArr = [];
    this.tempDisplayArray = []

    // Assuming this.scheduleChe  cker is an array of objects with properties date, timeIn, timeOut, dispalyTimeIn, and dispalyTimeOut
    if (this.facultyType == 3) {

      if (this.data.type === 'editTime' && this.colUpdateArray.length == 0) {
        // console.log('here');

        this.colUpdateArray.push(this.data.value)

        for (var x = 0; x < this.colUpdateArray.length; x++) {
          for (var y = 0; y < this.colUpdateArray[x].fromDateTime.length; y++) {

            const timeSched = this.colUpdateArray[x].time[y]
            const [startTime, endTime] = timeSched.split('-');
            const fromDisplay = startTime.replace(/(P|A)M$/, ' $1M');
            const toDisplay = endTime.replace(/(P|A)M$/, '$1M');
            const fromValue = this.convertTo24HourFormat(fromDisplay)
            const toValue = this.convertTo24HourFormat(toDisplay.trim())

            this.displayArr.push({
              date: this.colUpdateArray[x].displayDate,
              from: fromValue,
              to: toValue,
              fromDisplayValue: fromDisplay,
              toDisplayValue: toDisplay,
              section: this.colUpdateArray[x].section[y],
              subject: this.colUpdateArray[x].subject[y],
              hrmins: this.calculateHoursAndMinutes2(fromValue, toValue)
            });

          }
        }

      } else {
        // console.log('here', this.scheduleChecker);
        for (var x = 0; x < this.scheduleChecker.length; x++) {
          this.displayArr.push({
            date: this.scheduleChecker[x].date,
            from: this.scheduleChecker[x].timeIn,
            to: this.scheduleChecker[x].timeOut,
            fromDisplayValue: this.scheduleChecker[x].dispalyTimeIn,
            toDisplayValue: this.scheduleChecker[x].dispalyTimeOut,
            section: this.scheduleChecker[x].section,
            subject: this.scheduleChecker[x].subject,
            hrmins: this.calculateHoursAndMinutes2(
              this.scheduleChecker[x].timeIn,
              this.scheduleChecker[x].timeOut
            ),
            day: this.scheduleChecker[x].day
          });

          this.tempDisplayArray.push({
            date: this.scheduleChecker[x].date,
            from: this.scheduleChecker[x].timeIn,
            to: this.scheduleChecker[x].timeOut,
            fromDisplayValue: this.scheduleChecker[x].dispalyTimeIn,
            toDisplayValue: this.scheduleChecker[x].dispalyTimeOut,
            section: this.scheduleChecker[x].section,
            subject: this.scheduleChecker[x].subject,
            hrmins: this.calculateHoursAndMinutes2(
              this.scheduleChecker[x].timeIn,
              this.scheduleChecker[x].timeOut
            ),
            day: this.scheduleChecker[x].day
          })
        }
        // console.log(this.scheduleChecker);

      }

      // console.log(this.displayArr);

    } else if (this.facultyType == 2) {

      if (this.data.type == "editTime" && this.colUpdateArray.length == 0) {
        // console.log(this.data);

        this.colUpdateArray.push(this.data.value)

        for (var x = 0; x < this.colUpdateArray.length; x++) {
          for (var y = 0; y < this.colUpdateArray[x].fromDateTime.length; y++) {

            const timeSched = this.colUpdateArray[x].time[y]
            const [startTime, endTime] = timeSched.split('-');
            const fromDisplay = startTime.replace(/(P|A)M$/, ' $1M');
            const toDisplay = endTime.replace(/(P|A)M$/, '$1M');
            const fromValue = this.convertTo24HourFormat(fromDisplay)
            const toValue = this.convertTo24HourFormat(toDisplay.trim())

            this.displayArr.push({
              date: this.colUpdateArray[x].displayDate,
              from: fromValue,
              to: toValue,
              fromDisplayValue: fromDisplay,
              toDisplayValue: toDisplay,
              codeNo: this.colUpdateArray[x].codeNo[y],
              hrmins: this.calculateHoursAndMinutes2(fromValue, toValue)
            });
          }
        }
        // console.log(this.displayArr);

      } else {

        for (var x = 0; x < this.scheduleChecker.length; x++) {
          this.displayArr.push({
            date: this.scheduleChecker[x].date,
            from: this.scheduleChecker[x].timeIn,
            to: this.scheduleChecker[x].timeOut,
            fromDisplayValue: this.scheduleChecker[x].dispalyTimeIn,
            toDisplayValue: this.scheduleChecker[x].dispalyTimeOut,
            schoolYear: this.global.sy.slice(0, 6),
            codeNo: this.scheduleChecker[x].codeNo,
            hrmins: this.calculateHoursAndMinutes2(
              this.scheduleChecker[x].timeIn,
              this.scheduleChecker[x].timeOut),
            day: this.scheduleChecker[x].day
          });

          this.tempDisplayArray.push({
            date: this.scheduleChecker[x].date,
            from: this.scheduleChecker[x].timeIn,
            to: this.scheduleChecker[x].timeOut,
            fromDisplayValue: this.scheduleChecker[x].dispalyTimeIn,
            toDisplayValue: this.scheduleChecker[x].dispalyTimeOut,
            schoolYear: this.global.sy.slice(0, 6),
            codeNo: this.scheduleChecker[x].codeNo,
            hrmins: this.calculateHoursAndMinutes2(
              this.scheduleChecker[x].timeIn,
              this.scheduleChecker[x].timeOut),
            day: this.scheduleChecker[x].day
          })

        }
      }

    }

    if (this.selectedDates.length >= 3 && this.data.typeOfLeave == 0) {
      //   const totalScheduleHours = this.calculateTotalScheduledLeaveHours(this.scheduleChecker);
      //   // console.log("Total Hours:", totalScheduleHours);
      //   if (totalleaveHours > totalScheduleHours) {
      // }
      this.global.swalAlert("Warning!", "You are required to submit a leave request letter for a leave duration of three consecutive days or longer.", "warning");

    }

    this.getDayArray()
    this.sortTimeSched()

  }

  clear() {
    // console.log(this.tempDisplayArray);

    if (this.facultyType == 2) {
      this.facultyCodeTimeArray = []
      this.displayArr = []
      this.day = ''
      this.timeAndCode = ''
      // console.log(this.dayArray, this.tempfacultyCodeTimeArray);

    }

    if (this.facultyType == 3)
      // console.log('here');
      this.besFacultyCodeTimeArray = []
    this.displayArr = []
    this.besDay = ''
    this.timeAndSubject = ''
    this.timeArrayValue = ''
  }

  checkSchedule() {
    // console.log(this.leavetoUpdate);
    const dialogRef = this.dialog.open(CheckScheduleComponent, {
      width: '1200px',
      disableClose: false,
      autoFocus: false,
      data: {
        lType: this.leaveCategory,
        fType: this.facultyLeaveType
      }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  getDayArray() {
    for (var x = 0; x < this.displayArr.length; x++) {
      if (this.facultyType == 2) {
        this.dayArray.push({ day: this.displayArr[x].day })
        const uniqueDays = Array.from(new Set(this.dayArray.map(obj => obj.day))).map(day => ({ "day": day }));
        this.dayArray = uniqueDays
      }
      if (this.facultyType == 3) {
        this.besDayArray.push({ day: this.displayArr[x].day })
        const uniqueDays = Array.from(new Set(this.besDayArray.map(obj => obj.day))).map(day => ({ "day": day }));
        this.besDayArray = uniqueDays
      }
    }

    if (this.facultyType == 2) {
      if (this.selectedDates.length == 1) {
        let day = '';
        switch (this.selectedDates[0].getDay()) {
          case 0:
            day = 'Sunday';
            break;
          case 1:
            day = 'Monday';
            break;
          case 2:
            day = 'Tuesday';
            break;
          case 3:
            day = 'Wednesday';
            break;
          case 4:
            day = 'Thursday';
            break;
          case 5:
            day = 'Friday';
            break;
          case 6:
            day = 'Saturday';
            break;
          default:
            // Handle unexpected cases here
            break;
        }
        this.day = day
        // console.log(day); // Log the day for debugging purposes

        if (day !== '') {
          return this.getTimeAndCodeByDay(this.day, 1);
        }
      }

    }

    if (this.facultyType == 3) {
      if (this.selectedDates.length == 1) {
        let day = '';
        switch (this.selectedDates[0].getDay()) {
          case 0:
            day = 'Sunday';
            break;
          case 1:
            day = 'Monday';
            break;
          case 2:
            day = 'Tuesday';
            break;
          case 3:
            day = 'Wednesday';
            break;
          case 4:
            day = 'Thursday';
            break;
          case 5:
            day = 'Friday';
            break;
          case 6:
            day = 'Saturday';
            break;
          default:
            // Handle unexpected cases here
            break;
        }
        this.besDay = day
        // console.log(day); // Log the day for debugging purposes

        if (day !== '') {
          return this.getTimeAndSubjectByDay(this.besDay, 1);
        }
      }

    }

  }

  delete(date, from, to) {
    // console.log(this.displayArr);

    if (this.facultyType == 3) {
      for (var x = 0; x < this.displayArr.length; x++) {
        if (date == this.displayArr[x].date && from == this.displayArr[x].from && to == this.displayArr[x].to) {
            var removedTime = this.displayArr[x].fromDisplayValue.includes('AM') ? 'AM' : 'PM';
            var removedDate = this.displayArr[x].date;

            // Remove all entries with the same date and same time period (AM or PM)
            for (var i = this.displayArr.length - 1; i >= 0; i--) {
                if (this.displayArr[i].date === removedDate && this.displayArr[i].fromDisplayValue.includes(removedTime)) {
                    this.displayArr.splice(i, 1);
                }
            }
            this.sortCodeTimeArr(this.besTempfacultyCodeTimeArray);
            break; // Exit the loop after removing the items
        }
    }


      this.sortBesDayArray()

      // console.log(this.besTempfacultyCodeTimeArray);

    } else if (this.facultyType == 2) {
      for (var x = 0; x < this.displayArr.length; x++) {
        if (date == this.displayArr[x].date && from == this.displayArr[x].fromDisplayValue && to == this.displayArr[x].toDisplayValue) {

          this.displayArr.splice(x, 1);
          this.sortCodeTimeArr(this.tempfacultyCodeTimeArray)
          break; // Exit the loop after removing the item
        }
      }

      this.sortDayArray()
    }
  }

  timeChecker(value, num) {
    // console.log(value, num);

    if (this.facultyType == 3) {
      if (num == 1) {
        this.fromAmValue = this.convertTo24HourFormat(this.fromAmValue)
      }

      if (num == 2) {
        this.toAmValue = this.convertTo24HourFormat(this.toAmValue)
      }

      if (this.fromAmValue && this.toAmValue) {

        for (var x = 0; x < this.scheduleChecker.length; x++) {

          if (num === 1 && this.fromAmValue >= this.scheduleChecker[x].amTimeIn && this.fromAmValue <= this.scheduleChecker[x].amTimeOut) {
            this.convertTime(value, num);
          }

          if (num === 2 && this.toAmValue >= this.scheduleChecker[x].amTimeIn && this.toAmValue <= this.scheduleChecker[x].amTimeOut) {
            this.convertTime(value, num);
          }

        }
      }

    } else if (this.facultyType == 2) {
      if (num == 1) {
        this.fromValue = this.convertTo24HourFormat(this.fromValue)
      }

      if (num == 2) {
        this.toValue = this.convertTo24HourFormat(this.toValue)
      }

      // console.log(this.fromValue, this.toValue);
      if ((this.fromValue && this.toValue != '') && (this.fromValue <= this.toValue)) {
        for (var x = 0; x < this.scheduleChecker.length; x++) {
          // console.log(this.scheduleChecker[x].timeIn, this.scheduleChecker[x].timeOut,)
          // console.log('From Value', this.fromValue, 'toValue', this.toValue);
          // console.log((this.fromValue >= this.scheduleChecker[x].timeIn && this.fromValue <= this.scheduleChecker[x].timeOut), (this.toValue >= this.scheduleChecker[x].timeIn && this.toValue <= this.scheduleChecker[x].timeOut));

          if ((this.fromValue >= this.scheduleChecker[x].timeIn && this.fromValue <= this.scheduleChecker[x].timeOut) &&
            (this.toValue >= this.scheduleChecker[x].timeIn && this.toValue <= this.scheduleChecker[x].timeOut)) {

            // console.log('Success');
          }
        }
      }
    }
  }

  calculateDates() {
    // console.log(this.displayArr);
    this.dupTimeChker = false
    let checker: boolean = false
    if (this.facultyType == 3) {

      // console.log(this.fromAmValue, this.toAmValue);

      for (var x = 0; x < this.scheduleChecker.length; x++) {
        if ((this.fromAmValue >= this.scheduleChecker[x].timeIn && this.fromAmValue <= this.scheduleChecker[x].timeOut) &&
          (this.toAmValue >= this.scheduleChecker[x].timeIn && this.toAmValue <= this.scheduleChecker[x].timeOut)) {
          checker = true
        }
      }


      for (var x = 0; x < this.selectedDates.length; x++) {
        // Check if the date already exists in the scheduleChecker array
        const isDateDuplicate = this.displayArr.some(schedule => {
          return schedule.date === this.dateFormat(this.selectedDates[x]);

        });

        for (var x = 0; x < this.displayArr.length; x++) {
          if ((this.fromAmValue >= this.displayArr[x].from && this.fromAmValue <= this.displayArr[x].to) &&
            (this.toAmValue >= this.displayArr[x].from && this.toAmValue <= this.displayArr[x].to)) {
            this.dupTimeChker = true
          }
        }


        if (isDateDuplicate && this.dupTimeChker == true) {

          if (this.fromAmValue > this.toAmValue) {
            this.global.swalAlert("Invalid Input!", "The start time cannot be later than the end time.\nPlease make sure the start date comes before the end date.", "error");

          } else {
            this.global.swalAlert(
              "Duplicate Date and Time!",
              "The date and time you selected already exists in the leave schedule. Please choose a different date.",
              "error"
            );
          }


        } else {

          if (this.fromAmValue && this.toAmValue != '') {
            if (this.fromAmValue < this.toAmValue) {
              if (this.hourChecker(this.fromAmValue, this.toAmValue)) {

                for (var x = 0; x < this.scheduleChecker.length; x++) {

                  if ((this.fromAmValue >= this.scheduleChecker[x].timeIn && this.fromAmValue <= this.scheduleChecker[x].timeOut) &&
                    (this.toAmValue >= this.scheduleChecker[x].timeIn && this.toAmValue <= this.scheduleChecker[x].timeOut)) {

                    this.convertTime(this.fromAmValue, 1)
                    this.convertTime(this.toAmValue, 2)

                    this.displayArr.push({
                      date: this.dateFormat(this.scheduleChecker[x].date), fromDisplayValue: this.fromAmPassValue, toDisplayValue: this.toAmPassValue,
                      from: this.fromAmValue, to: this.toAmValue, hrmins: this.calculateHoursAndMinutes2(this.fromAmValue, this.toAmValue),
                      section: this.scheduleChecker[x].section, subject: this.scheduleChecker[x].subject
                    });

                  }
                  else {
                    if (checker == false) {
                      this.global.swalAlert("Invalid Input!", "The selected time is not within the valid time schedule.", "error")
                    }
                  }
                }
              } else {
                this.global.swalAlert("Invalid Time!", "There should be at least 1 hour gap between the specified time, and minutes should be 00 or 30.", "error");
              }
            } else {
              this.global.swalAlert("Invalid Input!", "The start time cannot be later than or equal to the end time.\nPlease make sure the start date comes before the end date.", "error");
            }
          } else {
            this.global.swalAlert("Invalid Input!", "Please make sure to enter both the start time and end time.", "error");
          }
        }
      }





    } else if (this.facultyType == 2) {
      if (this.selectedDates.length >= 1) {

        for (var x = 0; x < this.scheduleChecker.length; x++) {
          if ((this.fromValue >= this.scheduleChecker[x].timeIn && this.fromValue <= this.scheduleChecker[x].timeOut) &&
            (this.toValue >= this.scheduleChecker[x].timeIn && this.toValue <= this.scheduleChecker[x].timeOut)) {
            checker = true
          }
        }

        for (var x = 0; x < this.selectedDates.length; x++) {
          // Check if the date already exists in the scheduleChecker array
          const isDateDuplicate = this.displayArr.some(schedule => {
            return schedule.date === this.dateFormat(this.selectedDates[x]);

          });

          for (var x = 0; x < this.displayArr.length; x++) {
            if ((this.fromValue >= this.displayArr[x].from && this.fromValue <= this.displayArr[x].to) &&
              (this.toValue >= this.displayArr[x].from && this.toValue <= this.displayArr[x].to)) {
              this.dupTimeChker = true
            }
          }


          if (isDateDuplicate && this.dupTimeChker == true) {

            if (this.fromValue > this.toValue) {
              this.global.swalAlert("Invalid Input!", "The start time cannot be later than the end time.\nPlease make sure the start date comes before the end date.", "error");

            } else {
              this.global.swalAlert(
                "Duplicate Date and Time!",
                "The date and time you selected already exists in the leave schedule. Please choose a different date.",
                "error"
              );
            }


          } else {

            if (this.fromValue && this.toValue != '') {
              if (this.fromValue < this.toValue) {
                if (this.hourChecker(this.fromValue, this.toValue)) {

                  for (var x = 0; x < this.scheduleChecker.length; x++) {

                    if ((this.fromValue >= this.scheduleChecker[x].timeIn && this.fromValue <= this.scheduleChecker[x].timeOut) &&
                      (this.toValue >= this.scheduleChecker[x].timeIn && this.toValue <= this.scheduleChecker[x].timeOut)) {

                      this.convertTime(this.fromValue, 1)
                      this.convertTime(this.toValue, 2)

                      this.displayArr.push({
                        date: this.dateFormat(this.scheduleChecker[x].date), fromDisplayValue: this.fromValueNTime, toDisplayValue: this.toValueNTime,
                        from: this.fromValue, to: this.toValue, hrmins: this.calculateHoursAndMinutes2(this.fromValue, this.toValue), codeNo: this.scheduleChecker[x].codeNo
                      });

                    }
                    else {
                      if (checker == false) {
                        this.global.swalAlert("Invalid Input!", "The selected time is not within the valid time schedule.", "error")
                      }
                    }
                  }
                } else {
                  this.global.swalAlert("Invalid Time!", "There should be at least 1 hour gap between the specified time, and minutes should be 00 or 30.", "error");
                }
              } else {
                this.global.swalAlert("Invalid Input!", "The start time cannot be later than or equal to the end time.\nPlease make sure the start date comes before the end date.", "error");
              }
            } else {
              this.global.swalAlert("Invalid Input!", "Please make sure to enter both the start time and end time.", "error");
            }
          }
        }

      }

    }

    this.sortTimeSched()
  }

  addTimeAndCode(value) {
    var spliceTrigger = true;
    var valueAlreadySelected = false;

    for (let x = 0; x < this.facultyCodeTimeArray.length; x++) {
      const facultyTime = this.facultyCodeTimeArray[x];
      spliceTrigger = true;
      if (value.from === facultyTime.from && value.to === facultyTime.to) {
        for (let y = 0; y < this.selectedDates.length; y++) {
          const selectedDate = this.selectedDates[y];
          const dateString = selectedDate.toDateString();
          const firstThreeCharacters = dateString.substring(0, 3).toLowerCase();
          if (value.day.substring(0, 3).toLowerCase() === firstThreeCharacters) {
            // Check if the value is already in the displayArr
            if (this.isFacValueAlreadySelected(facultyTime, this.dateFormat(selectedDate))) {
              valueAlreadySelected = true;
              break;
            } else {
              this.displayArr.push({
                date: this.dateFormat(selectedDate),
                fromDisplayValue: facultyTime.fromDisplayValue,
                toDisplayValue: facultyTime.toDisplayValue,
                from: facultyTime.from,
                to: facultyTime.to,
                hrmins: this.calculateHoursAndMinutes2(facultyTime.from, facultyTime.to),
                codeNo: facultyTime.codeNo,
                day: facultyTime.day,
                schoolYear: facultyTime.schoolYear,
              });
              if (spliceTrigger) {
                // this.facultyCodeTimeArray.splice(x, 1);
                spliceTrigger = false;
              }
            }
          }
        }
      }
    }

    if (valueAlreadySelected) {
      // Prompt the message indicating that the value is already selected
      this.global.swalAlert("Invalid Input!", "The selected Time and Code is already in the leave schedule.", "error");
      this.timeAndCode = ''
    }

    this.sortTimeSched();
    this.sortDayArray();
  }

  isFacValueAlreadySelected(valueToCheck, date) {
    for (let i = 0; i < this.displayArr.length; i++) {
      const displayValue = this.displayArr[i];
      if (
        displayValue.date === date &&
        displayValue.from === valueToCheck.from &&
        displayValue.to === valueToCheck.to &&
        displayValue.day === valueToCheck.day
      ) {
        return true;
      }
    }
    return false;
  }

  getTimeAndCodeByDay(value, no) {

    this.facultyCodeTimeArray = []

    if (no == 1) { this.timeAndCode = '' }
    for (var x = 0; x < this.tempDisplayArray.length; x++) {
      if (value.substring(0, 3).toLowerCase() == this.tempDisplayArray[x].day.substring(0, 3).toLowerCase()) {
        const facTime = this.tempDisplayArray[x]
        this.facultyCodeTimeArray.push({
          fromDisplayValue: facTime.fromDisplayValue,
          toDisplayValue: facTime.toDisplayValue,
          from: facTime.from,
          to: facTime.to,
          hrmins: this.calculateHoursAndMinutes2(facTime.from, facTime.to),
          codeNo: facTime.codeNo,
          day: facTime.day,
          schoolYear: facTime.schoolYear,
        })

      }
    }

    const uniqueEntries = [];
    const uniqueKeys = new Set();

    this.facultyCodeTimeArray.forEach(entry => {
      const key = `${entry.fromDisplayValue}-${entry.toDisplayValue}-${entry.subject}-${entry.day}-${entry.section}`;
      if (!uniqueKeys.has(key)) {
        uniqueKeys.add(key);
        uniqueEntries.push(entry);
      }
    });

    // console.log(uniqueEntries);
    this.facultyCodeTimeArray = uniqueEntries

    this.facultyCodeTimeArray.sort((a, b) => {
      // Extract time components from "from" property
      const [hoursA, minutesA] = a.from.split(':').map(Number);
      const [hoursB, minutesB] = b.from.split(':').map(Number);

      // Convert time components to minutes
      const totalMinutesA = hoursA * 60 + minutesA;
      const totalMinutesB = hoursB * 60 + minutesB;

      // Compare the total minutes
      return totalMinutesA - totalMinutesB;
    });



  }

  addTimeAndSubject(value) {
    var spliceTrigger = true;
    var valueAlreadySelected = false;

    for (let x = 0; x < this.besFacultyCodeTimeArray.length; x++) {
      const facultyTime = this.besFacultyCodeTimeArray[x];
      spliceTrigger = true;
      if (value.from === facultyTime.from && value.to === facultyTime.to) {
        for (let y = 0; y < this.selectedDates.length; y++) {
          const selectedDate = this.selectedDates[y];
          const dateString = selectedDate.toDateString();
          const firstThreeCharacters = dateString.substring(0, 3).toLowerCase();
          if (value.day.substring(0, 3).toLowerCase() === firstThreeCharacters) {
            // Check if the value is already in the displayArr
            if (this.isValueAlreadySelected(facultyTime, this.dateFormat(selectedDate))) {
              valueAlreadySelected = true;
              break;
            } else {
              this.displayArr.push({
                date: this.dateFormat(selectedDate),
                fromDisplayValue: facultyTime.fromDisplayValue,
                toDisplayValue: facultyTime.toDisplayValue,
                from: facultyTime.from,
                to: facultyTime.to,
                hrmins: this.calculateHoursAndMinutes2(facultyTime.from, facultyTime.to),
                subject: facultyTime.subject,
                day: facultyTime.day,
                section: facultyTime.section,
              });
              // secondaryDatePush(this.selectedDates,facultyTime)
              if (spliceTrigger) {
                // this.besFacultyCodeTimeArray.splice(x, 1);
                spliceTrigger = false;
              }
            }
          }
        }
      }
    }

    if (valueAlreadySelected) {
      // Prompt the message indicating that the value is already selected
      this.global.swalAlert("Invalid Input!", "The selected Time and Subject is already in the leave schedule.", "error");
      this.timeAndSubject = ''
      // console.log(this.displayArr);
    }

    this.sortTimeSched();
    this.sortBesDayArray();
  }

  isValueAlreadySelected(valueToCheck, date) {
    // console.log(date);

    for (let i = 0; i < this.displayArr.length; i++) {
      const displayValue = this.displayArr[i];
      // console.log();

      if (
        this.displayArr[i].date === date &&
        displayValue.from === valueToCheck.from &&
        displayValue.to === valueToCheck.to &&
        displayValue.day === valueToCheck.day
      ) {
        return true;
      }
    }
    return false;
  }

  getTimeAndSubjectByDay(value, no) {
    // console.log(this.tempDisplayArray);

    this.besFacultyCodeTimeArray = []
    this.tempDisplayArray

    if (no == 1) { this.timeAndSubject = '' }
    for (var x = 0; x < this.tempDisplayArray.length; x++) {
      if (value.substring(0, 3).toLowerCase() == this.tempDisplayArray[x].day.substring(0, 3).toLowerCase()) {
        const besTime = this.tempDisplayArray[x]
        this.besFacultyCodeTimeArray.push({
          fromDisplayValue: besTime.fromDisplayValue,
          toDisplayValue: besTime.toDisplayValue,
          from: besTime.from,
          to: besTime.to,
          hrmins: this.calculateHoursAndMinutes2(besTime.from, besTime.to),
          subject: besTime.subject,
          day: besTime.day,
          section: besTime.section,
        })

      }
    }

    const uniqueEntries = [];
    const uniqueKeys = new Set();

    this.besFacultyCodeTimeArray.forEach(entry => {
      const key = `${entry.fromDisplayValue}-${entry.toDisplayValue}-${entry.subject}-${entry.day}-${entry.section}`;
      if (!uniqueKeys.has(key)) {
        uniqueKeys.add(key);
        uniqueEntries.push(entry);
      }
    });

    this.besFacultyCodeTimeArray = uniqueEntries

  }

  getTimeOfTheDay(value) {
    this.displayArr = []
    this.besDay = ''
    this.besFacultyCodeTimeArray = []
    if (value == 1) {

      this.displayArr = this.tempDisplayArray
    }

    if (value == 2) {

      for (var x = 0; x < this.tempDisplayArray.length; x++) {
        var fromTime = this.tempDisplayArray[x].from;
        var toTime = this.tempDisplayArray[x].to;
        if (fromTime.toString() <= "12:00" && toTime.toString() <= "12:00") {
          this.displayArr.push(this.tempDisplayArray[x]);
        }
      }

    }

    if (value == 3) {
      // console.log('here');

      for (var x = 0; x < this.tempDisplayArray.length; x++) {
        var fromTime = this.tempDisplayArray[x].from;
        var toTime = this.tempDisplayArray[x].to;
        // console.log(fromTime.toString() >= "13:00", toTime.toString() >= "13:00");

        if (fromTime.toString() >= "13:00" && toTime.toString() >= "13:00") {
          this.displayArr.push(this.tempDisplayArray[x]);
        }
      }

    }
  }

  onNoClickclose(): void {
    this.dialogRef.close({ result: 'cancel', data: this.cFacultyDisplay });
  }

  save() {
    // console.log(this.facultyType == 3)

    const consolidatedDisplay = {};
    let consolidatedArray = {}

    if (this.facultyType == 3) {

      for (var x = 0; x < this.displayArr.length; x++) {
        // console.log(this.displayArr)
        const date = new Date(this.displayArr[x].date)

        this.cFacultyDisplay.push({
          displayDate: this.displayArr[x].date,
          time: this.convertTime(this.displayArr[x].from, 1) + ' - ' + this.convertTime(this.displayArr[x].to, 2),
          hrs: this.displayArr[x].hrmins,
          fromDateTime: this.adjustTimeInDateTime(date, this.displayArr[x].from),
          toDateTime: this.adjustTimeInDateTime(date, this.displayArr[x].to),
          section: this.displayArr[x].section,
          subject: this.displayArr[x].subject,
          dateToBeUpdated: this.timeToBeUpdated
        })

      }

      for (let x = 0; x < this.cFacultyDisplay.length; x++) {
        const date = new Date(this.cFacultyDisplay[x].displayDate);
        const displayDate = this.cFacultyDisplay[x].displayDate;

        if (consolidatedDisplay[displayDate]) {
          consolidatedDisplay[displayDate].time.push(this.cFacultyDisplay[x].time);
          consolidatedDisplay[displayDate].fromDateTime.push(this.cFacultyDisplay[x].fromDateTime);
          consolidatedDisplay[displayDate].toDateTime.push(this.cFacultyDisplay[x].toDateTime);
          consolidatedDisplay[displayDate].hrs.push(this.cFacultyDisplay[x].hrs);
          consolidatedDisplay[displayDate].section.push(this.cFacultyDisplay[x].section);
          consolidatedDisplay[displayDate].subject.push(this.cFacultyDisplay[x].subject);
        } else {
          consolidatedDisplay[displayDate] = {
            displayDate: displayDate,
            time: [this.cFacultyDisplay[x].time],
            fromDateTime: [this.cFacultyDisplay[x].fromDateTime],
            toDateTime: [this.cFacultyDisplay[x].toDateTime],
            hrs: [this.cFacultyDisplay[x].hrs],
            section: [this.cFacultyDisplay[x].section], // Add codeNo
            subject: [this.cFacultyDisplay[x].subject], // Add schoolYear
            dateToBeUpdated: this.cFacultyDisplay[x].dateToBeUpdated
          };
        }

        // Convert consolidatedDisplay to an array
        consolidatedArray = Object.values(consolidatedDisplay);

      }
      // console.log('passDisplayArr', consolidatedArray)
      this.dialogRef.close({ result: 'success', data: consolidatedArray, fType: this.facultyType });

    } else if (this.facultyType == 2) {
      for (var x = 0; x < this.displayArr.length; x++) {
        // console.log(this.displayArr)
        const date = new Date(this.displayArr[x].date)

        this.cFacultyDisplay.push({
          displayDate: this.displayArr[x].date,
          time: this.convertTime(this.displayArr[x].from, 1) + ' - ' + this.convertTime(this.displayArr[x].to, 2),
          hrs: this.displayArr[x].hrmins,
          fromDateTime: this.adjustTimeInDateTime(date, this.displayArr[x].from),
          toDateTime: this.adjustTimeInDateTime(date, this.displayArr[x].to),
          codeNo: this.displayArr[x].codeNo,
          schoolYear: this.global.sy.slice(0, 6),
          dateToBeUpdated: this.timeToBeUpdated
        })

      }

      for (let x = 0; x < this.cFacultyDisplay.length; x++) {
        const date = new Date(this.cFacultyDisplay[x].displayDate);
        const displayDate = this.cFacultyDisplay[x].displayDate;

        if (consolidatedDisplay[displayDate]) {
          consolidatedDisplay[displayDate].time.push(this.cFacultyDisplay[x].time);
          consolidatedDisplay[displayDate].fromDateTime.push(this.cFacultyDisplay[x].fromDateTime);
          consolidatedDisplay[displayDate].toDateTime.push(this.cFacultyDisplay[x].toDateTime);
          consolidatedDisplay[displayDate].hrs.push(this.cFacultyDisplay[x].hrs);
          consolidatedDisplay[displayDate].codeNo.push(this.cFacultyDisplay[x].codeNo); // Add codeNo
          // consolidatedDisplay[displayDate].schoolYear.push(this.cFacultyDisplay[x].schoolYear); // Add schoolYear

        } else {
          consolidatedDisplay[displayDate] = {
            displayDate: displayDate,
            time: [this.cFacultyDisplay[x].time],
            fromDateTime: [this.cFacultyDisplay[x].fromDateTime],
            toDateTime: [this.cFacultyDisplay[x].toDateTime],
            hrs: [this.cFacultyDisplay[x].hrs],
            codeNo: [this.cFacultyDisplay[x].codeNo], // Add codeNo
            schoolYear: this.cFacultyDisplay[x].schoolYear, // Add schoolYear
            dateToBeUpdated: this.cFacultyDisplay[x].dateToBeUpdated
          };
        }

        // Convert consolidatedDisplay to an array
        consolidatedArray = Object.values(consolidatedDisplay);

      }
      // console.log('passDisplayArr', consolidatedArray)
      this.dialogRef.close({ result: 'success', data: consolidatedArray, fType: this.facultyType });

    }
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './home/home.component';
import { AssessmentComponent } from './pages/assessment/assessment.component';

import { ExamScheduleComponent } from './pages/exam-schedule/exam-schedule.component';
import { ClassScheduleComponent } from './pages/class-schedule/class-schedule.component';
import { GradesComponent } from './pages/grades/grades.component';
import { PersonalDataComponent } from './pages/personal-data/personal-data.component';
import { OpenCodesComponent } from './pages/open-codes/open-codes.component';
import { EnrollmentComponent } from './pages/enrollment/enrollment.component';
import { ChecklistComponent } from './pages/checklist/checklist.component';

import { OtherInfoComponent } from './pages/other-info/other-info.component';
import { EmployeeRecordsComponent } from './pages/employee-records/employee-records.component';
import { DailyTimeRecordsComponent } from './pages/daily-time-records/daily-time-records.component';
import { LeavesComponent } from './pages/leaves/leaves.component';
import { GenerateCvComponent } from './pages/generate-cv/generate-cv.component';
import { PersonalDataEmpComponent } from './pages/personal-data-emp/personal-data-emp.component';
import { ClasslistComponent } from './pages/classlist/classlist.component';
import { BasicEdClasslistComponent } from './pages/classlist/basic-ed-classlist/basic-ed-classlist.component';


import { ShortTermEnrollmentComponent } from './pages/short-term-enrollment/short-term-enrollment.component';
import { GsEnrollmentComponent } from './pages/gs-enrollment/gs-enrollment.component';
import { LmsAccessCodesComponent } from './pages/lms-access-codes/lms-access-codes.component';

import { HsEnrollmentComponent } from './pages/hs-enrollment/hs-enrollment.component';
import { HsElectiveComponent } from './pages/hs-elective/hs-elective.component';
import { ModalityComponent } from './pages/modality/modality.component';

import { HsClassScheduleComponent } from './pages/hs-class-schedule/hs-class-schedule.component';
import { BasicEdClassScheduleComponent } from './pages/basic-ed-class-schedule/basic-ed-class-schedule.component';
import { EGSComponent } from './pages/egs/egs.component';
import { PayslipComponent } from './pages/payslip/payslip.component';

import { PreEnrollmentComponent } from './pages/pre-enrollment/pre-enrollment.component';
import { ElemClassScheduleComponent } from './pages/elem-class-schedule/elem-class-schedule.component';

import { FacultyEvaluationComponent } from './pages/faculty-evaluation/faculty-evaluation.component';
import { LeaveManagementComponent } from './popup/leave-management/leave-management.component';
import { EditLeaveDurationsComponent } from './popup/leave-management/edit-leave-durations/edit-leave-durations.component';
import { AuthGuardGuard } from './auth-guard/auth-guard.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'main',
    component: MainComponent,
    children: [
      { path: 'Elem-Class-Schedule', component: ElemClassScheduleComponent, outlet: 'div' },
      { path: 'Pre-Enrollment', component: PreEnrollmentComponent, outlet: 'div' },
      { path: 'Payslip', component: PayslipComponent, outlet: 'div' },
      { path: 'Electronic-Grading-Sheet', component: EGSComponent, outlet: 'div' },
      { path: 'HS-class-Schedules', component: HsClassScheduleComponent, outlet: 'div' },
      { path: 'Modality', component: ModalityComponent, outlet: 'div' },
      { path: 'Elective', component: HsElectiveComponent, outlet: 'div' },
      { path: 'high-school-enrollment', component: HsEnrollmentComponent, outlet: 'div' },
      { path: 'LMS-access-codes', component: LmsAccessCodesComponent, outlet: 'div' },
      { path: 'graduate-school-enrollment', component: GsEnrollmentComponent, outlet: 'div' },
      { path: 'short-term-enrollment', component: ShortTermEnrollmentComponent, outlet: 'div' },
      { path: 'home', component: HomeComponent, outlet: 'div' },
      { path: 'assessment', component: AssessmentComponent, outlet: 'div' },
      { path: 'exam-schedule', component: ExamScheduleComponent, outlet: 'div' },
      { path: 'class-schedule', component: ClassScheduleComponent, outlet: 'div' },
      { path: 'grades', component: GradesComponent, outlet: 'div' },
      { path: 'personal-data', component: PersonalDataComponent, outlet: 'div' },
      { path: 'open-codes', component: OpenCodesComponent, outlet: 'div' },
      { path: 'enrollment', component: EnrollmentComponent, outlet: 'div' },
      { path: 'checklist', component: ChecklistComponent, outlet: 'div' },
      { path: 'personal-data-emp', component: PersonalDataEmpComponent, outlet: 'div' },
      { path: 'other-info', component: OtherInfoComponent, outlet: 'div' },
      { path: 'emp-records', component: EmployeeRecordsComponent, outlet: 'div' },
      { path: 'dtr', component: DailyTimeRecordsComponent, outlet: 'div' },
      { path: 'leaves', component: LeavesComponent, outlet: 'div' },
      { path: 'generate-cv', component: GenerateCvComponent, outlet: 'div' },
      { path: 'class-list', component: ClasslistComponent, outlet: 'div' },
      { path: 'basic-ed-class-list', component: BasicEdClasslistComponent, outlet: 'div' },
      { path: 'basic-ed-class-schedule', component: BasicEdClassScheduleComponent, outlet: 'div' },
      { path: 'faculty-evaluation', component: FacultyEvaluationComponent, outlet: 'div' },
      { path: 'leave-management-component', component: LeaveManagementComponent, outlet: 'div' },
      { path: 'edit-leavedurations-component', component: EditLeaveDurationsComponent, outlet: 'div' },
    ]
  },
  {
    path: '**', component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }

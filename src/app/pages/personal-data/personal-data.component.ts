import { Component, OnInit } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.scss']
})
export class PersonalDataComponent implements OnInit {
familyarray=[]
educarray=[]
  constructor(public global: GlobalService,private http: Http) { }

  ngOnInit() {

  }

  displaycs(x){
  	if (x=='S') {
  		return 'Single'
  	}
  	if (x=='M') {
  		return 'Married'
  	}
  	if (x=='W') {
  		return 'Widow'
  	}
  }
  displayg(x){
  	if (x=='M') {
  		return 'Male'
  	}
  	if (x=='F') {
  		return 'Female'
  	}
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsElectiveComponent } from './hs-elective.component';

describe('HsElectiveComponent', () => {
  let component: HsElectiveComponent;
  let fixture: ComponentFixture<HsElectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsElectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsElectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

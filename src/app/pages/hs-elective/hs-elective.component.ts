import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { StudentApiService } from './../../student-api.service';
import Swal from 'sweetalert2';
const swal = Swal;

@Component({
  selector: 'app-hs-elective',
  templateUrl: './hs-elective.component.html',
  styleUrls: ['./hs-elective.component.scss']
})
export class HsElectiveComponent implements OnInit {
  array=[]
  selected=undefined

  exist=false
  arrayexist=[]
  displaySY = "";
  constructor(public global: GlobalService,public api:StudentApiService) { }

  ngOnInit() {
    if(this.global.enrollmentsy == "none")
      this.displaySY = this.global.sy
    else
      this.displaySY = this.global.enrollmentsy

    // this.http.delete(this.global.api+'StudentPortal/HSEnrollment/Student/Elective/'+this.global.requestid()+"/2/"+this.global.sy,this.global.option)
    //           .map(response => response.json())
    //           .subscribe(res => {});
     this.loaddata()
  }
checkdis(id){
  for (var i = 0; i < this.array.length; ++i) {
    if ( this.array[i].available<=0&&this.array[i].id.toString()==id.toString()) {
      return true
    }
  }
   return false
}
  loaddata(){
    this.array=undefined
      // var sy = this.global.sy
      var sy = this.global.electivesy
      // if (this.global.studinfo.departmentCode=='ELEM'||this.global.studinfo.departmentCode=='HS') {
      //   sy = this.global.sy.substring(0, 6)
      // }
     this.api.apiHSEnrollmentStudentElective(sy)
        .subscribe(res => {
          if (res.data.length==0) {
            this.exist=false
          }else{
            this.exist=true
            this.arrayexist=res.data
          }

          this.api.apiHSEnrollmentElectives(sy)
            .subscribe(res => {
              this.array=res.data
            },Error => {
              this.array=[]
              this.global.swalAlertError(Error);
            });
        },Error => {
          this.global.swalAlertError(Error);
        });
  }
  checktemp=false
  check2(){
    if (!this.checktemp) {
      this.checktemp=true
      // var sy = this.global.sy
      var sy = this.global.electivesy
      // if (this.global.studinfo.departmentCode=='ELEM'||this.global.studinfo.departmentCode=='HS') {
      //   sy = this.global.sy.substring(0, 6)
      // }
      this.api.apiHSEnrollmentElectives(sy)
          .subscribe(res => {
            this.array=res.data
            if (!this.checkdis(this.selected)) {
             this.api.apiHSEnrollmentElectivesPost(this.selected,sy)
                .subscribe(res => {
                  if (res.message=='1') {
                    this.checktemp=false
                    this.loaddata()
                  }else{
                    this.global.swalAlert("Elective is full!",'','warning')
                    this.loaddata()
                    this.checktemp=false
                  }
                },Error=>{
                  this.global.swalAlertError(Error);
                  this.checktemp=false
                });
            }else{
              this.global.swalAlert("Elective is full!",'','warning')
            }

        },Error => {
          this.array=[]
          this.global.swalAlertError(Error);
          this.checktemp=false
        });

    }
  }

  check(){
    var text=''
    for (var i = 0; i < this.array.length; ++i) {
      if (this.selected==this.array[i].id) {
        text = this.array[i].name
      }
    }
      Swal({
      title: 'Are you sure?',
      html: "You have selected <b>"+text+"</b><br><br>"+"You may not be able to revert this.",
      type: 'warning',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Select this Elective',
      showCancelButton: true
    }).then((result) => {
      if (result.value) {
        this.check2();
      }
    })
}
}

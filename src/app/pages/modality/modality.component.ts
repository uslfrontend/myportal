import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import Swal from 'sweetalert2';
import { StudentApiService } from './../../student-api.service';

const swal = Swal;

@Component({
  selector: 'app-modality',
  templateUrl: './modality.component.html',
  styleUrls: ['./modality.component.scss']
})
export class ModalityComponent implements OnInit {
 array=[]
  selected=undefined
  exist=false
  arrayexist=[]
  constructor(public global: GlobalService,private api: StudentApiService) { }

  ngOnInit() {
    // this.http.delete(this.global.api+'StudentPortal/HSEnrollment/Student/Elective/'+this.global.requestid()+"/2/"+this.global.sy,this.global.option)
    //           .map(response => response.json())
    //           .subscribe(res => {});
     this.loaddata()
      // console.log(this.global.studinfo)

  }
  res;
  programLevel;
  stringer(a){
    this.res = "";
    this.res = a.split(';').join("\n\n");
    return this.res;
  }
  modalityTrigger = false;
  tempProgID = '';
  loaddata(){
    this.programLevel = "";
    this.array=undefined
    // console.log(this.global.sy);
    var sy=this.global.sy
      if (this.global.studinfo.departmentCode=='ELEM'||this.global.studinfo.departmentCode=='HS') {
        sy = this.global.sy.substring(0, 6)
      }
      this.api.apiStudentPortalCurriculum(this.global.studinfo.programID)
      .subscribe(res => {

        if(res.data.programLevel == '80' || res.data.programLevel == '90')
          this.programLevel = '80';
        else
        this.programLevel = res.data.programLevel;

        this.api.apiPreferredLearningModality(sy)
        .subscribe(res => {
          if (res.data.length==0) {
            this.exist=false
          }else{
            this.exist=true
            this.arrayexist=res.data
          }
          this.api.apiLearningModalities(sy)
              .subscribe(res => {
                this.array=res.data
                // console.log(this.array)
                if(res.data.length == 0)
                  this.modalityTrigger = false; // modality Close
                else
                  this.modalityTrigger = true; // modality Open
                // if (this.global.studinfo.departmentCode=='GS') {
                //   this.array=[]
                //   for (var i = 0; i < res.data.length; ++i) {
                //     if (res.data[i].id==1||res.data[i].id==2) {
                //      this.array.push(res.data[i])
                //     }
                //   }
                // }
                this.array=[]
                  for (var i = 0; i < res.data.length; ++i) {
                    if (res.data[i].programLevel == this.programLevel) {
                     this.array.push(res.data[i])
                    }
                  }
              },Error => {
                this.array=[]
                this.global.swalAlertError(Error);
              });
        },Error => {
          this.global.swalAlertError(Error);
        });
      },Error => {
        this.array=[]
        this.global.swalAlertError(Error);
      });

  }
  checktemp=false
  check2(){
    if (!this.checktemp) {
      this.checktemp=true
       var sy = this.global.sy
      if (this.global.studinfo.departmentCode=='ELEM'||this.global.studinfo.departmentCode=='HS') {
        if (this.global.sy.length==7) {
          sy = this.global.sy.slice(0, -1)
        }
      }
      this.api.apiLearningModalityPost(this.selected,sy)
        .subscribe(res => {
          this.checktemp=false
          this.loaddata()
        },Error=>{
          this.global.swalAlertError(Error);
          this.checktemp=false
        });
    }
  }

  check(){
    var text=''
    for (var i = 0; i < this.array.length; ++i) {
      if (this.selected==this.array[i].id) {
        text = this.array[i].name
      }
    }
      Swal({
      title: 'Are you sure?',
      html: "You have selected <b>"+text+"</b><br><br>"+"You may not be able to revert this.",
      type: 'warning',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Select this Modality',
      showCancelButton: true
    }).then((result) => {
      if (result.value) {
        this.check2();
      }
    })
}
}

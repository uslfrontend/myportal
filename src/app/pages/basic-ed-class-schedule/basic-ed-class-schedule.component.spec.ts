import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicEdClassScheduleComponent } from './basic-ed-class-schedule.component';

describe('BasicEdClassScheduleComponent', () => {
  let component: BasicEdClassScheduleComponent;
  let fixture: ComponentFixture<BasicEdClassScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicEdClassScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicEdClassScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

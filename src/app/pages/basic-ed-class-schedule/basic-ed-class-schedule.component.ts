import { Component, OnInit } from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { Inject} from '@angular/core';

@Component({
  selector: 'app-basic-ed-class-schedule',
  templateUrl: './basic-ed-class-schedule.component.html',
  styleUrls: ['./basic-ed-class-schedule.component.css']
})
export class BasicEdClassScheduleComponent implements OnInit {
  tableArr
  tableArr2=[]
  sy=''
  constructor(public global: GlobalService,private http: Http) { }

  ngOnInit() {
    this.start()
  }
  start() {

  	 var sy=this.global.sy
     this.sy = sy
      
        if (this.sy.length==7) {
          sy = this.sy.slice(0, -1)
          this.sy = this.sy.slice(0, -1)
        
      }
  	   if (this.global.empHSClassSchedule.length==0) {
  	    this.http.get(this.global.api+'EmployeePortal/BasicEdFaculty/Schedule/'+sy+'/'+this.global.requestid(),this.global.option)
            .map(response => response.json())
            .subscribe(res => {  
            	this.tableArr=[]
              //console.log(res.data)
            	if (res.data!=null) {
            		var temp=''
            		var tableArrtemp=0
    				  this.tableArr.push({
        				time:'',
        				m:'',
        				t:'',
        				w:'',
        				th:'',
        				f:'',
        			})
					
        			var x
        			for (var i = 0; i < res.data.length; ++i) {
        				if (res.data[i].time != ' ') {
        					x = i
        					break
        				}
        			}
        			this.tableArr[0] = {
        				time: res.data[x].time,
        				m:'',
        				t:'',
        				w:'',
        				th:'',
        				f:'',
        			}
        			for (var i = 0; i < res.data.length; ++i) {
            				if (res.data[i].time != ' ') {
            					var time = res.data[i].time
            					var check = false
	            				for (var i2 = 0; i2 < this.tableArr.length; ++i2) {
	            					if (this.tableArr[i2].time == res.data[i].time) {
	            						check = true
	            						break
	            					}
	            				}
	            				if (!check) {
	            					tableArrtemp++
	            					this.tableArr[tableArrtemp] = {
				        				time: res.data[i].time,
				        				m:'',
				        				t:'',
				        				w:'',
				        				th:'',
				        				f:'',
				        			}
	            				}
            				}
            			}

            			var start = 7
            			var temparr=[]
            			for (var i = 0; i < this.tableArr.length; ++i) {
            				var keyA = this.tableArr[i].time.split(":")
            				var keyb = keyA[1].split("-")
            				temparr.push({sort:parseFloat(keyA[0]+'.'+keyb[0]),index:i})
            			}
            			sortByKey(temparr, 'sort')
            			function sortByKey(array, key) {
  						    return array.sort(function(a, b) {
  						        var x = a[key]; var y = b[key];
  						        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
  						    });
						}

						var arr1 = []
						var arr2 = []

						for (var i = 0; i < temparr.length; ++i) {
							if (temparr[i].sort>=7) {
								arr1.push(temparr[i])
							}else{
								arr2.push(temparr[i])
							}
						}
						var arr3 = arr1.concat(arr2)
						//console.log(arr3)

						var arr4 = []

						for (var i = 0; i < arr3.length; ++i) {
							arr4.push(this.tableArr[arr3[i].index])
						}
						this.tableArr = arr4
            this.global.empHSClassSchedule=this.tableArr
            			for (var i3 = 0; i3 < this.tableArr.length; ++i3) {
	            			for (var i = 0; i < res.data.length; ++i) {
	            				if (this.tableArr[i3].time == res.data[i].time) {
            						/*if (res.data[i].adviser_Name==null) {
			        					  res.data[i].adviser_Name= ''
			        				  }else
			        					  res.data[i].adviser_Name="<hr>ADVISER:<br>"+res.data[i].adviser_Name

            					  if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('mon')) {
  				        				this.tableArr[i3].m =  "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+res.data[i].section +"</b>"+res.data[i].adviser_Name
  				        			}
  				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('tues')) {
  				        				this.tableArr[i3].t = "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+ res.data[i].section +"</b>"+res.data[i].adviser_Name
  				        			}
  				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('wed')) {
  				        				this.tableArr[i3].w =  "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+res.data[i].section +"</b>"+res.data[i].adviser_Name
  				        			}
  				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('thurs')){
  				        				this.tableArr[i3].th = "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+res.data[i].section +"</b>"+res.data[i].adviser_Name
  				        			}
  				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('fri')) {
  				        				this.tableArr[i3].f = "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+ res.data[i].section +"</b>"+res.data[i].adviser_Name
  				        			}*/

                        if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('mon')) {
                          this.tableArr[i3].m =  "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+res.data[i].section +"</b>"
                        }
                        if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('tues')) {
                          this.tableArr[i3].t = "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+ res.data[i].section +"</b>"
                        }
                        if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('wed')) {
                          this.tableArr[i3].w =  "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+res.data[i].section +"</b>"
                        }
                        if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('thurs')){
                          this.tableArr[i3].th = "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+res.data[i].section +"</b>"
                        }
                        if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('fri')) {
                          this.tableArr[i3].f = "<b style=\'font-size:18px\'>Grade "+res.data[i].gradeLevel+' - '+ res.data[i].section +"</b>"
                        }
	            				}
	            			}
            			}
            			

            			
	            	}
            },Error=>{
                this.global.swalAlertError()
                //this.global.logout()
              });
  	   }else{
         this.tableArr = this.global.empHSClassSchedule;
  	   }
  }

}

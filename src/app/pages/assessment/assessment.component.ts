import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from './../../global.service';
import { StudentApiService } from '../../student-api.service';


@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent implements OnInit {
  imagePath
  imagePath3
  webexist
  displaysem = ''
  sydisplay = ''
  sydisplay1 = ''
  sydisplay2 = ''
  sydisplay3 = ''
  lastSY
  encryptedID;
  selectedTab
  feesBreakdown=[]
  SOA=[]
  TotalFees
  DuesBreakdown=[]
  Clearance =[]
  desc0=''
  desc1=''
  desc2=''
  desc3=''
  desc4=''
  dueAmount0
  dueAmount1
  dueAmount2
  dueAmount3
  dueAmount4
  constructor(private _sanitizer: DomSanitizer, public global: GlobalService, public api: StudentApiService) { }

  ngOnInit() {
    this.selectedTab=0
    this.loadActiveTab()
    var sy = '';
    sy = this.global.enrollmenthist[this.global.enrollmenthist.length - 1].schoolYear
    this.sydisplay1 = this.generateDisplaySY(sy)
    sy = this.global.enrollmenthist[this.global.enrollmenthist.length - 2].schoolYear
    this.sydisplay2 = this.generateDisplaySY(sy)
    sy = this.global.enrollmentsy
    this.sydisplay3 = this.generateDisplaySY(sy)
  //   this.api.getEncryptedString(this.global.userinfo.idNumber).subscribe(res => {
  //     if (res.data) {
  //       this.encryptedID = res.data
  //       if (this.global.enrollmenthist.length > 0) {
  //         sy = this.global.enrollmenthist[this.global.enrollmenthist.length - 1].schoolYear
  //         this.sydisplay = "School Year " + sy.substring(0, 4) + "-" + sy.substring(0, 2) + sy.substring(4, 6)
  //         if (sy.charAt(6) == '1') {
  //           this.displaysem = "1st Semester";
  //         } else if (sy.charAt(6) == '2') {
  //           this.displaysem = "2nd Semester";
  //         } else if (sy.charAt(6) == '3') {
  //           this.displaysem = "Summer";
  //         } else
  //           this.displaysem = "";


  //         this.sydisplay = this.displaysem + " " + this.sydisplay
  //         this.lastSY = this._sanitizer.bypassSecurityTrustResourceUrl(this.global.acctgApi + 'getpdf/student-assessment.php?id=' + encodeURIComponent(this.encryptedID) + '&sy=' + sy);


  //       }

  //       if (this.global.enrollmenthist.length > 1) {
  //         sy = this.global.enrollmenthist[this.global.enrollmenthist.length - 2].schoolYear
  //         this.displaysem = ''
  //         this.sydisplay2 = ''
  //         this.sydisplay2 = "School Year " + sy.substring(0, 4) + "-" + sy.substring(0, 2) + sy.substring(4, 6)
  //         if (sy.charAt(6) == '1') {
  //           this.displaysem = "1st Semester";
  //         } else if (sy.charAt(6) == '2') {
  //           this.displaysem = "2nd Semester";
  //         } else if (sy.charAt(6) == '3') {
  //           this.displaysem = "Summer";
  //         } else
  //           this.displaysem = "";
  //         this.sydisplay2 = this.displaysem + " " + this.sydisplay2
  //         this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl(this.global.acctgApi + 'getpdf/student-assessment.php?id=' + encodeURIComponent(this.encryptedID) + '&sy=' + sy);
  //       }

  //       if (this.global.enrollmenthist.length > 1) {
  //         sy = this.global.enrollmentsy
  //         this.displaysem = ''
  //         this.sydisplay3 = ''
  //         this.sydisplay3 = "School Year " + sy.substring(0, 4) + "-" + sy.substring(0, 2) + sy.substring(4, 6)
  //         if (sy.charAt(6) == '1') {
  //           this.displaysem = "1st Semester";
  //         } else if (sy.charAt(6) == '2') {
  //           this.displaysem = "2nd Semester";
  //         } else if (sy.charAt(6) == '3') {
  //           this.displaysem = "Summer";
  //         } else
  //           this.displaysem = "";
  //         this.sydisplay3 = this.displaysem + " " + this.sydisplay3
  //         this.imagePath3 = this._sanitizer.bypassSecurityTrustResourceUrl(this.global.acctgApi + 'getpdf/student-assessment.php?id=' + encodeURIComponent(this.encryptedID) + '&sy=' + sy);
  //       }
  //     }
  //   },Error=>{
  //     this.global.swalClose();
  //     this.global.swalAlertError(Error);
  //  });

  }

  getindex(value) {
    this.selectedTab=value.index;
    this.loadActiveTab()
  }

  loadActiveTab(){
    var sy = '';
    if(this.selectedTab==0){
      sy = this.global.enrollmenthist[this.global.enrollmenthist.length - 1].schoolYear
      this.loadAssessment(sy)
    }
    else if(this.selectedTab==1){
      sy = this.global.enrollmenthist[this.global.enrollmenthist.length - 2].schoolYear
      this.loadAssessment(sy)
    }
    else if(this.selectedTab==2){
      sy = this.global.enrollmentsy
      this.loadAssessment(sy)
    }
  }

  generateDisplaySY(sy){
    this.sydisplay = "School Year " + sy.substring(0, 4) + "-" + sy.substring(0, 2) + sy.substring(4, 6)
    if (sy.charAt(6) == '1') {
      this.displaysem = "1st Semester";
    } else if (sy.charAt(6) == '2') {
      this.displaysem = "2nd Semester";
    } else if (sy.charAt(6) == '3') {
      this.displaysem = "Summer";
    } else
      this.displaysem = "";

    return this.displaysem + " " + this.sydisplay
  }

  loadAssessment(schoolYear){
    this.SOA=undefined
    this.feesBreakdown=[]
    this.Clearance=[]
    var balance, debit, credit
    this.api.getStudentAssessment(schoolYear).subscribe(res => {
      this.SOA=[]
      if (res.data) {
        this.feesBreakdown=res.data.assessmentSelects
        this.TotalFees=res.data.totalFees
        balance=0
        debit=0
        for(var i=0; i<res.data.aRLedgerSelects.length;++i){
          if(res.data.aRLedgerSelects[i].transType=='D'){
            balance=balance + res.data.aRLedgerSelects[i].amount
            debit=res.data.aRLedgerSelects[i].amount
            credit=''
          }
          else{
            balance=balance - res.data.aRLedgerSelects[i].amount
            credit=res.data.aRLedgerSelects[i].amount
            debit=''
          }
          this.SOA.push({
            refDate:res.data.aRLedgerSelects[i].refDate,
            refNo:res.data.aRLedgerSelects[i].refNo,
            particulars:res.data.aRLedgerSelects[i].particulars,
            debit:debit,
            credit:credit,
            balance:balance,
          });
        }
      }
      else{
        this.TotalFees=0.00
      }
    });

    this.api.getStudentDues(schoolYear).subscribe(res => {
      if (res.data) {
        this.DuesBreakdown=res.data.dueDates
        this.desc0=res.data.dueDates[0].description
        this.dueAmount0=res.data.dueDates[0].status
        if (this.dueAmount0!='CLEARED')
          this.dueAmount0 =Number(this.dueAmount0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.desc1=res.data.dueDates[1].description
        this.dueAmount1=res.data.dueDates[1].status
        if (this.dueAmount1!='CLEARED')
          this.dueAmount1 =Number(this.dueAmount1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.desc2=res.data.dueDates[2].description
        this.dueAmount2=res.data.dueDates[2].status
        if (this.dueAmount2!='CLEARED')
          this.dueAmount2 =Number(this.dueAmount2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        this.desc3=''
        this.dueAmount3=''
        this.desc4=''
        this.dueAmount4=''
        if(res.data.dueDates.length==4){
          this.desc3=res.data.dueDates[3].description
          this.dueAmount3=res.data.dueDates[3].status
          if (this.dueAmount3!='CLEARED')
            this.dueAmount3 =Number(this.dueAmount3).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        else if(res.data.dueDates.length==5){
          this.desc3=res.data.dueDates[3].description
          this.dueAmount3=res.data.dueDates[3].status
          if (this.dueAmount3!='CLEARED')
            this.dueAmount3 =Number(this.dueAmount3).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          this.desc4=res.data.dueDates[4].description
          this.dueAmount4=res.data.dueDates[4].status
          if (this.dueAmount4!='CLEARED')
            this.dueAmount4 =Number(this.dueAmount4).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
      else{
        this.desc0=''
        this.dueAmount0=''
        this.desc1=''
        this.dueAmount1=''
        this.desc2=''
        this.dueAmount2=''
        this.desc3=''
        this.dueAmount3=''
        this.desc4=''
        this.dueAmount4=''
      }
    });
    this.api.getStudentClearance().subscribe(res => {
      if(res.data){
        this.Clearance=res.data
      }
    });
  }

  urlExists(url) {
    return fetch(url, { mode: "no-cors" })
      .then(res => true)
      .catch(err => false)
  }
}

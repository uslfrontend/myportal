import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Component,ViewChild, OnInit,ElementRef } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { SeminarsTrainingsComponent } from './../../popup/seminars-trainings/seminars-trainings.component';
import { OrganizationsComponent } from './../../popup/organizations/organizations.component';
import { ResearchPubComponent } from './../../popup/research-pub/research-pub.component';
import { CommunityEngagementComponent } from './../../popup/community-engagement/community-engagement.component';
import { AwardsComponent } from './../../popup/awards/awards.component';
import { SpeakingEngagementComponent } from './../../popup/speaking-engagement/speaking-engagement.component';

@Component({
  selector: 'app-other-info',
  templateUrl: './other-info.component.html',
  styleUrls: ['./other-info.component.css']
})
export class OtherInfoComponent implements OnInit {


///////////////////PAGINATION VARIABLES///////////////////////
  seminarCtr = 0;
  seminarConfig: any;
  collection = { count: 60, data: [] };
//////////////////////////////////////////////////////////////

  constructor(public dialog: MatDialog,private domSanitizer: DomSanitizer,public global: GlobalService,private http: Http) { 

  	this.seminarConfig = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.seminarCtr
    };

  }

  ngOnInit() {
  	//console.table(this.global.empEducBcgArray)
  	this.getSeminars();
  	this.getOrganization();
  	this.getResearch();
    this.getCommunityEngagement();
  }

/////////////////////GET FUNCTIONS///////////////////////////
  getSeminars(){
  	this.http.get(this.global.api+'EmployeePortal/SeminarsAndTrainings/'+this.global.requestid(),this.global.option)
        .map(response => response.json())
        .subscribe(res => { 
          
          //console.log(res.data)
          this.global.SeminarTableArr=res.data;
          // console.table(this.global.SeminarTableArr);
          if(this.global.SeminarTableArr != undefined || this.global.SeminarTableArr != null)
          {
            var count = Object.keys(this.global.SeminarTableArr).length;
            this.seminarCtr = count
          }
          
        },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }

  getOrganization(){
  	this.http.get(this.global.api+'EmployeePortal/Organization/'+this.global.requestid(),this.global.option)
        .map(response => response.json())
        .subscribe(res => { 
          // console.table(res.data)
          this.global.OrganizationTableArr=res.data;
        },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }

  getResearch(){
  	this.http.get(this.global.api+'EmployeePortal/Research/'+this.global.requestid(),this.global.option)
        .map(response => response.json())
        .subscribe(res => {  //console.log(res.data)
          this.global.ResearchArray=res.data;
        },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }
  getCommunityEngagement(){
    this.http.get(this.global.api+'EmployeePortal/CommunityExtension/'+this.global.requestid(),this.global.option)
        .map(response => response.json())
        .subscribe(res => {  //console.log(res.data)
          this.global.CommunityExtArray=res.data;
          //console.log(this.global.CommunityExtArray)
        },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }

////////////////////////////////////////////////////////////

/////////////////////DEFINED FUNCTIONS//////////////////////
  addSaTOpenDialog(): void {
      const dialogRef = this.dialog.open(SeminarsTrainingsComponent, {
        width: '600px', disableClose: false,data:{selectedData:null,type:"Add"}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
            this.getSeminars();  
        }
      });
    
   }
  updateSaTOpenDialog(SaTData,sid):void{
        if(sid==0){
        	 const dialogRef = this.dialog.open(SeminarsTrainingsComponent, {
	          width: '600px', disableClose: false,data:{selectedData:SaTData,selectedSID:SaTData.seminarid,idnum:this.global.requestid(),type:"Update"}
	        });

	        dialogRef.afterClosed().subscribe(result => {
	          //this.keyDownFunction('onoutfocus');
	          //console.log(result);
	          if (result!=undefined)
	          {
	            if (result.result=='Update success')
	            {
	              this.getSeminars();
	            }
	            else if(result.result=='Delete Success')
	            {
					this.getSeminars();
	            }
	          }
	        });
        }else{

        }
    }

    addOrgOpenDialog(): void {
      const dialogRef = this.dialog.open(OrganizationsComponent, {
        width: '600px', disableClose: false,data:{selectedData:null,type:"Add"}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
            this.getOrganization();  
        }
      });
    
   }

    updateOrgOpenDialog(orgData,oid):void{
        if(oid==0 ){
        	 const dialogRef = this.dialog.open(OrganizationsComponent, {
	          width: '600px', disableClose: false,data:{selectedData:orgData,selectedOID:orgData.organizationid,idnum:this.global.requestid(),type:"Update"}
	        });

	        dialogRef.afterClosed().subscribe(result => {
	          //this.keyDownFunction('onoutfocus');
	          //console.log(result);
	          if (result!=undefined)
	          {
	            if (result.result=='Update success')
	            {
  					this.getOrganization();
	            }
	            else if(result.result=='Delete Success')
	            {
  					this.getOrganization();
	            }
	          }
	        });
        }else{

        }
    }

    addResOpenDialog(): void {
      const dialogRef = this.dialog.open(ResearchPubComponent, {
        width: '600px', disableClose: false,data:{selectedData:null,type:"Add"}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
            this.getResearch();  
        }
      });
    
   }

    updateResOpenDialog(resData,rid):void{
        if(rid == 0){
        	 const dialogRef = this.dialog.open(ResearchPubComponent, {
	          width: '600px', disableClose: false,data:{selectedData:resData,selectedRID:resData.researchID,idnum:this.global.requestid(),type:"Update"}
	        });

	        dialogRef.afterClosed().subscribe(result => {
	          //this.keyDownFunction('onoutfocus');
	          //console.log(result);
	          if (result!=undefined)
	          {
	            if (result.result=='Update success')
	            {
  					this.getResearch();
	            }
	            else if(result.result=='Delete Success')
	            {
  					this.getResearch();
	            }
	          }
	        });
        }else{

        }
    }

    addCEOpenDialog(): void {
      const dialogRef = this.dialog.open(CommunityEngagementComponent, {
        width: '600px', disableClose: false,data:{selectedData:null,type:"Add"}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
            this.getCommunityEngagement();  
        }
      });
    
   }

    updateCEOpenDialog(ceData,ceid):void{
        if(ceid==0){
           const dialogRef = this.dialog.open(CommunityEngagementComponent, {
            width: '600px', disableClose: false,data:{selectedData:ceData,selectedCEID:ceData.ceid,idnum:this.global.requestid(),type:"Update"}
          });

          dialogRef.afterClosed().subscribe(result => {
            //this.keyDownFunction('onoutfocus');
            //console.log(result);
            if (result!=undefined)
            {
              if (result.result=='Update success')
              {
                this.getCommunityEngagement();
              }
              else if(result.result=='Delete Success')
              {
                this.getCommunityEngagement();
              }
            }
          });
        }else{

        }
    }


////////////////////////////////////////////////////////////

///////////////////////PAGINATION FUNCTIONS////////////////
	SeminarPageChanged(event){
    	this.seminarConfig.currentPage = event;
  	}

}

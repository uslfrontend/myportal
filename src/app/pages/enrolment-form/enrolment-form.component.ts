import { Component, OnInit } from '@angular/core';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import { GlobalService } from './../../global.service';
import { DomSanitizer } from '@angular/platform-browser';
import {Http, Headers, RequestOptions} from '@angular/http';
import { ViewEncapsulation } from '@angular/core';

import * as jsPDF from 'jspdf';
import * as html2pdf from 'html2pdf.js';
import html2canvas from 'html2canvas';



@Component({
  selector: 'app-enrolment-form',
  templateUrl: './enrolment-form.component.html',
  styleUrls: ['./enrolment-form.component.scss']
})
export class EnrolmentFormComponent implements OnInit {

	url
  syear = '';
  constructor(private domSanitizer: DomSanitizer,public dialogRef: MatDialogRef<EnrolmentFormComponent>,@Inject(MAT_DIALOG_DATA) public data: any,public global: GlobalService) {
    this.syear = this.global.syDisplay(this.global.sy)
    if(this.global.nextSYDisplay)
      this.syear = this.global.nextSYDisplay
  }


 close(): void {
       this.dialogRef.close({result:'cancel'});
  }
  ngOnInit() {


    if (this.data.x=='col'||this.data.x=='hs') {
              var x=''
              for (var i = 0; i < this.data.subjects.length; ++i) {
                if (i>0) {
                  if(this.data.subjects[i].codeNo==this.data.subjects[i-1].codeNo){
                    this.data.subjects[i].codeNo = ''
                    this.data.subjects[i].subjectID = ''
                    this.data.subjects[i].subjectTitle = ''
                    this.data.subjects[i].units = ''
                  }
                }
                if (i>1) {
                if(this.data.subjects[i-1].codeNo==""){
                  if(this.data.subjects[i].codeNo==this.data.subjects[i-2].codeNo){
                    this.data.subjects[i].codeNo = ''
                    this.data.subjects[i].subjectID = ''
                    this.data.subjects[i].subjectTitle = ''
                    this.data.subjects[i].units = ''
                  }
                }
              }
              if (i>2) {
                if(this.data.subjects[i-2].codeNo==""&&this.data.subjects[i-1].codeNo==''){
                  if(this.data.subjects[i].codeNo==this.data.subjects[i-3].codeNo){
                    this.data.subjects[i].codeNo = ''
                    this.data.subjects[i].subjectID = ''
                    this.data.subjects[i].subjectTitle = ''
                    this.data.subjects[i].units = ''
                  }
                }
              }
             }
              for (var i = 0; i < this.data.subjects.length; ++i) {
                x = x+"&foo[]="+
                this.data.subjects[i].codeNo.toUpperCase()+"@-"+
                this.data.subjects[i].subjectID+"@-"+
                this.data.subjects[i].subjectTitle.replace("&", "and")+"@-"+
                this.data.subjects[i].units+"@-"+
                this.data.subjects[i].time+"@-"+
                this.data.subjects[i].day+"@-"+
                this.data.subjects[i].roomNumber+"@-"
              }
              var gender
              if (this.data.gender=='M'||this.data.gender=='m') gender="Male";
              else gender="Female";

              // if (this.global.studinfo.departmentCode=='HS') {
                if (this.global.nextSYStudInfo.departmentCode=='HS') {
                var y = this.global.sy.substring(0,4)
                var z = parseInt(y) + 1
                var a = y.toString() + " - " + z.toString();
                var b = x.substring(6,7)
                var year
                if (this.global.studinfo.yearOrGradeLevel==1) {
                  year = 7
                }
                if (this.global.studinfo.yearOrGradeLevel==2) {
                  year = 8
                }
                if (this.global.studinfo.yearOrGradeLevel==3) {
                  year = 9
                }
                if (this.global.studinfo.yearOrGradeLevel==4) {
                  year = 10
                }
                if (this.global.studinfo.yearOrGradeLevel==5) {
                  year = 11
                }
                if (this.global.studinfo.yearOrGradeLevel==6) {
                  year = 12
                }
                var home=''
                if (this.global.userdemog.homeAddress!=null) {
                  if (this.global.userdemog.permNoStreet!=null) {
                   home = this.global.userdemog.permNoStreet +" "+this.global.userdemog.homeAddress
                  }else{
                    home = this.global.userdemog.homeAddress
                  }
                }

                var board=''
                if (this.global.userdemog.currentAddress!=null) {
                  if (this.global.userdemog.currNoStreet!=null) {
                   board = this.global.userdemog.currNoStreet +" "+this.global.userdemog.currentAddress
                  }else{
                    board = this.global.userdemog.currentAddress
                  }
                }

               var course = this.global.studinfo.curriculum
                this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.global.acctgApi+'getpdf/hsenrollmentform.php?sy='+a+
                '&fname='+this.global.userinfo.firstName.toUpperCase()+
                '&mname='+this.global.userinfo.middleName.toUpperCase()+
                '&lname='+this.global.userinfo.lastName.toUpperCase()+
                '&suffix='+this.global.userinfo.suffixName.toUpperCase()+
                '&idno='+this.global.userinfo.idNumber+
                '&year='+year+
                '&course='+course+
                '&home='+home+
                '&board= '+board
                ));
              }else
              {


              this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.global.acctgApi+'getpdf/getpdf.php?gender='+gender+

            	 '&name='+this.data.name.toUpperCase()+
                '&idno='+this.data.id+
                '&year='+this.data.year+
                '&course='+this.data.course+
                '&major='+this.data.major+
                '&length='+this.data.subjects.length+
                 x+"&sy="+this.syear));
              }
        }

  }

 syDisplay(x){
    var y = x.substring(0,4)
    var z = parseInt(y) + 1
    var a = y.toString() + " - " + z.toString();
    var b = x.substring(6,7)
    var c
    if (b==1)
      c="1st Semester"
    else if (b==2)
      c="2nd Semester"
    else if (b==3)
      c="Summer"
    else
      c=""
    return c + " SY "+a
  }





}

import { Component, OnInit } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';

@Component({
  selector: 'app-employee-records',
  templateUrl: './employee-records.component.html',
  styleUrls: ['./employee-records.component.css']
})
export class EmployeeRecordsComponent implements OnInit {

  constructor(public global: GlobalService,private http: Http) { }

  ngOnInit() {
  }

}

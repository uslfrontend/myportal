import { Component, OnInit,ChangeDetectorRef } from '@angular/core';

import { StudentApiService } from './../../student-api.service';
import {Router} from "@angular/router";
import { GlobalService } from './../../global.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { QuestionnaireComponent } from './../faculty-evaluation/questionnaire/questionnaire.component';

@Component({
  selector: 'app-faculty-evaluation',
  templateUrl: './faculty-evaluation.component.html',
  styleUrls: ['./faculty-evaluation.component.css'],

})
export class FacultyEvaluationComponent implements OnInit {

  subjectsToEvaluateArray=[];
  questionsArray;

  newlyCreatedEval = false;
  evaluateButtonTrigger = true;

  constructor(public dialog: MatDialog,public global: GlobalService,private router: Router,public api:StudentApiService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.getSubjectsToEvaluate('','','');
  }
  getSubjectsToEvaluate(codeNo,subject,param){


    if(this.subjectsToEvaluateArray.length>0)
      this.subjectsToEvaluateArray.length=0;

    this.subjectsToEvaluateArray=[];

    this.global.swalLoading("Loading evaluation");
    this.api.apiFacEvalSubjectsToEvaluateGet()
      .subscribe(res => {
        if(res.data!=null){
          var subjectsData = res.data;
          var sy=this.global.sy
            if (this.global.studinfo.departmentCode=='ELEM'||this.global.studinfo.departmentCode=='HS') {
              if (this.global.sy.length==7) {
                sy = this.global.sy.slice(0, -1)
              }
            }
            this.api.apiHSEnrollmentStudentElective(sy)
            .subscribe(res => {
              if(res.data.length != 0){
                console.log(subjectsData);
                console.log(res.data);

                subjectsData.forEach(element => {
                  var status = this.getEvaluationStatus(get_status_MainQuestions(element.evaluationSheetAnswers_Main),element.evaluationSheetAnswers_Main,element.evaluationSheetAnswer_Additional, element.isOpen,element.evaluationSheetHeader)
                  // var status = this.getEvaluationStatus(element.status_MainQuestions,element.evaluationSheetAnswers_Main,element.evaluationSheetAnswer_Additional, element.isOpen,element.evaluationSheetHeader)
                  if("TLE" == element.subject.substring(0,3)){
                    var selectedElective = res.data[0].name;
                    if(selectedElective == "Bread and Pastry Production" && element.subject == "TLE - BPP")
                      selectedElective = "BPP";

                    if ("TLE - "+selectedElective == element.subject) {

                      this.subjectsToEvaluateArray.push({
                          codeNo: element.codeNo,
                          employeeID: element.employeeID,
                          employeeName: element.employeeName,
                          evaluationSheetAnswer_Additional: element.evaluationSheetAnswer_Additional,
                          evaluationSheetAnswers_Main: element.evaluationSheetAnswers_Main,
                          evaluationSheetHeader: element.evaluationSheetHeader,
                          isOpen: element.isOpen,
                          schedType: element.schedType,
                          schoolYear: element.schoolYear,
                          section: element.section,
                          status_AdditionalQuestions: get_status_AdditionalQuestions(element.evaluationSheetAnswer_Additional),
                          status_MainQuestions: get_status_MainQuestions(element.evaluationSheetAnswers_Main),
                          // status_AdditionalQuestions: element.status_AdditionalQuestions,
                          // status_MainQuestions: element.status_MainQuestions,
                          subject: element.subject,
                          subjectID: element.subjectID,
                          status: status
                      })
                    }
                  }else{
                    this.subjectsToEvaluateArray.push({
                      codeNo: element.codeNo,
                      employeeID: element.employeeID,
                      employeeName: element.employeeName,
                      evaluationSheetAnswer_Additional: element.evaluationSheetAnswer_Additional,
                      evaluationSheetAnswers_Main: element.evaluationSheetAnswers_Main,
                      evaluationSheetHeader: element.evaluationSheetHeader,
                      isOpen: element.isOpen,
                      schedType: element.schedType,
                      schoolYear: element.schoolYear,
                      section: element.section,
                      status_AdditionalQuestions: get_status_AdditionalQuestions(element.evaluationSheetAnswer_Additional),
                      status_MainQuestions: get_status_MainQuestions(element.evaluationSheetAnswers_Main),
                      // status_AdditionalQuestions: element.status_AdditionalQuestions,
                      // status_MainQuestions: element.status_MainQuestions,
                      subject: element.subject,
                      subjectID: element.subjectID,
                      status: status
                    })
                  }
                });
              }else{
                subjectsData.forEach(element => {
                  var status = this.getEvaluationStatus(get_status_MainQuestions(element.evaluationSheetAnswers_Main),element.evaluationSheetAnswers_Main,element.evaluationSheetAnswer_Additional, element.isOpen,element.evaluationSheetHeader)
                  this.subjectsToEvaluateArray.push({
                    codeNo: element.codeNo,
                    employeeID: element.employeeID,
                    employeeName: element.employeeName,
                    evaluationSheetAnswer_Additional: element.evaluationSheetAnswer_Additional,
                    evaluationSheetAnswers_Main: element.evaluationSheetAnswers_Main,
                    evaluationSheetHeader: element.evaluationSheetHeader,
                    isOpen: element.isOpen,
                    schedType: element.schedType,
                    schoolYear: element.schoolYear,
                    section: element.section,
                    status_AdditionalQuestions: get_status_AdditionalQuestions(element.evaluationSheetAnswer_Additional),
                    status_MainQuestions: get_status_MainQuestions(element.evaluationSheetAnswers_Main),
                    // status_AdditionalQuestions: element.status_AdditionalQuestions,
                    // status_MainQuestions: element.status_MainQuestions,
                    subject: element.subject,
                    subjectID: element.subjectID,
                    status: status
                  })
                })
              }
            },Error => {
              this.global.swalAlertError(Error);
            });

          var that = this;
          if(codeNo){

            this.subjectsToEvaluateArray.forEach(element => {
              if(element.codeNo == codeNo&&element.schedType == param.schedType){
                this.getEvaluationQuestions(element.evaluationSheetHeader.evaluationToolID,element)
              }
            });
          }else if(subject){
            // try {

              this.global.swalLoading("Creating evaluation");

              function iterate(item, index){
                var _subject = item.subject;
                if(_subject == subject){
                  that.getEvaluationQuestions(item.evaluationSheetHeader.evaluationToolID,item)
                }
              }
              //set a time delay for the DOM to process the array iteration befor parsing it to another data extraction iteration
              setTimeout(function(){
                that.subjectsToEvaluateArray.forEach(iterate);
              },200)
            // } catch (error) {
            //   this.global.swalAlertError(error)
            // }


          }
          this.global.swalClose();
        }else{
          this.global.swalAlert('','FACULTY EVALUATION IS CLOSE / NO SUBJECTS TO EVALUATE YET.','error');
        }

      },Error=>{
        this.global.swalClose();
        this.global.swalAlertError(Error);
     });

    function get_status_MainQuestions(array){
      let indexCtr = 0;
      if(array){
        array.forEach(element => {
          if(element.mainQuestionRate)
            indexCtr++;
        });

        if(indexCtr == array.length)
          return "Complete";
        else
          return "Incomplete";
      }else
        return "Incomplete";
     }

    function get_status_AdditionalQuestions(array){
      let indexCtr = 0;
      if(array){
        array.forEach(element => {
          if(element.additionalQuestionRate)
            indexCtr++;
        });

        if(indexCtr == array.length)
          return "Complete";
        else
          return "Incomplete";
      }else
        return "Incomplete";
    }

  }

  createEvaluation(param){
    this.evaluateButtonTrigger = false;
    if(param.evaluationSheetAnswers_Main == null){
      this.api.apiFacEvaluatinSheetPost(
        param.employeeID,
        param.subject,
        param.section,
        param.codeNo,
        param.schedType)
         .subscribe(res => {
            if(res.data!=null || res.data!=undefined){
              this.cdr.detectChanges();
              if(this.global.studinfo.level == 'COLLEGE' && this.global.studinfo.level == 'GRADUATE SCHOOL')
                this.getSubjectsToEvaluate(param.codeNo,'',param);
              else
                this.getSubjectsToEvaluate('',param.subject,param)
            }else{
              this.cdr.detectChanges();
              if(res.message.includes("has been created.")){
                if(this.global.studinfo.level == 'COLLEGE'&& this.global.studinfo.level == 'GRADUATE SCHOOL')
                  this.getSubjectsToEvaluate(param.codeNo,'',param);
                else
                  this.getSubjectsToEvaluate('',param.subject,param)
              }else{
                this.global.swalAlert('','UNABLE TO CREATE EVALUATION.<br><p style="color:red">Error: '+res.message+' </p><br>Please contact the CICT Office','error');
                this.evaluateButtonTrigger = true;
              }

            }
          },Error=>{
              this.global.swalAlertError(Error);
          });
    }else{
      this.getEvaluationQuestions(param.evaluationSheetHeader.evaluationToolID,param)
    }
    this.cdr.detectChanges();
  }



  getEvaluationQuestions(evalToolID,codeArray){
    this.global.swalLoading("Loading Questions")
    this.api.apiFacEvalQuestionsGet(evalToolID)
      .subscribe(res => {
        this.questionsArray = res.data;
        this.showQuestionnaireDialog(codeArray);
        this.global.swalClose();

      },Error=>{
        this.global.swalClose();
        this.global.swalAlertError(Error);
     });
  }

  showQuestionnaireDialog(codeArray){
    try {

      const dialogRef = this.dialog.open(QuestionnaireComponent, {
        width: '900px', disableClose: false,data:{selectedData:this.questionsArray,type:"Add",codeArr:codeArray}
      });
      // this.global.swalClose();

      dialogRef.afterClosed().subscribe(result => {
        if (result.result=="success") {
          // while(this.subjectsToEvaluateArray.length){//removes the objects in the array
          //   this.subjectsToEvaluateArray.pop();
          // }

          this.evaluateButtonTrigger = true;
          // this.getSubjectsToEvaluate('','','');
          // this.global.swalLoading("Loading evaluation");
          // setTimeout(() => this.getSubjectsToEvaluate('','','') , 1000 * 1)

          for(var i in this.subjectsToEvaluateArray){
            if(result.codeNo!="" || result.codeNo != null){
              if(this.subjectsToEvaluateArray[i].schedType == result.schedType && this.subjectsToEvaluateArray[i].subject == result.subject){
                this.subjectsToEvaluateArray[i].status = 'Evaluated'
                break;
              }
            }else{
              if(this.subjectsToEvaluateArray[i].schedType == result.schedType && this.subjectsToEvaluateArray[i].codeNo == result.codeNo){
                this.subjectsToEvaluateArray[i].status = 'Evaluated'
                break;
              }
            }
          }

        }
        else if(result.result == 'cancel'){
          this.evaluateButtonTrigger = true;
        }else{
          this.evaluateButtonTrigger = true;
          for(var i in this.subjectsToEvaluateArray){
            if(result.codeNo!="" || result.codeNo != null){
              if(this.subjectsToEvaluateArray[i].schedType == result.schedType && this.subjectsToEvaluateArray[i].subject == result.subject){
                this.subjectsToEvaluateArray[i].status = 'Incomplete Evaluation'
                break;
              }
            }else{
              if(this.subjectsToEvaluateArray[i].schedType == result.schedType && this.subjectsToEvaluateArray[i].codeNo == result.codeNo){
                this.subjectsToEvaluateArray[i].status = 'Incomplete Evaluation'
                break;
              }
            }
          }
        }

      });
    } catch (error) {
      this.global.swalLoading("Loading error. Please reload your portal to fetch again the needed data")

    }

  }

  //#region //Triggers
  getSchedType(schedType){
    if(schedType == 1)
      return "Laboratory";
    else
      return "Lecture";
  }


  getEvaluationStatus(status_MainQuestions,array_of_questions,array_of_additional_questions, isOpen,evaluationSheetHeader){

    let trigger=0;
    let trigger1=0;
    let aQctr = 0;//length of additional questions
    let mQctr = 0;//length of main questions

    if(array_of_additional_questions)
      aQctr = array_of_additional_questions.length;

    if(array_of_questions)
      mQctr = array_of_questions.length;

    // if(isOpen == 1){
      var returnStatus = '';
      if(this.global.studinfo.level =='HIGHSCHOOL'){
        if(array_of_additional_questions){
          //checking of answered questions
          array_of_additional_questions.forEach(element => {
            if(element.additionalQuestionRate!=null && element.additionalQuestionRate > 0){
              trigger1+=1;
            }
          });

           //returns incomplete if the total answered additional questions is less than the length of the questions
          if(trigger1>0 && trigger1 <aQctr){
            return "Incomplete Evaluation";
          }
        }
        else{
          if(isOpen == 1)
            returnStatus = "Ready for evaluation";
          else
            returnStatus = ""
        }
      }
      if(evaluationSheetHeader){
        if(status_MainQuestions == "Complete")
          returnStatus = "Evaluated"
        else{
          if(array_of_questions){
            //checking of answered questions
            array_of_questions.forEach(element => {
              if(element.mainQuestionRate!=null){
                trigger+=1;
              }
            });

            //returns incomplete if the total answered main questions is less than the length of the questions
            if(trigger>0 && trigger <mQctr){
              return "Incomplete Evaluation";
            }
            else{
              if(isOpen == 1)
                returnStatus = "Ready for evaluation";
              else
                returnStatus = ""
            }
          }
          else{
            if(isOpen == 1)
              returnStatus = "Ready for evaluation";
            else
              returnStatus = ""
          }
        }
      }else{
        if(isOpen == 1)
          returnStatus = "Ready for evaluation";
        else
          returnStatus = ""
      }
    // }
    // else{
    //   returnStatus = "Evaluation close";
    // }

    return returnStatus;

  }

  getIsOpenStatus(isOpen){
    if(isOpen == 0)
      return false;
    else
      return true;
  }

  getCreateEvaluationStatus(evaluationSheetAnswers_Main){
    if(evaluationSheetAnswers_Main == null || evaluationSheetAnswers_Main == ''){
      return true
    }
    else{
        return false
    }
  }
  checkButtonEvaluate(param){
    var value = true;
    if(this.global.studinfo.level != 'COLLEGE'){
      if(param.status_AdditionalQuestions == 'Incomplete' && param.evaluationSheetAnswers_Additional != null)
        value = true;
      else
        value = false;
    }

    return value;
  }
  //#endregion

}

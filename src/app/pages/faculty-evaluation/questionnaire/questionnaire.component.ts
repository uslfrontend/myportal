import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../global.service';
import {FormBuilder, FormGroup} from '@angular/forms';

import { StudentApiService } from './../../../student-api.service';

import Swal from 'sweetalert2';
const swal = Swal;
@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class QuestionnaireComponent implements OnInit {


  questionsArrayGroup1=[];
  questionsArrayGroup2=[];
  questionsArrayGroup3=[];
  questionsArrayGroup4=[];
  questionsArrayGroup5=[];

  additionalQuestionsArray=[];

  editable = true;
  completeAnswers = false;

  codeNo='';
  facultyName='';
  subject = '';
  comment='';
  commentID;
  headerID='';
  schedType='';
  proglevel='';

  constructor(public dialogRef: MatDialogRef<QuestionnaireComponent>,
  	@Inject(MAT_DIALOG_DATA) public data: any,
  	public dialog: MatDialog,
  	private global: GlobalService,
  	private http: Http,
    private fb: FormBuilder,
    public api:StudentApiService) { dialogRef.disableClose = true;}

  ngOnInit() {
    this.schedType = this.data.codeArr.schedType;
    this.proglevel = this.global.studinfo.level;
    this.codeNo = this.data.codeArr.codeNo;
    this.facultyName = this.data.codeArr.employeeName;
    this.subject = this.data.codeArr.subject;
    this.commentID = this.data.codeArr.evaluationSheetHeader.id;
    let counter = this.data.selectedData[0].mainQuestions.length;
    if(this.data.codeArr.evaluationSheetHeader.comments!=null)
      this.comment = this.data.codeArr.evaluationSheetHeader.comments;
    else
      this.comment = '';

    //this bulky batch of codes are breaking down the questions into 5 groups for easier display and data manipulation
    for(let i=0; i<=counter; i++){
      if(i>=0 && i<=4)
        this.questionsArrayGroup1.push({
          arrangement: this.data.selectedData[0].mainQuestions[i].arrangement,
          choices: this.data.selectedData[0].mainQuestions[i].choices,
          evaluationToolID: this.data.selectedData[0].mainQuestions[i].evaluationToolID,
          id: this.data.selectedData[0].mainQuestions[i].id,
          question:this.data.selectedData[0].mainQuestions[i].question,
          valueAnswer: this.data.codeArr.evaluationSheetAnswers_Main[i].mainQuestionRate,
          questionID: this.data.codeArr.evaluationSheetAnswers_Main[i].id,
          itemNo:(i+1)
        });
      else if(i>4 && i<=9)
        this.questionsArrayGroup2.push({
          arrangement: this.data.selectedData[0].mainQuestions[i].arrangement,
          choices: this.data.selectedData[0].mainQuestions[i].choices,
          evaluationToolID: this.data.selectedData[0].mainQuestions[i].evaluationToolID,
          id: this.data.selectedData[0].mainQuestions[i].id,
          question:this.data.selectedData[0].mainQuestions[i].question,
          valueAnswer: this.data.codeArr.evaluationSheetAnswers_Main[i].mainQuestionRate,
          questionID: this.data.codeArr.evaluationSheetAnswers_Main[i].id,
          itemNo:(i+1)
        });
      else if(i>9 && i<=14)
        this.questionsArrayGroup3.push({
          arrangement: this.data.selectedData[0].mainQuestions[i].arrangement,
          choices: this.data.selectedData[0].mainQuestions[i].choices,
          evaluationToolID: this.data.selectedData[0].mainQuestions[i].evaluationToolID,
          id: this.data.selectedData[0].mainQuestions[i].id,
          question:this.data.selectedData[0].mainQuestions[i].question,
          valueAnswer: this.data.codeArr.evaluationSheetAnswers_Main[i].mainQuestionRate,
          questionID: this.data.codeArr.evaluationSheetAnswers_Main[i].id,
          itemNo: (i+1)
        });
      else if(i>14 && i<=19)
        this.questionsArrayGroup4.push({
          arrangement: this.data.selectedData[0].mainQuestions[i].arrangement,
          choices: this.data.selectedData[0].mainQuestions[i].choices,
          evaluationToolID: this.data.selectedData[0].mainQuestions[i].evaluationToolID,
          id: this.data.selectedData[0].mainQuestions[i].id,
          question:this.data.selectedData[0].mainQuestions[i].question,
          valueAnswer: this.data.codeArr.evaluationSheetAnswers_Main[i].mainQuestionRate,
          questionID: this.data.codeArr.evaluationSheetAnswers_Main[i].id,
          itemNo:(i+1)
        });
      else if(i>19 && i<=25)
        this.questionsArrayGroup5.push({
          arrangement: this.data.selectedData[0].mainQuestions[i].arrangement,
          choices: this.data.selectedData[0].mainQuestions[i].choices,
          evaluationToolID: this.data.selectedData[0].mainQuestions[i].evaluationToolID,
          id: this.data.selectedData[0].mainQuestions[i].id,
          question:this.data.selectedData[0].mainQuestions[i].question,
          valueAnswer: this.data.codeArr.evaluationSheetAnswers_Main[i].mainQuestionRate,
          questionID: this.data.codeArr.evaluationSheetAnswers_Main[i].id,
          itemNo:(i+1)
        });
    }

    if(this.proglevel!='COLLEGE'){
      for(let i=0;i<this.data.selectedData[0].additionalQuestions.length; i++){
        this.additionalQuestionsArray.push({
          arrangement: this.data.selectedData[0].additionalQuestions[i].arrangement,
          choices: this.data.selectedData[0].additionalQuestions[i].choices,
          evaluationToolID: this.data.selectedData[0].additionalQuestions[i].evaluationToolID,
          id: this.data.selectedData[0].additionalQuestions[i].id,
          question:this.data.selectedData[0].additionalQuestions[i].question,
          valueAnswer: this.data.codeArr.evaluationSheetAnswer_Additional[i].additionalQuestionRate.toString(),
          questionID: this.data.codeArr.evaluationSheetAnswer_Additional[i].id,
          itemNo: (i+1)
        });
      }
    }
  }

  //records temporarily into the question group arrays the selected radio button values as "valueAnswer"
  recordValueTemp(value,questionID,array,qGroup,index){
    this.changeValue(value,questionID,array,qGroup);
  }

  changeValue( val, qID, array, qGroup ) {
    for (var i in array) {
      if (array[i].id == qID) {
        switch(qGroup){
          case 1:
            this.questionsArrayGroup1[i].valueAnswer = val;
            break;
          case 2:
            this.questionsArrayGroup2[i].valueAnswer = val;
            break;
          case 3:
            this.questionsArrayGroup3[i].valueAnswer = val;
            break;
          case 4:
            this.questionsArrayGroup4[i].valueAnswer = val;
            break;
          case 5:
            this.questionsArrayGroup5[i].valueAnswer = val;
            break;
          case 6:
            this.additionalQuestionsArray[i].valueAnswer = val;
            break;
        }
      }
    }
  }

  verifyAnswers(){
    var error=[];
    let ctrTrigger = 0;
    //variable counter to count for question groups that has no valueAnswer
    let checkCounter=0;
    this.questionsArrayGroup1.forEach(element => {
      if(element.valueAnswer == "" || element.valueAnswer == null){
        checkCounter+=1;
        ctrTrigger++;
      }
    });
    if(ctrTrigger>0)
      error.push("Page 1");

    ctrTrigger = 0;
    this.questionsArrayGroup2.forEach(element => {
      if(element.valueAnswer == "" || element.valueAnswer == null){ checkCounter+=1;
        ctrTrigger++;
      }
    });

    if(ctrTrigger>0)
      error.push("Page 2");

    ctrTrigger = 0;
    this.questionsArrayGroup3.forEach(element => {
      if(element.valueAnswer == "" || element.valueAnswer == null){ checkCounter+=1;
        ctrTrigger++;
      }
    });

    if(ctrTrigger>0)
      error.push("Page 3");

    ctrTrigger = 0;
    this.questionsArrayGroup4.forEach(element => {
      if(element.valueAnswer == "" || element.valueAnswer == null){ checkCounter+=1;
        ctrTrigger++;
      }
    });

    if(ctrTrigger>0)
      error.push("Page 4");

    ctrTrigger = 0;
    this.questionsArrayGroup5.forEach(element => {
      if(element.valueAnswer == "" || element.valueAnswer == null){ checkCounter+=1;
        ctrTrigger++;
      }
    });

    if(ctrTrigger>0)
      error.push("Page 5");

    ctrTrigger = 0;
    if(this.global.studinfo.level != 'COLLEGE'){
      this.additionalQuestionsArray.forEach(element => {
        if(element.valueAnswer == ""){
          checkCounter+=1;
        }
      });

    if(ctrTrigger>0)
    error.push("Others");

  ctrTrigger = 0;
    }

    return [checkCounter,error];

  }

  getAnswerValue(value){
    switch (parseInt(value)) {
      case 1:
        return "Never";
      case 2:
        return "Seldom";
      case 3:
        return "Sometimes";
      case 4:
        return "Often";
      case 5:
        return "Always";
    }
  }
  resData
  submit(){
    this.resData = this.verifyAnswers()
    var errorComment = ""
    if(this.comment == "" || this.comment == null){
      this.resData[0]+=1;
      errorComment = "**Comment is required."
    }

    if(this.resData[0]==0){
      //result is 0 get summary of questions and answers for submittion verification
      this.completeAnswers = true;

    }else{
       var errors = '';
        this.resData[1].forEach(element => {
          errors = errors+'-'+element+"<br>";
        });
        if(this.resData[1].length>0)
          this.global.swalAlert("Warning","**There is/are questions that has no answers. Please review your answers in:<br>"+errors+errorComment,"warning");
        else
        this.global.swalAlert("Warning",errorComment,"warning");

      this.completeAnswers = false;
    }
  }
  printTrigger = false;
  viewSummary(){
    this.completeAnswers = true;
    this.printTrigger = true;
  }
  print(){

  }

  swalConfirm(){
    swal({
        title: "Confirm",
        html: "You are about to submit your answers. Once confirmed, you can no longer changed your answers to the questions.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm',
        cancelButtonText: 'Review answers'
      }).then((result) => {

        if (result.value) {
          //array concatenation to make the questions array into one
          var temp = this.questionsArrayGroup1.concat(this.questionsArrayGroup2);
          temp = temp.concat(this.questionsArrayGroup3);
          temp = temp.concat(this.questionsArrayGroup4);
          temp = temp.concat(this.questionsArrayGroup5);

          var tempCtr = temp.length;
          var index = 0;
          var indexIncrement = 0;

          var additionalCtr = 0;
          var index1 = 0;
          var indexIncrement1 = 0;

          temp.sort((a, b) => {
            return a.itemNo - b.itemNo;
          });
          //------------------------
          //api calls to submit comment
          this.api.apiFacEvalSubmitComment(parseInt(this.commentID),this.comment)
            .subscribe(res=>{
              //api loop calls to submit answers
              this.global.swalLoading("Submitting and verifying answers")

              for(let i = 0;i<tempCtr;i++){
                this.api.apiFacEvalSubmitAnswers(parseInt(temp[i].questionID),temp[i].valueAnswer)
                .subscribe(res => {
                  //code to check whether submition of answer is successful or not
                  // console.log(res.message)
                  indexIncrement++
                  var text = res.message;
                  if(text.includes("has been updated")){
                    index++;
                  }
                  if(indexIncrement == tempCtr){
                    if(tempCtr == index){
                      if(this.proglevel!= 'COLLEGE'){
                        additionalCtr = this.additionalQuestionsArray.length;
                        this.additionalQuestionsArray.sort((a, b) => {
                          return a.itemNo - b.itemNo;
                        });

                        for (var x in this.additionalQuestionsArray) {
                          this.api.apiFacEvalSubmitAdditionalAnswers(parseInt(this.additionalQuestionsArray[x].questionID),this.additionalQuestionsArray[x].valueAnswer)
                          .subscribe(res => {
                            //code to check whether submition of answer is successful or not
                            // console.log(res.message)
                            indexIncrement1++
                            var text = res.message;
                            if(text.includes("has been updated")){
                              index1++;
                            }
                            if(indexIncrement1 == additionalCtr){
                              if(additionalCtr == index1){
                                this.global.swalClose();
                                this.dialogRef.close({
                                  result:"success",
                                  codeNo:this.codeNo,
                                  schedType: this.schedType,
                                  subject: this.subject
                                });
                              }else{
                                this.global.swalClose();
                                this.dialogRef.close({
                                  result:"incomplete",
                                  codeNo:this.codeNo,
                                  schedType: this.schedType,
                                  subject: this.subject
                                });
                              }
                            }

                          },Error=>{
                            this.global.swalAlertError(Error);
                          });

                        }
                      }else{
                        this.global.swalClose();
                        this.dialogRef.close({
                          result:"success",
                          codeNo:this.codeNo,
                          schedType: this.schedType,
                          subject: this.subject
                        });
                      }
                    }else{
                      this.global.swalClose();
                      this.dialogRef.close({
                        result:"incomplete",
                        codeNo:this.codeNo,
                        schedType: this.schedType,
                        subject: this.subject
                      });
                    }
                  }
                },Error=>{
                  this.global.swalAlertError(Error);
                });
              }
              // temp.forEach(element => {
              //   this.api.apiFacEvalSubmitAnswers(parseInt(element.questionID),element.valueAnswer)
              //   .subscribe(res => {
              //     //code to check whether submition of answer is successful or not
              //     // console.log(res.message)
              //     var text = res.message;
              //     if(text.includes("has been updated")){
              //       index++;
              //     }


              //   },Error=>{
              //     this.global.swalAlertError(Error);
              //   });
              // });
            },Error=>{
              this.global.swalAlertError(Error);
            })

        }
        else if (result.dismiss === Swal.DismissReason.cancel) {
          //user cancelled the confirmation
        }
      })
  }

  assignResultCounter(data){

  }

  noclick()
{
  this.dialogRef.close({result:"cancel",headerID:this.headerID})
}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElemClassScheduleComponent } from './elem-class-schedule.component';

describe('ElemClassScheduleComponent', () => {
  let component: ElemClassScheduleComponent;
  let fixture: ComponentFixture<ElemClassScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElemClassScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElemClassScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GsEnrollmentComponent } from './gs-enrollment.component';

describe('GsEnrollmentComponent', () => {
  let component: GsEnrollmentComponent;
  let fixture: ComponentFixture<GsEnrollmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GsEnrollmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GsEnrollmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AddressLookupComponent } from './../address-lookup/address-lookup.component';
import {Router} from "@angular/router";
import Swal from 'sweetalert2';
import { EnrolmentFormComponent } from './../enrolment-form/enrolment-form.component';
import { StudentApiService } from './../../student-api.service';

const swal = Swal;
@Component({
  selector: 'app-gs-enrollment',
  templateUrl: './gs-enrollment.component.html',
  styleUrls: ['./gs-enrollment.component.scss']
})
export class GsEnrollmentComponent implements OnInit {

permPSGC=''
homeaddress=''
currPSGC=''
boardingaddress=''
tno=''
cno=''
email=''
currNoStreet=''
permNoStreet=''
gender=''
setarray
tableArr=[]
inactive=[]
setfiltered=[]
setvar=''
warndisplay3=[]
major=''
constructor(public dialog: MatDialog,public global: GlobalService,private http: Http,private router: Router,public api:StudentApiService) { }
  ngOnInit(){
      this.start()
  }
  start() {
    this.permPSGC=''
    this.homeaddress=''
    this.currPSGC=''
    this.boardingaddress=''
    this.tno=''
    this.cno=''
    this.email=''
    this.currNoStreet=''
    this.permNoStreet=''
    this.gender=''
    this.setarray
    this.tableArr=[]
    this.inactive=[]
    this.setfiltered=[]
    this.setvar=''
    this.checkenroll='true'
    this.warndisplay3=[]

    if (this.global.ClassSchedule.length==0) {
    	//console.log(this.global.studinfo.blacklistedStopped)
      this.global.swalLoading('')
      var filt2=''
       if (this.global.studinfo.blacklistedStopped!=0) {
         filt2 = filt2 + '*Blacklisted (Stopped)<br>'
        }
        if (this.global.studinfo.blacklistedViolation!=0) {
         filt2 = filt2 + '*Blacklisted (Violation)<br>'
        }


        if (filt2 != '') {
          this.global.swalAlert('<h5 style="margin: 0 0 0 0;">Please contact <a href="mailto:dsas@usl.edu.ph">dsas@usl.edu.ph</a> or <a href="tel:09563078723">09563078723</a> for your admission to resolve the ff. </h5>',filt2,'warning')
          window.history.back();
        }else{
        	var indicate=0

                    if (this.global.userdemog!=undefined) {
                      var x=this.global.userdemog
                      this.permPSGC=x.permPSGC
                      this.homeaddress=x.homeAddress
                      this.currPSGC=x.currPSGC
                      this.boardingaddress=x.currentAddress
                      this.tno=x.telNo
                      this.cno=x.mobileNo
                      this.email=x.emailAdd
                      this.currNoStreet=x.currNoStreet
                      this.permNoStreet=x.permNoStreet
                                  var x = this.global.studinfo.yearOrGradeLevel
                                   if (this.global.setarray!=null) {
                                     for (var i = 0; i < this.global.setarray.length; ++i) {
                                      //if (this.global.setarray[i].course=='BSN '&&this.global.setarray[i].yearLevel==2) {
                                      //if (this.global.setarray[i].course.replace(/\s/g, "")==this.global.studinfo.course.replace(/\s/g, "")+this.global.studinfo.major.replace(/\s/g, "")&&this.global.setarray[i].yearLevel==x) {
                                      if (this.global.studinfo.course.charAt(0).toLowerCase()=='m') {
                                        if ('MAED'.replace(/\s/g, "")==this.global.setarray[i].course.replace(/\s/g, "")&&this.global.setarray[i].yearLevel==1) {
                                        this.setfiltered.push(this.global.setarray[i])
                                        }
                                      }else{
                                        if ('DBM'.replace(/\s/g, "")==this.global.setarray[i].course.replace(/\s/g, "")&&this.global.setarray[i].yearLevel==1) {
                                          this.setfiltered.push(this.global.setarray[i])
                                        }
                                      }

                                     }
                                     console.log(this.setfiltered)
                                   }else  this.global.setarray=[]

                                   if (this.setfiltered.length==0) {
                                    this.global.swalAlert('Please see your Program Chair.','*No Available Set for your course and year.','warning')
                                    window.history.back();
                                   }else{
                                       this.setvar=this.setfiltered[0].headerId.toString()
                                       this.selectheader(this.setfiltered[0].headerId.toString(),0)
                                   }

                    }
                }
    	// code...
    }else{
    }
  }
  subjectsarray:any=[]
  checkenroll='true'
   selectheader(id,index){
    this.tableArr=undefined
    this.http.get(this.global.api+'StudentPortal/SetDetails/'+id,this.global.option)
          .map(response => response.json())
          .subscribe(res => {
            var condition=false
            var subjectId=''
            var codeNo=''
            var subjectTitle=''
            var units=''
            this.tableArr=[]
            for (var i = 0; i < res.data.length; ++i) {
                if (i>0) {
                  if (res.data[i].codeNo==res.data[i-1].codeNo) {
                  }else
                    if (i>1) {
                      if (res.data[i].codeNo==res.data[i-2].codeNo) {
                        // code...
                      }else
                          if (i>2) {
                          if (res.data[i].codeNo==res.data[i-3].codeNo) {
                            // code...
                          }else{
                            if (i>3) {
                              if (res.data[i].codeNo==res.data[i-4].codeNo) {
                                // code...
                              }else{
                                if (i>4) {
                                  if (res.data[i].codeNo==res.data[i-5].codeNo) {
                                    // code...
                                  }else{
                                    if (i>5) {
                                      if (res.data[i].codeNo==res.data[i-6].codeNo) {
                                        // code...
                                      }else
                                        this.subjectsarray.push(res.data[i])
                                    }else
                                      this.subjectsarray.push(res.data[i])
                                  }
                                }else
                                  this.subjectsarray.push(res.data[i])
                              }
                            }else
                              this.subjectsarray.push(res.data[i])
                          }
                        }else
                          this.subjectsarray.push(res.data[i])
                    }else
                      this.subjectsarray.push(res.data[i])
                }else
                  this.subjectsarray.push(res.data[i])
            }
            var full = false
            for (var i = 0; i < res.data.length; ++i) {
              condition=false
              if (this.tableArr==undefined) {
                this.tableArr=[]
               }

              this.tableArr.push({
                classSize: res.data[i].classSize,
                codeNo: res.data[i].codeNo,
                day: res.data[i].day,
                detailId: res.data[i].detailId,
                headerId: res.data[i].headerId,
                roomNumber: res.data[i].roomNumber,
                subjectId: res.data[i].subjectId,
                subjectTitle: res.data[i].subjectTitle,
                time: res.data[i].time,
                units: res.data[i].units
              })
              if (res.data[i].availableSlot<=0) {
                if ((index+1)>=this.setfiltered.length){
                    this.global.swalAlert('Please proceed to your Program Chair. All available sets are full!','','warning')
                    window.history.back();
                   break
                }else{
                  this.setvar=this.setfiltered[index+1].headerId.toString()
                  this.selectheader(this.setfiltered[index+1].headerId,index+1)
                  break
                }
              }else{
                condition=true
              }
            }
            if (res.data.length==0) {
              if ((index+1)>=this.setfiltered.length) {
                this.global.swalAlert('Please proceed to your Program Chair. All available sets are full!','','warning')
                window.history.back();
              }else{
                this.setvar=this.setfiltered[index+1].headerId.toString()
                this.selectheader(this.setfiltered[index+1].headerId,index+1)
              }
            }
            if (condition) {
              if (this.setfiltered[index]!=undefined) {
                this.setvar=this.setfiltered[index].headerId.toString()
                var setf=this.setfiltered[index]
                this.setfiltered=[]
                this.setfiltered.push(setf)
                this.checkenroll= 'false'
              }
                this.global.swalClose()
            }
          },Error=>{
           this.tableArr=[]
            //console.log(Error);
            console.log(Error)
            this.global.swalAlertError();
         });
   }
    checkmajor(){
      this.global.studinfo.programID=this.major.toString()
      for (var i = 0; i < this.global.majors.length; ++i) {
        if (this.major == this.global.majors[i].programId) {

          this.global.studinfo.major = this.global.majors[i].major
          break
        }
      }
    }
   enroll(){
     var x=''
     if (this.checkenroll=='true') {
       x = x+"*Select Set!<br>"
     }
     if (this.homeaddress==''||this.homeaddress==null) {
       x = x+"*Home address is required!<br>"
     }
     if (this.major==''&&this.global.gsfirstyear) {
       x = x+"*Major is required!<br>"
     }
     if (x!='') {
       this.global.swalAlert("Warning",x,'warning')
     }else{
      this.checkenroll= 'true'
      this.global.swalLoading('');
      // this.checkconflict(this.tableArr.length-1)
      this.enrollStudent()
     }
   }
  //  admitstud(){
  //    this.http.post(this.global.api+'StudentPortal/AdmitStudent' ,{
  //               "idNumber": this.global.userinfo.idNumber,
  //               "schoolYear": this.global.sy,
  //               "programID": this.global.studinfo.programID,
  //               "year": this.getyear(this.global.studinfo.yearOrGradeLevel),
  //               "type": 1,
  //               "isAdmittedOnline": true
  //           },this.global.option)
  //             .map(response => response.json())
  //             .subscribe(res => {
  //               this.enrollcode(this.subjectstoenrolldisplay.length-1)
  //             },Error=>{
  //               this.global.swalAlertError();
  //               console.log(Error);
  //             });
  //  }
  //  tempconflict=0
  //  tempclasssize=0
  //  checkconflict(repeat){
  //    if (repeat>=0) {
  //      var codeno=''
  //      if (this.tableArr[repeat].codeNo=='') {
  //        if (this.tableArr[repeat-1].codeNo==''){
  //          codeno = this.tableArr[repeat-2].codeNo
  //          if (this.tableArr[repeat-2].codeNo==''){
  //              codeno = this.tableArr[repeat-3].codeNo
  //                    if (this.tableArr[repeat-3].codeNo==''){
  //                    codeno = this.tableArr[repeat-4].codeNo
  //                  }else
  //                    codeno = this.tableArr[repeat-3].codeNo
  //            }else
  //            codeno = this.tableArr[repeat-2].codeNo
  //        }else
  //        codeno = this.tableArr[repeat-1].codeNo
  //      }else
  //        codeno = this.tableArr[repeat].codeNo

  //       this.http.get(this.global.api+'StudentPortal/ConflictSchedules/'+this.global.userinfo.idNumber+'/'+codeno+'/'+this.tableArr[repeat].day+'/'+this.tableArr[repeat].time+'/'+this.global.sy,this.global.option)
  //           .map(response => response.json())
  //           .subscribe(res2 => {
  //             this.tempconflict = this.tempconflict + res2.data.lenth;
  //             this.http.get(this.global.api+'StudentPortal/EnrolledSubjects/Code No/'+codeno+'/'+this.global.sy+'/true',this.global.option)
  //               .map(response => response.json())
  //               .subscribe(res => {
  //                 console.log(res)
  //                 for (var i = 0; i < res.data.length; ++i) {
  //                   if ((res.data[i].oe+res.data[i].res)>=res.data[i].classSize) {
  //                      this.tempclasssize++;
  //                   }
  //                 }
  //                   this.checkconflict(repeat-1)
  //               });
  //           },Error=>{
  //            this.global.swalAlertError();
  //           });
  //     }else{
  //       var warns=''
  //       if (this.tempconflict>0) {
  //          this.tempconflict=0;
  //          warns=warns+"*Conflicting schedules found!<br>"
  //       }
  //       if (this.tempclasssize>0) {
  //       //if (this.tempclasssize!=0&&warns=='') {
  //          this.tempclasssize=0;
  //         warns=warns+'Set is full and no available set for your course and year.'

  //       }

  //       if (warns=='') {
  //         this.admitstud()
  //       }else{
  //          this.global.swalClose()
  //           this.checkenroll= 'false'
  //          this.global.swalAlert("Please notify your Program Chair to resolve this issue.",warns,'warning')

  //       }
  //     }
  //  }
  //   enrollcode(repeat){
  //    if (repeat>=0) {
  //      if (this.tableArr[repeat].codeNo=='') {
  //         this.enrollcode(repeat-1)
  //      }else{
  //        this.http.post(this.global.api+'StudentPortal/EnrolledSubjects/'+this.global.userinfo.idNumber+'/'+ this.tableArr[repeat].codeNo +'/'+this.global.sy,{},this.global.option)
  //           .map(response => response.text())
  //           .subscribe(res => {
  //             this.enrollcode(repeat-1)
  //           },Error=>{
  //             this.global.swalAlertError()
  //           });
  //      }
  //     }else{
  //       this.updateinfo()
  //     }
  //   }

  enrollStudent(){
    // console.log("enrollStudent")
    this.api.apiAdmitEnrollStudent(this.setvar,this.global.userinfo.idNumber,this.global.enrollmentsy,this.global.studinfo.programID,this.getyear(this.global.studinfo.yearOrGradeLevel))
        .subscribe(res => {
          // console.log(res)
          if(res.data[0].code == 5){
            this.global.swalAlert('Conratulations!',res.data[0].message,'success')
            this.updateinfo(res.data[0].message);
          }else
            this.global.swalAlert('Please contact your Program Chair to resolve this issue:',res.data[0].message,'warning')
        },Error=>{
          this.global.swalAlertError(Error);
          console.log(Error);
        });
  }
   updateinfo(message){
       this.http.put(this.global.api+'StudentPortal/ContactInfo/'+this.global.userinfo.idNumber,{
          "mobileNo": this.cno,
          "telNo": this.tno,
          "emailAddress": this.email,
          "permAddressNoStreet": this.permNoStreet,
          "permAddressPSGC": this.permPSGC,
          "currAddressNoStreet": this.currNoStreet,
          "currAddressPSGC": this.currPSGC,
          "landLordLady": "",
          "boardingPhoneNo": ""
       },this.global.option)
         .map(response => response.json())
          .subscribe(res => {
            this.global.swalClose()
            this.global.getallapi(1,message)
          },Error=>{
            this.global.swalAlertError()
            console.log(Error)
          });
   }

  displaycs(x){
  	if (x=='S') {
  		return 'Single'
  	}
  	if (x=='M') {
  		return 'Married'
  	}
  	if (x=='W') {
  		return 'Widow'
  	}
  }
  displayg(x){
  	if (x=='M') {
  		return 'Male'
  	}
  	if (x=='F') {
  		return 'Female'
  	}
  }

      openDialog(lookup): void {
        const dialogRef = this.dialog.open(AddressLookupComponent, {
          width: '500px', disableClose: true
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.result!='cancel') {
            if (lookup==1) {
              this.permPSGC = result.data;
              this.homeaddress = result.result;
            }else{
              this.currPSGC = result.data;
              this.boardingaddress = result.result;
            }
          }
        });
      }
      getyear(x){
        return x;
      }
  confirmenroll(){
    this.swalConfirm("You are about to enroll.","Please confirm to proceed!",'question','Confirm');
  }

  swalConfirm(title,text,type,button)
  {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          this.enroll()
        }
      })
  }

  swalConfirmsetfull(title,text,type)
  {
    swal({
        title: title,
        text: text,
        type: type,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "OK"
      }).then((result) => {
        if (result.value) {
          this.start()
        }
      })
  }
 ViewPrint(): void {
        const dialogRef = this.dialog.open(EnrolmentFormComponent, {
          width: '75%', data:{gender:this.global.userinfo.gender,id:this.global.requestid(),name: this.global.userinfo.lastName +", "+this.global.userinfo.firstName+" "+this.global.userinfo.middleName+" "+this.global.userinfo.suffixName,course:this.global.studinfo.course,major:this.global.studinfo.major,year:this.global.studinfo.yearOrGradeLevel,subjects:this.global.ClassSchedule}, disableClose: false
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result==undefined) {
            // code...
          }else
          if (result.result!='cancel') {
          }
        });
      }

ifcheck(codeNo){
  return this.subjExists(codeNo)
}
subjectstoenroll:any=[]
totunit=0
subjectstoenrolldisplay:any=[]
checksubj(z,units){
  var cond
   var max = 0
  if (this.subjectstoenrolldisplay.length!=0) {
    for (var i = 0; i < this.subjectstoenrolldisplay.length; ++i) {
     max = max + parseInt(this.subjectstoenrolldisplay[i].units)
    }
  }

  if (this.subjExists(z)) {
    for (var i = this.subjectstoenroll.length - 1; i >= 0; --i) {
        if (this.subjectstoenroll[i].codeNo == z) {
            this.subjectstoenroll.splice(i,1);
        }
    }
  }else{
     if (max<9) {
      for (var i = 0; i < this.tableArr.length; ++i) {
        if (z==this.tableArr[i].codeNo) {
          if (max<=6) {
           this.subjectstoenroll.push(this.tableArr[i])
          }
        }
      }
    }else{
       this.global.swalAlert("Cannot add subject.","maximum units has been reached!",'warning')
    }
  }
   this.subjectstoenrolldisplay = []
   for (var i = 0; i < this.subjectstoenroll.length; ++i) {
       var subjectId = this.subjectstoenroll[i].subjectId
       var units2 = this.subjectstoenroll[i].units
       var codeNo = this.subjectstoenroll[i].codeNo
       var subjectTitle = this.subjectstoenroll[i].subjectTitle

     if (i>0) {
       var x=i
       var check = 1
         if (codeNo==this.subjectstoenroll[x-1].codeNo) {
            codeNo=''
            units2=0
            subjectId=''
            subjectTitle=''
          }
            if (i>1){
              if (codeNo==this.subjectstoenroll[x-2].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
            if (i>2){
              if (codeNo==this.subjectstoenroll[x-3].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
            if (i>4){
              if (codeNo==this.subjectstoenroll[x-4].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
            if (i>4){
              if (codeNo==this.subjectstoenroll[x-5].codeNo) {
                codeNo=''
                units2=0
                subjectId=''
                subjectTitle=''
              }
            }
     }
     this.subjectstoenrolldisplay.push({
          classSize: this.subjectstoenroll[i].classSize,
          codeNo: codeNo,
          day: this.subjectstoenroll[i].day,
          detailId: this.subjectstoenroll[i].detailId,
          headerId: this.subjectstoenroll[i].headerId,
          roomNumber: this.subjectstoenroll[i].roomNumber,
          subjectId: subjectId,
          subjectTitle: subjectTitle,
          time: this.subjectstoenroll[i].time,
          units: units2
        })

   }
   this.totunit = 0
   for (var i = 0; i < this.subjectstoenrolldisplay.length; ++i) {
    this.totunit = this.totunit + parseInt(this.subjectstoenrolldisplay[i].units)
   }

}

subjExists(codeno) {
  return this.subjectstoenroll.some(function(el) {
    return el.codeNo === codeno;
  });
}
}

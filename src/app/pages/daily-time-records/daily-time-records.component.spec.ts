import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyTimeRecordsComponent } from './daily-time-records.component';

describe('DailyTimeRecordsComponent', () => {
  let component: DailyTimeRecordsComponent;
  let fixture: ComponentFixture<DailyTimeRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyTimeRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyTimeRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

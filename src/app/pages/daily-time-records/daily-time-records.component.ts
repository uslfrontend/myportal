import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Component,ViewChild, OnInit,ElementRef } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-daily-time-records',
  templateUrl: './daily-time-records.component.html',
  styleUrls: ['./daily-time-records.component.css']
})
export class DailyTimeRecordsComponent implements OnInit {

  lastdate=''
  edate=''
  sdate=''

  currentSDate
  currentEDate
  curmonth
  curyear
  monthlasday

  ///////////////////PAGINATION VARIABLES///////////////////////
  dtrCtr = 0;
  dtrConfig: any;
  collection = { count: 60, data: [] };
  //////////////////////////////////////////////////////////////


  constructor(public dialog: MatDialog,private domSanitizer: DomSanitizer,public global: GlobalService,private http: Http,private datePipe : DatePipe) { 

    this.dtrConfig = {
      itemsPerPage: 15,
      currentPage: 1,
      totalItems: this.dtrCtr
    };

  }

  ngOnInit() {
    this.getSdateEdate();
    this.getdtr();
  }


  getdtr(){
    this.global.swalLoading("");
    this.http.get(this.global.api+'EmployeePortal/DTR/'+this.global.requestid()+'/'+this.currentSDate+'/'+this.currentEDate,this.global.option)
                  .map(response => response.json())
                  .subscribe(res => {
                    this.global.empDtrArray=res.data
                    //console.table(res.data)
                    this.global.swalClose();

      },Error=>{
      this.global.swalAlertError();
    });  
  }

  getSdateEdate(){
    this.curmonth = (new Date().getMonth() + 1).toString();
    this.curyear = new Date().getFullYear().toString();
    this.getlastday(this.curyear,this.curmonth);
    this.currentSDate = this.curmonth.toString()+'-1-'+this.curyear.toString();
    this.currentEDate = this.curmonth.toString()+'-'+this.monthlasday.toString()+'-'+this.curyear.toString();

    //console.log("Start Date: "+this.currentSDate+"\n"+"End Date: "+this.currentEDate)
  }

  getlastday(syear,smonth){
    var lastday = function(y,m){
      return  new Date(y, m, 0).getDate();
      }
    this.monthlasday = lastday(syear,smonth)
    //console.log(this.monthlasday)
  }


  setLastDate(lastdate){
    this.lastdate = lastdate;
    //console.log(this.lastdate)
    return lastdate
  }

  checkDTR(){
    if(this.sdate!=""&&this.edate!=""){
      /*console.log(this.sdate)
      console.log(this.edate)*/
      this.currentSDate = this.datePipe.transform(this.sdate, 'MM-dd-yyyy');
      this.currentEDate = this.datePipe.transform(this.edate, 'MM-dd-yyyy');

      // console.log(this.currentSDate)
      // console.log(this.currentEDate)
      this.getdtr();

    }
    else
    {
      //console.log("aaa")
    }
  }


  dtrPageChanged(event){
      this.dtrConfig.currentPage = event;
    }

}

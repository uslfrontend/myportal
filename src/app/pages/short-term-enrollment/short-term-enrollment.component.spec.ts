import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortTermEnrollmentComponent } from './short-term-enrollment.component';

describe('ShortTermEnrollmentComponent', () => {
  let component: ShortTermEnrollmentComponent;
  let fixture: ComponentFixture<ShortTermEnrollmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortTermEnrollmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortTermEnrollmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AddressLookupComponent } from './../address-lookup/address-lookup.component';
import {Router} from "@angular/router";
import Swal from 'sweetalert2';
import { EnrolmentFormComponent } from './../enrolment-form/enrolment-form.component';
import { StudentApiService } from './../../student-api.service';
import * as moment from 'moment'
const swal = Swal;


import { HsEnrollmentFamilyBackgroundComponent } from './../hs-enrollment/hs-enrollment-family-background/hs-enrollment-family-background.component';
import { HsEnrollmentFamilyBackgroundFamilymemberComponent } from './../hs-enrollment/hs-enrollment-family-background-familymember/hs-enrollment-family-background-familymember.component';
import { HsEnrollmentFamilyBackgroundGuardianComponent } from './../hs-enrollment/hs-enrollment-family-background-guardian/hs-enrollment-family-background-guardian.component';
import { EducationalBcgComponent } from './../../popup/educational-bcg/educational-bcg.component';
import {Http, Headers, RequestOptions} from '@angular/http';

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {startWith} from 'rxjs/operators';
import { map } from 'rxjs/operators';

import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

// import * as pdfMake from "pdfmake/build/pdfmake";
// import * as pdfFonts from 'pdfmake/build/vfs_fonts';
// import { DomSanitizer } from '@angular/platform-browser'; //activate this batch of codes for admission form printing

// (<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-short-term-enrollment',
  templateUrl: './short-term-enrollment.component.html',
  styleUrls: ['./short-term-enrollment.component.scss']
})
export class ShortTermEnrollmentComponent implements OnInit {
permPSGC=''
homeaddress=''
currPSGC=''
boardingaddress=''
pbirth = '';
tno=''
cno=''
email=''
currNoStreet=''
permNoStreet=''
gender=''
setarray
tableArr=[]
inactive=[]
setfiltered=[]
setvar=''
warndisplay3=[]
major=''

displaysem=''
studinfo
ClassSchedule=[]
agree1=false
agree2=false
agree3=false
bdate='';
age='';

arrayschool
educarray=[]

personalInfo = [{
  cno: "",
  email:"",
  homeaddress:"",
  preSchool_name:"",
  preSchool_yrGraduated:"",
  preSchool_schoolAddress:"",
  elementary_name:"",
  elementary_yrGraduated:"",
  elementary_schoolAddress:"",
  jhs_name:"",
  jhs_yrGraduated:"",
  jhs_schoolAddress:"",
  shs_name:"",
  shs_yrGraduated:"",
  shs_schoolAddress:"",

}];

enrollmentSY

//////////filtered options for educ background



myControl = new FormControl();
filteredOptions: Observable<string[]>;
options: string[] =[]

private _filter(value: string): string[] {
  const filterValue = value.toLowerCase();
  return this.options.filter(option => option.toLowerCase().includes(filterValue));
}

constructor(public dialog: MatDialog,public global: GlobalService,private router: Router,public api:StudentApiService,private http: Http) {
  this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
}
triggerNextStudInfo = false
  ngOnInit(){

    for (let i3 = 0; i3 < this.global.sysettings.length; ++i3) {
      if (this.global.sysettings[i3].onlineEnrollment == 1) {
        this.enrollmentSY = this.global.sysettings[i3].schoolYear;
        break;
      }
    }

    if(this.global.nextSYStudInfo){
      if(this.global.nextSYStudInfo.enrollmentStatus=='Admitted'||this.global.nextSYStudInfo.enrollmentStatus=='Paid'){
        this.triggerNextStudInfo = true;
      }
    }
    this.global.swalLoading("Checking payment status")


    this.api.getDownpaymentStatus()
      .subscribe(res => {
        if (res.data) {
          this.global.swalClose();
          this.proceed();
        }else{
          this.global.swalAlert("Notice","Please <strong style='color:red'>settle your downpayment</strong> for you to proceed in your enrolment.<br><br><b>Note:</b><br>If <b>downpayment</b> is already settled in our other payment facilities, click here: <b><a href='https://bit.ly/usldownpaymentverification'>https://bit.ly/usldownpaymentverification</a></b> for verification.",'warning')

          window.history.back();
        }


      },Error=>{
        this.global.swalClose();
        this.global.swalAlertError(Error);
     });



  }
  proceed(){
    this.api.apiPublicAPISchools()
    .subscribe(res => {
      this.arrayschool = res
      for (var i = 0; i < this.arrayschool.length; ++i) {
          this.options.push(this.arrayschool[i].companyName)
      }
      this.calculateAge(this.global.userinfo.dateOfBirth);
      if(this.global.studentIsInList){
        this.start();
      }else{
        this.start();
        // this.api.apiPreEnrollmentSubmittedRequirement(this.global.colpreenrollmentyear)
        //   .subscribe(res => {
        //     if (res.data.length==0) {
        //           swal({
        //             title: "Please proceed for Admission first!",
        //             type:'warning',
        //             confirmButtonText: `OK`,
        //           }).then((result) => {
        //             window.history.back();
        //           })

        //     }else{
        //         var x=''
        //         if (res.data[0].idPicture_Status==0) {
        //         x=x+"ID picture is on Process<br>"
        //         }
        //         if (res.data[0].idPicture_Status==1) {
        //         x=x+"ID picture is not Verified ("+res.data[0].idPicture_Remarks+")<br>"
        //         }
        //         if (res.data[0].signatureStatus==0) {
        //           x=x+"Digital signature is on Process<br>"
        //         }
        //         if (res.data[0].signatureStatus==1) {
        //           x=x+"Digital signature is not Verified ("+res.data[0].signatureRemarks+")<br>"
        //         }
        //         if (res.data[0].birthCert_Status==0) {
        //         x=x+"Birth certificate is on Process<br>"
        //         }
        //         if (res.data[0].birthCert_Status==1) {
        //         x=x+"Birth certificate is not Verified ("+res.data[0].birthCert_Remarks+")<br>"
        //         }
        //         if (res.data[0].reportCardStatus==0) {
        //           x=x+"Report card is on Process<br>"
        //         }
        //         if (res.data[0].reportCardStatus==1) {
        //           x=x+"Report card is not Verified ("+res.data[0].reportCardRemarks+")<br>"
        //         }
        //         if (res.data[0].proofOfPayment_Status==0) {
        //         x=x+"Proof of payment is on Process<br>"
        //         }
        //         if (res.data[0].proofOfPayment_Status==1) {
        //         x=x+"Proof of payment is not Verified ("+res.data[0].proofOfPayment_Remarks+")<br>"
        //         }

        //         if(x!=''){
        //           swal({
        //             title: "Admission process not yet verified.",
        //             html:x,
        //             type:'warning',
        //             confirmButtonText: `OK`,
        //           }).then((result) => {
        //             window.history.back();
        //           })
        //         }else{
        //           this.start()
        //         }
        //       }
        //   },Error=>{
        //     this.global.swalAlertError(Error);
        //   });
      }

      },Error=>{
        this.global.swalAlertError(Error);
      });
  }
  start() {
  this.global.swalLoading('');
  this.global.apiStudentInfo(this.enrollmentSY)
      .subscribe(res => {
        this.studinfo = res.data;
        this.permPSGC=''
        this.homeaddress=''
        this.currPSGC=''
        this.boardingaddress=''
        this.tno=''
        this.cno=''
        this.email=''
        this.currNoStreet=''
        this.permNoStreet=''
        this.gender=''
        this.setarray
        this.tableArr=[]
        this.inactive=[]
        this.setfiltered=[]
        this.setvar=''
        this.checkenroll=true
        this.warndisplay3=[]
        this.loadfambg();

        this.global.apiClassSchedule(this.global.requestid(),this.enrollmentSY)
          .subscribe(res => {
              this.ClassSchedule=res.data
              console.log(this.ClassSchedule);

              if (this.studinfo.course == ''||this.studinfo.course == null|| this.global.getplacement) {
                 this.global.apiPlacement()
                    .subscribe(res => {
                      if (res.data.preferredCourseId!=null&&res.data.preferredCourseId!='') {
                        this.studinfo.yearOrGradeLevel=1
                        if (res.data.gResult==null||res.data.gResult==''||res.data.gResult==' '||res.data.result=="P") {
                          this.studinfo.programID=res.data.preferredCourseId
                        }else{
                          this.studinfo.programID=res.data.gResult
                        }

                        this.api.apiStudentPortalCurriculum(this.studinfo.programID)
                          .subscribe(res => {
                            this.studinfo.course=res.data.courseCode
                            this.coursesval=res.data.courseCode
                            this.studinfo.major=res.data.major
                            this.loaddata();
                          })
                        }else{
                          this.global.swalAlert("Warning","No placement record for College enrollment",'warning')
                          window.history.back();
                        }
                  })
               }else{
                 this.loaddata();
               }
          },Error=>{
            this.global.swalAlertError(Error);
            window.history.back();
          });
    },Error=>{
        this.global.swalAlertError(Error);
        window.history.back();
      });
  }
  calculateAge(birthdate: any) {
    var x = moment().diff(birthdate, 'years');
    if(x.toString() == 'NaN'  || x == undefined)
      this.age = '';
    else
      this.age = x.toString();
  }
  loaddata(){

      if (this.ClassSchedule.length==0) {
      var filt2=''
      var filt3=''
      if (this.studinfo.blacklistedStopped!=0) {
       filt2 = filt2 + '*Blacklisted (Stopped)<br>'
      }
      if (this.studinfo.blacklistedViolation!=0) {
       filt2 = filt2 + '*Blacklisted (Violation)<br>'
      }
      // if (this.global.requireAdmission==-1&&this.studinfo.enrollmentStatus=="For Admission") {
      //   filt3 = filt3 + '*Admission is closed. <br>'
      // }
      if (filt3 != '') {
        this.global.swalAlert('We are currently undergoing systems maintenance activity. For enrolment-related transactions, please try again later.',filt3,'warning')
        window.history.back();
      }else if (filt2 != '') {
        this.global.swalAlert('<h5 style="margin: 0 0 0 0;">Please contact <a href="mailto:dsas@usl.edu.ph">dsas@usl.edu.ph</a> or <a href="tel:09563078723">09563078723</a> for your admission to resolve the ff. </h5>',filt2,'warning')
        window.history.back();
      }else{
        var indicate=0
            if (this.global.userdemog!=undefined) {
              var x=this.global.userdemog
              this.permPSGC=x.permPSGC
              this.homeaddress=x.homeAddress
              this.personalInfo[0].homeaddress = this.homeaddress
              this.pbirth = x.placeOfBirth
              this.currPSGC=x.currPSGC
              this.boardingaddress=x.currentAddress
              this.tno=x.telNo

              this.cno=x.mobileNo
              this.personalInfo[0].cno = this.cno

              this.email=x.emailAdd
              this.personalInfo[0].email = this.email

              this.currNoStreet=x.currNoStreet
              this.permNoStreet=x.permNoStreet
              if (this.global.oldshs_enroll) {
                this.global.majors=[]
                this.startenroll()
              }else
              if (this.global.firstyear&&this.global.majors.length>0) {
                this.global.swalClose()
              }else{
               this.testenter()
              }


            }
        }
      }else{
       this.global.swalClose()
      }
  }


courses=[]
coursesval=''
startenroll(){
  this.api.apiOnlineRegistrationCoursesWithStrand()
      .subscribe(res => {
        this.global.swalClose()
          this.courses=[]
          for (var i = 0; i < res.data.length; ++i) {
           if (res.data[i].programLevel=="50") {
             this.courses.push(res.data[i])
           }
         }
         this.checkcourse()
      })
}
progid=''
year=''
course=''
sydisplay=''
checkcourse(){
  this.tableArr=[]
  this.major=''
  for (var i = 0; i < this.courses.length; ++i) {
    if (this.courses[i].courseCode==this.coursesval) {
      this.progid=this.courses[i].programId
      this.year='1'
      this.course=this.courses[i].courseCode
      this.studinfo.major = ''
      break
    }
  }

  if(this.coursesval!=''){
    var resto = this.coursesval.replace("/", "-");
    this.global.apiCourseMajors(resto)
        .subscribe(res => {
          this.global.majors=[]
          this.global.swalClose()
          for (var i2 = 0; i2 < res.data.length; ++i2) {
            if (res.data[i2].major!='') {
              this.global.majors.push(res.data[i2])
            }
          }
          if (this.global.majors.length==0) {
            this.testenter()
          }
        })
  }

}

checkmajor(){
  this.studinfo.programID=this.major.toString()
  for (var i = 0; i < this.global.majors.length; ++i) {
    if (this.major == this.global.majors[i].programId) {
      this.progid = this.global.majors[i].programId
      this.studinfo.major = this.global.majors[i].major
      this.testenter()
      break
    }
  }
}

///////////////////////////////////////////////////////////////
checkparent(){
  var count=0
  for (var i = 0; i < this.familyarray.length; ++i) {
    if (this.familyarray[i].relDesc=='Mother'||this.familyarray[i].relDesc=='Father') {
      count++
    }
  }
  if (count>1) {
    return true
  }
  return  false
}
checkedubackground(){
   if (this.sgfId=='') {
     return true
   }
   if (this.average=='') {
     return true
   }
   if (this.sgfsy=='') {
     return true
   }
   return false
}

checkbasicinfo(){
   if (this.global.userinfo.religion==null||this.global.userinfo.religion=='') {
     return true
   }
   if (this.global.studinfo.placeOfBirth==null||this.global.studinfo.placeOfBirth=='') {
     return true
   }
   return false
}

checkcontactinfo(){
   if (this.cno==null||this.cno=='') {
     return true
   }
   if (this.homeaddress==null||this.homeaddress=='') {
     return true
   }
   return false
}

checkpersonalinfo(){
     if (this.global.userdemog.ethnicity==''||this.global.userdemog.ethnicity==null) {
      return true
     }
     if (this.global.userdemog.languageSpoken==''||this.global.userdemog.languageSpoken==null) {
      return true
     }
     if (this.global.userdemog.motherTongue==''||this.global.userdemog.motherTongue==null) {
      return true
     }
     if (parseInt(this.global.userdemog.numberOfSibling)==0) {
      return true
     }
     if (parseInt(this.global.userdemog.birthOrder)==0) {
      return true
     }
   return false
}
/////////////////////////////////////////////////////////////////

  testenter(){
      this.global.swalLoading('')
        var x = this.studinfo.yearOrGradeLevel
         if (this.global.setarray!=null) {
           this.setfiltered=[]
           for (var i = 0; i < this.global.setarray.length; ++i) {
             let z
             if (this.global.oldshs_enroll) {
              var tempcourse = this.course.replace(/\s/g, "");
              z= this.global.setarray[i].course.replace(/\s/g, "")==tempcourse.replace('-','')+this.studinfo.major.replace(/\s/g, "")&&this.global.setarray[i].yearLevel=='1'
             }else
               z=this.global.setarray[i].course.replace(/\s/g, "")==this.studinfo.course.replace(/\s/g, "")+this.studinfo.major.replace(/\s/g, "")&&this.global.setarray[i].yearLevel==x

            if (z) {
              this.setfiltered.push(this.global.setarray[i])
            }
           }
         }
         if (this.setfiltered.length==0) {
          this.global.swalAlert('Please see your Program Chair.','*No Available Set for your course and year.','warning')
          // window.history.back();
         }else{
             this.setvar=this.setfiltered[0].headerId.toString()
             this.selectheader(this.setfiltered[0].headerId.toString(),0)
         }

  }

  checkenroll=true
   selectheader(id,index){
    this.tableArr=undefined
    this.api.apiSetDetails(id)
          .subscribe(res => {
            var condition=false
            var subjectId=''
            var codeNo=''
            var subjectTitle=''
            var units=''
            this.tableArr=[]
            var full = false
            for (var i = 0; i < res.data.length; ++i) {
              condition=false
              subjectId = res.data[i].subjectId
              units = res.data[i].units
              codeNo = res.data[i].codeNo
              subjectTitle = res.data[i].subjectTitle
              if (i!=0) {
                if (codeNo==res.data[i-1].codeNo) {
                  codeNo=''
                  units=''
                }
                if (subjectId==res.data[i-1].subjectId) {
                  subjectId=''
                  units=''
                }
                if (subjectTitle==res.data[i-1].subjectTitle) {
                  subjectTitle=''
                  units=''
                }
              }

              if (this.tableArr==undefined) {
                this.tableArr=[]
               }

              this.tableArr.push({
                classSize: res.data[i].classSize,
                codeNo: codeNo,
                day: res.data[i].day,
                detailId: res.data[i].detailId,
                headerId: res.data[i].headerId,
                roomNumber: res.data[i].roomNumber,
                subjectId: subjectId,
                subjectTitle: subjectTitle,
                time: res.data[i].time,
                units: units
              })
              if (res.data[i].availableSlot<=0) {
                if ((index+1)>=this.setfiltered.length){
                    swal({
                        title:'Please proceed to your Program Chair. All available sets are full!',
                        html:'',
                        type:'warning',
                        confirmButtonText: `OK`,
                      }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        window.history.back();
                      })
                   break
                }else{
                  this.setvar=this.setfiltered[index+1].headerId.toString()
                  this.selectheader(this.setfiltered[index+1].headerId,index+1)
                  break
                }
              }else{
                condition=true
              }
            }
            if (res.data.length==0) {
              if ((index+1)>=this.setfiltered.length) {
                swal({
                        title:'Please proceed to your Program Chair. All available sets are full!',
                        html:'',
                        type:'warning',
                        confirmButtonText: `OK`,
                      }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        window.history.back();
                      })
              }else{
                this.setvar=this.setfiltered[index+1].headerId.toString()
                this.selectheader(this.setfiltered[index+1].headerId,index+1)
              }
            }
            if (condition) {
              if (this.setfiltered[index]!=undefined) {
                this.setvar=this.setfiltered[index].headerId.toString()
                var setf=this.setfiltered[index]
                this.setfiltered=[]
                this.setfiltered.push(setf)
                this.checkenroll= false
              }
                this.global.swalClose()
            }
          },Error=>{
           this.tableArr=[]
            //console.log(Error);
            console.log(Error)
            this.global.swalAlertError(Error);
         });
   }

  enroll(){
    var x=''
    if (this.checkenroll==true) {
      x = x+"*Select Set!<br>"
    }
    if (this.global.userinfo.gender==''||this.global.userinfo.gender==null) {
      x = x+"*Basic Information - Gender is required!<br>"
    }if (this.global.userinfo.civilStatus==''||this.global.userinfo.civilStatus==null) {
      x = x+"*Basic Information - Civil Status is required!<br>"
    }if (this.global.studinfo.dateOfBirth==''||this.global.studinfo.dateOfBirth==null) {
      x = x+"*Basic Information - Birthday is required!<br>"
    }if (this.global.studinfo.placeOfBirth==''||this.global.studinfo.placeOfBirth==null) {
      x = x+"*Basic Information - Place of birth is required!<br>"
    }if (this.global.userinfo.religion==''||this.global.userinfo.religion==null) {
      x = x+"*Basic Information - Religion is required!<br>"
    }if (this.global.userinfo.nationality==''||this.global.userinfo.nationality==null) {
      x = x+"*Basic Information - nationality is required!<br>"
    }



    if (this.cno==''||this.cno==null) {
      x = x+"*Contact Information - Phone number is required!<br>"
    }
    if (this.permNoStreet==''||this.permNoStreet==null) {
      x = x+"*Contact Information - Home Address Street is required!<br>"
    }
    if (this.currNoStreet==''||this.currNoStreet==null) {
      x = x+"*Contact Information - Current Address Street is required!<br>"
    }
    if (this.homeaddress==''||this.homeaddress==null) {
      x = x+"*Contact Information - Home address is required!<br>"
    }
    if (this.email==''||this.email==null) {
      x = x+"*Contact Information - Email is required!<br>"
    }
    if (this.global.userdemog.skillTalent==''||this.global.userdemog.skillTalent==null) {
      x = x+"*Other Information - Special Abilities/Expertise/Talents is required!<br>"
    }
    if (this.global.userdemog.languageSpoken==''||this.global.userdemog.languageSpoken==null) {
      x = x+"*Other Information - Language Spoken is required!<br>"
    }
    if (this.global.userdemog.motherTongue==''||this.global.userdemog.motherTongue==null) {
      x = x+"*Other Information - Mother Tongue is required!<br>"
    }
    if (this.global.userinfo.numberOfSibling=='') {
      x = x+"*Other Information - Number of Siblings is required!<br>"
    }
    if (parseInt(this.global.userdemog.birthOrder)==0) {
      x = x+"*Other Information - Birth Order must not be 0!<br>"
    }



     if (x!='') {
       this.global.swalAlert("Warning",x,'warning')
     }else{
      this.checkenroll= true
      this.global.swalLoading('');
      // this.checkconflict(this.tableArr.length-1
      this.enrollStudent();
     }
   }
  //  admitstud(){
  //    var progid = this.studinfo.programID
  //    var year = this.studinfo.yearOrGradeLevel
  //    if (this.global.oldshs_enroll) {
  //      progid = this.progid
  //      year = this.year
  //    }
  //     this.api.apiAdmitStudentPost(this.global.userinfo.idNumber,this.global.enrollmentsy,progid,this.getyear(year))
  //     .subscribe(res => {
  //       this.successcode=[]
  //       this.scodewarn=''
  //       this.enrollcode(this.tableArr.length-1)
  //     },Error=>{
  //       this.global.swalAlertError(Error);
  //     });
  //  }
  //  tempconflict=0
  //  tempclasssize=0
  //  checkconflict(repeat){
  //    if (repeat>=0) {
  //      var codeno=''
  //      if (this.tableArr[repeat].codeNo=='') {
  //        if (this.tableArr[repeat-1].codeNo==''){
  //          codeno = this.tableArr[repeat-2].codeNo
  //          if (this.tableArr[repeat-2].codeNo==''){
  //              codeno = this.tableArr[repeat-3].codeNo
  //            }else
  //            codeno = this.tableArr[repeat-2].codeNo
  //        }else
  //        codeno = this.tableArr[repeat-1].codeNo
  //      }else
  //        codeno = this.tableArr[repeat].codeNo

  //           this.api.apiConflictSchedules(this.global.userinfo.idNumber,codeno,this.tableArr[repeat].day,this.tableArr[repeat].time,this.global.enrollmentsy)
  //           .subscribe(res2 => {
  //             this.tempconflict = this.tempconflict + res2.data.length;
  //               this.api.apiEnrolledSubjectsCodeNo(codeno,this.global.enrollmentsy)
  //               .subscribe(res => {
  //                 for (var i = 0; i < res.data.length; ++i) {
  //                   if ((res.data[i].oe+res.data[i].res)>=res.data[i].classSize) {
  //                      this.tempclasssize++;
  //                   }
  //                 }
  //                   this.checkconflict(repeat-1)
  //               });
  //           },Error=>{
  //            this.global.swalAlertError(Error);
  //           });
  //     }else{
  //       var warns=''
  //       if (this.tempconflict>0) {
  //          this.tempconflict=0;
  //          warns=warns+"*Conflicting schedules found!<br>"
  //       }
  //       if (this.tempclasssize>0) {
  //       //if (this.tempclasssize!=0&&warns=='') {
  //          this.tempclasssize=0;
  //         warns=warns+'Set is full and no available set for your course and year.'

  //       }

  //       if (warns=='') {
  //         this.admitstud()
  //       }else{
  //          this.global.swalClose()
  //          this.checkenroll= false
  //          this.global.swalAlert("Please notify your Program Chair to resolve this issue.",warns,'warning')
  //          window.history.back();
  //       }
  //     }
  //  }
  //   successcode=[]
  //   scodewarn=''
  //   enrollcode(repeat){
  //    if (repeat>=0) {
  //      if (this.tableArr[repeat].codeNo=='') {
  //         this.enrollcode(repeat-1)
  //      }else{
  //         this.api.apiEnrolledSubjectsPost(this.global.userinfo.idNumber,this.tableArr[repeat].codeNo,this.global.enrollmentsy)
  //           .subscribe(res => {
  //             if (res.message == 'Success') {
  //               this.successcode.push(this.tableArr[repeat].codeNo)
  //               this.enrollcode(repeat-1)
  //             }
  //             else{
  //               this.scodewarn=this.scodewarn+"*"+res.message+"<br>"
  //               this.enrollcode(repeat-1)
  //             }
  //           },Error=>{
  //             this.global.swalAlertError(Error);
  //           });
  //      }
  //     }else{
  //       if (this.scodewarn=='') {
  //         this.updateinfo()
  //       }else{
  //         this.deletecodeloop(this.successcode.length-1)
  //       }
  //     }
  //   }

  //   deletecodeloop(repeat){
  //    if (repeat>=0) {
  //       this.api.apiEnrolledSubjectsDelete(this.global.requestid(),this.successcode[repeat],this.global.enrollmentsy)
  //       .subscribe(res => {
  //          this.deletecodeloop(repeat-1)
  //       },Error=>{
  //         this.global.swalAlertError(Error);
  //       });
  //     }else{
  //       this.api.apiEnrollmentHistoryDelete(this.global.enrollmentsy)
  //         .subscribe(res => {
  //            swal({
  //               title:"Enrolment failed due to the ff.",
  //               html:this.scodewarn,
  //               type:'warning',
  //               confirmButtonText: `OK`,
  //             }).then((result) => {
  //               /* Read more about isConfirmed, isDenied below */
  //               window.history.back();
  //             })
  //         },Error=>{
  //         this.global.swalAlertError(Error);
  //         });
  //     }
  //   }
  SHStoCollegeProgramID = "";
  setProgramIDforSHStoCollege(programId){
    this.SHStoCollegeProgramID = programId;
  }
  enrollStudent(){
    var finalProgramID = ''
    var yearlevel

    if(this.global.studentIsInList){
      finalProgramID = this.SHStoCollegeProgramID
      yearlevel = 1;
    }else{
      finalProgramID = this.global.studinfo.programID
      yearlevel = this.getyear(this.global.studinfo.yearOrGradeLevel)
    }



    if(finalProgramID == "" || finalProgramID == null || finalProgramID == undefined)
      finalProgramID = this.progid

    if(yearlevel == 0)
      yearlevel = 1;


    this.api.apiAdmitEnrollStudent(this.setvar,this.global.userinfo.idNumber,this.enrollmentSY,finalProgramID,yearlevel)
        .subscribe(res => {
          if(res.data[0].code == 5){
            // this.global.swalAlert('Conratulations!',res.data[0].message,'success')
            // this.global.swalAlert('Congratulations!',"",'success')
            this.updateinfo(res.data[0].message);
          }else
            this.global.swalAlert('Please contact your Program Chair/Concern Department to resolve this issue:',res.data[0].message,'warning')
        },Error=>{
          this.global.swalAlertError(Error);
          console.log(Error);
        });
  }

   updateinfo(message){
      this.api.apiContactInfo(this.cno,this.tno,this.email,this.permNoStreet,this.permPSGC,this.currNoStreet,this.currPSGC)
        .subscribe(res => {
          this.global.swalClose()
          this.global.getallapi(1,message)
          this.getNextSYStudinfo()
        },Error=>{
          this.global.swalAlertError(Error);
        });
   }

  getNextSYStudinfo(){

  }

  displaycs(x){
  	if (x=='S') {
  		return 'Single'
  	}
  	if (x=='M') {
  		return 'Married'
  	}
  	if (x=='W') {
  		return 'Widow'
  	}
  }
  displayg(x){
  	if (x=='M') {
  		return 'Male'
  	}
  	if (x=='F') {
  		return 'Female'
  	}
  }

      openDialog(lookup,type): void {
        const dialogRef = this.dialog.open(AddressLookupComponent, {
          width: '500px', disableClose: true
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.result!='cancel') {

            if (lookup==1) {
              if(type == 'pbirth'){
                this.global.studinfo.placeOfBirth = result.result;
              }else{
                this.permPSGC = result.data;
                this.homeaddress = result.result;
              }
            }
            else{
              this.currPSGC = result.data;
              this.boardingaddress = result.result;
            }
          }
        });
      }
      getyear(x){
        return x;
      }
  confirmenroll(){
    this.swalConfirm("You are about to enroll.","Please confirm to proceed!",'question','Confirm');
  }

  swalConfirm(title,text,type,button)
  {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          this.enroll()
          // console.log("1")
        }
      })
  }

  swalConfirmsetfull(title,text,type)
  {
    swal({
        title: title,
        text: text,
        type: type,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "OK"
      }).then((result) => {
        if (result.value) {
          this.start()

        }
      })
  }
 ViewPrint(): void {
        const dialogRef = this.dialog.open(EnrolmentFormComponent, {
          width: '75%', data:{
            x:'col',
            gender:this.global.userinfo.gender,
            id:this.global.requestid(),
            name: this.global.userinfo.lastName +", "+this.global.userinfo.firstName+" "+this.global.userinfo.middleName+" "+this.global.userinfo.suffixName,
            course:this.studinfo.course,
            major:this.studinfo.major,
            year:this.studinfo.yearOrGradeLevel,
            subjects:this.global.ClassSchedule}, disableClose: false
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result==undefined) {
            // code...
          }else
          if (result.result!='cancel') {
          }
        });
      }

  /////////////////////////////////////
  familyarray=[]
  familyarrayinfo=[]
  fatherInfo;
  motherInfo;
  guardianInfo;
  sgfAddress=''
  sgfId=''
  average=''
  sgfsy=''
  // slaSy=''
  // slaId=''
  // lsaAddress=''
  slaplaceholder=''

  preSchool_name = '';
  preSchool_yrGraduated = '';
  preSchool_schoolAddress = '';

  elementary_name = '';
  elementary_yrGraduated = '';
  elementary_schoolAddress = '';

  jhs_name = '';
  jhs_yrGraduated = '';
  jhs_schoolAddress = '';

  shs_name = '';
  shs_yrGraduated = '';
  shs_schoolAddress = '';


  loadfambg(){
    this.api.apiStudentPortalFamilyBG()
    .subscribe(res => {
      this.familyarray=res.data
      this.familyarray.forEach(element => {

        if(element.relDesc == "Father"){

          this.api.apiStudentPortalgetParentInfoBG(element.memberIdNumber.toString())
                .subscribe(res => {
                    this.fatherInfo = [];
                    this.fatherInfo = res.data
                    this.global.familyarrayinfo.push({
                      parentName: this.fatherInfo.parentName,
                      occupation: this.fatherInfo.occupation,
                      officeAddress: this.fatherInfo.officeAddress,
                      contactNo: this.fatherInfo.cellphoneNo,
                      ofw: this.fatherInfo.ofw,
                      relDesc: element.relDesc
                    })
                  });
        }else if(element.relDesc == "Mother"){
          this.api.apiStudentPortalgetParentInfoBG(element.memberIdNumber)
          .subscribe(res => {
              this.motherInfo = [];
              this.motherInfo = res.data
              this.global.familyarrayinfo.push({
                parentName: this.motherInfo.parentName,
                occupation: this.motherInfo.occupation,
                officeAddress: this.motherInfo.officeAddress,
                contactNo: this.motherInfo.cellphoneNo,
                ofw: this.motherInfo.ofw,
                relDesc: element.relDesc
              })
            },Error=>{
              console.log(Error)
            });

        } else if (element.relDesc=='Brother/Sister'||element.relDesc=='Guardian'||element.relDesc=='Cousin'||element.relDesc=='Uncle/Aunt'||element.relDesc=='Grand Parent'||element.relDesc=='StepFather'||element.relDesc=='StepMother'||element.relDesc=='Spouse'){
          this.api.apiStudentPortalgetParentInfoBG(element.memberIdNumber.toString())
                  .subscribe(res => {
                      this.guardianInfo = [];
                      this.guardianInfo = res.data
                      this.global.familyarrayinfo.push({
                        parentName: this.guardianInfo.parentName,
                        occupation: this.guardianInfo.occupation,
                        officeAddress: this.guardianInfo.officeAddress,
                        contactNo: this.guardianInfo.cellphoneNo,
                        ofw: this.guardianInfo.ofw,
                        relDesc: element.relDesc
                      })
                    });
        }
      });

      this.loadeducbg()
    });
  }
  loadeducbg(){
    this.api.apiStudentPortalEducBG()
      .subscribe(res => {

          if (res.data[0]!=undefined) {
            this.educarray = res.data
            this.educarray.forEach(element => {
              if(element.programLevel == 10){//elementary

                this.elementary_name = element.schoolName;
                this.elementary_yrGraduated = element.yearGraduated;
                this.elementary_schoolAddress = element.otherInfo;
                this.personalInfo[0].elementary_name = this.elementary_name;
                this.personalInfo[0].elementary_yrGraduated = this.elementary_yrGraduated;
                this.personalInfo[0].elementary_schoolAddress = this.elementary_schoolAddress;

              }
              else if(element.programLevel == 20){//secondary, jhs and shs
                if(element.programName == 'Senior High School'){

                  this.shs_name = element.schoolName;
                  this.shs_yrGraduated = element.yearGraduated;
                  this.shs_schoolAddress = element.otherInfo;

                  this.personalInfo[0].shs_name = this.shs_name;
                  this.personalInfo[0].shs_yrGraduated = this.shs_yrGraduated;
                  this.personalInfo[0].shs_schoolAddress = this.shs_schoolAddress;
                }else{

                  this.jhs_name = element.schoolName;
                  this.jhs_yrGraduated = element.yearGraduated;
                  this.jhs_schoolAddress = element.otherInfo;

                  this.personalInfo[0].jhs_name = this.jhs_name;
                  this.personalInfo[0].jhs_yrGraduated = this.jhs_yrGraduated;
                  this.personalInfo[0].jhs_schoolAddress = this.jhs_schoolAddress;

                }
              }else if(element.programLevel == 0){//pre-school
                this.preSchool_name = element.schoolName;
                this.preSchool_yrGraduated = element.yearGraduated;
                this.preSchool_schoolAddress = element.otherInfo;

                this.personalInfo[0].preSchool_name = this.preSchool_name;
                this.personalInfo[0].preSchool_yrGraduated = this.preSchool_yrGraduated;
                this.personalInfo[0].preSchool_schoolAddress = this.preSchool_schoolAddress;
              }
            });
          }
          // console.log(this.educarray)
      });

  }
  deletefamily(id){
    this.swalConfirmdelete("Are you sure?","You won't be able to revert this!",'warning','Delete Family Member Info','Family Member Info has been Removed','deletefamily',id);
  }
  deleteeduc(id){
      this.swalConfirmdelete("Are you sure?","You won't be able to revert this!",'warning','Delete Educational Background','Educational Background has been Removed','deleteeb',id);
  }

  swalConfirmdelete(title,text,type,button,successm,remove,id){
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          if (remove=='deletefamily') {
          this.global.swalLoading('');
            this.api.apiStudentPortalFamilyMemberDelete(id)
              .subscribe(res => {
                this.global.swalSuccess(successm);
                this.loadfambg();
              },Error=>{
                this.global.swalAlertError(Error);
              });
          }
          else
          if (remove=='deleteeb') {
          this.global.swalLoading('');
            this.api.apiStudentPortalEducBGDelete(id)
              .subscribe(res => {
                this.global.swalSuccess(successm);
                this.loadeducbg();
              },Error=>{
                this.global.swalAlertError(Error);
              });
          }
        }
      })
  }

  openedit(x,y){
    if (y=='Mother'||y=='Father') {
      this.openDialogAddParent(x)
    }
    else if (y=='Brother/Sister'||y=='Guardian'||y=='Cousin'||y=='Uncle/Aunt'||y=='Grand Parent'||y=='StepFather'||y=='StepMother'||y=='Spouse'){
      this.openDialogguardian(x)
    }else{
      this.openDialogfamilymember(x)
    }
  }

  openDialogAddParent(lookup): void {
    const dialogRef = this.dialog.open(HsEnrollmentFamilyBackgroundComponent, {
    width: '600px', disableClose: false, data: {id:this.global.userinfo.idNumber,kind:lookup},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result!=undefined) {
      if (result.result=='cancel') {
      }
      if (result.result=='saved') {
       this.loadfambg();
      }
    }
    });
  }

  openDialogguardian(lookup): void {
    const dialogRef = this.dialog.open(HsEnrollmentFamilyBackgroundGuardianComponent, {
    width: '600px', disableClose: false, data: {id:this.global.userinfo.idNumber,kind:lookup},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result!=undefined) {
        if (result.result=='cancel') {
        }
        if (result.result=='saved') {
          this.loadfambg();
        }
    }
    });
  }

  openDialogfamilymember(lookup): void {
      const dialogRef = this.dialog.open(HsEnrollmentFamilyBackgroundFamilymemberComponent, {
      width: '600px', disableClose: false, data: {id:this.global.userinfo.idNumber,kind:lookup},
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
        if (result.result=='cancel') {
        }
        if (result.result=='saved') {
          this.loadfambg();
        }
      }
      });
  }

  openDialogAddEducationBackground(lookup): void { //open dialog for educ bcg popup
    const dialogRef = this.dialog.open(EducationalBcgComponent, {
    width: '600px', disableClose: false, data: {type:1, data:''}
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result!=undefined) {
      if (result.result=='cancel') {
      }
      if (result.result=='saved') {
       this.loadeducbg();
      }
    }
    });
  }

  updateEducBcg(params){//dialog box for updating educ bcg info
    const dialogRef = this.dialog.open(EducationalBcgComponent, {
      width: '600px', disableClose: false, data: {type:2, selecteddata:params}
      });

      dialogRef.afterClosed().subscribe(result => {

        if (result!=undefined) {
        if (result.result=='cancel') {
        }
        if (result.result=='saved') {
         this.loadeducbg();
        }
      }
      });
  }

  ////////////////FILTERED OPTIONS GET INDEX
  getindex(x){
    var index = this.options.indexOf(x);
    if (this.arrayschool[index].address == null) {
      this.sgfAddress = ''
    }else
      this.sgfAddress = this.arrayschool[index].address
    this.sgfId = this.arrayschool[index].companyID
  }

  /////////////QUICK SAVES
  quickSaveBasicInformation(){
    var pdate = new Date(this.global.userdemog.dateOfBirth).toLocaleString();
    if(pdate=="Invalid Date"){
      pdate=''
    }
    var x=''
    if (this.global.userinfo.gender==''||this.global.userinfo.gender==null) {
      x = x+"*Basic Information - Gender is required!<br>"
    }if (this.global.userinfo.civilStatus==''||this.global.userinfo.civilStatus==null) {
      x = x+"*Basic Information - Civil Status is required!<br>"
    }if (this.global.studinfo.dateOfBirth==''||this.global.studinfo.dateOfBirth==null) {
      x = x+"*Basic Information - Birthday is required!<br>"
    }if (this.global.studinfo.placeOfBirth==''||this.global.studinfo.placeOfBirth==null) {
      x = x+"*Basic Information - Place of birth is required!<br>"
    }if (this.global.userinfo.religion==''||this.global.userinfo.religion==null) {
      x = x+"*Basic Information - Religion is required!<br>"
    }if (this.global.userinfo.nationality==''||this.global.userinfo.nationality==null) {
      x = x+"*Basic Information - nationality is required!<br>"
    }



    if (this.cno==''||this.cno==null) {
      x = x+"*Contact Information - Phone number is required!<br>"
    }
    if (this.permNoStreet==''||this.permNoStreet==null) {
      x = x+"*Contact Information - Home Address Street is required!<br>"
    }
    if (this.currNoStreet==''||this.currNoStreet==null) {
      x = x+"*Contact Information - Current Address Street is required!<br>"
    }
    if (this.homeaddress==''||this.homeaddress==null) {
      x = x+"*Contact Information - Home address is required!<br>"
    }
    if (this.email==''||this.email==null) {
      x = x+"*Contact Information - Email is required!<br>"
    }
    if (x!='') {
      this.global.swalAlert("Warning",x,'warning')
    }else{
      this.api.apiStudentPortalPersonInfoPut(
      this.global.userinfo.idNumber,
      this.global.userinfo.firstName,
      this.global.userinfo.middleName,
      this.global.userinfo.lastName,
      this.global.userinfo.suffixName,
      this.global.studinfo.placeOfBirth,
      this.global.userinfo.religion,
      pdate,
      this.global.userdemog.gender,
      this.global.userdemog.civilStatus,
      this.global.userinfo.idNumber,
      this.cno,
      this.tno,
      this.email,
      this.global.userinfo.nationality)
      .subscribe(res => {
        // this.global.swalSuccess(res.message)
        this.quickSaveContactInformation();

      },Error=>{
        this.global.swalAlertError(Error)
        console.log(Error)
      });
    }
  }

  quickSaveContactInformation(){

      this.global.swalLoading('');
      this.api.apiContactInfo(this.cno,this.tno,this.email,this.permNoStreet,this.permPSGC,this.currNoStreet,this.currPSGC)
      .subscribe(res => {
          this.global.swalSuccess(res.message)
      },Error=>{
        this.global.swalAlertError(Error)
        console.log(Error)
      });

  }

  quickSaveSacramentsReceivedPut(x=null){
    this.api.apiStudentPortalSacramentsReceivedPut(this.global.userdemog.baptism,this.global.userdemog.confession,this.global.userdemog.communion,this.global.userdemog.confirmation,this.global.userdemog.religiousInstruction)
      .subscribe(res => {
        if (x==1) {
          // this.quickSavePersonalInfo(1)
          //this.global.getallapi(2)
        }else{
          this.global.swalSuccess(res.message)
        }
      },Error=>{
        this.global.swalAlertError(Error);
      });
  }

  quickSaveOtherInformation(){
    var xy=''
    if (this.global.userdemog.skillTalent==''||this.global.userdemog.skillTalent==null) {
      xy = xy+"*Other Information - Special Abilities/Expertise/Talents is required!<br>"
    }
    if (this.global.userdemog.languageSpoken==''||this.global.userdemog.languageSpoken==null) {
      xy = xy+"*Other Information - Language Spoken is required!<br>"
    }
    if (this.global.userdemog.motherTongue==''||this.global.userdemog.motherTongue==null) {
      xy = xy+"*Other Information - Mother Tongue is required!<br>"
    }
    if (this.global.userdemog.numberOfSibling=='') {
      xy = xy+"*Other Information - Number of Siblings is required!<br>"
    }
    if (parseInt(this.global.userdemog.birthOrder)==0) {
      xy = xy+"*Other Information - Birth Order must not be 0!<br>"
    }

    if (xy!='') {
      this.global.swalAlert("Warning",xy,'warning')
    }else{
      this.global.swalLoading('');
      this.api.apiStudentPortalPersonalInfoPut(
            this.global.userdemog.numberOfSibling,
            this.global.userdemog.birthOrder,
            this.global.userdemog.skillTalent,
            this.global.userdemog.contestToJoin,
            this.global.userdemog.physicalDefect,
            this.global.userdemog.healthProblem,
            this.global.userdemog.medicalAdvise,
            this.global.userdemog.nearestNeighbor,
            this.global.userdemog.ethnicity,
            this.global.userdemog.motherTongue,
            this.global.userdemog.languageSpoken,
            this.global.userdemog.generalAverage,
            this.global.userdemog.neatScore)
          .subscribe(res => {
              this.global.swalSuccess(res.message);
          },Error=>{
            this.global.swalAlertError(Error);
          });
    }
  }

  /////////////////////////////////////////////ADMISSION FORM
  PrintAdmissionForm(){

  }

  ///////////////////////////////////////////STUDENT INFORMATION SHEET
  PrintStudentInformationSheet(){

    var guardianName ='';
    var guardianOccupation ='';
    var guardianAddress ='';
    var guardianCno ='';
    var SY = this.global.syDisplaynoSemester(this.global.sy)
    var startRowX = 53, startColY = 159;
    var that = this;
    var bday:any = new Date(this.studinfo.dateOfBirth);
        // var dd = String(bday.getDate()-1).padStart(2, '0');
        var dd = String(bday.getDate()).padStart(2, '0');
        var mm = String(bday.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = bday.getFullYear();
        bday = mm + '/' + dd + '/' + yyyy;

        var dob = bday
        var today = new Date();
        var birthDate = new Date(dob);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

    var gender = "Male";

    if(this.studinfo.gender == "F")
        gender = "Female";
    else
        gender = "Male";

    var civilStatus = "Single";
    if(this.global.userinfo.civilStatus == "M")
        civilStatus = "Married"
    else if(this.global.userinfo.civilStatus == "SP")
        civilStatus = "Single Parent"
    else
        civilStatus = "Single"


    var baptism
    var confession
    var holyCommunion
    var confirmation

    var sibling=[]
    var lastdigit = 543
      for (var i5 = 0; i5 < this.familyarray.length; ++i5) {
        if(this.familyarray[i5].relDesc == "Sibling"){
          if(i5==0){
            lastdigit+=26
            sibling.push( {
                text: 'IDNumber: _____________ Name:_______________________________________',
                absolutePosition: { x: 225, y: lastdigit+=141.5 }
              })
            sibling.push( {
                text: '\u200B\t                 '+this.familyarray[i5].memberIdNumber +'                       '+this.familyarray[i5].fullName ,
                absolutePosition: { x: 225, y: lastdigit },
                fontSize:9.75
              })
          }
          else if(i5%2===1){
            lastdigit+=12
            sibling.push( {
                text: 'IDNumber: ________ Name:____________________________',
                absolutePosition: { x: 50, y: lastdigit }
              })
            sibling.push( {
                text: '\u200B\t                    '+this.familyarray[i5].memberIdNumber +'                    '+this.familyarray[i5].fullName ,
                absolutePosition: { x: 50, y: lastdigit },
                fontSize:8
              })
          }else {
            sibling.push( {
                text: 'IDNumber: ________ Name:_______________________________',
                absolutePosition: { x: 300, y: lastdigit }
              })
            sibling.push( {
                text: '\u200B\t                   '+this.familyarray[i5].memberIdNumber +'                  '+this.familyarray[i5].fullName ,
                absolutePosition: { x: 305, y: lastdigit },
                fontSize:9.75
              })
          }
        }

      }




    var header = {
      table:{
        widths: [350,200],
        body:[
          [
            {
                stack:[
                    {text:'UNIVERSITY OF SAINT LOUIS\nTuguegarao City\nOFFICE OF THE STUDENT AFFAIRS AND SERVICES\n\n',fontSize:10,alignment:'center'},
                    {text:'\nADMISSION FORM FOR INCOMING\nFIRST YEAR STUDENTS, TRANSFEREES & CROSS_ENROLEES',bold:true,fontSize:10,alignment:'center'}
                ]

            },
            {
              table:{
                width:[145],
                body:[
                  [{text: "Document No.: FM-SAS-005\n\n\nRevision No.:00\n\n\nEffectivityDate: January 18, 2021"}]
                ]
              }
            }
          ]
        ]
      },
			layout: 'noBorders'
    }
     var addr = that.motherInfo.officeAddress;
    // var addr = 'asdsad';
    function checkAddressLength(){
      if(addr.length<50){
        return {text:'\n'};
      }
    }

    var docDefinition = {
      pageSize:'FOLIO',
      pageMargins: [ 30, 30, 30, 20 ],
      content: [
        header,
        {
          text:[
            '\n',
            {text:SY,bold:true,decoration:'underline',alignment:'center'},
            {text:' /Short Term ______\n\n',bold:true,alignment:'center'},
          ],bold:true,alignment:'center'
        },
        {
            table:{
                widths:[220,313],
                body:[
                    [
                        {text:'ID #:  '+that.studinfo.idNumber+'   Course & Year: '+that.studinfo.course+" - "+that.studinfo.yearOrGradeLevel},
                        {
                            stack:[
                                {
                                    canvas: [
                                        {
                                            type: 'polyline',
                                            lineWidth: 1,
                                            closePath: true,
                                            points: [{ x: 2, y: 2}, { x: 9, y: 2 }, { x: 9, y: 9 }, { x: 2, y:9 }]
                                        }
                                    ]
                                },
                                {
                                  canvas: [
                                        {
                                            type: 'polyline',
                                            lineWidth: 1,
                                            closePath: true,
                                            points: [{ x: 149, y: -7}, { x: 156, y: -7 }, { x: 156, y: 0 }, { x: 149, y:0 }]

                                        }
                                    ]
                                },
                                {
                                  canvas: [
                                        {
                                            type: 'polyline',
                                            lineWidth: 1,
                                            closePath: true,
                                            points: [{ x: 232, y: -7}, { x: 225, y: -7 }, { x: 225, y: 0 }, { x: 232, y:0 }]

                                        }
                                    ]
                                },
                                {
                                    text: 'Incoming 1st Year College',
			                        absolutePosition: { x: 283, y: 145 }
                                },
                                {
                                    text: 'Transferee',
			                        absolutePosition: { x: 425, y: 145 }
                                },
                                {
                                    text: 'Cross-Enrollee',
			                        absolutePosition: { x:500, y: 145 }
                                }

                            ]
                        }
                    ]
                ]
            },layout: 'noBorders'
        },
        {
          table:{
            widths:[540],
            body:[
              [
                {
                  stack:[
                    {text: 'PERSONAL INFORMATION\n', bold: true},
                    {
                      stack:[
                          {
                              table: {
                                widths:[53,45,53,65,70,55,70,20,30],
                                body: [
                                  [
                                    {text:'Name:'},
                                    {text: that.studinfo.lastName+", "+that.studinfo.firstName+" "+that.studinfo.middleName,bold:true, colSpan:4},
                                    {text:''},
                                    {text:''},
                                    {text:''},
                                    {text:'Birth Date:'},
                                    {text:bday,bold:true},
                                    {text:'Age:'},
                                    {text:age,bold:true}
                                  ],
                                  [
                                    {text:'Gender:'},
                                    {text:gender,bold:true},
                                    {text:'Civil Status:'},
                                    {text:civilStatus,bold:true},
                                    {text:'Birth Place:'},
                                    {text:that.studinfo.placeOfBirth,bold:true, colSpan:4},
                                    {text:'',bold:true},
                                    {text:'',bold:true},
                                    {text:'',bold:true}
                                  ],
                                  [
                                    {text:'Citizenship:'},
                                    {text:that.global.userinfo.nationality,bold:true},
                                    {text:'CP No.:'},
                                    {text:that.cno,bold:true},
                                    {text:'Email Address:'},
                                    {text:that.email,bold:true, colSpan:4},
                                    {text:'',bold:true},
                                    {text:'',bold:true},
                                    {text:'',bold:true}
                                  ]

                              ]
                            },
                            layout: {
                                      hLineWidth: function() {
                                      // Here you can use ternary operator or if condtions to change its value according to row and column
                                        return 0;
                                      },
                                      vLineWidth: function() {
                                        // Here you can use ternary operator or if condtions to change its value according to row and column
                                        return 0;
                                      }
                                  }
                          },

                          {
                              table: {
                                  widths:[130,386],
                              body: [
                                [
                                    {text:'Permanent Home Address:'},
                                    {text:that.homeaddress,bold:true},

                                ]

                              ]
                            },
                            layout: {
                                      hLineWidth: function() {
                                      // Here you can use ternary operator or if condtions to change its value according to row and column
                                        return 0;
                                      },
                                      vLineWidth: function() {
                                        // Here you can use ternary operator or if condtions to change its value according to row and column
                                        return 0;
                                      }
                                  }
                          },
                      ],
                    },
                    {
                      table:{
                        widths:[50,240,55,160],
                        body: [
                          parents_father(),
                          parents_father_other_info(),
                          parents_mother(),
                          parents_mother_other_info()
                        ]
                      },layout: {hLineWidth: function() {return 0;},vLineWidth: function() {return 0;}}
                    },
                    checkAddressLength(),
                    {text:'GUARDIAN RESPONSIBLE FOR THE FINANCIAL SUPPORT IN COLLEFE (IF NOT PARENTS)', bold: true},
                    {
                      table:{
                        widths:[40,240,60,165],
                        body: guardian()
                      },layout: {hLineWidth: function() {return 0;},vLineWidth: function() {return 0;}}
                    },
                    {text:'EMPLOYER (IF WORKING STUDENT)', bold: true},
                    {
                      table:{
                        widths:[40,240,60,165],
                        body:[
                          [
                            {text:'Name: '},
                            {
                              text:'_____________________________________________________',
                              bold:true
                            },
                            {text:'Occupation: '},
                            {
                              text:'____________________________________',
                              bold:true
                            }
                          ],
                          [
                            {text:'Address: '},
                            {
                              text:'_____________________________________________________',
                              bold:true
                            },
                            {text:'Contact No: '},
                            {
                              text:'____________________________________',
                              bold:true
                            }
                          ]
                        ]
                      },layout: {hLineWidth: function() {return 0;},vLineWidth: function() {return 0;}}
                    },

                    {text:'LANDLORD/LANDLADY (IF STAYING IN A BOARDING HOUSE)', bold: true},
                    {
                      table:{
                        widths:[40,240,60,165],
                        body:[
                          [
                            {text:'Name: '},
                            {
                              text:'_____________________________________________________',
                              bold:true
                            },
                            {text:'Occupation: '},
                            {
                              text:'____________________________________',
                              bold:true
                            }
                          ]
                        ]
                      },layout: {hLineWidth: function() {return 0;},vLineWidth: function() {return 0;}}
                    },
                    {
                      table:{
                        widths:[120,403],
                        body:[
                          [
                            {text:'Boarding House Address:'},
                            {
                              text:'__________________________________________________________________________________________',
                              bold:true
                            }
                          ]
                        ]
                      },layout: {hLineWidth: function() {return 0;},vLineWidth: function() {return 0;}}
                    },

                    {text: '\nSCHOOL ATTENDED', bold: true},
                    {
                        table:{
                            body:[
                                [
                                    {text:'Pre-School:                __________________________________________________________________'},
                                    {text:'Yr. Graduated: _________________'}
                                ],
                                [
                                   {text:'Address:                     ___________________________________________________________________________________________________',colSpan:2}
                                ],
                                [
                                    {text:'Elementary School:  _________________________________________________________________'},
                                    {text:'Yr. Graduated: _________________'}
                                ],
                                [
                                   {text:'Address:                     ___________________________________________________________________________________________________',colSpan:2}
                                ],
                                [
                                    {text:'Junior High School:  _________________________________________________________________'},
                                    {text:'Yr. Graduated: _________________'}
                                ],
                                [
                                   {text:'Address:                     ___________________________________________________________________________________________________',colSpan:2}
                                ],
                                [
                                    {text:'Senior High School:  _________________________________________________________________'},
                                    {text:'Yr. Graduated: _________________'}
                                ],
                                [
                                   {text:'Address:                     ___________________________________________________________________________________________________',colSpan:2}
                                ]
                            ]
                        },layout: 'noBorders'
                    },
                    {text:'\nLAST SCHOOL/COLLEGE ATTENDED (IF TRANSFEREE)'},
                    {text:'Name of School:       __________________________________________________________________   School Year: __________________'},
                    {text:'Course & Year:           __________________ Address: ______________________________________________________________________'},


                    {text:'\nOTHER INFORMATION\n\n', bold:true},
                    {text:'Number of Siblings: ____________   Your Birth Order: ____________    Religion: __________________________'},
                    {text:'Sacraments Received:          Baptism  (      )      Confession  (      )     Holy Communion  (      )     Confirmation (      ) '},
                    {text:'Did you received Religious Instruction during High School:     Yes (   )  No (   )'},
                    {text:'Brothers/Sisters studying in this school:'},
                    {text:'  '},
                    {text:'Special Abilities/Experties/Talents: ____________________________________________________________________________________'},
                    {text:'Language Spoken: _____________________________________________________    Dialect: _____________________________________'},
                    {text:'Contest you want to join: ______________________________________________________________________________________________'},
                    {text:'Physical Difficulties: __________________________________________________________________________________________________'},
                    {text:'Health Concern: ______________________________________________________________________________________________________'},


                    {text:['\nCREDENTIALS PRESENTED',{text:' (to be filled out by OSAS)\n\n', italics:true, bold: false}], bold:true},
                    {
                        table:{
                            widths:[170,170,170],
                            body:[
                                [
                                  {text:'(  ) Form 137'},
                                  {text:'(  ) GMC Certificate'},
                                  {text:'(  ) Honorable Dismissal'}
                                ],
                                [
                                  {text:'(  ) Form 138 (Gr. 12 Card'},
                                  {text:'(  ) Certificate of Live Birth from PSA'},
                                  {text:'(  ) Certificate of True Copy of Grades'}
                                ],
                                [
                                  {text:'(  ) USL Placement Exam Result'},
                                  {text:'(  ) Church Marriage Certificate (if married)'},
                                  {text:'(  ) Police Clearance'}
                                ],
                                [
                                  {text:'(  ) Affidavit of Non - Cohabitation / Solo Parent ID (if single parent)',colSpan:2},
                                  {text:''},
                                  {text:'(  ) Recommendation from any USL employee'}
                                ]
                            ]
                        },fontSize: 8,layout: 'noBorders'
                    },
                    {text:'\n*S - Single, M - Married, SP - Single Parent',italics:true,bold:true}
                  ]
                }
              ]
            ]
          },fontSize:9.75,layout:{
            hLineWidth: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 2 : 1;
            },
            vLineWidth: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 2 : 1;
            }
          }
        },
        schoolAttended(),
        otherInformation()

      ],
      footer:function(currentPage, pageCount, pageSize) {
            return [
                {text:'',alignment:'right',fontSize:9,margin:[0,0,40,0]}//currentPage+' of '+pageCount
            ]
        },
        defaultStyle: {
          fontSize: 10
        }

    }


    function parents_father(){
      var  parents = [
        {text: 'FATHER: '},
        {text:that.fatherInfo.parentName,bold:true},
        {text: 'Occupation: '},
        {text:that.fatherInfo.occupation,bold:true}
      ];
      return parents;
    }
    function parents_father_other_info(){
      var  parents = [
        {text: 'Address: '},
        {text:that.fatherInfo.officeAddress,bold:true},
        {text: 'Contact No: '},
        {text:that.fatherInfo.cellphoneNo,bold:true}
      ];
      return parents;
    }
    function parents_mother(){
      var  parents = [
        {text: 'MOTHER: '},
        { text:that.motherInfo.parentName,bold:true},
        {text: 'Occupation: '},
        {text:that.motherInfo.occupation,bold:true}
      ];
      return parents;
    }

    function parents_mother_other_info(){
      var  parents = [
        {text: 'Address: '},
        {text:addr, bold:true},
        {text: 'Contact No: '},
        {text:that.motherInfo.cellphoneNo,bold:true}
      ];
      return parents;
    }



    function guardian(){
      var res = [
        [
          {text:'Name: '},
          {
            text:that.guardianInfo.parentName,
            bold:true
          },
          {text:'Occupation: '},
          {
            text:that.guardianInfo.occupation,
            bold:true
          }
        ],
        [
          {text:'Address: '},
          {
            text:that.guardianInfo.officeAddress,
            bold:true
          },
          {text:'Contact No: '},
          {
              text:that.guardianInfo.cellphoneNo,
              bold:true
          }
        ]

      ]
      return res
    }

    function schoolAttended(){
      var res = {
        stack:[
          {
              text:that.preSchool_name,//pre-school name
              absolutePosition: { x:startRowX+74, y: startColY+=314 },
              bold:true
          },
          {
              text:that.preSchool_yrGraduated,//yr graduated
              absolutePosition: { x:startRowX+444, y: startColY},
              bold:true
          },
          {
              text:that.preSchool_schoolAddress,//address
              absolutePosition: { x:startRowX+74, y: startColY+=15 },
              bold:true
          },
          {
              text:that.elementary_name,// elem name
              absolutePosition: { x:startRowX+74, y: startColY+=16 },
              bold:true
          },
          {
              text:that.elementary_yrGraduated,
              absolutePosition: { x:startRowX+444, y: startColY},
              bold:true
          },
          {
              text:that.elementary_schoolAddress,
              absolutePosition: { x:startRowX+74, y: startColY+=15 },
              bold:true
          },
          {
              text:that.jhs_name,//jhs name
              absolutePosition: { x:startRowX+74, y: startColY+=16 },
              bold:true
          },
          {
              text:that.jhs_yrGraduated,
              absolutePosition: { x:startRowX+444, y: startColY},
              bold:true
          },
          {
              text:that.jhs_schoolAddress,
              absolutePosition: { x:startRowX+74, y: startColY+=15 },
              bold:true
          },
          {
              text:that.shs_name,//shs name
              absolutePosition: { x:startRowX+74, y: startColY+=16 },
              bold:true
          },
          {
              text:that.shs_yrGraduated,
              absolutePosition: { x:startRowX+444, y: startColY},
              bold:true
          },
          {
              text:that.shs_schoolAddress,
              absolutePosition: { x:startRowX+74, y: startColY+=16 },
              bold:true
          },

        ]
      }
      return res
    }
    function otherInformation(){
      sacraments();
      var res = {
        stack:[
          {
              text:that.global.userdemog.numberOfSibling,// no. of siblings
              absolutePosition: { x:startRowX+74, y: startColY+=93 },
              bold:true
          },
          {
              text:that.global.userdemog.birthOrder,//birth order
              absolutePosition: { x:startRowX+210, y: startColY},
              bold:true
          },
          {
              text:that.global.userdemog.religion,//religion
              absolutePosition: { x:startRowX+315, y: startColY},
              bold:true
          },
          baptism,
          confession,
          holyCommunion,
          confirmation,
          sibling,

          {
            text: that.global.userdemog.skillTalent,//Special Abilities/
            absolutePosition: { x: startRowX+142, y: startColY+=56.8 },
            bold:true,
          },
          {
            text: that.global.userdemog.languageSpoken,//Language Spoken
            absolutePosition: { x: startRowX+67, y: startColY+=12 },
            bold:true,
          },
          {
            text: that.global.userdemog.motherTongue,//Dialect
            absolutePosition: { x: startRowX+352, y: startColY },
            bold:true,
          },
          {
            text: that.global.userdemog.contestToJoin,//Contest
            absolutePosition: { x: startRowX+95, y: startColY+=11 },
            bold:true,
          },
          {
            text: that.global.userdemog.physicalDefect,//Physical Difculties
            absolutePosition: { x: startRowX+75, y: startColY+=11.8 },
            bold:true,
          },
          {
            text: that.global.userdemog.healthProblem,//Health Concern
            absolutePosition: { x: startRowX+58, y: startColY+=11.8 },
            bold:true,
          },

        ]
      }

      return res
    }

    function sacraments(){
      if(that.global.userdemog.baptism){
        baptism = {
          text: '√',
          absolutePosition: { x: startRowX+150, y: 688 },
          bold:true,fontSize:13,
        }
      }
      if(that.global.userdemog.confession){
          confession = {
              text: '√',
              absolutePosition: { x: startRowX+241, y: 688 },
              bold:true,fontSize:13,
            }
      }

      if(that.global.userdemog.communion){
          holyCommunion = {
              text: '√',
              absolutePosition: { x: startRowX+354, y: 688 },
              bold:true,fontSize:13,
            }
      }

      if(that.global.userdemog.confirmation){
          confirmation = {
              text: '√',
              absolutePosition: { x: startRowX+452, y: 688 },
              bold:true,fontSize:13,
            }
      }
    }

    pdfMake.createPdf(docDefinition).open();
  }

}

import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddressLookupComponent } from './../address-lookup/address-lookup.component';
import { Router } from "@angular/router";
import Swal from 'sweetalert2';
import { EnrolmentFormComponent } from './../enrolment-form/enrolment-form.component';
import { HsEnrollmentFamilyBackgroundComponent } from './hs-enrollment-family-background/hs-enrollment-family-background.component';
import { HsEnrollmentFamilyBackgroundFamilymemberComponent } from './hs-enrollment-family-background-familymember/hs-enrollment-family-background-familymember.component';
import { HsEnrollmentFamilyBackgroundGuardianComponent } from './hs-enrollment-family-background-guardian/hs-enrollment-family-background-guardian.component';

import { startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { StudentApiService } from './../../student-api.service';
const swal = Swal;
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
@Component({
  selector: 'app-hs-enrollment',
  templateUrl: './hs-enrollment.component.html',
  styleUrls: ['./hs-enrollment.component.scss']
})
export class HsEnrollmentComponent implements OnInit {
  permPSGC = ''
  homeaddress = ''
  currPSGC = ''
  boardingaddress = ''
  tno = ''
  cno = ''
  email = ''
  currNoStreet = ''
  permNoStreet = ''
  gender = ''
  setarray
  tableArr = []
  inactive = []
  setfiltered = []
  setvar = ''
  warndisplay3 = []
  strandval = ''
  strand = '';
  strandfiltered = [];
  preference
  encryp = ''
  proofofPaymentData

  studinfo
  agree = false
  agree1 = false
  agree2 = false
  agree3 = false
  agree4 = false
  agree5 = false
  hspreenrollmentvar = false

  studentYearLevel
  cons_studentYearLevel



  myControl = new FormControl();
  filteredOptions: Observable<string[]>;
  options: string[] = []


  myControl2 = new FormControl();
  filteredOptions2: Observable<string[]>;
  options2: string[] = []


  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
  private _filter2(value: string): string[] {
    const filterValue2 = value.toLowerCase();
    return this.options2.filter(option => option.toLowerCase().includes(filterValue2));
  }


  arrayschool
  triggerNextStudInfo = '';
  nextSYStudInfoCourse = ''

  constructor(public dialog: MatDialog, public global: GlobalService, private router: Router, public api: StudentApiService) { }
  ngOnInit() {
    // this.checkPlacement()
    if (this.global.nextSYStudInfo)
      if (this.global.nextSYStudInfo.enrollmentStatus) {
        this.triggerNextStudInfo = this.global.nextSYStudInfo.enrollmentStatus
        this.nextSYStudInfoCourse = this.global.nextSYStudInfo.course
      }


    this.api.apiPublicAPISchools()
      .subscribe(res => {
        this.arrayschool = res
        for (var i = 0; i < this.arrayschool.length; ++i) {
          this.options.push(this.arrayschool[i].companyName)
          this.options2.push(this.arrayschool[i].companyName)
        }

        /////////////////////////
        // this.api.apiPreEnrollmentSubmittedRequirement(this.global.HSplacementyear)
        //   .subscribe(res => {
        //     if (res.data.length==0) {
        //       if ((this.global.hsfirstyear||this.global.oldjhs_shs)) {
        //           swal({
        //             title: "Please proceed for Pre-enrollment first!",
        //             type:'warning',
        //             confirmButtonText: `OK`,
        //           }).then((result) => {
        //             /* Read more about isConfirmed, isDenied below */
        //             window.history.back();
        //           })
        //       }else{
        //         this.start()
        //       }

        //     }else{
        //       // if (this.global.hsfirstyear) {
        //       //   var x=''
        //       //   if (res.data[0].idPicture_Status==0) {
        //       //    x=x+"ID Picture is on Process<br>"
        //       //   }
        //       //   if (res.data[0].idPicture_Status==1) {
        //       //    x=x+"ID Picture is not Verified ("+res.data[0].idPicture_Remarks+")<br>"
        //       //   }
        //       //   if (res.data[0].birthCert_Status==0) {
        //       //    x=x+"Birth Certificate is on Process<br>"
        //       //   }
        //       //   if (res.data[0].birthCert_Status==1) {
        //       //    x=x+"Birth Certificate is not Verified ("+res.data[0].birthCert_Remarks+")<br>"
        //       //   }
        //       //   if (res.data[0].proofOfPayment_Status==0) {
        //       //    x=x+"Proof of payment is on Process<br>"
        //       //   }
        //       //   if (res.data[0].proofOfPayment_Status==1) {
        //       //    x=x+"Proof of payment is not Verified ("+res.data[0].proofOfPayment_Remarks+")<br>"
        //       //   }

        //       //   if(x!=''){
        //       //     swal({
        //       //       title: "Pre-enrollment Process not yet verified.",
        //       //       html:x,
        //       //       type:'warning',
        //       //       confirmButtonText: `OK`,
        //       //     }).then((result) => {
        //       //       /* Read more about isConfirmed, isDenied below */
        //       //       window.history.back();
        //       //     })
        //       //   }else{
        //           // this.start()
        //         // }
        //       // }
        //       if(this.global.preenrollmenteligibleHS)
        //       {
        //         this.global.colpreenrollmentyear=this.global.HSplacementyear
        //         this.hspreenrollmentvar=true
        //         this.global.swalLoading("Checking payment status")
        //         this.api.getDownpaymentStatus()
        //           .subscribe(res => {
        //             if (!res.data) {
        //               this.global.swalClose();
        //               this.start()
        //             }else{
        //               this.global.swalAlert("Notice","Please <strong style='color:red'>settle your downpayment</strong> for you to proceed in your enrolment.",'warning')

        //               window.history.back();
        //             }


        //           },Error=>{
        //             this.global.swalClose();
        //             this.global.swalAlertError(Error);
        //         });
        //       }
        //     }
        //   },Error=>{
        //     this.global.swalAlertError(Error);
        //   });
        /////////////////////////
        var that = this;
        var status = "";
        function checkEnrollmentStatus() {
          that.global.apiStudentInfo(that.global.enrollmentsy).subscribe(res => {


            status = res.data.enrollmentStatus;
            if (status != "For Admission")
              that.start();
            else
              that.CheckDownPaymentStatus();

          });

        }

        this.global.colpreenrollmentyear = this.global.HSplacementyear
        this.hspreenrollmentvar = true
        this.global.swalLoading("Checking payment status")
        console.log(this.global.nextSYStudInfo);


        if (this.global.nextSYStudInfo) {
          if (this.global.nextSYStudInfo.departmentCode == "HS" && (this.global.nextSYStudInfo.yearOrGradeLevel != this.global.lastSYStudInfo.yearOrGradeLevel))
            this.start();

          if (this.global.nextSYStudInfo.departmentCode == "HS" && (this.global.nextSYStudInfo.yearOrGradeLevel == 5 || (this.global.nextSYStudInfo.yearOrGradeLevel >= 1 && this.global.nextSYStudInfo.yearOrGradeLevel < 4)))
            this.start();
          else if (this.global.nextSYStudInfo.enrollmentStatus == "Paid")
            checkEnrollmentStatus();
          else
            this.CheckDownPaymentStatus();

        } else if (this.global.nextSYStudInfo && this.global.studinfo) {
          if (this.global.studinfo.departmentCode == "HS" && (this.global.studinfo.yearOrGradeLevel == 5 || (this.global.studinfo.yearOrGradeLevel >= 1 && this.global.studinfo.yearOrGradeLevel < 4)))
            this.start();
          else
            this.CheckDownPaymentStatus();
        }
        else{
          if (this.global.enrollmenthist.length>1 && this.global.studinfo.departmentCode == "HS" && (this.global.studinfo.yearOrGradeLevel == 5 || (this.global.studinfo.yearOrGradeLevel >= 1 && this.global.studinfo.yearOrGradeLevel < 4))) {
            this.start();
          }else
            this.CheckDownPaymentStatus();
        }

      }, Error => {
        this.global.swalAlertError(Error);
      });

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

    this.filteredOptions2 = this.myControl2.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter2(value))
      );
  }
  CheckDownPaymentStatus() {
    this.api.getDownpaymentStatus()
      .subscribe(res => {
        if (res.data) {
          this.global.swalClose();
          if (this.global.preenrollmenteligibleHS || this.global.hsfirstyear)
            this.start();
        } else
          this.global.swalAlert("Notice", "Please <strong style='color:red'>settle your downpayment</strong> for you to proceed in your enrolment.<br><br><b>Note:</b><br>If <b>downpayment</b> is already settled in our other payment facilities, click here: <b><a href='https://bit.ly/usldownpaymentverification'>https://bit.ly/usldownpaymentverification</a></b> for verification.", 'warning')
      }, Error => {
        this.global.swalClose();
        this.global.swalAlertError(Error);
      });
  }
  start() {

    this.global.userdemog.generalAverage = ''
    this.global.swalLoading('');

    var year = this.global.enrollmentsy
    if (year.length == 7)
      year = year.substring(0, 6)

    this.global.apiStudentInfo(year)
      .subscribe(res => {
        this.global.swalClose()
        this.studinfo = res.data

        if (this.global.studinfo.yearOrGradeLevel == 4 && this.studentYearLevel == 1 || this.global.studinfo.departmentCode == 'HS') {
          this.studentYearLevel = this.global.studinfo.yearOrGradeLevel
        }
        if (this.global.studinfo.yearOrGradeLevel == 6 || this.global.studinfo.departmentCode == 'ELEM') {
          this.studentYearLevel = 1
        }

        this.permPSGC = ''
        this.homeaddress = ''
        this.currPSGC = ''
        this.boardingaddress = ''
        this.tno = ''
        this.cno = ''
        this.email = ''
        this.currNoStreet = ''
        this.permNoStreet = ''
        this.gender = ''
        this.setarray
        this.tableArr = []
        this.inactive = []
        this.setfiltered = []
        this.setvar = ''
        this.warndisplay3 = []
        var x = this.global.userdemog
        this.permPSGC = x.permPSGC
        this.homeaddress = x.homeAddress
        this.currPSGC = x.currPSGC
        this.boardingaddress = x.currentAddress
        this.tno = x.telNo
        this.cno = x.mobileNo
        this.email = x.emailAdd
        this.currNoStreet = x.currNoStreet
        this.permNoStreet = x.permNoStreet
        this.loadfambg()
        this.triggerNextStudInfo = res.data.enrollmentStatus
        if (this.studinfo.departmentCode == 'HS' && this.studentYearLevel == 6 && res.data.enrollmentStatus == "For Admission") {
          var filt2 = ''
          if (!this.global.oldshs_col) {
            filt2 = filt2 + '*Not in the list of SHS students to enroll in college<br>'
          }
          if (filt2 != '') {
            this.global.swalAlert('<h5 style="margin: 0 0 0 0;">Please contact <a href="mailto:dsas@usl.edu.ph">dsas@usl.edu.ph</a> or <a href="tel:09563078723">09563078723</a> for your admission to resolve the ff. </h5>', filt2, 'warning')
            window.history.back();
          }
        }
        // console.log(this.global.hsfirstyear, this.global.oldjhs_shs, res.data.enrollmentStatus)
        if ((this.global.hsfirstyear || this.global.oldjhs_shs) && res.data.enrollmentStatus != "Admitted") {
          this.hspreenrollment(this.studinfo)
        }
        else if (!this.global.hsfirstyear && res.data.enrollmentStatus != "Admitted") {
          //  if (this.studinfo.yearOrGradeLevel==4) {
          //    swal({
          //         title: '<h5 style="margin: 0 0 0 0;">Please contact the Guidance office for your admission to resolve the ff. </h5>',
          //         html:'No placement information found.<br>Student did not pre-enroll for '+this.global.syDisplay(this.global.HSplacementyear),
          //         type:'warning',
          //         confirmButtonText: `OK`,
          //       }).then((result) => {
          //         /* Read more about isConfirmed, isDenied below */
          //         window.history.back();
          //       })
          //  }
          this.cons_studentYearLevel = this.studinfo.yearOrGradeLevel;
          this.studinfo.yearOrGradeLevel = this.studentYearLevel + 1
        }
        this.getstrands()
      });
  }

  getstrands() {
    this.api.apiPublicAPIStrands()
      .subscribe(res => {
        this.strand = res.data
        for (var i = 0; i < res.data.length; ++i) {
          if (res.data[i].strandCode == 'ABM' || res.data[i].strandCode == 'HUMSS' || res.data[i].strandCode == 'STEM-NH' || res.data[i].strandCode == 'STEM-H') {
            this.strandfiltered.push(res.data[i])
          }
        }
        this.strandfiltered = []
        this.strandfiltered.push({ strandId: '900009', strandTitle: 'Accountancy, Business and Management Strand' })
        this.strandfiltered.push({ strandId: '900011', strandTitle: 'Humanities and Social Sciences Strand' })
        this.strandfiltered.push({ strandId: '900013', strandTitle: 'Science, Technology, Engineering and Mathematics Health Strand' })
        this.strandfiltered.push({ strandId: '900010', strandTitle: 'Science, Technology, Engineering and Mathematics-Non-Health Strand' })

      }, Error => {
        this.global.swalAlertError(Error)
      });
  }

  hspreenrollment(studInfo) {

    //this.sy=this.global.HSplacementyear
    // this.cperson=''
    // this.gradfrom="University of Saint Louis Tuguegarao"
    // this.courseval=''
    // this.courseval1=''
    // this.courseval2=''
    // this.address="Carig Sur, Tuguegarao City, Cagayan"
    // this.permPSGC="021529053"
    // this.YearGraduated=2021

    // this.global.swalLoading('')
    this.api.apiPublicAPIStrands()
      .subscribe(res => {
        this.strand = res.data
        for (var i = 0; i < res.data.length; ++i) {
          if (res.data[i].strandCode == 'ABM' || res.data[i].strandCode == 'HUMSS' || res.data[i].strandCode == 'STEM-NH' || res.data[i].strandCode == 'STEM-H') {
            this.strandfiltered.push(res.data[i])
          }
        }
        this.strandfiltered = []
        this.strandfiltered.push({ strandId: '900009', strandTitle: 'Accountancy, Business and Management Strand' })
        this.strandfiltered.push({ strandId: '900011', strandTitle: 'Humanities and Social Sciences Strand' })
        this.strandfiltered.push({ strandId: '900013', strandTitle: 'Science, Technology, Engineering and Mathematics Health Strand' })
        this.strandfiltered.push({ strandId: '900010', strandTitle: 'Science, Technology, Engineering and Mathematics-Non-Health Strand' })


        this.checkPlacement(studInfo)
      }, Error => {
        this.global.swalAlertError(Error)
      });
  }

  checkPlacement(studInfo) {
    // this.global.swalLoading('')
    this.studinfo = studInfo;
    this.global.apiPlacement()
      .subscribe(res => {
        this.preference = res.data
        if (!this.hspreenrollmentvar) {
          this.strandval = this.preference.preferredCourseId
        } else {
          this.strandval = this.preference.shS_PriorityStrandId1
        }

        this.preference.examForSchoolYear = this.global.colpreenrollmentyear

        //start of process
        if (res.data.shS_PriorityStrandId1 != null && res.data.shS_PriorityStrandId1 != '') {
          this.studinfo.programID = res.data.shS_PriorityStrandId1;

          this.api.apiStudentPortalCurriculum(res.data.shS_PriorityStrandId1)
            .subscribe(res => {
              this.studinfo.course = res.data.courseCode
              this.studinfo.major = res.data.abbreviatedMajor
            })
          this.studinfo.yearOrGradeLevel = 5;
          this.studinfo.departmentCode = "HS";
        } else
          if ((res.data.jhS_CourseId != null && res.data.jhS_CourseId != '')) {
            this.studinfo.programID = res.data.jhS_CourseId;
            this.api.apiStudentPortalCurriculum(res.data.jhS_CourseId)
              .subscribe(res => {
                this.studinfo.curriculum = res.data.programTitle
              })
            this.studinfo.yearOrGradeLevel = 1;
            this.studinfo.departmentCode = "HS";
          }
          else {
            //  if (this.studinfo.yearOrGradeLevel==4) {
            //    var filt2=''
            //    if (!this.global.oldshs_col) {
            //      filt2 = filt2 + '*Junior High to Senior High (Self enrollment is not allowed)<br>'
            //    }
            //    if (filt2 != '') {
            //      this.global.swalAlert('<h5 style="margin: 0 0 0 0;">Please contact <a href="mailto:dsas@usl.edu.ph">dsas@usl.edu.ph</a> or <a href="tel:09563078723">09563078723</a> for your admission to resolve the ff. </h5>',filt2,'warning')
            //      window.history.back();
            //    }
            //  }
          }
      }, Error => {
        window.history.back();
        this.global.swalAlertError(Error);
      });
  }
  slaplaceholder = ''
  checkenroll = false
  familyarray = []
  loadfambg() {
    this.api.apiStudentPortalFamilyBG()
      .subscribe(res => {
        this.familyarray = res.data
        this.loadeducbg()
      });
  }
  loadeducbg() {
    var year_Graduated = '';
    var sla_placeholder = 'University of Saint Louis Tuguegarao';
    this.api.apiStudentPortalEducBG()
      .subscribe(res => {
        if (res.data != null || res.data.length > 0) {
          if (res.data[0] != undefined) {
            this.sgfId = res.data[0].companyID
            this.average = res.data[0].otherInfo
            this.sgfsy = res.data[0].yearGraduated
            this.slaplaceholder = res.data[0].schoolName
          } else {
            if (searchArray(this.global.enrollmenthist)) {
              this.sgfId = '02049'
              this.average = '0'
              this.sgfsy = year_Graduated
              this.slaplaceholder = sla_placeholder
            }
          }
        }
      });

    function searchArray(arr) {
      var res = false;

      for (let i = 0; i < arr.length; i++) {
        const obj = arr[i];
        if (obj.course === "Elem" && obj.type === 1 && obj.status === "PAID" && obj.yearOrGradeLevel === 6) {
          year_Graduated = obj.schoolYear
          res = true;
          i = arr.length + 1;
        } else
          res = false
      }
      return res
    }
  }

  sgfAddress = ''
  sgfId = ''
  average = ''
  sgfsy = ''
  slaSy = ''
  slaId = ''
  lsaAddress = ''
  getindex(x) {
    var index = this.options.indexOf(x);
    if (this.arrayschool[index].address == null) {
      this.sgfAddress = ''
    } else
      this.sgfAddress = this.arrayschool[index].address
    this.sgfId = this.arrayschool[index].companyID
  }
  getindex2(x) {
    var index = this.options2.indexOf(x);
    if (this.arrayschool[index].address == null) {
      this.lsaAddress = ''
    } else
      this.lsaAddress = this.arrayschool[index].address
    this.slaId = this.arrayschool[index].companyID
  }

  checkparent() {
    var count = 0
    for (var i = 0; i < this.familyarray.length; ++i) {
      if (this.familyarray[i].relDesc == 'Mother' || this.familyarray[i].relDesc == 'Father') {
        count++
      }
    }
    if (count > 1) {
      return true
    }
    return false
  }
  checkedubackground() {
    if (this.sgfId == '') {
      return true
    }
    if (this.average == '') {
      return true
    }
    if (this.sgfsy == '') {
      return true
    }
    return false
  }

  checkbasicinfo() {
    if (this.global.userinfo.religion == null || this.global.userinfo.religion == '') {
      return true
    }
    if (this.global.studinfo.placeOfBirth == null || this.global.studinfo.placeOfBirth == '') {
      return true
    }
    return false
  }


  checkcontactinfo() {
    if (this.cno == null || this.cno == '') {
      return true
    }
    if (this.homeaddress == null || this.homeaddress == '') {
      return true
    }
    return false
  }

  checkpersonalinfo() {
    if (this.global.userdemog.ethnicity == '' || this.global.userdemog.ethnicity == null) {
      return true
    }
    if (this.global.userdemog.languageSpoken == '' || this.global.userdemog.languageSpoken == null) {
      return true
    }
    if (this.global.userdemog.motherTongue == '' || this.global.userdemog.motherTongue == null) {
      return true
    }
    if (parseInt(this.global.userdemog.numberOfSibling) == 0) {
      return true
    }
    if (parseInt(this.global.userdemog.birthOrder) == 0) {
      return true
    }
    return false
  }

  enroll() {
    var x = ''

    if (this.global.userinfo.religion == '' || this.global.userinfo.religion == null) {
      x = x + "*Basic Information - Religion is required!<br>"
    }
    if ((this.global.studinfo.placeOfBirth == '' || this.global.studinfo.placeOfBirth == null) && (this.global.hsfirstyear || this.global.oldjhs_shs)) {
      x = x + "*Basic Information - Place of Birth is required!<br>"
    }
    if (this.homeaddress == '' || this.homeaddress == null) {
      x = x + "*Contact Information - Home address is required!<br>"
    }
    if (this.cno == '' || this.cno == null) {
      x = x + "*Contact Information - Phone number is required!<br>"
    }
    if (this.email == '' || this.email == null) {
      x = x + "*Contact Information - Email is required!<br>"
    }

    if (this.sgfId == '' && (this.global.hsfirstyear || this.global.oldjhs_shs) && this.studentYearLevel == 1) {
      x = x + "*Educational Background - School is required!<br>"
    }
    if (this.sgfsy == '' && (this.global.hsfirstyear || this.global.oldjhs_shs) && this.studentYearLevel == 1) {
      x = x + "*Educational Background - School Year is required!<br>"
    }
    if (this.average == '' && (this.global.hsfirstyear || this.global.oldjhs_shs) && this.studentYearLevel == 1) {
      x = x + "*Educational Background - Average is required!<br>"
    }

    if(this.global.studinfo.yearOrGradeLevel == 1 && (this.global.studinfo.yearOrGradeLevel > 3 && this.global.studinfo.yearOrGradeLevel < 6)){
      if ((this.global.userdemog.generalAverage == '' || this.global.userdemog.generalAverage == null) && !(this.global.hsfirstyear || this.global.oldjhs_shs)) {
        x = x + "*Personal Information - General Average is required!<br>"
      }
    }else{
      if(this.global.userdemog.generalAverage == '' || this.global.userdemog.generalAverage == null )
        this.global.userdemog.generalAverage = 0;
    }


    if (this.global.userdemog.ethnicity == '' || this.global.userdemog.ethnicity == null) {
      x = x + "*Personal Information - Ethnicity is required!<br>"
    }
    if (this.global.userdemog.languageSpoken == '' || this.global.userdemog.languageSpoken == null) {
      x = x + "*Personal Information - Language Spoken is required!<br>"
    }
    if (this.global.userdemog.motherTongue == '' || this.global.userdemog.motherTongue == null) {
      x = x + "*Personal Information - Mother Tongue is required!<br>"
    }
    if (parseInt(this.global.userdemog.numberOfSibling) == 0) {
      x = x + "*Personal Information - Number of Children in the Family must not be empty!<br>"
    }
    if (parseInt(this.global.userdemog.birthOrder) == 0) {
      x = x + "*Personal Information - Birth Order must not be empty!<br>"
    }
    if (this.global.studinfo.yearOrGradeLevel == 4 && this.global.studinfo.departmentCode == 'HS') {
      if (this.strandval == null || this.strandval == '')
        x = x + "*You must select a strand"
    }
    if (x != '') {
      this.global.swalAlert("Warning", x, 'warning')
    } else {
      this.checkenroll = true
      this.global.swalLoading('Processing...');


      if (this.global.preenrollmenteligibleHS)
        this.setPlacement();
      // else if (this.global.hsfirstyear && (this.global.studinfo.yearOrGradeLevel == 5 || (this.global.studinfo.yearOrGradeLevel == 6 && this.global.studinfo.departmentCode == 'ELEM')))
      else if (this.global.hsfirstyear && this.global.studinfo.departmentCode == 'ELEM')
        this.admitstud();
      else if (this.global.studinfo.departmentCode == "HS" && (this.global.studinfo.yearOrGradeLevel == 5 || (this.global.studinfo.yearOrGradeLevel >= 1 && this.global.studinfo.yearOrGradeLevel < 4)))
        this.admitstud();
    }
  }

  admitstud() {
    this.checkenroll = false
    this.updateinfo(1)
  }


  admitupdate() {
    var programID = this.studinfo.programID;
    var basicEDHigherYear = false;


    if (this.global.studinfo.departmentCode == "HS" && (this.studentYearLevel == 5 || (this.studentYearLevel >= 1 && this.studentYearLevel < 4))) {
      this.studentYearLevel = this.studinfo.yearOrGradeLevel
      basicEDHigherYear = true
    } else if (this.studinfo.enrollmentStatus == 'For Admission' && this.studentYearLevel == 4) {

      this.studentYearLevel = this.studentYearLevel + 1;
      if (this.strandval)
        programID = this.strandval
    }

    if (this.studinfo.enrollmentStatus == 'For Admission' && (this.studinfo.yearOrGradeLevel == 1 && this.global.studinfo.departmentCode == 'ELEM')) {
      this.studentYearLevel = this.studinfo.yearOrGradeLevel
    }

    if(this.studinfo.enrollmentStatus == 'For Admission' && this.global.studinfo.departmentCode == 'ELEM' && this.global.hsfirstyear && (this.preference.shS_PriorityStrand1!=null)){
      this.studentYearLevel = 5; //this is for incoming grade 11 with usl elem enrollment record history
    }
    else if(this.studinfo.enrollmentStatus == 'For Admission' && this.global.studinfo.departmentCode == 'ELEM' && this.global.hsfirstyear && (this.preference.jhS_Course!=null)){
      this.studentYearLevel = 1; //this is for incoming grade 7 with usl elem enrollment record history
    }



    var syear = this.global.enrollmentsy
    if (this.global.enrollmentsy.length == 7) {
      syear = this.global.enrollmentsy.slice(0, -1)
    }

    this.api.apiAdmitStudentPost(this.global.userinfo.idNumber, syear, programID, this.studentYearLevel)
      .subscribe(res => {
        if (this.global.studinfo.departmentCode == 'ELEM')
          this.global.getallapi(3, '')
        else if (basicEDHigherYear)
          this.global.getallapi(4, '')
        else
          this.global.getallapi(2, '')

      }, Error => {
        this.global.swalAlertError(Error);
      });
  }
  quickSavePersonalInfo(x = null) {
    var xy = ''
    if(this.global.studinfo.yearOrGradeLevel == 1 && (this.global.studinfo.yearOrGradeLevel > 3 && this.global.studinfo.yearOrGradeLevel < 6)){
      if ((this.global.userdemog.generalAverage == '' || this.global.userdemog.generalAverage == null) && !this.global.hsfirstyear) {
        xy = xy + "*Personal Information - General Average is required!<br>"
      }
    }else{
      if(this.global.userdemog.generalAverage == '' || this.global.userdemog.generalAverage == null )
        this.global.userdemog.generalAverage = 0;
    }

    if (this.global.userdemog.ethnicity == '' || this.global.userdemog.ethnicity == null) {
      xy = xy + "*Personal Information - Ethnicity is required!<br>"
    }
    if (this.global.userdemog.languageSpoken == '' || this.global.userdemog.languageSpoken == null) {
      xy = xy + "*Personal Information - Language Spoken is required!<br>"
    }
    if (this.global.userdemog.motherTongue == '' || this.global.userdemog.motherTongue == null) {
      xy = xy + "*Personal Information - Mother Tongue is required!<br>"
    }
    if (parseInt(this.global.userdemog.numberOfSibling) == 0) {
      xy = xy + "*Personal Information - Number of Children in the Family must not be 0!<br>"
    }
    if (parseInt(this.global.userdemog.birthOrder) == 0) {
      xy = xy + "*Personal Information - Birth Order must not be 0!<br>"
    }

    if (xy != '') {
      this.global.swalAlert("Warning", xy, 'warning')
    } else {

      if (x == null)
        this.global.swalLoading('');
      this.api.apiStudentPortalPersonalInfoPut(
        this.global.userdemog.numberOfSibling,
        this.global.userdemog.birthOrder,
        this.global.userdemog.skillTalent,
        this.global.userdemog.contestToJoin,
        this.global.userdemog.physicalDefect,
        this.global.userdemog.healthProblem,
        this.global.userdemog.medicalAdvise,
        this.global.userdemog.nearestNeighbor,
        this.global.userdemog.ethnicity,
        this.global.userdemog.motherTongue,
        this.global.userdemog.languageSpoken,
        this.global.userdemog.generalAverage,
        this.global.userdemog.neatScore)
        .subscribe(res => {
          if (x == 1) {
            this.admitupdate()
          } else
            this.global.swalSuccess(res.message);
        }, Error => {
          this.global.swalAlertError(Error);
        });
    }
  }
  saveedycbackground() {
    var x = ''

    if (this.sgfId == '' && (this.global.hsfirstyear || this.global.oldjhs_shs) && this.studentYearLevel == 1) {
      x = x + "*Educational Background - School is required!<br>"
    }
    if (this.sgfsy == '' && (this.global.hsfirstyear || this.global.oldjhs_shs) && this.studentYearLevel == 1) {
      x = x + "*Educational Background - School Year is required!<br>"
    }
    if (this.average == '' && (this.global.hsfirstyear || this.global.oldjhs_shs) && this.studentYearLevel == 1) {
      x = x + "*Educational Background - Average is required!<br>"
    }
    if (x != '') {
      this.global.swalAlert("Warning", x, 'warning')
    } else {
      this.global.swalLoading('');
      this.updateeducbackground(2)
    }
  }
  savebasicinfo() {
    var x = ''
    if (this.global.userinfo.religion == '' || this.global.userinfo.religion == null) {
      x = x + "*Religion is required!<br>"
    }
    if (this.global.studinfo.placeOfBirth == '' || this.global.studinfo.placeOfBirth == null && (this.global.hsfirstyear || this.global.oldjhs_shs)) {
      x = x + "*Place of Birth is required!<br>"
    }

    if (x != '') {
      this.global.swalAlert("Warning", x, 'warning')
    } else {
      this.global.swalLoading('');
      this.updatebasicinfo(2)
    }
  }

  savecontact() {
    var x = ''
    if (this.cno == '' || this.cno == null) {
      x = x + "*Contact Information - Phone number is required!<br>"
    }
    if (this.homeaddress == '' || this.homeaddress == null) {
      x = x + "*Contact Information - Home address is required!<br>"
    }
    if (this.email == '' || this.email == null) {
      x = x + "*Contact Information - Email is required!<br>"
    }
    if (x != '') {
      this.global.swalAlert("Warning", x, 'warning')
    } else {
      this.global.swalLoading('');
      this.updateinfo(2)
    }
  }
  updateinfo(x = null) {
    this.api.apiContactInfo(this.cno, this.tno, this.email, this.permNoStreet, this.permPSGC, this.currNoStreet, this.currPSGC)
      .subscribe(res => {
        if (x == 1) {
          this.updatebasicinfo(1)
        } else {
          this.global.swalSuccess(res.message)
        }
      }, Error => {
        this.global.swalAlertError(Error)
        console.log(Error)
      });
  }
  updatebasicinfo(x = null) {
    var pdate = new Date(this.global.userdemog.dateOfBirth).toLocaleString();
    if (pdate == "Invalid Date") {
      pdate = ''
    }

    this.api.apiStudentPortalPersonInfoPut(
      this.global.userinfo.idNumber,
      this.global.userinfo.firstName,
      this.global.userinfo.middleName,
      this.global.userinfo.lastName,
      this.global.userinfo.suffixName,
      this.global.studinfo.placeOfBirth,
      this.global.userinfo.religion,
      pdate,
      this.global.userdemog.gender,
      this.global.userdemog.civilStatus,
      this.global.userinfo.idNumber,
      this.global.userinfo.mobileNo,
      this.global.userinfo.telNo,
      this.global.userinfo.emailAdd,
      this.global.userinfo.nationality)
      .subscribe(res => {
        if (x == 1) {
          this.updateeducbackground(1)
        } else {
          this.global.swalSuccess(res.message)
        }
      }, Error => {
        this.global.swalAlertError(Error)
        console.log(Error)
      });
  }

  quickSaveSacramentsReceivedPut(x = null) {
    this.api.apiStudentPortalSacramentsReceivedPut(this.global.userdemog.baptism, this.global.userdemog.confession, this.global.userdemog.communion, this.global.userdemog.confirmation, this.global.userdemog.religiousInstruction)
      .subscribe(res => {
        if (x == 1) {
          this.quickSavePersonalInfo(1)
          //this.global.getallapi(2)
        } else {
          this.global.swalSuccess(res.message)
        }
      }, Error => {
        this.global.swalAlertError(Error);
      });
  }
  updateeducbackground(x = null) {
    if (this.studentYearLevel == 1) {
      this.api.apiStudentPortalEducBGHSPut(this.sgfId, this.sgfsy, this.average, this.slaId, this.slaSy)
        .subscribe(res => {
          if (x == 1) {
            this.quickSaveSacramentsReceivedPut(1)
          } else {
            this.global.swalSuccess(res.message)
          }
        }, Error => {
          this.global.swalAlertError(Error)
          console.log(Error)
        });
    } else {
      this.quickSaveSacramentsReceivedPut(1)
    }
  }


  deletefamily(id) {
    this.swalConfirmdelete("Are you sure?", "You won't be able to revert this!", 'warning', 'Delete Family Member Info', 'Family Member Info has been Removed', 'deletefamily', id);
  }
  deleteeduc(id) {
    this.swalConfirmdelete("Are you sure?", "You won't be able to revert this!", 'warning', 'Delete Educational Background', 'Educational Background has been Removed', 'deleteeb', id);
  }

  swalConfirmdelete(title, text, type, button, successm, remove, id) {
    swal({
      title: title,
      text: text,
      type: type,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: button
    }).then((result) => {
      if (result.value) {
        if (remove == 'deletefamily') {
          this.global.swalLoading('');
          this.api.apiStudentPortalFamilyMemberDelete(id)
            .subscribe(res => {
              this.global.swalSuccess(successm);
              this.loadfambg();
            }, Error => {
              this.global.swalAlertError(Error);
            });
        }
        else
          if (remove == 'deleteeb') {
            this.global.swalLoading('');
            this.api.apiStudentPortalEducBGDelete(id)
              .subscribe(res => {
                this.global.swalSuccess(successm);
                //this.loadeducbg();
              }, Error => {
                this.global.swalAlertError(Error);
              });
          }
      }
    })
  }


  openedit(x, y) {
    if (y == 'Mother' || y == 'Father') {
      this.openDialog2(x)
    }
    else if (y == 'Brother/Sister' || y == 'Guardian' || y == 'Cousin' || y == 'Uncle/Aunt' || y == 'Grand Parent' || y == 'StepFather' || y == 'StepMother' || y == 'Spouse') {
      this.openDialogguardian(x)
    } else {
      this.openDialogfamilymember(x)
    }
  }
  displaycs(x) {
    if (x == 'S') {
      return 'Single'
    }
    if (x == 'M') {
      return 'Married'
    }
    if (x == 'W') {
      return 'Widow'
    }
  }
  displayg(x) {
    if (x == 'M') {
      return 'Male'
    }
    if (x == 'F') {
      return 'Female'
    }
  }

  openDialog(lookup): void {
    const dialogRef = this.dialog.open(AddressLookupComponent, {
      width: '500px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        if (lookup == 1) {
          this.permPSGC = result.data;
          this.homeaddress = result.result;
        } else {
          this.currPSGC = result.data;
          this.boardingaddress = result.result;
        }
      }
    });
  }

  openDialog2(lookup): void {
    const dialogRef = this.dialog.open(HsEnrollmentFamilyBackgroundComponent, {
      width: '600px', disableClose: false, data: { id: this.global.userinfo.idNumber, kind: lookup },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result == 'cancel') {
        }
        if (result.result == 'saved') {
          this.loadfambg();
        }
      }
    });
  }

  openDialogguardian(lookup): void {
    const dialogRef = this.dialog.open(HsEnrollmentFamilyBackgroundGuardianComponent, {
      width: '600px', disableClose: false, data: { id: this.global.userinfo.idNumber, kind: lookup },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result == 'cancel') {
        }
        if (result.result == 'saved') {
          this.loadfambg();
        }
      }
    });
  }

  openDialogfamilymember(lookup): void {
    const dialogRef = this.dialog.open(HsEnrollmentFamilyBackgroundFamilymemberComponent, {
      width: '600px', disableClose: false, data: { id: this.global.userinfo.idNumber, kind: lookup },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result.result == 'cancel') {
        }
        if (result.result == 'saved') {
          this.loadfambg();
        }
      }
    });
  }

  confirmenroll() {

    this.swalConfirm("You are about to enroll.", "Please confirm to proceed!", 'question', 'Confirm');
  }

  swalConfirm(title, text, type, button) {
    swal({
      title: title,
      text: text,
      type: type,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: button
    }).then((result) => {
      if (result.value) {

        this.enroll();
      }
    })
  }
  postSY
  setPlacement() {
    this.global.swalLoading('')
    if (this.global.studinfo.yearOrGradeLevel == 6 && this.global.studinfo.departmentCode != "ELEM") { //setting up of schoolyear for incoming first year college from SHS department
      this.postSY = this.global.colpreenrollmentyear
    }
    else if (this.global.studinfo.yearOrGradeLevel == 4) { //setting up of schoolyear and preferred strand for incoming SHS from HS department
      this.postSY = this.global.HSplacementyear
      this.preference.shS_PriorityStrandId1 = this.strandval
    }
    else if (this.global.studinfo.yearOrGradeLevel == 6 && this.global.studinfo.departmentCode == "ELEM") { //setting up of schoolyear for incoming g7 from elem department
      this.postSY = this.global.HSplacementyear
    }
    else { // settign up of schoolyear for incoming students - outsiders
      if (this.global.currentplacementstatus == 'JHS' || this.global.currentplacementstatus == 'SHS')//basic ed
        this.postSY = this.global.HSplacementyear
      else {
        this.postSY = this.global.colpreenrollmentyear//college
      }
    }

    if (this.global.currentplacementstatus == 'JHS' || this.global.currentplacementstatus == 'SHS') {
      this.preference.shS_PriorityStrandId1 = this.strandval
    }

    this.api.apiPlacementPut(
      this.preference.examDate,
      this.preference.preferredCourse,
      this.preference.alternativeCourse,
      0,
      this.preference.vit,
      this.preference.nvit,
      this.preference.preferredCourseId,
      this.preference.alternativeCourseId1,
      this.preference.alternativeCourseId2,
      this.preference.exemptionType,
      this.preference.strand,
      "P",
      this.preference.gResult,
      this.preference.testScheduleId,
      this.preference.examRoom,
      this.postSY,
      this.preference.elem_ExamResult,
      this.preference.elem_CourseId,
      this.preference.elem_Course,
      this.preference.jhS_ExamResult,
      this.preference.jhS_CourseId,
      this.preference.jhS_Course,
      this.preference.shS_ExamResult,
      this.preference.shS_PriorityStrandId1,
      this.preference.shS_PriorityStrand1,
      this.preference.shS_PriorityStrandId2,
      this.preference.shS_PriorityStrand2)
      .subscribe(res => {
        if (res.message == 'Placement exam information updated successfully.' || res.message == 'Placement exam information updated successfully.') {
          this.global.swalClose();
          this.admitstud();
        } else {

        }
      });
  }

  //  ViewPrint(): void {
  //         console.log(this.global.nextSYClassSchedule)
  //         const dialogRef = this.dialog.open(EnrolmentFormComponent, {
  //           width: '75%', data:{
  //             gender:this.global.userinfo.gender,
  //             id:this.global.requestid(),
  //             name: this.global.userinfo.lastName +", "+this.global.userinfo.firstName+" "+this.global.userinfo.middleName+" "+this.global.userinfo.suffixName,
  //             course:this.global.nextSYStudInfo.course,
  //             major:this.global.nextSYStudInfo.major,
  //             year:this.global.nextSYStudInfo.yearOrGradeLevel,
  //             subjects:this.global.nextSYClassSchedule
  //           }, disableClose: false
  //         });

  //         dialogRef.afterClosed().subscribe(result => {
  //           if (result==undefined) {
  //             // code...
  //           }else
  //           if (result.result!='cancel') {
  //           }
  //         });
  //       }


  // }
  nonull(x) {
    if (x == null || undefined == x)
      return ''
    return x
  }
  getgrade() {

    let gradeLevel = this.studentYearLevel;
    if (this.global.nextSYStudInfo)
      gradeLevel = this.global.nextSYStudInfo.yearOrGradeLevel
    return gradeLevel + 6
  }
  getwordgender(x) {
    if (x == "M")
      return 'Male'
    return 'Female'
  }
  educ
  sibling = []
  idPicture
  Getdatab4pdf() {

    this.global.swalLoading('')
    this.api.apiStudentPortalFamilyBG()
      .subscribe(res => {
        this.familyarray = res.data
        this.sibling = []
        for (var i = 0; i < res.data.length; ++i) {
          if (res.data[i].relDesc == 'Sibling')
            this.sibling.push(res.data[i])
        }
        this.api.apiPublicAPISchools()
          .subscribe(res => {
            this.arrayschool = res
            if (this.familyarray.length != 0)
              this.getfamilydetails(0, this.familyarray[0].relDesc)
            else
              this.generatePDF()
          });
      });
  }


  father = {
    parentName: null,
    cellphoneNo: null,
    landlineNo: null,
    occupation: null,
    officeAddress: null,
    ofw: null,
    relID: null,
    status: null
  }
  mother = {
    parentName: null,
    cellphoneNo: null,
    landlineNo: null,
    occupation: null,
    officeAddress: null,
    ofw: null,
    relID: null,
    status: null
  }
  guardian = {
    parentName: null,
    cellphoneNo: null,
    landlineNo: null,
    occupation: null,
    officeAddress: null,
    ofw: null,
    relID: null,
    status: null
  }
  getfamilydetails(x, rel) {
    if (x < this.familyarray.length) {
      this.api.apiStudentPortalParent(this.familyarray[x].memberIdNumber)
        .subscribe(res => {
          if (rel != undefined && res.data != null) {
            if (rel.toLowerCase() == 'father') {
              this.father = {
                parentName: res.data.parentName,
                cellphoneNo: res.data.cellphoneNo,
                landlineNo: res.data.landlineNo,
                occupation: res.data.occupation,
                officeAddress: res.data.officeAddress,
                relID: res.data.relID,
                ofw: res.data.ofw,
                status: res.data.status
              }
            } else
              if (rel.toLowerCase() == 'mother') {
                this.mother = {
                  parentName: res.data.parentName,
                  cellphoneNo: res.data.cellphoneNo,
                  landlineNo: res.data.landlineNo,
                  occupation: res.data.occupation,
                  officeAddress: res.data.officeAddress,
                  relID: res.data.relID,
                  ofw: res.data.ofw,
                  status: res.data.status
                }
              }
              else {
                this.guardian = {
                  parentName: res.data.parentName,
                  cellphoneNo: res.data.cellphoneNo,
                  landlineNo: res.data.landlineNo,
                  occupation: res.data.occupation,
                  officeAddress: res.data.officeAddress,
                  relID: res.data.relID,
                  ofw: res.data.ofw,
                  status: rel
                }
              }
          }
          if (this.familyarray[x + 1] == undefined)
            this.getfamilydetails(x + 1, '')
          else
            this.getfamilydetails(x + 1, this.familyarray[x + 1].relDesc)
        }, Error => {
          this.global.swalAlertError(Error);
        });
    } else {
      this.generatePDF()
    }

  }
  fields = []
  generatePDF() {

    this.global.swalClose()
    //place this block of codes inside a function
    var SY = this.global.syDisplay(this.global.schoolyear);
    var status
    if (this.global.enrollmenthist.length == 1) {
      status = {
        text: '√',
        absolutePosition: { x: 451, y: 120 },
        bold: true, fontSize: 13,
      }
    }
    else {
      status = {
        text: '√',
        absolutePosition: { x: 400, y: 120 },
        bold: true, fontSize: 13,
      }
    }
    if (this.studinfo.enrollmentType == 2) {
      status = {
        text: '√',
        absolutePosition: { x: 530, y: 120 },
        bold: true, fontSize: 13,
      }
    }


    let reportcard
    if (this.global.userdemog.reportCard += 'True') {
      reportcard = {
        text: '√',
        absolutePosition: { x: 259, y: 140 },
        bold: true, fontSize: 13,
      }
    }

    else {
      reportcard = {
        text: '√',
        absolutePosition: { x: 295, y: 140 },
        bold: true, fontSize: 13,
      }
    }

    let nso
    if (this.global.userdemog.nso == 'False') {
      nso = {
        text: '√',
        absolutePosition: { x: 320, y: 149 },
        bold: true, fontSize: 13,
      }
    }

    else {
      nso = {
        text: '√',
        absolutePosition: { x: 286, y: 149 },
        bold: true, fontSize: 13,
      }
    }


    let gender
    if (this.studinfo.gender == 'F') {
      gender = {
        text: '√',
        absolutePosition: { x: 481, y: 149 },
        bold: true, fontSize: 13,
      }
    }

    else {
      gender = {
        text: '√',
        absolutePosition: { x: 417, y: 149 },
        bold: true, fontSize: 13,
      }
    }

    let elemschool
    let yeargrad
    let average
    let companyID
    for (var i2 = 0; i2 < this.global.educarray.length; ++i2) {
      if (this.global.educarray[i2].programName == 'Primary Education') {
        elemschool = this.global.educarray[i2].schoolName
        yeargrad = this.global.educarray[i2].yearGraduated
        average = this.global.educarray[i2].otherInfo
        companyID = this.global.educarray[i2].companyID
      }
    }
    var bday: any = new Date(this.global.userinfo.dateOfBirth);
    // var dd = String(bday.getDate()-1).padStart(2, '0');
    var dd = String(bday.getDate()).padStart(2, '0');
    var mm = String(bday.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = bday.getFullYear();
    bday = mm + '/' + dd + '/' + yyyy;

    let fatherstatus
    if (this.father.status != null) {
      if (this.father.status == 'S') {
        fatherstatus = {
          text: '√',
          absolutePosition: { x: 355, y: 350 },
          bold: true, fontSize: 13,
        }
      }
      if (this.father.status == 'P') {
        fatherstatus = {
          text: '√',
          absolutePosition: { x: 115, y: 350 },
          bold: true, fontSize: 13,
        }
      }
      if (this.father.status == 'A') {
        fatherstatus = {
          text: '√',
          absolutePosition: { x: 244, y: 350 },
          bold: true, fontSize: 13,
        }
      }
      if (this.father.status == 'W') {
        fatherstatus = {
          text: '√',
          absolutePosition: { x: 399, y: 340 },
          bold: true, fontSize: 13,
        }
      }
      if (this.father.status == 'X') {
        fatherstatus = {
          text: '√',
          absolutePosition: { x: 248, y: 340 },
          bold: true, fontSize: 13,
        }
      }
    }
    var fatherofw
    if (this.father.ofw) {
      fatherofw = {
        text: '√',
        absolutePosition: { x: 90, y: 340 },
        bold: true, fontSize: 13,
      }
    }

    let motherstatus
    if (this.mother.status != null) {
      if (this.mother.status == 'S') {
        motherstatus = {
          text: '√',
          absolutePosition: { x: 355, y: 410 },
          bold: true, fontSize: 13,
        }
      }
      if (this.mother.status == 'P') {
        motherstatus = {
          text: '√',
          absolutePosition: { x: 115, y: 410 },
          bold: true, fontSize: 13,
        }
      }
      if (this.mother.status == 'A') {
        motherstatus = {
          text: '√',
          absolutePosition: { x: 244, y: 410 },
          bold: true, fontSize: 13,
        }
      }
      if (this.mother.status == 'W') {
        motherstatus = {
          text: '√',
          absolutePosition: { x: 399, y: 400 },
          bold: true, fontSize: 13,
        }
      }
      if (this.mother.status == 'X') {
        motherstatus = {
          text: '√',
          absolutePosition: { x: 248, y: 400 },
          bold: true, fontSize: 13,
        }
      }
    }
    var motherofw
    if (this.mother.ofw) {
      motherofw = {
        text: '√',
        absolutePosition: { x: 90, y: 400 },
        bold: true, fontSize: 13,
      }
    }
    var married
    if ((this.mother.status == 'M' && this.father.status == 'M')) {
      married = {
        text: '√',
        absolutePosition: { x: 180, y: 430 },
        bold: true, fontSize: 13,
      }
    } else {
      married = {
        text: '√',
        absolutePosition: { x: 224, y: 430 },
        bold: true, fontSize: 13,
      }
    }
    var baptism
    if (this.global.userdemog.baptism) {
      baptism = {
        text: '√',
        absolutePosition: { x: 221, y: 480 },
        bold: true, fontSize: 13,
      }
    }
    var confession
    if (this.global.userdemog.confession) {
      confession = {
        text: '√',
        absolutePosition: { x: 316, y: 480 },
        bold: true, fontSize: 13,
      }
    }
    var holyCommunion
    if (this.global.userdemog.communion) {
      holyCommunion = {
        text: '√',
        absolutePosition: { x: 436, y: 480 },
        bold: true, fontSize: 13,
      }
    }
    var confirmation
    if (this.global.userdemog.confirmation) {
      confirmation = {
        text: '√',
        absolutePosition: { x: 531, y: 480 },
        bold: true, fontSize: 13,
      }
    }
    var instruction
    if (this.global.userdemog.religiousInstruction) {
      instruction = {
        text: '√',
        absolutePosition: { x: 309, y: 489 },
        bold: true, fontSize: 13,
      }
    } else
      instruction = {
        text: '√',
        absolutePosition: { x: 342, y: 489 },
        bold: true, fontSize: 13,
      }

    var sibling = []
    var lastdigit = 510

    for (var i5 = 0; i5 < this.sibling.length; ++i5) {
      if (i5 == 0) {
        lastdigit += 26
        sibling.push({
          text: 'IDNumber: _____________ Name:___________________________',
          absolutePosition: { x: 240, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                       ' + this.sibling[i5].memberIdNumber + '                            ' + this.sibling[i5].fullName,
          absolutePosition: { x: 240, y: lastdigit },
          fontSize: 8
        })
      }
      else if (i5 % 2 === 1) {
        lastdigit += 12
        sibling.push({
          text: 'IDNumber: _____________ Name:___________________________',
          absolutePosition: { x: 50, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                    ' + this.sibling[i5].memberIdNumber + '                    ' + this.sibling[i5].fullName,
          absolutePosition: { x: 50, y: lastdigit },
          fontSize: 8
        })
      } else {
        sibling.push({
          text: 'IDNumber: _____________ Name:___________________________',
          absolutePosition: { x: 300, y: lastdigit }
        })
        sibling.push({
          text: '\u200B\t                   ' + this.sibling[i5].memberIdNumber + '                  ' + this.sibling[i5].fullName,
          absolutePosition: { x: 305, y: lastdigit },
          fontSize: 8
        })
      }
    }
    this.fields = [
      {
        text: this.nonull(this.global.requestid()),
        absolutePosition: { x: 90, y: 108 },
        bold: true
      },
      {
        text: this.nonull(this.global.userinfo.lrNumber),
        absolutePosition: { x: 224, y: 108 },
        bold: true
      },
      {
        text: this.nonull(this.global.serverdate),
        absolutePosition: { x: 375, y: 108 },
        bold: true
      },
      {
        text: this.nonull(this.getgrade()),
        absolutePosition: { x: 90, y: 120 },
        bold: true
      },
      status,
      reportcard,
      {
        text: this.nonull(this.global.userdemog.generalAverage),
        absolutePosition: { x: 410, y: 140 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.neatScore),
        absolutePosition: { x: 525, y: 140 },
        bold: true
      },
      nso,
      gender,
      {
        text: this.nonull(this.studinfo.lastName),
        absolutePosition: { x: 85, y: 166 },
        bold: true
      },
      {
        text: this.nonull(this.studinfo.firstName),
        absolutePosition: { x: 230, y: 166 },
        bold: true
      },
      {
        text: this.nonull(this.studinfo.middleName),
        absolutePosition: { x: 440, y: 166 },
        bold: true
      },
      {
        text: this.nonull(bday),
        absolutePosition: { x: 113, y: 190 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.placeOfBirth),
        absolutePosition: { x: 305, y: 190 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.homeAddress),
        absolutePosition: { x: 170, y: 200 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.currentAddress),
        absolutePosition: { x: 170, y: 210 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.boardingPhone),
        absolutePosition: { x: 428, y: 220 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.sgfName),
        absolutePosition: { x: 213, y: 250 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.sgfsy),
        absolutePosition: { x: 110, y: 260 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.sgfAddress),
        absolutePosition: { x: 262, y: 260 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.lsaName),
        absolutePosition: { x: 150, y: 280 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.lsasy),
        absolutePosition: { x: 110, y: 289 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.lsaAddress),
        absolutePosition: { x: 262, y: 289 },
        bold: true
      },

      {
        text: this.nonull(this.father.parentName),
        absolutePosition: { x: 90, y: 309 },
        bold: true
      },
      {
        text: this.nonull(this.father.cellphoneNo),
        absolutePosition: { x: 428, y: 309 },
        bold: true
      },
      {
        text: this.nonull(this.father.landlineNo),
        absolutePosition: { x: 449, y: 320 },
        bold: true
      },
      {
        text: this.nonull(this.father.occupation),
        absolutePosition: { x: 115, y: 330 },
        bold: true
      },
      {
        text: this.nonull(this.father.officeAddress),
        absolutePosition: { x: 378, y: 330 },
        bold: true
      },

      {
        text: this.nonull(this.mother.parentName),
        absolutePosition: { x: 90, y: 369 },
        bold: true
      },
      {
        text: this.nonull(this.mother.cellphoneNo),
        absolutePosition: { x: 428, y: 369 },
        bold: true
      },
      {
        text: this.nonull(this.mother.landlineNo),
        absolutePosition: { x: 449, y: 380 },
        bold: true
      },
      {
        text: this.nonull(this.mother.occupation),
        absolutePosition: { x: 115, y: 390 },
        bold: true
      },
      {
        text: this.nonull(this.mother.officeAddress),
        absolutePosition: { x: 378, y: 390 },
        bold: true
      },
      fatherstatus,
      fatherofw,
      motherstatus,
      motherofw,
      married,
      {
        text: this.nonull(this.guardian.parentName),
        absolutePosition: { x: 250, y: 440 },
        bold: true
      },
      {
        text: this.nonull(this.guardian.status),
        absolutePosition: { x: 469, y: 440 },
        bold: true
      },
      {
        text: this.nonull(this.guardian.cellphoneNo),
        absolutePosition: { x: 430, y: 450 },
        bold: true
      },
      {
        text: this.nonull(this.guardian.officeAddress),
        absolutePosition: { x: 100, y: 450 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.numberOfSibling),
        absolutePosition: { x: 205, y: 470 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.birthOrder),
        absolutePosition: { x: 350, y: 470 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.religion),
        absolutePosition: { x: 435, y: 470 },
        bold: true
      },
      baptism,
      confession,
      holyCommunion,
      confirmation,
      instruction,


      {
        text: this.nonull(this.global.userdemog.ethnicity),
        absolutePosition: { x: 458, y: 490 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.languageSpoken),
        absolutePosition: { x: 140, y: 500 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.motherTongue),
        absolutePosition: { x: 425, y: 500 },
        bold: true
      },
      sibling,

      {
        text: this.nonull(this.global.userdemog.skillTalent),
        absolutePosition: { x: 210, y: lastdigit + 30 },
        bold: true
      },


      {
        text: this.nonull(this.global.userdemog.contestToJoin),
        absolutePosition: { x: 165, y: lastdigit + 40 },
        bold: true
      },

      {
        text: this.nonull(this.global.userdemog.physicalDefect),
        absolutePosition: { x: 145, y: lastdigit + 50 },
        bold: true
      },

      {
        text: this.nonull(this.global.userdemog.healthProblem),
        absolutePosition: { x: 135, y: lastdigit + 60 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.medicalAdvise),
        absolutePosition: { x: 412, y: lastdigit + 60 },
        bold: true
      },
      {
        text: this.nonull(this.global.userdemog.nearestNeighbor),
        absolutePosition: { x: 265, y: lastdigit + 70 },
        bold: true
      },


    ]

    var header = { text: 'UNIVERSITY OF SAINT LOUIS\nTuguegarao City\nSENIOR HIGH SCHOOL DEPARTMENT\nENROLLMENT FORM\n', bold: true, fontSize: 10, alignment: 'center' }
    if (this.getgrade() < 11) {
      header = { text: 'UNIVERSITY OF SAINT LOUIS\nTuguegarao City\nJUNIOR HIGH SCHOOL DEPARTMENT\nENROLLMENT FORM\n', bold: true, fontSize: 10, alignment: 'center' }
    }
    if (this.global.image.changingThisBreaksApplicationSecurity == "data:image/jpeg;base64,null") {
      this.idPicture = {
        image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw4PDxANDxAQDg8QEA8ODQ8QFRAQEA8QFRUWFxYXFRkYHSgjGBslGxUVITIhJSkrLi46GB8zODMsNygtLisBCgoKDQ0NDg0NDjcZFRkrLSstKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABgcBAgUEA//EAEMQAAIBAwAGBggEAwQLAAAAAAABAgMEEQUGEiExkRMUQVFxgQciU2GSobHRMlJiciMzQhWisvAkJTVDVGRzdLPB4f/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABURAQEAAAAAAAAAAAAAAAAAAAAB/9oADAMBAAIRAxEAPwC6AAAAAAAAAAAAAAAAAAAAAAAAAAAGWAAy+9jL72AAy/eMvvYADL7xl94ADafeAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYbSy28Jb23hJIDIIPp30hU6bdO0h00ludWW6nn9KW+XjuRE7rXHSdR56w6a7I04wgl54z8wLkBEdTtYbd0IU6146lzLMqnTvZak3+GLe5pePf4EuAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFe+knTss/2fSeFhSuWuLysxh4Yw35FhFG6arurdXFSXGVap8pNL5JcgPEkZwZBVauJ2dAaz3Vk0oydSin61GbzHH6X/T5bjkGGgLw0NpSld0Y16TzF7pJ/ihLtjL3nuKn9Hmk5ULxUM/w7j1GuzbSbg/HivMtgiAAAAAAAAAAAAAABkAAAAAAAAAAAAAAGGyh7uWatV99So/7zL3nwfgyhaj9aT/VL6gYABWgAAdDVt4vrR/8AMUVzkkXaUhq+/wDTbT/urf8A8kS7yMgAAAAAAAAAAAAAAAAAAAAAAAAAAAADE1ufgyhJxxKSfFSkn45L8K21+1YhQze0Fs05TxXp5bUZye6Uc9je5rvawBCwAVoAMAe/V9ZvbTH/ABVu+VSLLvILqBqzCMIX9ZbVSXr20cvFOO9bT75P38F7ydEZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5msej3c2leglmUoN01+uPrR+aR0wBQMf8APuMk7111QqOc7y0g5qXrVqEV66l2ygv6s8XHjxxnJA5PDcXuktzi90k/envRVZN6FCVScKUFmc5RhBfqbwjSlFzkoQjKpN8IU05zfgo72WTqRqlK3kru5SVbDVGlul0Kaw5N/mabW7hv3vLwEtsbaNGlSoR/DSp06Uc790IqK+h9wCIAAAAAAAAAAAAAAAAAAAAAAAAAAAAfG6uqVKO3VqQpx45m1FfMD7HwvruFClOtN4hTi5y8F3e/sI1pHX+xpZVLbuJL8icYZ/dLGfJMg+setNxfepJRpUU9pUo5eWuDm3+J8e5e4Cdana3delOjVhGlWWZwUXmM4dvHtRIbrR9Ct/NpU6v74xk/mikNH3c7etTuKf46clKPv7GvNNrzLv0bewuKNOvTeYVIKa71nin708ryA2tbOlSWKVOFNfojGP0IrpzXeFveQtowU6cZKNzUz+Fvio+GU3nwO3rPpdWdtOvu236lFPtqPOOSTfkUs8tuTbbbbbe9tvi2Bf0XlJrenvT70ZKm1d10uLSMaM4qvRjhQTbU6aXZF9q9z5kz0brzo+thSm7eT7Kqaj8SyubQEmBpRqxmtqEozi+EotSXNG4AAAAAAAAAAAAAAAAAAAAAAI1pnXaytnKEW7ipFtOFLDimuxy4fU09IOl5W1rsU241a76OMlxjBb5teW7zKnUQJTpPX29rZVLZto/o9efxSX0SI1cVqlWW3VnOrL81SUpy5tmuDJVaqJkyAMMsj0W3rlQr2739DUjKP7ailu5wlzK4Jv6Kv5t13dHR+swVp6Urxyr0bfshTdVrsbm2l8ovmQpEl9Iq/wBYTz7Kjjk//pGwMYMOJsAPpZ3lehLao1alJ/ok4p+K4PwZKdGekK6p4jXpxuI7k5L+HU8eGHyREjGALi0JrVZ3jUKc9iq/91U9WT8OyXkzuFAxcotSi3GUWnGS3NNcGi6dWNKdbtKVd/jacKv/AFI7peHf5kR1QAAAAAAAAAAAAAAAAABWHpPuNq8pUs7qdBPHdOcpN/KMCIEh9Ic0tI1U2l6lHj+xEc6SPeuaCtwadJHvXNDpI/mXNFG4NOkj3rmh0kfzLmgNySajafo2VWt02VTrRgtuKcnGUHLG5dj2n8iMdJHvXNBzj3rmgOzrZpaF5dyr001TUYU6ed0pKPa135b+RyTRTj3rmh0ke9c0BuDTpI965odJHvXNAbg06SPeuaHSR71zQGzLD9FVx/DuaOeFSFVL90dlv+7ErrpI965om/opmnWucNP+FT/xMgskABAAAAAAAAAAAAAAAAHxqWlKT2pU6cn3yjFvm0a9QoexpfBD7ET1w03c215SjSm9joYVJU8JqT26mc+UVyPdrHpqXUIXdtNwc5U8NYys8YvPanu8gO91Ch7Gl8EPsOoUPY0vgh9jhae07Ut6FvGktu5uIwUM4eMpZl4ttJeJ47qx0tb03c9bVWUFtzpYzHC443b8eQEp6hQ9jS+CH2MdQoexpfBD7Hn0DpNXdvCulst5jOP5Zrj5dvmcbXPSFxSqW1OhVdLpZOMmknvbilnmBIuoUPY0vgh9h1Ch7Gl8EPsRTStTSdhGNxO5hcU1NRnBxxx4fTiei/0xV69ZQhNxo14Uqk4bsNSy9+fdgCR9QoexpfBD7GOoUPY0vgh9jj656RqULVToVNifSwjmOy3suMs/RHy03p2pQtrZU8TubiFPYb34bjHMsd+WufuA73UKHsaXwQ+xjqFD2NL4IfYi91ZaWoU3c9bVSUIupUo4ysJZaXfheHA91O6ub+1pVrWrG2ntSjXWMrK3YXHdwfmB2uoUPY0vgh9jPUKHsaXwQ+xCrKrpOtc1rRXeJUc7Umlh4aW7d7zqaZ0rc2lG3tVJVrytmPSY3b5YTx3vaSXgwJD1Ch7Gl8EPsb0banDLhCEG+OzGMc+OCJXlppa2pu562qzgtqpSazHHbjvx5Ek0JpKN1QhXisbWVKP5ZLc0B7gAAAAAAAAAAAAAAAAABDtNxT0zZxaTToRTT4NN19xwtY6NSzVWx3yt6s417dv+lJvKXNLyTLCraLoTrwupQzWpxUac9qa2V63Ynh/ilxXaY0noq3uoxjXpqootuO+cWm9z3xaYET1lTpS0deNN04RpxnhcGsPnjPIkOltOWsLapUVanPahJQjGUXKbawklx7fI6VS0pSp9DKClT2VHYlvWFw4nIo6o6PjPbVHON6Up1JR5N7/PIHw1BtpU7NOSa6ScqkU/y7kn54yc/wBIEW6tnGL2ZObUZflblHD5kzSS3LcluS7keS/0Vb3Eqc60NuVJ7VN7U47Lyn/S1nelxA4NTVOvWlFXV9Ur04vPR7Ljl+O08cjwa02cKmkbS3eYwlCFP1dzUdqXDkTk8dxoq3qVoXM4bVanjo57U1s4y+CeHxfFAQvW3Vuha26q05VHJ1YwxOWVhxk//R9tYoun/Zl205U6dOjGeN+GlGXzWeRMNI6Oo3MOirw24bSnjanH1kmk8xafazedpSlS6CUFKlsqGxLetlcFv8AOdpTTtrG2qVVWpTUqcujjGUXKba3LGc9u/uPFqBbSp2e1JNdJUlOKe71dyT88P5Hopao6PjPbVHLTylKdSUeTe/zO5FJLC3JcF3AQ/V7/AGre+Ev8UTGuGaN7ZXkk3SjKMZvGdnZll+eJZXgyTW+irenVncQhs1an8yW1N7Xk3heSPvdW1OrB06kVOEuMZcGBydO6ato2tSSq059JTnCnGMoyc3JNbkvE+WotrKnZR2006k51Un2ReEvpnzPpQ1SsIS21Ry08pSlUlFeTe/zO4gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwZAAAAAAAAAAAAAAAAAAAAAAAAAAGAAMgAAAAAAAAAD/9k=',
        width: 80,
        opacity: 1,
        absolutePosition: { x: 500, y: 20 },
      }

    } else {
      this.idPicture = {
        image: this.global.image.changingThisBreaksApplicationSecurity,
        width: 80,
        opacity: 1,
        absolutePosition: { x: 500, y: 20 },
      }
    }

    var doc = {
      pageSize: 'FOLIO',
      pageMargins: [50, 30, 50, 20],
      content: [
        header,
        { text: ['School Year ', { text: SY + '\n\n', bold: true, decoration: 'underline', alignment: 'center' }], bold: true, alignment: 'center' },
        { text: '\n\nI.D. No.: ___________________  LRN: __________________    Date: _____________________________' },
        { text: 'Grade:                                                                                                         Old (      )   New (      )   Transferee (      )' },
        { text: 'Curriculum/Strand: ' + this.studinfo.curriculum },
        { text: 'Report Card or its equivalent submitted:    Yes (   )  No (   )   General Average: ________   NEAT Score: ________' },
        { text: 'Photocopy of NSO Birth Certificate submitted:    Yes (   )  No (   )   Gender:  Male (      )   Female (      )' + "\n\n" },
        { text: '____________________________________________________________________________________________' },
        { text: '\u200B\t            (Last Name)                                              (First Name)                                                  (Middle Name)' },
        { text: 'Date of Birth: ___________________  Place of Birth: _________________________________________________' },
        { text: 'Complete Home Address:  ______________________________________________________________________' },
        { text: 'Boarding House Address: _______________________________________________________________________' },
        { text: 'Name of Landlord/Landlady: ____________________________________ Phone No.: ______________________\n\n\n' },

        { text: 'Elementary School Graduated from: ______________________________________________________________' },
        { text: 'School Year: _________________  Address: _______________________________________________________' },
        { text: 'For Transferees only:', bold: true, decoration: 'underline' },
        { text: 'School Last Attended: ________________________________________________________________________' },
        { text: 'School Year: _________________  Address: _______________________________________________________\n\n' },

        { text: 'Father: ______________________________________________________ C.P. No.:________________________' },
        { text: '\u200B\t                                                 (Full Name)                                                  Landline No.: ____________________' },
        { text: 'Occupation: ___________________________________ Office Address: _________________________________\n' },
        { text: '\u200B\tOFW (      )                               Deceased (      )                   Widow/Widower (      )' },
        { text: '\u200B\tSeparated (      )                       Annulled (      )                    Single (      )\n\n' },
        { text: 'Mother:______________________________________________________ C.P. No.:________________________' },
        { text: '\u200B\t                                                 (Full Name)                                                  Landline No.: ____________________' },
        { text: 'Occupation: ___________________________________ Office Address: _________________________________\n' },
        { text: '\u200B\tOFW (      )                               Deceased (      )                   Widow/Widower (      )' },
        { text: '\u200B\tSeparated (      )                       Annulled (      )                    Single (      )\n\n' },
        { text: 'Parents living together: Yes (      )  No (      )' },
        { text: 'Guardian other than Parents supporting you: ____________________________ Relationship:________________' },
        { text: 'Address: __________________________________________________ Phone No.: ______________________\n' },
        { text: '  ' },
        { text: 'Number of Children in the family: ____________   Your Birth Order: _____ Religion: _____________________' },
        { text: 'Sacraments received:          Baptism  (      )      Confession  (      )     Holy Communion  (      )     Confirmation (      ) ' },
        { text: 'Received Religious Instruction during Elementary:     Yes (   )  No (   )          Ethnicity: ______________________' },
        { text: 'Language Spoken: ______________________________________  Mother Tongue: _____________________' },
        { text: 'Brothers/Sisters studying in this school:' },
        { text: '  \n\n' },
        { text: 'Special Abilities/Experties/Talents: _______________________________________________________________' },
        { text: 'Contest you want to join: _______________________________________________________________________' },
        { text: 'Physical Difficulties: __________________________________________________________________________' },
        { text: 'Health Problems: ____________________________________  Medical Advice: __________________________' },
        { text: 'Nearest neighbor who is studying in this school: _____________________________________________________\n\n' },
        { text: '\u200B\t    I, with the guidance of my parents/guardian declare that all the information provided is COMPLETE, ACCURATE, and TRUE. I certify that, to date, I understand that any information I have misrepresented, concealed or falsely given on my enrollment be a ground basis to invalidate and cancel my admission/enrollment in the USL, and the forfeiture of any down payment made by me in favor of USL, I also promise to comply with all the guidelines, requirements and instructions, and special conditions.', alignment: 'justify' },
        {
          alignment: 'justify',
          text: '\u200B\t    My registration in this school is considered as expression of my willingness to abide with all the standing rules and regulations of USL and whatsoever may be prescribed by the School Authorities from time to time.'
        },
        {
          alignment: 'justify',
          text: [
            '\u200B\t    I hereby agree to pay all assessed fees and obligations before the end of the School Year. Should I drop/withdraw my studies, I agree to pay the assessed fees according to the provisions sanctioned by Dept. Ed. and stipulated in the USL Handbook which states, ',
            {
              alignment: 'justify',
              text: "\"A student dropping from the school after enrollment shall be charged for his/her fees in accordance with the Manual Regulations for Private Schools, Article XIII, Section 66. When a student registers in a school, it is understood he/she is enrolling for the whole school year. A student who withdraws in writing within the first week of classes may be charged 10% of the total amount due or 20% if within the second week of classes regardless of whether or not he/she has actually attended classes. The student may be charged all the fees in full if he/she withdraws any time after the second week of classes. However, if the withdrawal is due to justifiable reason (i.e. serious illness) he/she should be charged the pertinent fees only up to the last month of attendance.\"",
              fontSize: 9,
              italics: true,
              bold: true,
            }
          ]
        },

        { text: '\n\nFather\'s/Mother\'s Signature: __________________________ Guardian\'s Signature: ______________________  \n\n' },
        { text: 'Student\'s Signature: _____________________ Principal\'s/Representative\'s Signature: ____________________  ' },
        {
          image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAQAAAAHUWYVAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAFfuSURBVHjaxJ11nFzl1ce/596xdYm7e7BAIAES3Itb0UILLZRCC6VAS1+kLVagLVCc4i6hOMETXGMkEPdNsslu1ndH7j3vH3Nn9toEmrTL7CeyM3PvfZ7nPMd+Rx5R/vPXOCTwngbe63zH/Zmg+T+5d2wEMMD5X+53O3Av/78AxhD5kT1WTXOF/Rqz3HdWCB2DIs5zjMD7tvN79i6Gc5fsfXIj77x359/4nqUIc7dibSNs5cs92fApd74jPnK5N0F2OorhWwwJTDW3jN6rMdje3l2HY9p92MgSaVbfvTuv7HxfPNvFvZjiG7t6vqX55e4kh39N1DfG/+wl/y0OKcQpWuC7Ehi0iWLnJyQYqGs5/LvPuctgvZqjKHc+S8tMuVw/DW4P7w7uJDEejsveO8cZ2W2i+c3h/h4eLnfPxS0B5nUVh8gWOEV8xPBzjbim3SmywPDsL7+gEc/98uQYpI8z2fXAKPvpMDlVP9TQMWmoqO3kGA39tnj4SD2chzOXTm5RFx9tzcvYWnEVRgBc7Kq+qeAREeqbpngWh/xE3SKj8xrNbaarmKx+ThusN1INugWx4eIwz5MkQEZxCTmvOOskpF+wCdvyMraOQwqRRL6TiOrbjV52994/p+oFQTCcfyU36N31qE7h45Lxk/VocS2u+vSan9id20AD8j/Hzfo9NYN4uKoLOURc+zZM1Xvtqs4drwEl6pW+6uM1CZH/kiPRVCr9ajr7t7GvETMwHBKazr+GQ1z//ndrE/ER2D1+cT1bfeaAeAwOCciR/zmH4CyvePZZUAcQIpikgH3j3rtS0CrLv2cypKDEH2SX2djOdXZ+eW0fx2ioZYhPTwS1g9cs8G/WMPH3PyaIf69q4D0NXXKvtnBbKurbu+49K77FyosnKWhoiHgMUA3oMgkhu3gsKg3sfAnRkhQglHQth/gnIwGVLh53KriDvGLCOx31EMCt0rM6xcheZRmrC01a1tCS/a74hAkh3Ky+reCXAm5byq0jJODXuMmmXalDwugvIcLHu386WVlDtYK6TEgpsH9xBJGi2O/TEi7U7BmaVJSs2LI9+93rK3iF0ZZNfAkhaVBobpvA2kaRFWbw+r1yyatDCfVuCcAPGmIwiGuf5vftTKaHbRGdI8+rz05y++z+xS+8eOLaQhLQh52Gr/oEuGyD+WtsOyE0xGoJ31Fs8R2vzV/Yw89/J8WVsiDHeXZuKhvkUtZJKOfi8yq27F2pa1ndFqD43EA82k1/CD/Er3qDii6MXLjssnCfWUNMR/XYaOJVnvPlx8Zz0gpCiiSS4T05lel8D2PcK0zD/AcJOK9+UezeHrLNBu9/hUPEI4vx4UcacPwKq0/Js7/4TAdx7T3x7E/Qefp32mwSnE4FLRgP85YEuFQ8ekhD54BPl/hBRfWBjriEoN8Mka62svzGn3rYNuh4BacblONhLpUEDEn17EewT7KeoJuQ4QzuZkSk8QbrUkyjgNDUgBkrHgPcC65riCXl9pTwgJ+EGu1djGWpB9GRAiaw167XUKRXAzJfAzaWfxEkkbrKerD7AAxFaWUCD3FEj47r2/9Fd/fO1QIWk3r2v/jATfHpDTw6ZEv6UbcJgDe2VlDhsfGlgNPo3+3BWIP6Fih0Oibbya5ZTztP/n6t/zKvvCR2Gm15L7wPt/B7in/S9JzsZPgEid8/UA//is9KLKTgXW6pB2oR391+EB0ShupKwAGTgh6+H08NCjqFbnqwXs3LvGZf3emFgE5qfLH3yX/nZ0Q9gIjB2dzJmKlNz9tHu7ZPN36l9/F/7BoGtEhoXCOMW71+vPpstS0b112gQ8QH/4XD8RoCSmoBZ8wbhxPkSD7gKb2CA1jJ15Q6+9DglPS03Sc8wMFAKmBO7MGDHD3IfpTLJQZAqX0nt/Ez/qRv8GcxEQKweZgp7OUFb5xRXPYiIXbjD6ZDZIt+h4TGQPxWT+DORezJEQy2kQXai0Y5hv6yh/6Odmea/fSSSJ/fMrIg6N+Lq6kuTv9Ods5SSI7nPsZyOiv0j/q4gxFLGK/rFnjYbW+pS7lrAN6h69He74p7SEjsRDxRCykUD+nNw/b7vGB9wIm6WGbSJM+zkcnSUyzn23VGXYpVoeo591pEA+Z6rQOgB0i7tPEI++t8mmgHhTI9n10kEQ5sqkvRh2EQYa6mbMGn6RKRRaibJQVgD3V5Jf6QUu5fG8S+1Dyu9OHouYkN+gC78aAO1sd52X6N/XNaQtpYmuHbLYbF5tOOsZ4aAXQxm/V8fY1/SImcIBfQIQh6DLfyApODYSu3uRK8u/gMcD9I/4NwyJasqiAPaQDH0iAaFGcHDIGodUTxh9W/kLsqjy9fmf6TfCYNehJ7yCfijsIsNljuiWr4XwtQWEULCPIZv2QRY/i1vkqbtoOiJ9p3KCg1EgA8pSCCIKGLLD5PXbfJ0tpqtDeotDVEx/jtmSBnAMhIXuIibFAxzeTKug6bPsuqr5MDtQ8fyTy251CezCljhVWSXJOHeoNTt1mGCUsUoLdcoC/J9vxEP2QspwPoUfpo5aeRD2jSekXA1BNkqEb9NqL6wMVgXMUbO91y6KoLoRN8HrkQnnrjj6QIChGFlIzWEkWRFLMzQ0uKe9IKHybW2LvyPP0pxcZWzXPUcrO5nrUFR7WCtZhpyTLRIfoP+RVReViu0ex8d9c7yt8ccLoktVUyoGg/btdXZJA3CiIBSMgbDVGPpSU+8+AHQHs1NFbGFlWa+C2rM/ReymQFX0l/BVB9QncZsPc4NlG/ntUyhk+kSKZ6SWmslMZmVhZ8zhIakWZWgGIv5Eu93n5Vb+cmadLnZYg+avSxXltla3/ZqElB4CK687Ju4BDdYcuJGuoT2xoCyv8AEUN8+0W+A1APA7cF+nANp3AG03U0C6imCBTjVevTxdfO7amkqqweYlOjH+gJlHoWZoOxtoMVBVXnYjowWlgpAB/p0cZd7Ka/pC/nS40+LQnebL28faY9hFqSwG78nC+4zDTlal7XowhAmG7UWgNZWeKzFbeFP/4L8RAJ1Sxh3rzX/NXhPMIfjGc5iipmcgARugsCzcZvG/pvnm6fkv6TNVw+Ial38QBpz53VWKwOQQjlEBtjnaxz3MvVeq4xxNhZRsoT3KUT+LkcIqdZy+wYG7DE4BLiXGJa+mudSG+jOkwGiC/aKAHwxxvP2Xq1HtlWgoSlVIfn/nnAwkP4O4O4Xb/kCz7mKv2ppOmrqwH4NPLj5N+Sj4I8rtNBnsvey0AxHaBElsBKUsRCtkMDyxBksaZd79ZQoxDnNpmuM7B4k1bZn/WCfZgco3fJu7qb/g5I6UJBoVjb/HkBnalGYcGsLeMXXcYh6ktR0C1a4I4/UsZlPM0oTcqd8oVArfzSOEFszshP8S0OMX4sR/ELNnfiix4ob6HoOmpDn7KWGgxYTD6pLh9xSfIe90gzqMixiizB5GZ7uVxnFOtd2iQfoNquENNbOEpDvA/xCSUJGCphYewuI4iGQgyFmNX5dpFM4Wc0y9myTu/UKsdcfFGv4GzZJy+P1+pT8gItubRrw48RzTFbN7Mh9HmraSRqM1sCiWwuzlXeM17TpF6kI+RGWWVfbu/IVTqTuCQFLtSz+C0DxQimsvrjl/7slG1zC7cBy9JQ/D+sNsMHnfxDr5Ub5HC9j18zSW7Oi4L7ZKbezkBccUJBMQOpBlrJQIZHks2sI5gDDItIYiq9GUw1ppdrjU4CvsTh1HCafMKd7M2lMp17qEJlhf6Ia2QBF8lf9JJgSFlCM44lZFvSdTrEa6FLIN6Rm7o/s9xAsd+QY/Ukngbe4Nd6h87iNlBo1WvkUi3zah3DTfYKGaXjdSdG2yPpK5E0S0NwtTTfAIZp32nUsoSFOkfmsFA3EEhNUIuv5Qj6MJY7tVbOV6QP63QXuV1rOYFWncxpdiu3SSigHrYBKRB+6wKCuHGqsMieG2RzZelO4SC5j5P13/Icx9KkdxnjuJm5zADgA2aS9scWAYizl+7D7jpae+YiESawtDPXJP9qZBkmCob2prfuCTTKIj7nXd5ms/jT81awgl1okztYLFFKKOF+reIk5gMHc499s2Fzu39e/sIJf57yD2BlhQVovEEaPySnRfxVT6dc+xlnchLTuF9OIs3FMlL/xbEyByTpTZlzJtafE+0TGEVFdgEsbCxn4GtoCxBkI+uIkiaDYmJgYFQwkYmcaS/nBR5nfnaArpF+oQfTDlKiRVRQIWfxiqLIUr6RffVWmuQR3WKUkYCPsvVaJLL1yjwsGCM+ltVO3/xUfsU7/E1W21XykhyvT+g/OZcO/QXvcCRzQqsUt+N0Pdnua6BYpLAwKaMfQxlDGX9jBa1ZbvAA780kuIAE81jKGprIIMSIFOlYxnKe/Fv/pR9L2lOLtRGAkYxCuUz/JYBEuJHz5G366p3YPCtJf0mFnwThUccuFFluZev/XIOQ/M6a4Vd8I/vwByz+IafYj0ktV8kqDtL1IVkoo+UM+6f0UIQUHZj0ZQIT2J5RlADtTONzNgU4ZAEd9OBoegNJljCHL5nNKtpIEIdy+3T5sTyhdxmfeE0UA9Js5FW5ERTi3MSveF5+ag/gbf05r2sST1BK80BmEMeSbfBEtskx9OOe3v3irQsxnuNszmOtXqblgu6nx8o5erck+QuL1FOhJFCiZ+t5DM9WxbZTwn4cxC4Mcz07zlDeZwmmb0QLUPqTcL4zjnGczGrm8Sbvs5EEUTTGT/RQfYibZb1bw8ksjjJaVBWKuIWzeVTOtxto5AzWUScB05dQouTWwO5KDvEXCAt4Sr7cnytaLucwSO/nYq7TOPAUf+JauVd2pVxXSQCgZyI36dSsvkhSzhGcxCiKAvb6cIRF9PMIukZWYtDfC30xgAEcxDKm8W/WESeK9LAvloO4kuchV98IshgE7c51nMXdcqG2gyivaig85E3ZDkt5+oH8ECFYxOKyk4SbrBvkJH1PGjhA/mH8TH7MAnmbEeynt8jzPmAlzvn2qzpVUFqJcBSPcQM7BsgBMJg4C2n3THwRm4gwNGSfmYzgUp7lbEppJoOg2+kT3ESFN8VPDS7Rs7hZzqXds6jF7BG2WhLiev5AaUBSsEDNY6+fnjm710sVh0QftP8pJVzIc7ofp+rFtGaDfp4renGX3qrdhSTN7Mzt/J3xBZ/fhwpW0uqZwre0EfOINu+rH5fzAIeSoQ0DjetveZaxHq/KlnvlEv6EevKwyrhSX5YTBPeP4SODhMQQuzge4s76VpfsVKcug+0zN1bMHnfegE/7/kOL7amKipzDI8S5WOf6EpnH6zN6Bhi0EuU33M9eW3x+L/pQy0bHzspO/2sylNFni9dtz+1cSy9aspPfnxd1X/K1JwKLuZEmT+BpO31OL2EVVrauXwMRDwlUQ0rXx0MKJ8G5lvlnRg+zcWFzA8krbeQE+QvDuYkz2YP7fAU+u+kzTAGliSHcyUVUfMfTu9GHFpYSzT8zzVKUanp/56RP4FEm04QFMIynOVHw5sW4qulP4Q05QP8pB+kzQcGsoU7hD5SXVSh52hlKTCdxl3FR06gNHzXdvuGkyMv6PifqG9zPtyz13XGyPsVowaKJPXiQqd9jFDEGY7HMtRfXUIPQi27f4+ph3MMpdJDM0vZfnB6246VK/6mP0sZxcr6uB1UvMYJX6DY214hsGxWlQG8QAehl389qOZyZ9v9t/qWRlpvlPcpkf3aSld7ESybJYwwSLNo4mqup+p7jGIxJA5G8Yl1CI8KQUBMg+CrlGnpxGyniaAn/xOKxnMiVrC9yEBcxWR6VK3Q5njh6uAcmvjYcXSiyOlWxHdAiWcvcxj6dMXyrcflSjov8XFbp7XISzfI8V7DOE6Hegcd1SNbjOJW/fm9ywBDH38iNZRFtRBj0H4jcX/NHhCQCZXq7/Eg9/pBxInvJ45zGcsVfdCShQepC9ftdYvZ6yZEtK3MRZhi/ZQ5XY+toMnov+8pHeg9PZNfbZb33516GgEWSn3KNa4m/+zWKynz6dRGwBItiRv9HczmDq4lmI8QV3MturihHkstZau/JUDtQ2dVp1LgLPQmpjukiLCtoS3g75oihl0gV51Avh+ljXMU/ZBX/0G8YZLe7AQhJyK06EYQko9mbWbR7wDsJicfnfjNpI+EIDps51LAAkwQb+JKUzzkV/B2ucmM1Gc2evJONu/TmXg7VNXnFvYJL5VmulDNUQUzdnp0p5RtmaIe/TKEQyPofG0pbc9n4UDb1eOi9+IyB3MH9cqNO4Fx9wthOX+V5+a2d9uTIXqeX5RbMJEPmP9pX4gJObGwnzGt/J2zh7d9gEsPqFEX/5lRayffu0kflJD1H7mUQV+hPnbZnj3O+1AcDtv6Y+tddB534QZNONed8epyeK7vpxZyslTyqTxjohfRnVXbb5KAKjtMLOxcmnYX4QjpT4TMzpWBPFW8Kd7BEORc3VI/MTrnesY+Si+TPLg/8Jt3OyDDcet4cn3jQetnOJM5InWxtkIuCuVnhlY1dxiFhjcqcQQ3Vd5nDGVRwrp4qUe6WL+1njffkEFLZ3SwoMsx+WUb7kwa8u812GZFGSJUuAUA82GQMXyVv4Xwx59stcjjv5VsPxqWHrtNXzQPjp0aeTFsZyipaX+7YzRzuxuH8/JF9zesqpe5Wana+uUtOzdvYDVzMBD6UOJfIRO7jVJ2GwW2a6vTlRfQPWXJIvgOiTZo0STpop41WWmilhXbSCJGQthbBkpogAJidZAQTi478PVtpo50kSTKOuHJF3Ev5k1TlhVaSNeynexffwWOWZSO0NGY+NKKMloL+uG6DlbXNHeX8ql0ArecZmaX36QzO5FV+L3foBTLIftXTEeEIOS1LxhYHvjYQTCJEiBGniATFVFNJMRaNrGEFHQi20+tEA/WNEkKcnPofwCCqidJBE3U000EHSVKkyGS3kPPdEkwUmcK5XOu6+yCJpL8y6MCgiIxpb6fIuuDcNTRvqwtE1nY+6N0j3wV1zMBi/bP8gj/zV1TQqs4cK0WqdKaMz3LLLvSlmFIqKKeYIooppphSSoF1fMs3fMtq6mjHIooZyLgXvD0TOwWVgWCRxiBBJQMYySjGMggzzyUdtNFGE8000UIjH9Oe3aMNOlU6Jc6eOj3+cfyY5qYIRd1SlyYv1n/L8VhGoGOpl2fmdR1BwkK5+cjRhdqdOdoga6jhx1zF81ymtb5v/pE/g9LGntxFceA+65nFl8xnBfV0YGNQTDfKSdKQBTvwtngqNJooFZTSQh0tTii3igGMZgI7Z9tteQRNmhv4Vy6W8jQn5vWSqffJGcYcnWGU6l7WMH1PTpCNubX3b4XOLTG3qwgyLrTxqzOwKp7U7QRMTJQ19Ke71sixfOIi4jBm0C+rfx50dbFsYhPf8AVzWMlmUigGxXSnH32IUstK6rGRNEvlIz1ce0hoqzRBkZQ8pSPZzi42KGMgfRE2sIaNtJDBIEoZA9iOCWxPLxc2sIGj2UgCG1qNE/TVvM6qtK/U4yWmFmvkbn1cijmAiXqDbNAAeNT5+rorCRJmkgIwliF8QiXdqaSbVkopfXWcnO+pQbtRLxaEFo7hZqCFlSxlLnNZzmZSTpfSbgxjJL2JsoZvWEYrJpFVfMqrxqu6WRcxOCyG6SxLip2NJXqoHsru1pgMcQYyhsFEqGUhi9lEGhCilDOQ8ezIcAbRDXiIqynO6oKX9ERpz5n2NvSkmo1i6/Z6mBzFCG3jIPnAy6fepLr5XWn2hvXGElTkdp3EW8zkfRrzFxSRwsrrmdG8Rb+sAryKKLOZz0rqSTv3itOHHdiVAWxmIV+xjE0I8VTkXV5mpsxVBKubzGJAuH8sAGn2kQ9B0OEyWX9k7Z+stihnGNsxnh5s4ku+YhXtzsgjVDCA0ezIQP7AOuIA7Rwvr3gSrnvzcz2MXQWdxZsyi09zKFd4Lu/XXUmQQn6vcSaH6gGYbGa2vsybsiobdHA5kVfZV2YnGCXBZjocjohQzXAmMYk+bOJDPmAxjdhE7fg3Mo2X7W+lKdfll27MYsAWhphmbz7K+S5aJIM5VI9N7ZKKCqUMYhL7MIB6PudDvmUTKcdCilGFTXuukfLT8hM68vccyiPsztc8xZssoU48iEBYt6H5PwxBPITpLaWsUZuJHMHeDKSE+TzN3U7/CwTty7u5EvM07UCEMvoznilMxqSO93idb2jDIKLmGuNdeY53tKXTSQTQPIcE+7srkidI1tJy3o/LrhxnH2wNysQtYgxjXw6hJ3G+YCazWEUjaZQ48dwd23Uv+Tw/wV9yO7fz25xV8d1oVZdBJ94aCE8U4GT+pO/Ih/qZ/EMvlQHsp4czMm8YARycI0cHUUYyih3ZlR2ABbzA28ylGZMEJev1M3md6SxTX2er7D7S0BY1Lg9Awd0YUJAk79vvGz1k/8iPdBJDF/E19zCK/diTy4mwiE+ZzTespdUhiRbJqfJ5/hllCq92zkWzCHEj/9XXNpi9wSVR5Eg9gB2ZQJEulbm8zyesQqnptEP1bZmiQJLdOZsR9AYWM4MZLKAOJUFMmakv8joLgvBl/mlFfJvNlC8A36SZpF9JoKA5j1EPlr05hv3SxR3YVDCSqezJzkA9S3mOaUSyuNp6e4Ksc647Ql+Qv+tFWeLIWB3HofqB/KOQ+O5CHbKdz7ZSr5URoydjOY6zFengW07i2/xQJzOTCGQQ7mAfapjOO8ynAZsIcTWW8xLPM1f34UCNaqQgapPgR5QEPZD8/2xep9YLDHV+27DE0q+MRxjNoXKMNT5tpoESRrAXhzKSGo6h3qnO0nPkbkcK9NXXGKd/k81M1J2kkjJMnue4nCrRgHLvQrPX7x+7phuVAbqXHM6eGpcNOluW6M1Spw5Qbt/EbwEyVPNTZvMRDQgRTIx18qk8o69LPdhwp5yTKYATSSCyUaixZvipCopBBPtj2V0AKWYvjrf3sQfaRgaLEnbkQJ5hEdHsNW/IIdiOUD5Ob6EHSW1jg8zlK76ggflZoyWM9F1qZYnP+gdgNIcxgQOJ8Snvy8f6eaeENQBK7A/ZIVc1kiRNnAjSIF/o67zOfMljvPYt9gW7MJWObUxdDnslmM9byLvs6yreHCgH60FM0r4WSaC4c2k3yv46N2tSCDqCHWQT37ABFHowWpbpWglwR/b3LlPqhicC4irs2pebnK3xln5ILbagETL55Jg9dHhuCWyiJNAvdLpM58ucFeb2bKKUYgSAw2B5shTIonR/YtCZb1lMkcMnrtcqvYd7rZ2M/SIHRabaUbtzXt10H2Ouw1ulrGGNDmUyw3WEDNWeMpQTZC0BOHPrX1tFENsHLOZ9kG/0KrozmEH8jsvooN1ult/xpuYCQHtKSX6x1sur+gxfSW0OkhfnCKIcfN2PSXkOsZ3itpwoMkJz7r3ZUW4vwULzZaMJ2twp1tlUcGz66MSv3v2q8V5zrB5jHxkZ6lDMYIrems1jpKc+ooOkiGISAm0s4zNNSCBou6U2t/+jiGFoRMyw18gjNEla05RITwbJWCZJS/6bCdlTATbKl0yTV3OsDmkqnLTMFjL5iEcHTbRiU0Yd9wJn043mvJDpoLWgzlCKKM6TOIZyPzWcyhiasCmjxRVTsYkTZRKVQJwDeb2h/aPqj/Sa+v0ix+ue9AV2ZIgsA4FN2sxHslBrWK3fyErJAEW2r1IMT+v/LvJDggcKCSj/0P2NZaxnPeupYZH9pNyEmc8AHEVf+YTX5HU+c7elSdGNE2jHJMqjpPKFmRv4kg6UCJuYjfARPbAcE+sTFjGVTIiKzwLOXxLhR6QcoWrxBQ18QiNJhITTlCOH8PblEJqxkcpUeWz13lrDRKKbX3l26bOx0fZBHM72Ml6WgY22yolGMRZ1ajFOztUy3pOPCI3LdHnWSUh7vjjT5VutpIr+bK896S6LuEyfz0vzTVwgs9jo9Q3a6cWJxNlMFIud+Ni5m8VOnAps5ipqiKN8Th+upgoLkzE8zF+2MMIrGMSpZIiQ4TrmATEWsoFLGQW8wev5JSxmGI3ZtgR7S5H1RCmjSxsmFM3er8li5bfmt3KP7s4aJ7vEQu3XpYlzJMFzOgio42fywpYK27pIZGkYoJbiVlCISZyYxJnAA/yNN7XF2S9rda17yDZKjBOpJuEARjbD+IyMcwzLZxhY1PMOaYoR5vEtPajEwqD2OwSC8CENZDBI8Qr1lGDwLTZ3MxSDhXljJEMvzqONGbSYxij5RlHD/gUNHR+XMoDlRNB23rY7d/yxbM8/WMbdDJI79UWm2VfKa5IST/+gbWk3vg1l0QHb33beS0kKFGr0SzmI4lz5v7sRWAaDCiwmMYQMGUxDSmgnk8x/02Y0J9NCmoOo4a/YXEs/ehHFJsa3PLvFEVrsyhG0YWCxNy3cwFrOZye6UUQJrzMznzQ0mEpKKaGlnD58BBzOeH5ppJW0TycoirG3NspjtDMU+JzX+VzG0stpCRKosOpiDgl0NOnBEZpisxqCprB1Z9lLPpXN/vZ/Qoae9GQ3UiTZnLWsDmKQ9SAZ2yXMmlhIGigiRhtKnChrUYQI676TQzay1OlYGqeUdlKYxNmITYxNLuV/Oe0YDKS2TEqoI8FZXE+7jeV6QqeBbccF2oEK0DZBI2pL2i8vpOsJQkjSDVVcRw+PX/AJvyHt1RlKioHsg0k7NjbNWff0ZPNx7RCXQS3UsZJWbGK0MhFYTT0ZBJsoq79jFwrrWNoJozOOgbSygBRKKetdXsMq4kg0kyZhI+s4hU36YTYS30HSCXu6Yh4fcSK/4m0G0izzdHu2k6/YZPgg1m3RIttQFu225bO5JnJWtkMbgmhSamWN1rmJZqNUMokybNL5rEMTPZ7DzCs1n4mYFVpjOCLXlYkTiRB38ngFg4Xc8x2e0p4cSSbvFp5ICtPxcqLEeNUZk818omikvTdlRkrbOIFrsnktZVxBiv/jW2IOURTgEY7g55xBQu6yV8lTlHEPmeDxSXRtFW7YybYgm+RFf5Z4pytnYVNCMfsQx8LK53BFYHtu0CpJeLuFGMzjcdqdp0Roo4MqZ1kirHK79qE65G2aSDrdUpqAMsdkhuI8pCFALRE0wwTT1nr20Yz9ibIvw8lQSZx7+DVr2Ugit7yb9ShOkjHM0yfo4ELeZCYeD11/CB3irRqynShesLu7G40dhMUYBtCW37dKEcmBVpWex2CptdcogkXGETM2g9k7m+gIRLiVDxnj/G7Szog8L6RIAEsx6I/pXF3EtzQ5/BVjBdX8Nf/UOBZvuxZPkTS9NUq7HGm/pakDGEsSkxQZSnic2fycZhI5DdjCvfndv4hFYVbntij2yH/rInF2Pb5M9dxwpxAlRSu5FE1BqaBlbPM9lAFJ3V3f19YIY1ngLGqG1rzIUjKcyUQyCAYxlhNlLU08QgcXUMV11NKLen7Px6zkBM7gAAzSpBGEdTzL5mycHMiQci1XgggZrCIdJBlrRObuA9mepnx/CKWFnbifn9Lu9CXyd+228/nI8l1Ixv+OIGYB7z3j4hERVbcN1pEHRZzOJxfI8ZvQGYZFJegAnuctfm6sG5rtuYtBDXMckQVCHTsyxfntb2yijs+5nA8x6cMNPMfTVHI/sJm/kuIK3uYzTmfPLHLI43xFwgkCF7Myn1xtECVKBtuSSdqarj3y2+Fm294ykk+ZjZ1d5ma2437OcsavDtqe21amz8TpdAx+oF4nvsd2Yy95QS0Ol17cJ6GBLGAAN8oo/caKsI+WAilZrt3kR1yb+WVV+458hYHFWI5xwBGwWeIg+cqVzOBJYiyhnO1YRJy5lLMT8GeUKN2Zxd3cRIJTnEE1Usnx+VIgk4RzmpjF0STIxBmjY4wJKavP7wcf0H5hZopGtCP6kl7O4uzitjGYCFanqh6uw5gu8BNp0BcKN2/qUj8keOq5Y2vdqmPkVi61F3OfQEKS6hud9NKzjdfkNDstEOeverRcri/RXx63fyR/MZYaThe5BTyfBxCFZezrwAFfEnUKn9uoZCkrOIbreJ44HUxlNcfzBL3owyiHPyDCZp4m7hxgX8qc/PZYQXcMm17sb8d007gRXJGuFgRJZI7v6FV6grEhq2WKvE5xBffon5jGJfZT+oJAAs8su9wPCR73I6hgktE6WahX6FQmyir663m6Vm7Pqs7Onom6PZ/rS86gk/Qy7rcfKaK1QZ8yilnVWYTTm3Gk83vvK8dHibI/1/M3BvAVF9KDPdnIyWzkCXZhf1ppYAL7M5o3OdFlcxUxId8wM0KDMwuDL9mBWJrpxgOZa8tKep1JcScvp6d2/D56iZ2CBNfTkSWFYGpGvqJNb+UkHWMUMVZ/Jl/qE8G+QF1IECvcLzlTj5C31SDGgaAH8L6UyDG2Zo3ej5ji1EfJTEnm6/N66G7GY/b4IoNFyfnW15H0BuYQAWxi9HBxSCxvDp9PJZ+inEZPTuZo3uI2GmnlM6rYjUsp51RmcKCrONrEoMwxXZVSivJCN0orKWykXqkoKmYd5fk6FCF5UPTaSC3A3ryQO9LyQibp25qRYvYDPZ3TadaHUA3p//IDoL0u1aX6qh7OjbkNrt2lu/ySD7JxAZMJ+SIxDKYyVOv4jA0yWGJ6NLWpvwzuaHp7aTNYtBNFMVnGK3Tk+aoGaMcGIpzMsdhs5nE+o54qujOeYlYzn5mUMYHd2ZnJZBybLmsrvUHcGXeCb1zL5RTRFWVz75TdnHcUgVHsIc+DzfYIFgaq/JuDuU3E0Yh9BTlF50bwn7bepQSxA8rLBlgr1+qeUpUXa6qHUasfsEGAaO6KKVxjby81FNFoXyAR+rKveUvzfoveqG6uQPgc04Hfd+HsfI2tyTruY5aDCmcj8pvpyT5OaKmZDnoAbczhA/5GOSX5mGGUlSS4MC9mDabzjqs3ZDaJLkaKmrzWcQgp7b+JTdc2w4lcKsBiuVF3J5E3VNIcLW3MkM34DnbtYizL30lOhWO5ojORXBGxD5Od5Q/ygDqlmIocJA/oq8bvZakWywX6sLyqGa3masNMPtt0UZ86i2qWOz0VZ/EQbc4TItSykSX5c21tTIYzkM08j7AewSRFb4QYA4mxkHXZ0huUKKso5V5XAfW84P4tNtlMET2wPViUtZP0lSWuw8+inK6XkHCVBUX1ON0lcp686m0+3qV+iJXHlDqZVKo4WDOykFF5ErUav+J9acwuCghUyul6sTzuCL3fsL3+XA1ZKRfq2sjE4n7puggT+NzBjvoxhdb84kwl5oq6A6RJAmmmU8+5lHEvSzicMmx25AhPYSdkXFBLKUnHU+8EzYlDMyMpzqO8SgYbY4VsyJIjnR1JHzmERlnB4LyeqDfPtudkMbvOTid21yr1Sof+TY7nDIps5pdi6+FMy7eHLZER+qACGU6gJykkVn+15fSPsxH0n7qXwB28BfKNYQg2a/MesEm0U9BhB1KCTMpo5iOGcwxFwJW8zkdMoTdJXxctQZwWNaBEiODuc2UAWpJCKWc1DTSTJk5femOBpjLZ+E1fVmHCGk7G4ly9LR98qGYI0zqrHTNEqOhqkXUYYJBkHjVEqcnqcY2lBPZS5E12pIf8HyPsQ82Hsg38ltINA2ql1lMX+6Ys1N4yI+v3WrZJB687sIXBCt4MQIhuhMzgc0o5k0n5zw5hOP9iEZOIY4UmDWU99a99IIiNloLJtzSQxCIDFLELu2CZKoJSxtWchonakjLQyWLzpuyt7fJX9rCPkKd0TcbBKsoYwHjHgOgygmQRolaGEWModbRi0sJcIqZRZf3a+JfcJ3vJXxSZKPXZpJ0v2dHZozkMyATsZt6XPfg2u8QtZEhhOka1zc78NA9E+iFtweR94Me+Vkwj+CNPUcwxriv95+ZEeYl38llZBmBgVxhALeaqyD3Delb/aoOxhk/oyZD2jJW9ur3TvO+hqmfJs/ISJtdZpjHBaMuBRiMoYwBtmJ02ZVdGDNMkibIbNURoow+LrFXnd2sawhePmXMwDFs/V8eKieFvyOIQZ5Hdg978VNfLtHR9hgyGo1YNZvFgHu0NZgVGmcfEkM5YpQzhMTaFnM+OA8IkmO+MirwmpBwg2sJFqecGbrfrqQ3Vm3mHxQybZaRykL/pwIhWk/EbNoHeYZZGOcma/fk6tkexqALmk3IE7Q/QlTQrQFrpIEqGSNkhJUWaKd2Uib9T/3IVVayhlgH0oYIUTZ4TqfLtwWqkRB9mqkb5pfFb3k0whbcwUJT+THE5hn6QW5jIJ6yln29MjSzhNKpIBTIbc7qphA7eyJPEQiBmlwuqUiL/SI/c8Jiu1ure7M8S7FfLNZukl+Ak2tlEObOT/ZJxNtPz5Z6lPfuXMzGzpLW0OUGa3qzb5vPUt7FNrGBTQpQ2YnvudFmfgfEiS5saB66u/abl87rZ7St2ogUTk4hjmTlL00cm22NI8ZYssnfhMR6QQzmeR/VoPjNdWVapXAWUl6sc/owA73Cab0yz2cDkfHZXTix1orEGaY84EQS7xCi3kWdo1LNj166369+L7dBOOaMsWZ5ynvo6RaQwsamgfHDFjiN27TO6on9ZlUmv9upVi65v+qCYaD4EwQ/Ttzdr/ZSToLbfkQ/tMTSDnbVadskcXceyr+e9s/ap1EdunFdQkfP0TCYoiqy1H9JVxq0s4QlNcIz8lK80g+OHrOJzV3ait+lGFBshzVf8yNNfK8P7wEwsjLzf4c+BL2GJ+wBwFCq0TDI8qB/JweaATWfNv2qXtel+NtX3xhbZQCIXFiRNye5TThy/79Dx3YjkhXB0uw/HvDi1em1lvgEBP4RjiBPhy0i7lhy989BWJ96Q3b/V9Bi/y/iaU959fs01uiI/TENu4RyNiCIC/bjMWGf0sJeoTQtoD6K51OwM4zg6v6ieQ+0xeQmLIzB5j/c42jWiOVRxIvAp3/ATT8s9t7kc5e28yDIQqNAi3SyLpFG/MAbo8JWlk24w/2Tfb/yf3S4kmMkHxFEyg3e6fJ+j+3Yzscjk9ZuQYuehbx7d9s+0ZNTath2+bZ56O5V79j6qvEePRmvqSmIUESOGiY1NCsFgQLf9zpo9YvnR+S4Ox+iPjBOYyxCZxBjdTYdRpZW5/g+y0DkWFcFkEdNDIudCnFriVPE4ldSxnB/lvRV4hRSv0IpJN+5mkKti0+WUM8eVrC0AFcRZy1rFWKbE2FS28qZBT6c2ZAXa+3xAAiVTtcODO+7Vj6RTL2w6fSJStJFmwFmRkeUVvTdu+HfHB5EfgiA2Uj38DweeO7A4w2QsNtNGHTYRiqkgTrbcpo4W9t3rmdPW3xohgg39M9fK8wbmUt4CHSRXcIqWK6gsVou3Ok0Fm2oGOiFcb9uxFj7mMmLcx3iGEOejfDvZpSTYkxgfcgAjuIqBjMp34OrUIjFWOp5OLiNAuxGT9UaHDWkw63TGS/x8gwAR6nibYjKk6X3afnutpI4yJzcsSSPtpDGIU8bJO5g7WExm1blv3rnhWru+65Mcuu12395HRWl3ENNKqrBJ0UorjcTpQ5QMNfSnhL6HrvqXtkIMeWZo0mR9Lot9Jecxgu4A+p584O5VqpQx0NMZLkeULzmZgcBRtNGDNj5lirO479GToTSzNxOAs/iUbsSdBIxOsRWnKi/ns516tYqIttsIVIHOYG4kj0fNREgB6ZK+hxbTk9WMIkKaVXSQoJQSYmRPb8gAFoOKz/jte8PmnpUDU7qEIMXY9Lhyr6PIx7xzkeYYcapIsYlFDGATFZSQYfCu8wZlFghR7LW9SdBEA5Fs0KqDbykTbOzPE1dUdRh0dvBZyEsBuETIsI4YG9nIcyxmChZNzGYwShM1lPIOnxEHRrCRuaymOIArFfF1njsmEcOGasdhFR1DRh7UpJIhShH/5muKUJSSQYN3zVBCJSvpzmoqGeiga7YH/W4nyl5HbV698QKzKyOGRXtPPdfIHy7vB+bj9KOd2YykJzZpBlT1OKJmgdJCMZYD0znHGRmasqLZZhjGzJgIDQ5JLHbkzLzidCVPkOFLruBd2hiJTRkWv8DMqlb2wGIzn/IKEzmW31LS2aov/2+cabzn/FaddUT7KkYM7H7ST+8zXoIML3I6jax1AmNKjyMGVKVRelPLbHrTNyRQl12bNFGmnPvatPR7XUaQsp69zukWSRdwgQzSrCDBHhhYjm88+sxZd0qjzSCizhSdtmdK2ozlaztUeR3TgdcX8mJeqXfmBmYN19HMxWYNN2MjmI538i5vYRAhRgkjUd7ML5nbNSxlVv50Kyu7v/sJdjcRo4Pf8E7uMLJmbmdtLhJfsfeZUZIINr3pwVoWMzirFUNIkqFHZOA5GxYUONfvv0+Qhj122V22YAovp4y+WPmEmRTjRg4+bdM/E9gsxKTRMToFVckQyzF3lLfyO9mmjN5OlX62uVnG5Yz2YjJP8SIJomTbPFkUO/2x2pjI+dg0U5rXHRHX9UVU4un0VSTdFenHOP1aXs4Z1i08Syt9nRSm7qeNG5nbgBlgIOtYzlAfyO9+Ddh94R7ZwzC6QodMquhmFyCHsJpS+pF2cpZyNU+Tzn3tqdKNFusAI590JqB2xCaX/7gQi4izWN2ZQIOzbG0oJR48K8E5LOYbLmYoC5hPgmom08KVVPFrhtOGuztwGzZFjitXwWIvNlZhFtvYveUkvbxzHo1AT6f+MNNj0rlFnnYUGfpisZrBBVxAm8puxZO6jCB9h8eLtQBB6skwhAxQyjS6sytpIM3YsQvP2nhdzGkMnnZfEsklCDxNQ35ABl9zL0mEChqZhs2xVNHg8r4TTKSG6exFAwmiJFnAIlIcyidOAEqcpAaDZ6njUIbQRIYivs576hYWWmS32Ijo0fIvluUWtCgvJlMMPWvs2Fxaquk0y8nQj8XU082dr+XaMLHifsO7zsrqZhTQHzab6O8InG+4ll3YLT/Bnc759PnSb7Mm7PpO/0AkksvEsvO+tY3FbvwKmzRrWclTwE4MoR8xrLwZHGE8L3Em8C4NHIlwDafyG5eyFSxqaOB5koxiMr0owmQaH7oP9WuTdgGkv/RgWY6rujnVvtA8esdzikgBJSxkAYcSQUkj9GWtA9xoiCYt6tZlBElHwxlV2EyMMtLE2chFRDjBWT7IMGxg7aXLfpFOlQDRvH4happZZCqeL0bIao35PIVFA0862SOXUMKJVOaz2LOIVxO30ZuvaSBFB9+wJ486RXE5m2caNRjEuYXbOJLBRPg830gzTgLZmDYVgWI9XeZLSy5hTBDaiMaGXDp8YBqlmKWcT4QDnUyvJGVE2Zz3avw4eDraZQSJpwxMDEfuu5sSb6YnGUwMrmQddzCFDtLYKDZRtj9t9Vv1jyWwSTjGL2g5mlXaHq4TJcoo0tRSTJo0EKWIwfTIG9tZW+g82immDzZRkvyChIP02g4PpSgjStavMOnPCMdTV8mKTpuMrR9yKLAQIyc8i7FRDJrodfx2p0VIE6eRiynnb7zCfZTwB3YkTXfWO0KrU4fmzt6Jp7qMIK1t0GS1dUQj5fE4ZFPNyCbLlKAkuYv3OZ5iHuNrhnEqSZQ0VeaE69/6QhYaCOU0YqEiJabajphyhZJi8DU2Q+jGrazh98C19Md0BqxEsSjKwZsYTgJEtmtpB0YeCLe5kiS/ZzUXMhmIUkxPFGJgMoPDiSB/M1r1LP6sz4qdNYzLSGbvN2qn66vNJEKUB2jiZWZxLRXUcS93OIZGBzFH38SADpqTmUxRImq2tnUZQb58bdX6zCcrN/QtjQ/vv+eo3YaWZxehiSKEIj7iEeB1XiZCI73Yi/4kgRRj+2+8be4JpQ3KSqJUYSm309fGm5oqMDvGRi7lSIQo9bQBn9ATg3YyQITlrKedPgyklJiTNpGhlXWsIE4FYx1lW4JNinrSzCVNMxGivIqBMcdRvljQwS3yqp0RWzHIsJ619EdoqtzxtrH9UwgmDbzFAWzmYrrzMDdS47i3cZrpSZoEwrKmhZ+u+SC1ZG3LoF7mpLpZx3UVQb6+w7D3JEYRs5lT9MWug0/f/7Q+0QztlAJJxnMYCfoxgun8mx9RnXegLCYfsOrahl9H0p/Tn1IiWDMkrGf6Ixwf27eGOn6PTTN9gMOIMZfRTmOY+5lAGbNpoY0IMYQMKTKUchS9eZ/TiQAW8xhEBb2oY38GkCbOoyzFXMn12fF8xYTs/l6cM5FbWcoGDicTrbp20gGdJdEJpvMWKW6jL4uYSowUki0ypoia9NuPLH+49jPadySGMONeNbqMQ2J2zo+IEW9vnzHnw7lPH3zV3pNMZ2HiXEKCIr7hLfblEmw2UEoGwSLG4b94e+nam2NAE6VOxpZ6wBFFWvX3Mj1W+RLdGEQbFcCHLOMb+jKEBDEWcya7cSibaaGNNGBSTAkVVFDH87yO0E4NK+jDdkTow1d8RgkNPABErtBV2XyTNUwkQ667fIZ6p7N2iv4X7PeLGCmyHW97cD7/R4pr2I3LaOZUOhwOMxHe/mT6Vfp2NFPmaDiDRLakuCvjITmPOkJxpnb6wo+WX7vzWb0S2cI0E5sa/g/hOF7kY2bxJ3agDSFJlTHlulfq0g8KUE8VZU5iXCwfsBWAz7jOuCHNPfyUybRhEGUGZ2JwP+soxaY7UJxvpOQFP5XnSFHKjzmOWygn5rQpWM6tJIk/oE92LkAu6S9F2skeVlKUnzHluiojiRJDmEcdw3kZYTWnMJ/r6Eer4w2t6Hj7vtQfmpp7Oscmb0u8cBuOq/AefRWhpPmL8zc2j7nEMHMG7V3MI8HvSFHBKFYxkggZhA56Rg+67fNk6xOC0IFNK8VEaMh3gXdiF3cxyTw6zXscyghaEarowTimUsNySrdwWmERl7KOAQwF1tCNPlRgUUor95AiNk+vIkW+n0SO2xdRSQXZMQ066aDbekaz5IC/8CIWMX7Cr9hADddzgBNcNsB64+9r/zAxb2z4RG/XcYh6sqyUjVTSfsvSE0cPtZxkgu2Yx/YMYzgjSfB7PuFKJ18pSf/Sy+5+yOh4TIiwmWX0p5SV9MegJl9bTpP8RkbHxizhbA6jEptFrGUc0NfBmAq/RuY6bdLCUt4iQpRmXmMdiTrOZ1UndNKDRmrpThuNdAOUDkac8pM7+5d2OOHeK3mN8+nFv7iLGGcznh605A+hWbyy7ZZKX6uPH6STgz+Y30KCzIZ500ZdnH03yUHsSRVxhPlcxmJOACKkyLZeGlp29r1PF62+r9Q5TEIQ+mBQyqJOXHcVZ/HvRI+NfMJNdGM3HqEHO/0H41zNffyYyQgN/IG1FCmXyIxcKoLNKMpJsYkykg5QmGTgWcff2r8oa7OWcA+v8VcOQNiFM3me06h2jmVyDJxp5gbT0/Bp217bdI6h5oMyfZy9v/juJfXxPLnKSNPCk5zMav7CFTSSztfCttGn6LS7Bl/eEXNDdimGsQ8Z8v0WP5JfaGsRi7iaz2ihBxfyFN/PwLd5l3NpweQrPuMq5lFsyx/l/lzdfIa9GeY5m0ToiA2+/NS7+jjkiPAUd3MxB9JEE/2dnvedlcExFtUvvtvAxqAP7b70oh8k6wSEctIU59L3l8y5b/glRv4gU+WvPMcwrmF7/s4LDOCPDHOKDJJUmGf+5aVBH1+m9Z1pOhm6M5xG6nJ75Xl+qXfGi7/gG37OXuzIVyzgcHb5jrEt4iXWcxrlGMzmoexW+AvX59zFHpTTwyluy3V6bKmefP3hZydIOq5nI8/SzkpaiWGwkk85giJXZbDNnPuiSyL5tYjmw2tb/zKv2oqLnsagH81UUIvBYEqJkSBBEQk6lpUc2bvSykc4lmJyN0P4kqvYA+EJplDtQIsWCcbtbE1YNjtSW0QDVTRTApTRG6jP7bI5so59zHiKBvZnT3agkRf4CIMBBRh8Fg/xCv05hR0YzgoeopEo8mf9s1hZwHEIY6nGIEMrZTRQQRu63SH3Hn181KlrzAIvu7Ocl1jMVIq4APizk+qajYsuXLHkguLNCRIkiFNOGZuw6EET5axFXVWOXUSQSjbSHSVFmgxp0mRIbm6JDj0wlq+y2IlDmM9DjOMNevE3pjOHQ/INXWwMxg4bePDK9cn5TVTTTCkx2rDpSSMdudOqZrPI2MsoXcebfMwmQFnE13zGNzSjKBZpWtjIPKYzjRksox8VLOVN/slT1BHPyO+5VqysBdeN7UhhU0SaVsqop4iyE39y3947586Jy2U59mRfVvEW8/iQ2dzCgHxUxKSdGdfUv2KRdn5SpIjSRk+aKKMGe6sIsk1+SJooEV+GOqx4dO4JkydmXJmNm3iW07iQK3me87mAr5jsdFeEDDY7Dun3xNO7brg+vbHTmO5gBBHmszmb4jxNN3BbfKdWPqOSXzEYJcUm5vM1H2I50tukhIGcTB8SCK18yIssJkpirfyOJ7KR+mrGknEFm4Q0kR4DLjvhwh6SdCDFuHOAn9JCgpu5kUeI8je2o8m1cJ9/vuLRIsfzyJm7JhHnIIytPYVqqzjkKQz600oHxWTIkCGNRZoMFmloqa8bdkyxmeORNMN4j6+5hNU8zoGMYRyfUklZvug5Q6lMnFwxpWbdusVVRJ3lshBGs4HmbIPM1cZr9DC3E1nIh9jUsob1QDe6U0kZFfSmL72IUs96lrKAh7mHTRQhb8vZTAebNNVMoS3fIjaBxQZ6HXrYXccfXySp/HEZM+igT350aXaghUM5iqa8qo6wOf3mbzo+t/Kzt8hgYVNEG0opa7eSQ7ZBZLW4KpHEBT0rurB10IidOsthYoziXlZQxudM5BQ+4BJmsw+lee6ygFH9Bx3Z0bthQbyx3eEtgxgJBrKZJAY08pIsN0ZHutfzATNopS/lDrBfTDERTEyiNPM+D/IaizCJbeAGuZhlkKGYiXQjQdJpOSskaB84/k/HX7vz0KQjqgxK+JLfsoADSGBhUMET/IXzOIA2T3uZdx6qvVFVfMfa52bcxQR5CtMhiIT+oLWflx4+oFtOHlsMZAyP8xEj+DXvchnDyfASe1HpkESy+zc2YbfuRyxJt823MiYWBjFshjOEWloRsGU2Lxpt5jij2GI1q4gwiHGMZjjDGEw3mvmC1/iSJCaxFuMh4xdMo0NRunEAvWkhSgrFxEKKEmcfdfehh1TEOhyv3cBmOYPYxDss4wCiFPMS1zOFQzBcOZBx5iz69HSaKTD/DGXUbKVS36pW48cSYSKrO7tIhfgAZUce93TfWDK/f4qoZynj+ITf0o+HiXMWae6m3KODDKK0MmPGpzc0vGe3F1FKM30oo4451JBx0C4ZpmdzUSbaik2cEnpQQoQmNtFMCigipsaj9t+ZJQ6y1o8dqaKFdZTRTAdGUeXeu126116lzgFL2RF2cCXzeZzuXMB0juLPvMb/cRKXOK1zctbV2tSzJzS/YBTUrUn68TkZpnUdQUx2J0kE//mvnTWAltH3Lwf83nRF00yKeI4/M4ybGYDBBo7nUC6h3VcfFSHGBuudad8+sP6NXlYrfSimnuXEmIflCMVMvHzhBYM28THLyTgnsEE5Jn2ZyjpeTRu7ySycDK/tSDGUKlpZTynrzD4Hjj5zv2N6mqn8rhfirCPGwzzIXtxCkvP4ij2ZzbH8lg7PLCzeuG79Hw0bChzZQZoiPsTiua6zsmziDPVl7nk74pj2xutn7LTfwYYrFpKkiBhDGU4LJhWUMx+II659ms2s6maedPzawz5+beHjdS9luzZalLMd83IeTKkaUxjEj3mEnZjJWD7jKCp5l53Zk6d5gWiR6SzgeMpZ79w9Ga06/MCTJx/Sr9jyACARnuIBruQy6niRv3E5N3E2M7iAc2nNVlnlgY0Zr6evH2Vbnpl7+/aarN3qwuhtMHvTFMrNcjq1NS09v+/L40clXVccQk8u5jz+RAUPsIJDgc/JMI6q/AFd2dS6DH2Kjzm29pB5X856mOfSmzNkKGM7Zuef0UT2LLdx1HIITUwFFhAB2lzBgR0oI4NFCq3SY484ffudexWrK2c4AmRQyqjlJnbgSpbxMIM5hV9Ry0m0ucgBUb5euPT8Hk0ZrC1u1x8AfteQd8RX1mku+fSX8WkjKjpJ0spEbuMSjqGShYxnIqfyDXGq+RlHefhESSJ0K95/yqQp317yyVOb/m3NtdNxJ7Ui57xlSNNOB81OJUjS8QI6c7eKyaDRqu27HzXxxDEjSsiQ9KRltLGJftgcwTpu4Rpu5k/8nGsQjkFp95AjzuLGT39pLil0Cq78Fw7X2EpwUQvgv/7jIjLvvHZOTUvc9b0mxnA/+xHheK7jnyzgXO7gIG7gHqcrlfu8hQwdRNllxPl/PPPfjIw64SQNiEgNBT9txmaTTUee8e/z/zhxRJRkvtZRiRCngd9yJRYmHZzK7rzCE4znbIbTH4ukBy6MU9Py2jmZdwofS+FtWNWFHFL4cGL/SR5C8sl3yva7pXdRZwFYG934A0lKmMs8juY8MuxGKXcxiR2cM6O90+wgRrkRS8byWV7+IFDYqerZgyNjGEiyzLBIekRPnAYMqqjidV7gVFoo5WJ+wmMcySEcTHW+z4oTtmZd+zsXJZ9MeELNheWHdi2HGMSd85aTJOkgGfhpI0p3ujOUxL2zL6/PRF2PTNLqFL2Z9AOaSXIAFp8R4ym+JSzDbN1qXWHnu59qoLRSQ/5VBz2wV6xf3UnCGCXUcgMn8GPmcCW9uZ0VFBOhFOiFUEp5gBz1mdmXJ+4dSne6EaONJB3O7JP5dcj9r4OirRZfWy2yTKdURfPnqmv+t+z/hRgx4sSI/n3uxbXJuGcHWbQznCk8yWIqifMuKXqxnL9xAV9TBPkDXLJPXPNZJGM4DQjCmkyGHRZhMheDCJHM2s86v13DjZzMB+xPA/fQjfNo4iYa+ZhzqOYS51R3NzkSbEjOvTj69+xsYk4Omfpm7567udUwvLG1Sj1o7hE40js3SAO55Znz1zYX+2wRmz8wkJ/yFy7hZgYzhZtJku2tFUEoclLQTBp0xasVVLpCU+o55F5D9Fn27zYqKGfFqw2aPa/9YU7mXc7jNU6niBV8xuHsxTsczvn05W4GB8qQilnb/Mz5couRn8/3C91JVxLE31FOQ4jl/p5B6t6nfjavttgj7VNUcjsn8QkfsAd38iUzmUQckzh1/JG/UkMkmxY3V99vYCHfhDwr/Hy23LGri1lME9b7y+dGsTHpRRN78iNu5ERKMfgFs/kzP2FPruYO+ub7xXeSY17tkz9L3WuE9q7W79CuXUQQCZwnsyUFl5PCHc88cPz7c+Ourr9CBwZn8zBPcwdl/JUxHEcL8C5n8TpPsw4Dkw594V+rWzc5kRDLd5C8hvKu5AuGNtPAmtYX/tWuEWwOYQemcTRvcQ5P8lNamUcvfse1HETGaTHT6ZUneH/uA8e3PxP1CcPCnRU1QLQu4pDvn1+REx5R0jMfOeLJaUnnlDTHMKaNONXAA9Tya8pI8xf+QF9iTGICFkV80DRjkfRpqU4VdRhJ0nSQakzaJhDBIIKR74ZlZLsMkdRkSwdpknREOkqau8X7fbj0o+YiMhRxOB2M5HWO5y3uYyT70UYHHbTmgZQctpXkyWmPHJGZGSuQ4hHWvXhbeSSyLTrEjeRIQWRHXB0YrJUv/3jTJYdeNKK6w4WeZhAyHEZfxvMZHTRzA/P5guOI08FGpPiaB+vq6uqaapvrN21uru/Y2G5p8SqKWE8DNWxmI83UoDSwjpU00VOqD4/tnOhTWdWtuqy6rGf3bid2ayvZSClpptKHpTzCNGrYn3Pp7jGIc3hanKX1L//t078Wpc3AQhfiEXdf660lzFaBi8dgchjm9+QgpYwIs0mygRagG9a+B1+5+9Sor8Y2RpQUS/iYE4mwP4O5lygZllHBIGxHCNmk6CBJu9pikyZJhFaKaaQ7Bu3Z0xGJOnHuGEa+rdJKGhkKlHAtj1DFfpzEOJKeUrWceE3z4czpV0fe2QiU0ps4O5Kh2SdQtABZBItXyGxNRdu2nY5Q+HO3vLVdoktREtS8M3v23HMOvmBkr86TbIQUaWAIYzG5hkaOpZRWGrHpRlOe1wyixBFEcq1uLEwyRJyEHrDyx7BkY+25ozOq2UwT5WTYlXIOZihKiwcayR6MYbBow+u3Wnc11PcLMVLcckBcJrjgrhXeeuEV4X/wCh4A5I4rgklR/YJr//nv3f8w9fB+5bbTOyR3RTvCQZSwIyls1tPPfZZovmtoDBsLwykvA4s4GVLEiCMkQ5Bok17UUEY7k9kbi7Y8KpZzdmMIa5vef+mDa40FY50SUSNgWIvrwCcJtSy/v379HxAk3OIOb9avLlDBhAWfnbr4oBFn7XHQgLJOAD47+fHsRBsW6ynyhbByyQQbiFOGSQ09iGATYR3lxGhgE3X0oV8Ai85QQQPrGEA6f1BM5x0TKKuaP5q+6L7G6VEf/OPXmhKYk4TIhy7kENkCMYLnM2vAYeqMQpjTX3qzZp/+J+501OAeEVeTihRpTJppYmRIY/M48/grlzKBO/iGvpxJNQ+wjJ78jGksYAQGA0OCA9nK2WZKPPfMdgZauHH2v1c/9dW74+yIC0sTH4wZPlv5Dqe5y3RI0KbCY3eF7xz1iLUyu/3tr9/75OaR/3fAKeWU5ssDhAxr6IsZIIhgU0SKOEt5hyt4gHcYxCccRAUlbGITAyktsBwmfVjDMEynA7CQoYkm3nxs0Z/LlsSsMvAcCK6B1gAasLe23iH4n/ghYdwSrtbE13WRfJeFqNWysGVmN1pYxkrqnSjEGkqpCA0EZehPH6AfY3mLtTSziGZqeYUajud4unMn4Y0/LCooZY0Tl6xnJctooRstM1sWRqxIAH3Y0hHIGuoQqu8Eni7TIVoAiP8+4k5dyj5FigZKsCsrKCdFM600EaGFWKjYyUH4dRgI29NMKUMxqWAfPmEDBt0xsJyjiMKieX1YynzKyBCjgjKiCFZlMQ1U5ltnurMXpcAcgifgqouYXd7iT7bAyF4xpgUQ2WylSE8syslQVgUpDKqoBjZSy2gSpAqEQw0OoJIktdSxD5MwWczDHMp4vmI6yk9J+KIquecaRBjEXHrR3SnYSROnvKqFCBY9SLkaWYZ1aPBaVEHtsS0K/b8UoFJXnx5CvNpOoWa7ToxVShhBhqjjL/QYkIPlTdrYxPasoJ4+VJIgJ0os5zw4gzLOJInNT2mmAhubn9BCBWkmsj0ZyslgOGYxjr5QmknQxnpGsB3LKabYqdQV+g2IOweIZzBZ5CRACHbIlgrHIzRE4XepUjfyJ5m591L4gXEmJoJJAguhB9VYDEfoACcGbkd6jhCndV4bSxlAMSbPEacXu1FK9uzCcSSIYAIZmikmRoYK0tnSZsrJYGJTQoYMm6nAxGAhFhUsYAWNVGLTi1bGo/RiGcModkzqniPWRoxMTpT2Q7EYhckmIO6MXjHyR4vrFmLpkk8l7zKCRDBdrC2EHO/i+aQVgxJKqHaERAT/CejJoX3HZMgWuNXQn0qSjGMsE/iWJOtZQRMDqWIWu1HLWlooYwSNLGcSY3mafanlU4qYRJKP+BkvsICBnMhMPmAoNusYziZ+wUO8wdWksKhGWEY/qsiQoe+YL4exUPLBApz83zF54VqHRUcgdCueDvOd29XynKX7PyfIJuDlAANr4LjizlIYwQzhotxUMux+RJ8yMFhNC4OcEupW1hOhhQTCcvZnPMvZTIJ6VtHAVNaxnEkUU8vz9KaZVZQyk3I+ZQiLOYFnmc2nnMoYPqWZnVmBzcHMpRoLyFBNhDW00A+hZ1nZ4esWim+b4EII6sFlAkuBXi+d7/boSg5pRVyJ+bhwHE/L5JCB+omR/b1j5C/PL6aGjSQY5hQ4CGl2phdjeZFiDmUuC2kmygYaMBjKTnzKEEzeoZIYM9mZFlqZyitEeI2+9CGCTV/eYzV1Tm5jM+XsTdIpq0tTwjDWsYie9GLX8x57Lrp862CisFdFV6K9RwaWN8i+4RpFfATL/m0dudt1Y3sNqB5KMZbzo6jTv7oOk+7MYhETaeIVRlHCVwykG4tJ00w5O/A+ZexCigUUsx3vU85GBnMCaaZRxmSa6EeSYgwiTtsbI19s2spia2XdojVzLjdfL7zzJeAOh9ti7jk/21UE2c4XO5fv4TaK55vq4yurbOyOg3csmjBidL8BA/pWS8xR3ZaT+GMRI5otSkBI00AjPbFZSx86GMgqGuhLjBX0pYp6Sml3jiNOOPkt2ebjJiam0wqwg5rUurVrVy+b3/zVwlk18ypT9veYCSGiWQtAKh92FUG295wS7fdqJdBp19soPCjoFJhAms/p1b3vgLYB1WNGjKwe3G9gv37dikqIObhrZ05LroeiOK0IUsQwSaPESZMhioXhJGZneSHb9DJFimbqmlatWbdyw4oVi3p8W79myaqmpgk0s5xyT7vmoDEbbrBIKJiU/f2DrvND1Pd48YEjYYmmuoXdl8skLNsU2bR01pIX10V6lcZKl3SLDBwwqLJf5cA+fap7lHcvLy2OFxtRIyZgksZyzl2IkCLqOII2GafHT0ozJO12qyXZ3Ni0aVPtupr2VevWrl+ZWF3ZkGquaanTqVRSmue7sFEVFmHq6WYhHiT7BzngfsthfMGbDiQFbffg+1GEokxxg92weY01BxppFoxn4hvKolWllT2qu5dnSpt7lvzu+MqeNLOavtTSixqGY7KYMgbyBW9Yxt/j88yO+oaNm1sbEvXdWrdP19gduo46KqimlGKaMZ3O1uLjcDw7XwIc4E9+Urwny2vXw+9eSN3vp6sPKFEPTO3XPUEvNxcdNJ1k0IhGLG2z26wNaTJAA3OMorN+WzmeNSxgIMvYndmcjMnXDGBHvuQTq+ihMV9XORWQWSEXJUPEcSyDvKuBxfQdLeuaR5gHoh7ibL2nvtWJcvnyNV8Glnd3+fMz/Ak8GojHaQjEnxto9hiXZhYiFUWGCURJEKWIGMWY4PTxjRNHypbS6tQShuNQYeAO+ZCseuaogesVfw6lbuEZ/3OCKMF+J+KajNf09e8gP1n9qJg36qaeO7XwNRai6uoWYedPDsy9C4ZafJutTAxAm+oTN/h0oHuB1RewVcKOL3cLPlz81oUcQiC0qQEMRwJTVE/Nql8PBVWhf4oZ1pBxDVpCDQZx0DaLDb5cKwIRGSmYaOfWIOrjIfURUTzc0uXlCF7Rox6u6PwsqDckoODDFkUCUYbcHrWdhrFBIVN4nB2BqH6YsxpG0DAz3e/4qicGEhTZXUYQCU0CUM/u89sbwV323RaJe2GMfBRQtxDZD7qeliuP3p8rIriT+bxIg/rUt4YYxX4DR7YhiXQbRJb66jPUQwwhPM9PCsad/Xfz71DD0RQS0GESEtn3x2RMWujIT1V98wgPo6mHC8MyszRgCODRMNKVOkR8RTPiWnT/JMVXhCahGVvBwJa6+odkeClfR0sgYh3MstVA2HYdb5AukGspIdtNfKpefVpTA09Wl/bRAnGh/ymHeI1Ct1L3W1D+AXr1T3hJgbjM3Qxf0Pwd+00DZx3iG0szc7ACPrlX+4XJAL+DGyYwg1yvXSuyOjVGMEQVFAHqcr78alt8e1oCEjzNYjpCzunYMkGDZqdJOwtJF4B18NhKW0qlDndm/1sB3G3GstSn4jQEhBYfmwdBSLfi9IeA1pMuGDgttE/Ds89N2kjme5CGRcM14EVtCeTx5pn5TeQudQyVsDzWsFQACVghEhKy6iSPuCRxDEg7XkcYsXULeiBcdBjY2FjOGSZSgCSEpsK5wZOwaI8/valLRVahCvFgspyG+vR+c1hdVj1OX54MKxwAfUtPCQoI3QKJcpmK6wOiS0MCbv6xSQAk1ZDITlBkd4kfEpYQo76W4eA/wdYrrTUQe3MTcz51oYMTF35cuHRMA7CfW/ovYzNlvox8r0sbZvmFx0cksB2ULs/LUh9mG9QFXt9WAopdfR68Fz02WEF7fmjeOLz3bhKI4QX1gwTwhQiNtDtocqFyNN3CHf1bI5hB0OVWltc4lRDLSkMDPUF/RT1qNLtISSdoFLwXPnhPQzwb78g04L1kgf00Ng35ch0JxWulIAbgRyPUde9te0W2RWBpgH3D2FoD8joIznVOcEPBfCYpcLSQFixO9qPH6vP+v6S/Uy4aLLXWgGleaAb+qJD+EATRgupWQwC4MKBOQxBTgw20FNhlUrDdi4R+5i0kCNd1EWoodqy4wvH/QuYL31GerV0vsoIDEcILEzTEHPDGTrIns7VuIdu80HQ18By/exf0HjpxgFYy+YRuLThe8eQvi8fXEV806AdxDAtJ02DoJywkKgGfWFjDuoATWdjr1QIqWHyCTHw6LJi4Y5LmKyrzfBKMcnh1lQZsOw3kZf0A54f4owbhoDoh+ziIHmUPBKspkHBTKO/D70WHA6D+kQqEprzW54+TCSs38GfzhyMG36+y6n9CENtnRoankHaSyw41kd1RRDvE/CVETocb3+HNZ4LaIxx1ykGYLVvc17KFqpGw/PcfgCDiS2OwfcIjrEmLhsIedoDLwrSVhPjj4SKt0H4N2nZubuko6LEHu66Gf4rPPOhiT33Li/L9dkyYE6UhwJ5/h3ozXiS0QlwLmhhBIqkHxQ2m9FBAmLm1i+TbMf8gVlZh2c1/AEWHexBaAKMSD58Vhim8CycFgRUtKIAo6Ob55y0hYM226JFtrMItJNm/n5VWaG+HLYUUhBB1iw0pcSXCEcCcg2q7MEJWSGdJAcj/B+goR8gkt7T8YQIsuMflO0Sgs4wibKndn4jfLpKCfRrDRhgktR9m1IKmhKI/VJ26fI8d4a/bLpyWI4GdWIj1VSQaLQjhmBDzXhYu/cMg+DA0ArwZAp2OYaFT5aVrRZYEPFY/lB38RH1er18pS8ArFl/Ayv2J2dry1YIQYmfLDr7FWmosC17vXS7Dp/EM33MNz9WG833DM0/DNSojf49t6Ze11Vkn4iNA52S8A889wghMpnM6uK4IAySMAJnMVMe19zdtdorT3Pu0mIW8jHmNrMe10PiW2Aglgfiem/2O4SKK+CIoeN4xvsMg6AIdQgCE8+c0BjEl8WFe4kuZ8LubBAAZBRIfzX7kRYp8wsFEeILad4qeFQ/86e+9EiYqw0WtBkwDP0CvAfh+W17bpEOC8T8Jgaf9yQ3qC+m6k4oI2aP4lladiIZxx0MrlxH3aJE4X/JKuvgmaQ6CjG6gxd/zSkIRsWB+f3gmvYZkq3Qph3hbKKsnB939J/e98M+9//N+2/2b7fst917RgpX336fiG9mjND0Tf9vGm08VhqZ5iyLCDV0NpKB6yaAFOx51qZVl5KWpkf/b8FT0uTWC4fx0/s/wXOf9zH9Xo8CxQoJB0T9fnlPjVMBnX20sqovd2NnM0Wsq+GOC6lP1Qd9fAsFiP34QzBqQrk+2DqZ/hgU7pSBCG/yObMFALcz6Up/568rHxRWKep/YfTI76OPod4LjGnAKJQTilAKJ3u6SCd2CQfw/E1kaEFje3ufe9zTwu+YFVXjveDvkG2E/8ac/e20GZc703+aDpbG/Fwq+SgiWpQX87fDQk9eEDyYMSYF6lS7xQwpNLSgeCnu24kOn5HtbQLlX1Gq89v6WzcSIkuZx2q6NbPAbFuEgaCF3VV2BaA3MwRs0CA9f/QCOYacBuKX96wcBs1fans/Vl9Zs+yZNAc7q/H/ko8UPrKaUBO/z4TuJJyUkMulNl/YLpWB6tteo9eb2S8AV9SrxbS2M3oYaQ69QCEuz9pMkHJsthBcVbnvvET127K6vlm+mnYfb226MtoWleoZrOAkBNL0RDw1dbPLc4zV5t/24o212DMPwT/HsGfFFC/4TUgdL98MR2OIF8+9/ms/57IWy6bbTGMmtfzrNZstjOFue92ynw0r2c8tjmmv+25r/16/rOv+28nffqpXdGrqOD11If08gf8BJtkiGsAR/DSTmhD0TrJ4VH1j9W/aIzHKPRHz9RaXgXYMxfgpUrfuzuyQkFbbzt3ldFzEM5lz5zVh/2qWEpv6Hy2xwZ8Or01DMC4zn2FuBaG3Tjcbw6Kzcu7lEOPF45RqAPsMK0nLXu88gCW+OLL5g1raVIfwX/BANQNkSarULhNRaSIHTN8SjkNWDF6kPae68ylwkrVvWTFoQpSqkAYJlRuAv7NECwnXrX/8/APLEYI1Fxn6iAAAAAElFTkSuQmCC',
          width: 200,
          opacity: 0.3,
          absolutePosition: { x: 210, y: 370 },
        },
        this.idPicture
        ,
        this.fields
      ],
      footer: function (currentPage, pageCount, pageSize) {
        return [
          { text: '', alignment: 'right', fontSize: 9, margin: [0, 0, 40, 0] }//currentPage+' of '+pageCount
        ]
      },
      defaultStyle: {
        fontSize: 10
      }

    }

    pdfMake.createPdf(doc).open();
  }
}

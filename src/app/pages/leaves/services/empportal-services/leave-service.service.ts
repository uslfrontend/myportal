import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Observable, throwError } from 'rxjs';
import { GlobalService } from './../../../../global.service';
import * as moment from 'moment';



@Injectable({
  providedIn: 'root'
})
export class LeaveService{

  	constructor(private http: Http,public global: GlobalService,) { }

  	aLeaves = [];
  	leaveCredit = '';
  	selectedDept = [];
  	appointmentList = [];

    addedLeaveDateTime = [];

    rres=[];
    progTypeArr = [];

  //API SERVICES////////////////////////////////////////////////////////////////////////////////////////////////////////
  	getAllLeaves(id,sy){
	  	this.http.get(this.global.api+'LeaveManagement/Leaves/'+id+'?schoolYear='+sy,this.global.option)
	      	.map(response => response.json())
	      	.subscribe(res => {
	      		this.aLeaves = res.data
	      	},Error=>{
                console.log(Error);
                this.global.swalAlertError();
	      	});
	    return this.aLeaves;
  	}

  	deleteEntry(id,leaveID){
      var ctr = 0;
  		this.http.delete(this.global.api+'LeaveManagement/Leave/'+id+'?leaveId='+leaveID,this.global.option)
            .map(response => response.json())
            .subscribe(res => {

                // this.global.swalClose();
              ctr = 1;
            },Error=>{
              ctr = 0;
            	console.log(Error)
                this.global.swalAlertError();
            });
        return 0
  	}

  	getLeaveCredit(id){
  		this.http.get(this.global.api+'EmployeePortal/LeaveCredit/'+id,this.global.option)
	        .map(response => response.json())
	        .subscribe(res => {
	          	this.leaveCredit = res.data;
	        },Error=>{
	                  console.log(Error);
	                  this.global.swalAlertError();
	                  //console.log(Error)
	        });
	    return this.leaveCredit;
  	}

	  getDepartment(deptID){
      var that = this;
    	this.http.get(this.global.api+'HRISMaintenance/Department?departmentID='+deptID,this.global.option)
            .map(response => response.json())
            .subscribe(res => {
              
               that.selectedDept.push(res.data[0].departmentName)
               // res.data.forEach(function(row){
               //   that.selectedDept.push(row.departmentName)
               // })
            },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
        // console.log(this.selectedDept)
        return this.selectedDept;
  	}

  	getAppointment(id){
      var tempres;
      var that = this;
  		this.http.get(this.global.api+'EmployeePortal/Appointment/'+id+'?active=1',this.global.option)
        .map(response => response.json())
        .subscribe(res =>{
          res.data.forEach(function(row){
            that.appointmentList.push(row);
          });

        },Error=>{
          this.global.swalAlertError();
        });
      return this.appointmentList;
  	}
    getSchedule(){
      var xtin='';
      var xtout='';
      var that = this;
      this.http.get(this.global.api+'LeaveManagement/Schedule/'+this.global.requestid(),this.global.option)
            .map(response => response.json())
            .subscribe(res => {
               res.data.forEach(function(row){
                 that.rres.push(row)
               })
            },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
          });
          // console.log(this.appointmentList)
      return this.rres
    }

    getProgramType(){
      var that = this;
      that.progTypeArr = [];
      this.http.get(this.global.api+'EmployeePortal/TraningType',this.global.option)
      .map(response => response.json())
      .subscribe(res => {
       res.data.forEach(function(row){
         that.progTypeArr.push(row)
       });

      },Error=>{
        this.global.swalAlertError();
      });

      return this.progTypeArr;
    }
    
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //ADD DATE COMPONENT SERVICES////////////////////////////////////////////////////////////////////////////////////////
    checkfireTrigger(_12hourIn,_12hourOut){
    // ||
      if(_12hourIn.length>0&&_12hourOut.length>0)
        return true;
      else
        return false;
    }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  verifyTotalMinutes(fromdate,todate,recordTimeIn_24hour,recordTimeOut_24hour,totComputedMins){

    let start = moment(fromdate);
    let end = moment(todate);
    var tempDateStart = start;
    var minDiff
    var TotalComputedMins = totComputedMins
    console.log(TotalComputedMins)
    var totalDays = end.diff(start,'days')+1;
    // console.log(addedTimeArr_24hour)
    for(var x = 0; x<totalDays;x++)
    {
      let totRes = this.getTotalMins(recordTimeIn_24hour,recordTimeOut_24hour)
      minDiff = (totRes.mindiff)

      if(tempDateStart.isoWeekday() === 6 || tempDateStart.isoWeekday() === 7){
        // console.log("aaa")
      }
      else{
        // console.log(minDiff)
        TotalComputedMins+=minDiff
        console.log(TotalComputedMins)
      }
      tempDateStart.add(1,'days')
    }
    // console.log(TotalComputedMins)
    return TotalComputedMins;
  }

  getTotalMins(from,to){
    var timeStart
    var timeEnd;
    var diff;
    var mindiff

    timeStart = new Date("01/01/2007 " + from+':00 GMT+0800').getTime();
    timeEnd = new Date("01/01/2007 " + to+':00 GMT+0800').getTime();
    diff = timeEnd-timeStart;
    mindiff = diff / 60 / 1000;
    return {
      mindiff,
      timeStart,
      timeEnd
    };
  }

  computeLC(input) {
    // set minutes to seconds
    var seconds = input * 60

    // calculate (and subtract) whole days
    /*var days = Math.floor(seconds / 86400);//24hours computation
    seconds -= days * 86400;*/
    var days = Math.floor(seconds / 28800);//8hours computation
    seconds -= days * 28800;

    // calculate (and subtract) whole hours
    // var hours = Math.floor(seconds / 3600) % 24; //24hours computation
    var hours = Math.floor(seconds / 3600) % 8; //8hours computation
    seconds -= hours * 3600;

    // calculate (and subtract) whole minutes
    var minutes = Math.floor(seconds / 60) % 60;

    return {days:days,hours:hours,minutes:minutes};
  }
}

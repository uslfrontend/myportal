import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import { GlobalService } from './../../../../global.service';
import { LeaveService } from './../../services/empportal-services/leave-service.service';



@Injectable({
  providedIn: 'root'
})
export class LeaveValidationService {

  constructor(
    private http: Http,
    public global: GlobalService,
    public leaveService: LeaveService,) { }
  
    curSL = [];
    curVL = [];
    futSL = [];
    futVL = [];
    Schedule=[];
    
    dayofweekE = [
      {id:1,day:'Monday'},
      {id:2,day:'Tuesday'},
      {id:3,day:'Wednesday'},
      {id:4,day:'Thursday'},
      {id:5,day:'Friday'},
      {id:6,day:'Saturday'},
      {id:7,day:'Sunday'}
    ];
    arrangedScheds = [];


    validationObtriggerctr = false;
    validationOBFromDate = '';
    validationOBtoDate = '';

    validatetime(data){
      if(parseInt(data.substring(0,2))>=12){
        if((parseInt(data.substring(0,2))>=13)&&(parseInt(data.substring(0,2))<=24)){
          data = '0'+(parseInt(data.substring(0,2))-12).toString()+data.substring(2,5)
        }
        else{
          data = (12).toString()+data.substring(2,5)
        }
      }
      else if(parseInt(data.substring(0,2))==0){
        data = (12).toString()+data.substring(2,5)
      }
      return data;
    }

    validateTimeAMPM(x){
      if(parseInt(x.substring(0,2))>=12){
        if((parseInt(x.substring(0,2))>=13)&&(parseInt(x.substring(0,2))<=24)){
          x = '0'+(parseInt(x.substring(0,2))-12).toString()+x.substring(2,5)+' PM'
        }
        else{
          x = (12).toString()+x.substring(2,5)+' PM'
        }
      }
      else if(parseInt(x.substring(0,2))==0){
        x = (12).toString()+x.substring(2,5) +' PM'
      }
      else
        return x+' AM'

      return x;
    }

    

    validateSchedule(numberofDay,TimeIn_24hour,TimeOut_24hour){
      var that = this;
      that.Schedule=this.leaveService.getSchedule();

      that.Schedule.forEach(function(row){
        if(row.dayOfWeek == getDaystring()){
          that.arrangedScheds.push({
            day:row.dayOfWeek,
            tin:row.timeIn,
            tout:row.timeOut,
            hours:row.hours
          })
        }
      })
      function getDaystring(){
        that.dayofweekE.forEach(function(e){
          if(e.id == numberofDay)
            return e.day;
        });
      }

      if(that.arrangedScheds.length==1){
        if((this.validatetime(TimeIn_24hour) == that.arrangedScheds[0].tin)&&(this.validatetime(TimeOut_24hour) == that.arrangedScheds[0].tout)){
          return true;
        }
        else
          return false;
      }
      else{

      }
    }

    confirmLeaveCredis(currenttotalmins,type):boolean{
      console.log('VL current Total Minutes: '+this.curVL[0].vvlTotalMin+'\nSL current Total Minutes: '+this.curSL[0].sslTotalMin)
      if(2 == type){
        if(currenttotalmins<=this.curVL[0].vvlTotalMin){
          console.log('confirmation VL: True')
          return true;
        }else{
          console.log('confirmation VL: false')
          return false;
        }
      }else if(1 == type){
        if(currenttotalmins<=this.curSL[0].sslTotalMin){
          console.log('confirmation SL: True')
          return true;
        }else{
          console.log('confirmation SL: false')
          return false;
        }
      }
    }



    
}

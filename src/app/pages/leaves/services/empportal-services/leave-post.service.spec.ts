import { TestBed } from '@angular/core/testing';

import { LeavePostService } from './leave-post.service';

describe('LeavePostService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeavePostService = TestBed.get(LeavePostService);
    expect(service).toBeTruthy();
  });
});

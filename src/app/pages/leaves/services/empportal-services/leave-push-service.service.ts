import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import { GlobalService } from './../../../../global.service';


@Injectable({
  providedIn: 'root'
})
export class LeavePushService {


  constructor(private http: Http,public global: GlobalService,) { }

  	tempDatesArr = [];

  	pushDateTimeDetails(date,tin_24,tout_24,LID,AID,totMins){
  		this.tempDatesArr.push({
          tdate: date,
          tin: tin_24,
          tout: tout_24,
          LID: LID,
          AID: AID,
          totMins:totMins
        });
    }
}

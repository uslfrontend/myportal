import { TestBed } from '@angular/core/testing';

import { LeavePushService } from './leave-push-service.service';

describe('LeavePushService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeavePushService = TestBed.get(LeavePushService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { LeaveValidationService } from './leave-validation-service.service';

describe('LeaveValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeaveValidationService = TestBed.get(LeaveValidationService);
    expect(service).toBeTruthy();
  });
});

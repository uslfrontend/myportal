import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';

import { Inject, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { LeaveComponent } from './../../popup/leave/leave.component';
import { FilterComponent } from './../../popup/leave/filter/filter.component';
import { LeaveManagementComponent } from './../../popup/leave-management/leave-management.component';
import { HrisLeaveManagementApiService } from 'src/app/hris-leave-management-api.service';
import { AddDateComponent } from 'src/app/popup/leave/add-date/add-date.component';
import { CheckScheduleComponent } from 'src/app/popup/leave-management/check-schedule/check-schedule.component';
import Swal from 'sweetalert2'

import { LeaveService } from './services/empportal-services/leave-service.service';

// import { LeavePushService } from './services/empportal-services/leave-push-service.service';
// import { LeaveValidationService } from './services/empportal-services/leave-validation-service.service';

import { DeleteConfirmationDialogComponent } from './../../popup/delete-confirmation-dialog/delete-confirmation-dialog.component';
import { EditLeaveDurationsComponent } from 'src/app/popup/leave-management/edit-leave-durations/edit-leave-durations.component';


@Component({
  selector: 'app-leaves',
  templateUrl: './leaves.component.html',
  styleUrls: ['./leaves.component.css'],

  // providers: [LeaveService,LeavePushService,LeaveValidationService]
})
export class LeavesComponent implements OnInit {
  //VARIABLES//////////////////////////////////////////////////////////////////////////

  linfo
  sickleave
  slDay
  slHour
  slMin
  vacationleave
  vlDay
  vlHour
  vlMin

  pendingLeaveArr = []
  fVL = [{ days: 0, hours: 0, minutes: 0 }];
  fSL = [{ days: 0, hours: 0, minutes: 0 }];
  pendingTrigger = false;

  leaveTableArr
  VacationLeaveArr;
  SickLeaveArr;
  BereavementLeaveArr;
  OBLeaveArr;
  MatPaternityLeaveArr;
  SoloParentLeaveArr;
  SabaticalLeaveArr;
  BirthdayLeaveArr;

  LeaveHistoryArr


  filteredPendingLeaveArr = []
  filteredLeaveHistoryArr = []
  pendingLeaveTrigger = 0

  dfiledfilter
  reasonfilter
  fromdatefilter
  todatefilter
  actionfilter
  totalminsfilter

  LeaveHistoryCtr = 0;
  LeaveHistoryConfig: any;
  LHcollection = { count: 60, data: [] };

  VLeaveCtr = 0;
  VLeaveConfig: any;
  VLcollection = { count: 60, data: [] };

  SLeaveCtr = 0;
  SLeaveConfig: any;
  SLcollection = { count: 60, data: [] };

  OBLeaveCtr = 0;
  OBLeaveConfig: any;
  OBLcollection = { count: 60, data: [] };

  tabbing = 0;

  filteredArray = []
  dateFiled = ''
  leaveType = ''
  reason = ''
  from = ''
  to = ''
  totalMinutes = ''
  action = ''
  remarks = ''
  typeOfLeaveChecker = 0

  recentSchoolYears = []
  schoolYear = ''
  schoolTerm = 0

  lType

  leaveTypes = [
    { type: 'Bereavement Leave', leaveTypeId: 7 },
    { type: 'Birthday Leave', leaveTypeId: 6 },
    { type: 'Maternity/Paternity Leave', leaveTypeId: 3 },
    { type: 'Official Business', leaveTypeId: 4 },
    { type: 'Sick Leave', leaveTypeId: 1 },
    { type: 'Solo Parent Leave', leaveTypeId: 8 },
    { type: 'Vacation Leave', leaveTypeId: 2 },
  ];

  actionTypes = [
    { type: 'Approved with pay', typeId: 1 },
    { type: 'Approved without pay', typeId: 2 },
  ];

  schoolTermArr = [
    { term: 'First Semester', termId: 1 },
    { term: 'Second Semester', termId: 2 },
    { term: 'Summer', termId: 3 },
  ]

  leaveArray = []

  appointmentList = []
  leaveCategory = []
  facultyType = []

  /////////////////////////////////////////////////////////////////////////////////////
  ////service function variables////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////

  constructor(
    public dialog: MatDialog,
    private domSanitizer: DomSanitizer,
    public global: GlobalService,
    private http: Http,
    public leaveService: LeaveService,
    public hrisApi: HrisLeaveManagementApiService,
    public cdr: ChangeDetectorRef
    // public LeavePushService:LeavePushService,
    // public LeaveValidationService:LeaveValidationService,
  ) {

    this.LeaveHistoryConfig = {
      itemsPerPage: 20,
      currentPage: 1,
      totalItems: this.LeaveHistoryCtr
    };

    this.VLeaveConfig = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.VLeaveCtr
    };

    this.SLeaveConfig = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.VLeaveCtr
    };

    this.OBLeaveConfig = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.VLeaveCtr
    };

  }

  ngOnInit() {
    // this.global.sy = '2020211'; ////comment this line out when building production
    this.fVL = [{ days: 0, hours: 0, minutes: 0 }];
    this.fSL = [{ days: 0, hours: 0, minutes: 0 }];

    this.getLeaves();
    this.getRecentSchoolYears()
    this.getEmployeeFiledLeaves()
    this.getAppointment()

  }

  initializeVariables() {
    this.tabbing = 0;
  }

  getindex(tab) {
    this.tabbing = tab.index;
  }

  getAppointment() {
    // console.log(this.global.sy.substring(0, 6));

    let uniqueArrayFT
    this.appointmentList = [];
    this.global.swalLoading('Loading Active Appointment.')
    this.hrisApi.getEmployeePortalAppointmentbyActive(this.global.requestid(), 1)
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res);

        this.appointmentList = res.data.filter(appointment => appointment.active === 1); // Filter active appointments only

        if (this.appointmentList.length > 0) {

          // Initialize leaveCategory array
          let leaveCategory = [];

          // Iterate over appointmentList and check appointmentTypeID
          this.appointmentList.forEach(appointment => {

            if (appointment.appointmentCategoryID === 2 || appointment.appointmentCategoryID === 3 || appointment.appointmentCategoryID === 4 ||
              appointment.appointmentCategoryID === 5 || appointment.appointmentCategoryID === 8 || appointment.appointmentCategoryID === 9
            ) {
              leaveCategory.push({ id: 1, category: 'NTP' });
            }

            if (appointment.appointmentCategoryID === 1) {
              leaveCategory.push({ id: 2, category: 'FACULTY' });

              // console.log(appointment.appointmentCategoryID);
              this.global.swalLoading('Loading resources...')
              this.hrisApi.getEmployeePortalCodeSummary(this.global.sy, this.global.requestid(), "")
                .map(response => response.json())
                .subscribe(res => {

                  if (res.data.length > 0) {
                    // console.log(res);
                    this.facultyType.push({ id: 2, category: 'COLLEGE FACULTY', })
                    uniqueArrayFT = this.facultyType.filter((obj, index, self) =>
                      index === self.findIndex((o) => (
                        o.id === obj.id && o.category === obj.category
                      ))
                    );

                    // console.log(uniqueArrayFT);
                    this.facultyType = uniqueArrayFT
                    // console.log(this.facultyType);
                  }
                  this.global.swalClose();
                })

              this.global.swalLoading('Loading resources...')
              this.hrisApi.getEmployeePortalBasicEdFacultySchedule(this.global.sy, this.global.requestid())
                .map(response => response.json())
                .subscribe(res => {

                  if (res.data.length > 0) {
                    console.log(res);

                    this.facultyType.push({ id: 3, category: 'BES FACULTY', })
                    uniqueArrayFT = this.facultyType.filter((obj, index, self) =>
                      index === self.findIndex((o) => (
                        o.id === obj.id && o.category === obj.category
                      ))
                    );

                    this.facultyType = uniqueArrayFT
                    console.log(this.facultyType);

                  }
                  this.global.swalClose();
                })


            }

            if (appointment.appointmentCategoryID === 6 || appointment.appointmentCategoryID === 7) {
              leaveCategory.push({ id: 4, category: 'EAB' });
            }
          });

          let uniqueArrayLC = leaveCategory.filter((obj, index, self) =>
            index === self.findIndex((o) => (
              o.id === obj.id && o.category === obj.category
            ))
          );

          leaveCategory = uniqueArrayLC

          if (leaveCategory.length > 0) {
            this.leaveCategory = leaveCategory
          }

          if (leaveCategory.length == 1 && this.leaveCategory.length == 0) {
            this.lType = leaveCategory[0].id
          }

          this.global.swalClose();
        }

      }, error => {
        this.global.swalAlertError();
      });
  }

  getSy() {
    this.filteredArray = [];
    this.http.get(this.global.api + 'EmployeePortal/Leaves/' + this.global.requestid() + '/' + this.schoolYear, this.global.option)
      .map(response => response.json())
      .subscribe(res => {
        console.log(res.data)
        // this.filteredArray = res.data
        this.filterLeaveHistory(res.data);
        // console.log(this.filteredArray)
      }, Error => {
        // console.log(Error);
        this.global.swalAlertError();
      });
  }

  getSt() {

    this.getSy()
    // console.log(this.schoolTerm)
  }

  empSchedule = []
  tempArray = []
  daysOftheWeek
  getEmployeeLeaveManagementSchedule() {

    this.hrisApi.getLeaveManagementSchedule()
      .map(response => response.json())
      .subscribe(res => {
        this.empSchedule = res.data

        // sorts the days of the week
        const uniqueDaysOfWeek = new Set(res.data.map(item => item.dayOfWeek));
        const daysOfWeek = Array.from(uniqueDaysOfWeek);
        daysOfWeek.sort((a: string, b: string) => {
          const order = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
          return order.indexOf(a) - order.indexOf(b);
        });

        this.daysOftheWeek = daysOfWeek

        for (var x = 0; x < this.daysOftheWeek.length; x++) {
          const dateString = this.daysOftheWeek[x]
          const firstThreeCharacters = dateString.substring(0, 3);

          for (var y = 0; y < this.empSchedule.length; y++) {
            if (firstThreeCharacters.toLowerCase() == this.empSchedule[y].dayOfWeek.toLowerCase().substring(0, 3)) {
              const totalHrs = this.empSchedule[y].hours
              const existingEntryIndex = this.tempArray.findIndex(entry => entry.day === this.daysOftheWeek[x]);

              if (existingEntryIndex === -1) {
                this.tempArray.push({ day: this.daysOftheWeek[x], amTotalHrs: totalHrs, totalHrs: '' });
              } else {

                this.tempArray[existingEntryIndex].pmTotalHrs = totalHrs;
              }
            }
          }

        }

        for (var i = 0; i < this.tempArray.length; i++) {
          this.tempArray[i].totalHrs = this.tempArray[i].amTotalHrs + this.tempArray[i].pmTotalHrs
        }
        this.computeTotalLeave()
      })


  }

  getRecentSchoolYears() {
    this.recentSchoolYears = [];

    var currentYear = new Date().getFullYear();

    for (var i = 0; i < 5; i++) {
      var startYear = currentYear - i;
      var endYear = startYear + 1;
      var yearValue = startYear.toString() + endYear.toString().substring(2);
      this.recentSchoolYears.push({ year: startYear + "-" + endYear, yearValue: yearValue });
      // console.log(this.recentSchoolYears)
    }
  }

  clearFilters() {
    this.dateFiled = ''
    this.leaveType = ''
    this.reason = ''
    this.from = ''
    this.to = ''
    this.totalMinutes = ''
    this.action = ''
    this.remarks = ''
    this.filterall();
  }

  clearLeave() {
    this.leaveType = ''
    this.filterall()
  }

  clearAction() {
    this.action = ''
    this.filterall()
  }

  fromDateChange() {
    const date = new Date(this.from);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;
    this.from = formattedDate;
    this.filterall();
  }

  toDateChange() {
    const date = new Date(this.to);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;
    this.to = formattedDate;
    this.filterall();
  }

  dateFiledChange() {
    const date = new Date(this.dateFiled);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const formattedDate = `${year}-${month}-${day}`;
    this.dateFiled = formattedDate;
    this.filterall();
  }

  filterall() {
    // console.log(this.remarks);
    this.filteredArray = [];
    var tempAr = []

    if (this.from != '' && this.to != '') {
      for (var x in this.filteredLeaveHistoryArr) {
        var sDate = this.filteredLeaveHistoryArr[x].startDate
        const date = new Date(sDate);
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        const formattedDate = `${year}-${month}-${day}`;
        sDate = formattedDate

        var eDate = this.filteredLeaveHistoryArr[x].endDate
        const date1 = new Date(eDate);
        const year1 = date1.getFullYear();
        const month1 = String(date1.getMonth() + 1).padStart(2, '0');
        const day1 = String(date1.getDate()).padStart(2, '0');
        const formattedDate1 = `${year1}-${month1}-${day1}`;
        eDate = formattedDate1

        if (sDate >= this.from && eDate <= this.to) {
          // this.filteredArray.push(this.filteredLeaveHistoryArr[x]);
          tempAr.push(this.filteredLeaveHistoryArr[x]);
        }
      }

      for (var x in tempAr) {
        if (
          tempAr[x].dateFiled.includes(this.dateFiled) &&
          tempAr[x].leaveTypeDescription.toLowerCase().includes(this.leaveType.toLowerCase()) &&
          tempAr[x].leaveDescription.toLowerCase().includes(this.reason.toLowerCase()) &&
          tempAr[x].totalMinutes.toString().includes(this.totalMinutes.toString()) &&
          tempAr[x].actionTaken.toLowerCase().includes(this.action.toLowerCase())
          // (remarks !== null && remarks.toLowerCase().indexOf(searchValue) !== -1)
        ) {
          this.filteredArray.push(tempAr[x]);
        }
      }

    } else {

      for (var x in this.filteredLeaveHistoryArr) {
        if (
          this.filteredLeaveHistoryArr[x].dateFiled.includes(this.dateFiled) &&
          this.filteredLeaveHistoryArr[x].leaveTypeDescription.toLowerCase().includes(this.leaveType.toLowerCase()) &&
          this.filteredLeaveHistoryArr[x].leaveDescription.toLowerCase().includes(this.reason.toLowerCase()) &&
          this.filteredLeaveHistoryArr[x].totalMinutes.toString().includes(this.totalMinutes.toString()) &&
          this.filteredLeaveHistoryArr[x].actionTaken.toLowerCase().includes(this.action.toLowerCase())
          // (remarks !== null && remarks.toLowerCase().indexOf(searchValue) !== -1)
        ) {
          this.filteredArray.push(this.filteredLeaveHistoryArr[x]);
        }
      }
    }

  }

  filterOpenDialog() {
    const dialogRef = this.dialog.open(FilterComponent, {
      width: '500px', disableClose: false, data: { leaveHistoryArr: this.filteredLeaveHistoryArr }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log(result.result.length)
      if (result.result != 'cancel') {
        this.filteredArray = []
        // this.filteredArray = result.result
        for (var x = 0; x < result.result.length; x++) {
          this.filteredArray.push(result.result[x])
        }
      }
      //this.filterLeaveHistory();
    });
  }

  getLeaves() {
    this.filteredPendingLeaveArr = [];
    this.filteredLeaveHistoryArr = [];

    this.http.get(this.global.api + 'EmployeePortal/LeaveCredit/' + this.global.requestid(), this.global.option)
      .map(response => response.json())
      .subscribe(res => {

        this.linfo = res.data;
        this.sickleave = res.data.sickleave;
        this.slDay = res.data.slDay;
        this.slHour = res.data.slHour;
        this.slMin = res.data.slMin;
        this.vacationleave = res.data.vacationleave;
        this.vlDay = res.data.vlDay;
        this.vlHour = res.data.vlHour;
        this.vlMin = res.data.vlMin;
        this.getLeaveHirstory();
      }, Error => {
        // console.log(Error);
        this.global.swalAlertError();
        //console.log(Error)
      });

  }

  getLeaveHirstory() {
    // console.log(this.global.requestid()+"--->"+this.global.setactivesy.schoolYear)
    // this.http.get(this.global.api + 'EmployeePortal/Leaves/' + this.global.requestid() + '/' + this.global.sy, this.global.option)
    this.hrisApi.getEmployeePortalLeaves(this.global.requestid(), this.global.sy)
      .map(response => response.json())
      .subscribe(res => {
        // console.log(res.data)
        this.filterLeave(res.data);
        this.pendingLeaveTrigger = 0;
        this.pendingLeaveTrigger = this.filteredPendingLeaveArr.length;
        this.filterLeaveHistory(res.data);
      }, Error => {
        // console.log(Error);
        this.global.swalAlertError();
      });

  }

  filterLeave(param) {
    var ctr = 0
    var tempArr = [];
    if (param != null) {
      ctr = param.length;
      this.pendingTrigger = true;
    }

    for (var i = 0; i < ctr; ++i) {
      if (param[i].actionTakenByAdmin == null) {
        tempArr.push(param[i])
      }
    }
    this.filteredPendingLeaveArr = tempArr;
    this.computeFutureLeaveCredits(this.filteredPendingLeaveArr, this.vacationleave, this.sickleave);
  }

  filterLeaveHistory(param) {
    this.filteredLeaveHistoryArr = []
    // console.log(param)
    var ctr = 0;
    if (param != null)
      ctr = param.length
    for (var i = 0; i < ctr; ++i) {
      if (param[i].actionTaken != null) {
        this.filteredLeaveHistoryArr.push(param[i])
      }
    }
    this.filteredArray = this.filteredLeaveHistoryArr
    // console.log(this.filteredLeaveHistoryArr)
  }

  computeFutureLeaveCredits(table, vl, sl) {
    var vltotalmins = 0;
    var sltotalmins = 0;
    var fvlday;
    var fvlhr;
    var fvlmin;
    var tempVL = 0
    var tempSL = 0;
    var fslday;
    var fslhr;
    var fslmin;
    table.forEach(function (row) {
      if (row.leaveTypeId == 2) {
        vltotalmins += row.totalMinutes;
      }
      else if (row.leaveTypeId == 1) {
        sltotalmins += row.totalMinutes;
      }
    });

    tempVL = vl - vltotalmins;
    tempSL = sl - sltotalmins;

    // console.log('VL Total Mins: '+vl+'\nVL to deduct: '+vltotalmins+'\nVL Future Total Mins: '+tempVL)
    // console.log('SL Total Mins: '+sl+'\nSL to deduct: '+sltotalmins+'\nSL Future Total Mins: '+tempSL)

    this.fVL = [this.computeLC(tempVL)];
    this.fSL = [this.computeLC(tempSL)];
    // console.table(this.fVL[0])
    // console.table(this.fSL[0])

  }

  computeLC(input) {
    // set minutes to seconds
    var seconds = input * 60

    // calculate (and subtract) whole days
    /*var days = Math.floor(seconds / 86400);//24hours computation
    seconds -= days * 86400;*/
    var days = Math.floor(seconds / 28800);//8hours computation
    seconds -= days * 28800;

    // calculate (and subtract) whole hours
    // var hours = Math.floor(seconds / 3600) % 24; //24hours computation
    var hours = Math.floor(seconds / 3600) % 8; //8hours computation
    seconds -= hours * 3600;

    // calculate (and subtract) whole minutes
    var minutes = Math.floor(seconds / 60) % 60;

    return { days: days, hours: hours, minutes: minutes, totminutes: input };
  }

  compare(i, x): boolean {
    if (i > 0) {
      if (x.dateFiled == this.filteredPendingLeaveArr[i - 1].dateFiled &&
        x.leaveTypeId == this.filteredPendingLeaveArr[i - 1].leaveTypeId &&
        x.leaveDescription == this.filteredPendingLeaveArr[i - 1].leaveDescription) {
        return true;
      }
      else return false;
    } else return false;
  }



  ////////////////////////////BUTTON CLICKS/////////////////////////////////////
  sdata;
  leavetoUpdate = [];
  setdata(param) {
    this.leavetoUpdate = [];
    this.sdata = param
    var that = this;
    this.filteredPendingLeaveArr.forEach(function (row) {
      if (that.sdata.dateFiled == row.dateFiled && that.sdata.leaveTypeId == row.leaveTypeId && that.sdata.leaveDescription == row.leaveDescription) {
        that.leavetoUpdate.push(row);
        // console.log(row.leaveTypeId)
      }
    });
    // console.table(this.leavetoUpdate);
  }

  curSL = [];
  curVL = [];

  addLeaveOpenDialog(param) {
    this.curSL = [];
    this.curVL = [];
    this.curSL.push({
      'sslDay': this.slDay,
      'sslHour': this.slHour,
      'sslMin': this.slMin,
      'sslTotalMin': this.sickleave,
    });
    this.curVL.push({
      'vvlDay': this.vlDay,
      'vvlHour': this.vlHour,
      'vvlMin': this.vlMin,
      'vvlTotalMin': this.vacationleave,
    });

    // console.log(this.leavetoUpdate);
    const dialogRef = this.dialog.open(LeaveComponent, {

      width: '900px',
      disableClose: false,
      data: {
        selectedData: this.leavetoUpdate,
        type: param,
        currentVL: this.curVL,
        currentSL: this.curSL,
        futureVL: this.fVL,
        futureSL: this.fSL,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.leavetoUpdate = [];
      // if (result.result != "cancel")
      //   this.getLeaves();
      this.getLeaves();
    });

  }



  //////////////////////cris///////////////////////
  leaveReason = ''
  leaveDateFiled
  leaveVenue = ''
  leaveDuration = []
  leaveFirstData = []
  leave_LeaveTpyes = []
  leaveId = ''
  leaveIdDuration = ''
  startDateDuration = ''
  endDateDuration = ''
  day
  totalLeaveTime
  leaveStatus

  getDayOfWeek(value): string {
    const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    if (value != undefined) {
      const dayIndex = value.getDay();
      this.day = daysOfWeek[dayIndex]
      return daysOfWeek[dayIndex];
    }

  }

  setleaveId(leaveDurationid, leaveId, startDate, endDate) {
    this.leaveId = leaveId
    this.leaveIdDuration = leaveDurationid
    this.startDateDuration = startDate
    this.endDateDuration = endDate
    // console.log('leaveDurationid', leaveDurationid, 'leaveId', leaveId, 'leaveTypeID', this.lType,)
  }

  setLeaveDetails(value) {
    // console.log(value)

    this.leaveReason = value.reason
    this.leaveDateFiled = value.dateFiled
    this.leaveVenue = value.venue
    this.leaveDuration = value.leave_LeaveDurations
    this.leaveStatus = value.leave_LeaveStatuses[0].statusDesc
    this.leaveFirstData = value
    this.leaveId = value.leaveID
    this.lType = value.leaveCategoryID

    // console.log(this.leaveDuration[0].leave_LeaveTypes.length);

    if (this.leaveDuration[0].leave_LeaveTypes.length == 0) {
      this.typeOfLeaveChecker = 0
    } else {
      this.typeOfLeaveChecker = this.leaveDuration[0].leave_LeaveTypes[0].leaveTypeID
    }
    // console.log(this.typeOfLeaveChecker);
    this.leaveDuration.sort((a, b) => {
      const dateA = new Date(a.startDate);
      const dateB = new Date(b.startDate);
      if (dateA < dateB) return -1;
      if (dateA > dateB) return 1;
      // If the dates are the same, compare hours
      const hourA = dateA.getHours();
      const hourB = dateB.getHours();
      if (hourA < hourB) return -1;
      if (hourA > hourB) return 1;
      return 0;
    });
    const originalDateString = this.leaveDateFiled
    const originalDate = new Date(originalDateString);
    this.getDayOfWeek(originalDate)
    this.computeTotalLeave()
  }

  leaveDurationDetails = []
  computeTotalLeave() {
    let totalHours = 0;
    let totalMinutes = 0;
    this.leaveDurationDetails = [];

    // Create a map to store accumulated hours and minutes for each day
    const dayTotals = new Map();

    // Loop through the data and calculate the time difference for each entry
    this.leaveDuration.forEach(entry => {
      const startDate = new Date(entry.startDate);
      const endDate = new Date(entry.endDate);
      const timeDiff = Number(endDate) - Number(startDate);

      // Convert time difference to hours and minutes
      const hours = Math.floor(timeDiff / (1000 * 60 * 60));
      const minutes = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));

      // Add the hours and minutes to the totals
      totalHours += hours;
      totalMinutes += minutes;

      const dateString = startDate.toString();
      const firstThreeCharacters = dateString.substring(0, 3);

      // Check if the day's entry exists in the dayTotals map
      if (dayTotals.has(firstThreeCharacters)) {
        // If it exists, accumulate hours and minutes
        const existingTotal = dayTotals.get(firstThreeCharacters);
        dayTotals.set(firstThreeCharacters, {
          hours: existingTotal.hours + hours,
          minutes: existingTotal.minutes + minutes,
        });
      } else {
        // If it doesn't exist, initialize a new entry in the map
        dayTotals.set(firstThreeCharacters, { hours, minutes, });
      }

    });

    // Adjust totalMinutes if it exceeds 60
    totalHours += Math.floor(totalMinutes / 60);
    totalMinutes %= 60;

    // Convert the dayTotals map to an array of objects
    this.leaveDurationDetails = Array.from(dayTotals).map(([day, total]) => {
      let { hours, minutes } = total;
      if (minutes >= 60) {
        const extraHours = Math.floor(minutes / 60);
        hours += extraHours;
        minutes %= 60;
      }
      return { day, hours, minutes, };
    });


    this.compute()
  }

  compute() {

    let totalDays = 0;
    let totalHours = 0;
    let totalMinutes = 0;

    for (let i = 0; i < this.leaveDurationDetails.length; i++) {
      const input = this.leaveDurationDetails[i];
      const correspondingSchedule = this.tempArray.find(schedule => schedule.day.substring(0, 3) === input.day);

      if (correspondingSchedule) {
        if (input.hours === correspondingSchedule.totalHrs) {
          totalDays += 1;
        } else {
          totalHours += input.hours;
          totalMinutes += input.minutes;
        }
      }
    }

    // Handle excess minutes
    if (totalMinutes >= 60) {
      const extraHours = Math.floor(totalMinutes / 60);
      totalHours += extraHours;
      totalMinutes %= 60;
    }

    // Check if total hours is greater than or equal to 9
    if (totalHours >= 8) {
      const additionalDays = Math.floor(totalHours / 8);
      totalDays += additionalDays;
      totalHours %= 8; // Subtract the hours that contribute to additional days
    }

    const resultParts = [];

    // Check if totalDays is greater than 0
    if (totalDays > 0) {
      resultParts.push(`${totalDays} day${totalDays === 1 ? '' : 's'}`);
    }

    // Check if totalHours is greater than 0
    if (totalHours > 0) {
      const hourText = totalHours === 1 ? "hr" : "hrs";
      resultParts.push(`${totalHours} ${hourText}`);
    }

    // Check if totalMinutes is greater than 0
    if (totalMinutes > 0) {
      resultParts.push(`${totalMinutes} mins`);
    }

    const result = resultParts.join(', ');

    this.totalLeaveTime = result;
    this.global.swalClose();
  }

  async getEmployeeFiledLeaves() {
    try {
      this.global.swalLoading('Loading Employee Leave.');

      const response = await this.hrisApi.getEmployeePortalLeave().toPromise();
      const res = response.json();

      this.hrisApi.getEmployeePortalLeave().map(response => response.json())
        .subscribe(res => {

          // console.log(res.data);

          if (res.data.length !== 0) {


            this.leaveArray = res.data;

            const filteredData = res.data.filter(item => {
              return item.leave_LeaveStatuses[0].leaveStatusID !== 4 && item.leave_LeaveStatuses[0].leaveStatusID !== 5 && item.leave_LeaveStatuses[0].leaveStatusID !== 6;
            });
            this.leaveArray = filteredData
            // console.log(this.leaveArray);
            if (this.leaveArray.length != 0) {
              const sortedLeaveRecords = this.leaveArray.sort((a, b) => new Date(b.dateFiled).getTime() - new Date(a.dateFiled).getTime()); // Reverse comparison
              this.leaveArray = sortedLeaveRecords;
              this.leaveReason = this.leaveArray[0].reason;
              this.leaveDateFiled = this.leaveArray[0].dateFiled;
              this.leaveVenue = this.leaveArray[0].venue;
              this.leaveDuration = this.leaveArray[0].leave_LeaveDurations;
              this.leaveFirstData = this.leaveArray[0];
              this.leaveId = this.leaveArray[0].leaveID;
              this.lType = this.leaveArray[0].leaveCategoryID;
              this.leaveStatus = this.leaveArray[0].leave_LeaveStatuses[0].statusDesc;

              if (this.leaveDuration[0] != undefined && this.leaveDuration[0].leave_LeaveTypes.length !== 0) {
                this.typeOfLeaveChecker = this.leaveDuration[0].leave_LeaveTypes[0].leaveTypeID
                // console.log(this.typeOfLeaveChecker);
              }

              this.leaveDuration.sort((a, b) => {
                const dateA = new Date(a.startDate);
                const dateB = new Date(b.startDate);
                if (dateA < dateB) return -1;
                if (dateA > dateB) return 1;
                // If the dates are the same, compare hours
                const hourA = dateA.getHours();
                const hourB = dateB.getHours();
                if (hourA < hourB) return -1;
                if (hourA > hourB) return 1;
                return 0;
              });

              const originalDateString = this.leaveDateFiled;
              const originalDate = new Date(originalDateString);
              this.getDayOfWeek(originalDate);
              this.getEmployeeLeaveManagementSchedule();
            }
          } else {
            this.leaveArray = [];
          }
        })
      await
        this.global.swalClose();
    } catch (error) {

    }
  }

  cancelLeave() {
    // console.log();

    Swal({
      title: "Are you sure?",
      html: 'You are about to cancel this leave.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, cancel it!',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {

        this.global.swalLoading('Canceling Leave.');
        this.hrisApi.putEmployeePortalLeaveStatusByEmployee(this.leaveId, 2).map(response => response.json())
          .subscribe(res => {
            this.global.swalClose()
            this.getEmployeeFiledLeaves()
            this.global.swalAlert("Leave has been canceled successfully.", '', 'success');
          })

      }

    })

  }

  refileLeave() {
    Swal({
      title: "Are you sure?",
      html: 'You are about to refile this leave.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, refile it!',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {

        this.global.swalLoading('Refiling Leave.');
        this.hrisApi.putEmployeePortalLeaveStatusByEmployee(this.leaveId, 1).map(response => response.json())
          .subscribe(res => {
            this.global.swalClose()
            this.getEmployeeFiledLeaves()
            this.global.swalAlert("Leave has been refiled successfully.", '', 'success');
          })

      }

    })
  }

  deleteLeave() {

    Swal({
      title: "Are you sure?",
      html: 'You are about to delete this leave.<br>This action cannot be undone.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel'
    }).then((result) => {

      if (result.value) {

        this.hrisApi.deleteEmployeePortalLeave(this.leaveId).map(response => response.json())
          .subscribe(res => {
            // console.log("deleted", res.message, "success");

            this.getEmployeeFiledLeaves()
            this.global.swalAlert("Leave deleted successfully.", '', 'success');
          })
      }

    })
  }

  deleteLeaveTime() {

    Swal({
      title: "Are you sure?",
      html: 'You are about to delete this leave duration.<br>This action cannot be undone.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel'
    }).then((result) => {

      if (result.value) {
        let totalLeaveDurationMinutes = 0;
        this.leaveArray.forEach(leave => {
          if (leave.leaveID == this.leaveId) {
            leave.leave_LeaveDurations.forEach(leaveDuration => {
              const startTime: Date = new Date(leaveDuration.startDate);
              const endTime: Date = new Date(leaveDuration.endDate);
              // Calculate the difference in minutes
              const durationInMinutes = (endTime.getTime() - startTime.getTime()) / (60 * 1000);
              totalLeaveDurationMinutes += durationInMinutes
            });
          }
        });

        for (var x = 0; x < this.leaveArray.length; x++) {
          if (totalLeaveDurationMinutes / 60 > 1) {
            if (this.leaveArray[x].leave_LeaveDurations.length > 1) {
              if (this.leaveArray[x].leaveID == this.leaveId) {
                this.hrisApi.deleteEmployeePortalLeaveDuration(this.leaveId, this.leaveIdDuration).map(response => response.json())
                  .subscribe(res => {
                    // console.log("deleted", res.message, "success");
                    this.getEmployeeFiledLeaves()
                    this.global.swalAlert("Success!", 'Leave duration deleted.', 'success');
                  })
              }
            } else {
              this.global.swalAlert("Error!", 'Cannot delete the last leave duration.<br> At least one leave duration must remain.', 'error');
            }
          } else {
            this.global.swalAlert("Error!", 'The total leave duration should be at least 1 hour.', 'error');
          }

        }

      }
    })

  }

  calculateHoursAndMinutes(startDate: string, endDate: string): string {
    const startDateTime = new Date(startDate);
    const endDateTime = new Date(endDate);

    const timeDifference = endDateTime.getTime() - startDateTime.getTime();

    const hours = Math.floor(timeDifference / (1000 * 60 * 60));
    const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60));

    if (hours === 1) {
      var hourText = "hr";
    } else {
      var hourText = "hrs";
    }

    if (minutes === 1) {
      var minsText = "min";
    } else {
      var minsText = "mins";
    }

    if (hours > 0 && minutes > 0) {
      return `${hours} ${hourText} & ${minutes} ${minsText}`;
    } else if (hours > 0) {
      return `${hours} ${hourText}`;
    } else {
      return `${minutes} ${minsText}`;
    }

  }

  addTime(param) {

    // console.log(param)
    // console.log(this.leavetoUpdate);
    const dialogRef = this.dialog.open(LeaveManagementComponent, {

      width: '900px',
      disableClose: false,
      data: {
        type: param,
      }
    });

    dialogRef.afterClosed().subscribe(result => {

      this.getLeaves();
      this.getEmployeeFiledLeaves();
      this.cdr.detectChanges()
    });


  }

  addLeaveManagementOpenDialog(param) {
    // console.log(param)
    // console.log(this.leavetoUpdate);
    const dialogRef = this.dialog.open(LeaveManagementComponent, {

      width: '1200px',
      disableClose: true,
      data: {
        type: param,
        value: this.leaveFirstData,
        leaveId: this.leaveId,
        Ltype: this.lType,
        leaveTypeChecker: this.typeOfLeaveChecker

      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log(result)
      if (result.result != "cancel") {
        this.getEmployeeFiledLeaves()
      }
      this.getLeaves();

    });

  }

  updateLeave(param) {

    // console.log(param)

    const dialogRef = this.dialog.open(LeaveManagementComponent, {

      width: '900px',
      disableClose: true,
      data: {
        type: param,
        value: this.leaveFirstData,
        leaveId: this.leaveId,
        facultyType: this.lType
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log(result)
      if (result.result != "cancel") {
        this.getEmployeeFiledLeaves()
        this.getLeaves();
        this.lType = result.value
      }


    });
  }

  deleteEntry() {
    const dialogRef = this.dialog.open(DeleteConfirmationDialogComponent, {
      width: '500px', disableClose: true, data: { message: "the selected leave" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result == 'deleteConfirm') {
        var that = this;
        this.leavetoUpdate.forEach(function (row) {
          that.leaveService.deleteEntry(that.global.requestid(), row.leaveId);
          that.filteredPendingLeaveArr = [];
          that.getLeaves();
        });
      }
      else {

      }
    });
  }

  editLeaveDurations(param) {
    // console.log(param)
    // console.log(this.leavetoUpdate);
    const dialogRef = this.dialog.open(EditLeaveDurationsComponent, {

      width: '600px',
      disableClose: false,
      autoFocus: false,
      data: {
        type: param,
        value: this.leaveFirstData,
        sdate: this.startDateDuration,
        edate: this.endDateDuration,
        leaveId: this.leaveId,
        leaveDurationId: this.leaveIdDuration,
        Ltype: this.lType

      }
    });
    {

    }
    dialogRef.afterClosed().subscribe(result => {

      if (result.result == 'success') {
        this.getEmployeeFiledLeaves()
      }

    });
  }

  checkSchedule(param) {
    // console.log(this.facultyType);
    // console.log(this.leavetoUpdate);
    // console.log( this.leaveCategory, this.facultyType);

    const dialogRef = this.dialog.open(CheckScheduleComponent, {
      width: '1200px',
      disableClose: false,
      autoFocus: false,
      data: {
        lType: this.leaveCategory,
        fType: this.facultyType
      }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  //////////////////////////////////////////////////

  ///////////////////////////TRIGGERS//////////////////////////////////////////
  routetrigger = false;
  validateRoute(param): boolean {
    if (param == null || param == "") {
      this.routetrigger = false;
      return true
    }
    else {
      this.routetrigger = true;
      return false;
    }
  }

  actionTrigger = false;
  validateActionTaken(param): boolean {
    if (param == '' || param == null) {
      this.actionTrigger = false;
      return true
    }
    else {
      this.actionTrigger = true;
      return false;
    }
  }

  ///////////////////////PAGINATION FUNCTIONS////////////////
  LeaveHistoryPageChanged(event) {
    this.LeaveHistoryConfig.currentPage = event;
  }
  VLeavePageChanged(event) {
    this.VLeaveConfig.currentPage = event;
  }
  SLeavePageChanged(event) {
    this.SLeaveConfig.currentPage = event;
  }
  OBLeavePageChanged(event) {
    this.OBLeaveConfig.currentPage = event;
  }


}




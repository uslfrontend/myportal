import { Component, OnInit } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';

@Component({
  selector: 'app-grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.scss']
})
export class GradesComponent implements OnInit {
gradearray
  constructor(public global: GlobalService,private http: Http) { }

  ngOnInit() {
  	 this.gradearray=this.global.gradearray
     var x = ('N-').replace(/\D/g, "")
     console.log()
  }
 x=''
  sy(i,x){
  	if (i==0) {
      return true
    }else{
      if (this.gradearray[i-1].schoolYear.substring(0,6)==this.gradearray[i].schoolYear.substring(0,6)){
       return false
      }else
        return true
    }
  }
  y=''


  sem(i,y){
  	if (i==0) {
      return true
    }else{
      if (this.gradearray[i-1].schoolYear==this.gradearray[i].schoolYear){
         return false
      }else
         return true
    }

  }
 yy=''

 sem2(y,i){
 	if (this.gradearray[i+1]!=undefined) {
	 	if (this.gradearray[i].schoolYear!=this.gradearray[i+1].schoolYear) {
			  return true
	 	}
		 return false
 	}
 	return true
  }


  track
  average=0;
  count=0;
 

  calcgrade(y){
  	var average=0
  	var count=0
  	for(var i = 0; i < this.gradearray.length; i++) {
    if (this.gradearray[i].schoolYear == y) {
    	var x;
    	if (this.gradearray[i].grade.replace(/\D/g, "")=='') {
    		x=0
    	}else 
        x = parseInt(this.gradearray[i].grade.replace(/\D/g, ""));
        average = average + x*parseFloat(this.gradearray[i].units);
        count=count +parseFloat(this.gradearray[i].units);
	    }
	}
	return "Average: "+(Math.floor((average/count)*100)/100).toFixed(2).toString() + "%"
  }


  getsy(sem){
	 var y = parseInt(sem.substring(0,4)) + 1;
	 return  "SY " + sem.substring(0,4) + "-" + y
  }
   getsem(sem){
     if (sem.substring(6)=='1')
     return "1st Semester";
   else if (sem.substring(6)=='2')
     return "2nd Semester";
   else
     return "Summer";
  }

}

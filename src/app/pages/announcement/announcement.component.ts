import { Component, OnInit } from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss']
})
export class AnnouncementComponent implements OnInit {

  constructor(public dialog: MatDialog,public dialogRef: MatDialogRef<AnnouncementComponent>,@Inject(MAT_DIALOG_DATA) public data: any,private http: Http) { }

  ngOnInit() {
  }
  
noclick()
{
  this.dialogRef.close(1)
}

}

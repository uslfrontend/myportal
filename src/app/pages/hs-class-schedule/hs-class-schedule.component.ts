import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { Inject} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { StudentApiService } from './../../student-api.service';

@Component({
  selector: 'app-hs-class-schedule',
  templateUrl: './hs-class-schedule.component.html',
  styleUrls: ['./hs-class-schedule.component.scss']
})
export class HsClassScheduleComponent implements OnInit {

  tableArr
  tableArr2=[]
  sy=''
  constructor( private domSanitizer: DomSanitizer,public global: GlobalService,public api:StudentApiService) { }


  elective=''
  ngOnInit() {
    var sy=this.global.sy
      if (this.global.studinfo.departmentCode=='ELEM'||this.global.studinfo.departmentCode=='HS') {
        if (this.global.sy.length==7) {
          sy = this.global.sy.slice(0, -1)
        }
      }
      this.api.apiHSEnrollmentStudentElective(sy)
              .subscribe(res => {
                if (res.data!=null) {
                  if (res.data.length!=0) {
                    this.elective = res.data[0].name
                    if (this.elective == "Bread and Pastry Production") {
                      this.elective = 'BPP'
                    }
                  }
                }

               this.sy = sy
          	   if (this.global.HSClassSchedule.length==0) {
                 if (this.global.enrollmenthist[this.global.enrollmenthist.length-1].section==""||this.global.enrollmenthist[this.global.enrollmenthist.length-1].section==null) {
                   this.global.enrollmenthist[this.global.enrollmenthist.length-1].section = 'noSection'
                 }

                 this.api.apiBasicEdStudentSchedule(sy)
                    .subscribe(res => {
                    	this.tableArr=[]
                    	if (res.data!=null) {
                    		var temp=''
                    		var tableArrtemp=0
            				  this.tableArr.push({
                				time:'',
                				m:'',
                				t:'',
                				w:'',
                				th:'',
                				f:'',
                			})

                			var x
                			for (var i = 0; i < length; ++i) {
                				if (res.data[i].time != ' ') {
                					x = i
                					break
                				}
                			}
                      if (res.data[x]!=undefined) {
                  			this.tableArr[0] = {
                  				time: res.data[x].time,
                  				m:'',
                  				t:'',
                  				w:'',
                  				th:'',
                  				f:'',
                  			}
                        // code...
                      }
                			for (var i = 0; i < res.data.length; ++i) {
                    				if (res.data[i].time != ' ') {
                    					var time = res.data[i].time
                    					var check = false
        	            				for (var i2 = 0; i2 < this.tableArr.length; ++i2) {
        	            					if (this.tableArr[i2].time == res.data[i].time) {
        	            						check = true
        	            						break
        	            					}
        	            				}
        	            				if (!check) {
        	            					tableArrtemp++
        	            					this.tableArr[tableArrtemp] = {
        				        				time: res.data[i].time,
        				        				m:'',
        				        				t:'',
        				        				w:'',
        				        				th:'',
        				        				f:'',
        				        			}
        	            				}
                    				}
                    			}

                    			var start = 7
                    			var temparr=[]
                    			for (var i = 0; i < this.tableArr.length; ++i) {
                    				var keyA = this.tableArr[i].time.split(":")
                            if (keyA[1]!=undefined) {
                      				var keyb = keyA[1].split("-")
                      				temparr.push({sort:parseFloat(keyA[0]+'.'+keyb[0]),index:i})
                            }
                    			}
                    			sortByKey(temparr, 'sort')
                    			function sortByKey(array, key) {
          						    return array.sort(function(a, b) {
          						        var x = a[key]; var y = b[key];
          						        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
          						    });
        						}

        						var arr1 = []
        						var arr2 = []

        						for (var i = 0; i < temparr.length; ++i) {
        							if (temparr[i].sort>=7) {
        								arr1.push(temparr[i])
        							}else{
        								arr2.push(temparr[i])
        							}
        						}
        						var arr3 = arr1.concat(arr2)
        						var arr4 = []

        						for (var i = 0; i < arr3.length; ++i) {
        							arr4.push(this.tableArr[arr3[i].index])
        						}
        						this.tableArr = arr4
                    this.global.HSClassSchedule=this.tableArr
                    			for (var i3 = 0; i3 < this.tableArr.length; ++i3) {
        	            			for (var i = 0; i < res.data.length; ++i) {
        	            				if (this.tableArr[i3].time == res.data[i].time) {
        	            						if (res.data[i].teacher==null) {
        				        					res.data[i].teacher= ''
        				        				}else
        				        					res.data[i].teacher="<hr>"+res.data[i].teacher

                              if (res.data[i].subject.toUpperCase().includes("TLE")&&this.global.studinfo.departmentCode=='HS'&&this.global.studinfo.yearOrGradeLevel==4) {
                               res.data[i].subject=''
                               res.data[i].teacher=''
                              }

                              if (res.data[i].subject.toUpperCase().includes(this.elective.toUpperCase())) {
                                res.data[i].subject = '<span style="margin: 0;background-color: #006600;color: white;padding: 3px;border-radius: 5px">'+ res.data[i].subject+ "</span>"
                              }else
                                res.data[i].subject = '<span>'+ res.data[i].subject+ "</span>"


        	            			  if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('mon')) {
        				        				this.tableArr[i3].m =  this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('tues')) {
        				        				this.tableArr[i3].t =this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('wed')) {
        				        				this.tableArr[i3].w =  this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('thurs')){
        				        				this.tableArr[i3].th = this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        				        			if ( res.data[i].day.toLowerCase().replace(/\s/g, "").includes('fri')) {
        				        				this.tableArr[i3].f = this.domSanitizer.bypassSecurityTrustHtml("<b>"+ res.data[i].subject +"</b>"+res.data[i].teacher);
        				        			}
        	            				}
        	            			}
                    			}


                    			for (var i = 0; i < res.data.length; ++i) {
                    				if (res.data[i].time==' ') {
                    					this.tableArr2.push(res.data[i])
                    				}
                    			}
                          this.global.HSClassSchedule=this.tableArr
                          this.global.noHSClassSchedule=this.tableArr2
        	            	}
                    },Error=>{
                        this.global.swalAlertError()
                        //this.global.logout()
                      });
          	   }else{
                 this.tableArr = this.global.HSClassSchedule;
                 this.tableArr2 = this.global.noHSClassSchedule;
          	   }
         },Error=>{
            this.global.swalAlertError()
            //this.global.logout()
          });
  }

  checkcolor(sub){
      if (sub.toUpperCase().includes(this.elective.toUpperCase())) {
        return true
      }else
        return false
  }
  checkcolor2(sub){
      if (!sub.toUpperCase().includes(this.elective.toUpperCase())) {
        return true
      }else
        return false
  }
}

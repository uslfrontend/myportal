import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreEnrollmentComponent } from './pre-enrollment.component';

describe('PreEnrollmentComponent', () => {
  let component: PreEnrollmentComponent;
  let fixture: ComponentFixture<PreEnrollmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreEnrollmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreEnrollmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShsCollegePreEnrollmentComponent } from './shs-college-pre-enrollment.component';

describe('ShsCollegePreEnrollmentComponent', () => {
  let component: ShsCollegePreEnrollmentComponent;
  let fixture: ComponentFixture<ShsCollegePreEnrollmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShsCollegePreEnrollmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShsCollegePreEnrollmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

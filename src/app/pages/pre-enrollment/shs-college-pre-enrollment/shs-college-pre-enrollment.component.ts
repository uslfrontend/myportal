
import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { GlobalService } from './../../../global.service';
import Swal from 'sweetalert2';
const swal = Swal;
import { StudentApiService } from './../../../student-api.service';
import { PreEnrollmentModalComponent } from './../pre-enrollment-modal/pre-enrollment-modal.component';
@Component({
  selector: 'app-shs-college-pre-enrollment',
  templateUrl: './shs-college-pre-enrollment.component.html',
  styleUrls: ['./shs-college-pre-enrollment.component.css']
})
export class ShsCollegePreEnrollmentComponent implements OnInit {
  reportCardVar="assets/noimage4.jpg"
  reportCardSave=''
  popvar="assets/noimage3.jpg"
  popsave=''
  pdate=''
  record=[
    {
     proofOfPayment_Status:3,
     reportCardStatus:3,
     proofOfPayment_Remarks:'',
     reportCardRemarks:'',
   }]

  encryp=''

  exist=false
  data=[]
  proofOfPayment
  accept = true

  preenrollment

  constructor(public dialog: MatDialog,public global: GlobalService,public api:StudentApiService) { }

  ngOnInit() {
    this.proceedAdmission();
  }

  proceedAdmission(){

    this.api.apiEncryptedString()
            .subscribe(res => {
              this.encryp=res.data
              this.api.apigetphpfileacctgapis(res.data)
                  .subscribe(res => {
                    this.preenrollment=res.data
                    // console.log(res.data)
                    // this.preenrollment=false
                    this.getSubmittedRequirements();


                  },Error=>{

                    // if(Error.name == "TimeoutError"){
                      // console.log(Error);
                    //   this.loaddata();
                    // }else{
                    //   window.history.back();
                    //   this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
                    // }

                      // window.history.back(); //commented out to continue the admission process 02/23/2023
                      this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
                      this.getSubmittedRequirements();

                  });
             },Error=>{
                window.history.back();
                this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
              });


  }


  getSubmittedRequirements(x=null){
    // console.log("this.global.colpreenrollmentyear",this.global.colpreenrollmentyear)
    this.api.apiPreEnrollmentSubmittedRequirement(this.global.colpreenrollmentyear)
    .subscribe(res => {
      // console.log(res.data)
      // this.global.swalClose()
      if(res.data.length === 0){

        this.reportCardVar = "assets/noimage4.jpg"
        this.reportCardSave = ''
        this.popvar="assets/noimage3.jpg"
        this.popsave=''
        this.pdate = ''

        if(x==1){
          this.record[0].proofOfPayment_Status=2
        }
      }else{
        this.record=res.data
        this.reportCardVar = "data:image/png;base64,"+res.data[0].reportCard
        this.popvar="data:image/png;base64,"+res.data[0].proofOfPayment
        this.reportCardSave = res.data[0].reportCard
        this.popsave=res.data[0].proofOfPayment
        this.pdate = res.data[0].dateOfPayment

        if(x==1&&res.data[0].proofOfPayment_Status!=2){
          var pdate
          pdate = new Date(this.pdate).toLocaleString();
           if(pdate=="Invalid Date"){
             pdate=''
           }
          this.api.apiPreEnrollmentSubmittedRequirementPut(
            null,null,null,
            null,null,null,
            this.reportCardSave,this.record[0].reportCardStatus,this.record[0].reportCardRemarks,
            null,null,null,
            this.popsave,2,this.record[0].proofOfPayment_Remarks,pdate,1)
          .subscribe(res => {
              this.proceedAdmission()
          },Error=>{
             this.global.swalAlertError(Error)
            });
        }
      }
    },Error=>{
      window.history.back();
      this.global.swalAlertError(Error)
    });
  }


  SubmitReq(){
    var x=''
    if (this.reportCardSave=='' && this.global.currentplacementstatus!='JHS') {
      x=x+"*Report Card is required.<br>"
    }
    if (this.record[0].proofOfPayment_Status!=2) {
      if (this.popsave=='') {
        x=x+"*Proof of payment is required.<br>"
      }
      if (this.pdate=='') {
        x=x+"*Date of payment is required.<br>"
      }
    }
    if (x==='') {
      this.global.swalLoading('')
      var pdate
      pdate = new Date(this.pdate).toLocaleString();
     if(pdate=="Invalid Date"){
       pdate=null
     }
    //  console.log(this.record[0])
      if (this.record[0].reportCardStatus===3) {
                this.api.apiPreEnrollmentSubmittedRequirementPost(
                  this.global.requestid(),
                  this.global.colpreenrollmentyear,
                  null,
                  null,
                  this.reportCardSave,
                  null,
                  this.popsave,
                  pdate,1
                  )
                .subscribe(res => {
                    // console.log(res)
                    this.global.swalSuccess(res.message)
                    this.proceedAdmission()
                },Error=>{
                   this.global.swalAlertError(Error)
                  });
      }else{

        if (this.record[0].reportCardStatus!=2) {
          this.record[0].reportCardStatus=0
        }
        if (this.record[0].proofOfPayment_Status!=2) {
          this.record[0].proofOfPayment_Status=0
        }
        this.api.apiPreEnrollmentSubmittedRequirementPut(
          null,null,null,
          null,null,null,
          this.reportCardSave,this.record[0].reportCardStatus,this.record[0].reportCardRemarks,
          null,null,null,
          this.popsave,this.record[0].proofOfPayment_Status,null,
          pdate,1
          )
          .subscribe(res => {
              this.global.swalSuccess(res.message)
              this.proceedAdmission()
          },Error=>{
             this.global.swalAlertError(Error)
            });
      }
    }else{
       this.global.swalAlert("Missing requirement:", x,"warning")
    }
  }


  //UPLOADING OF DOCUMENT (FUNCTIONS and METHODS)
  onFileChangeReportCard(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if(file.size<10000000){
        if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
          //this.filetype = file.type
          this.reportCardVar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
          this.reportCardSave = reader.result.toString().split(',')[1]
        }else{
            this.global.swalAlert("Invalid Image Type",'','warning');
        }
       }else{
          this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
       }
      };
    }
  }

  onFileChangePoP(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        if(file.size<10000000){
          if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
            //this.filetype = file.type
            this.popvar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
            this.popsave = reader.result.toString().split(',')[1]
          }else{
            this.global.swalAlert("Invalid Image Type",'','warning');
          }
       }else{
          this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
       }
      };
    }
  }

  opendits(x,y=null,rec=null){
    var wid='500px'
    if (x==2) {
     wid='600px'
    }
    if (y==null) {
        const dialogRef = this.dialog.open(PreEnrollmentModalComponent, {
          width: wid, disableClose: false,data:{
            data:this.data,
            pop:this.proofOfPayment,
            strandval:this.data[0].shS_PriorityStrandID1,
            strandval1:this.data[0].shS_PriorityStrandID2,
            type:x,
            preenrollment:this.preenrollment,
            pre:0,
          }
      });
          dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
          if (result.result=="updated") {
            }
          }
        });
      }else{
        const dialogRef = this.dialog.open(PreEnrollmentModalComponent, {
          width: '850px', disableClose: false,data:{
            pre:1,
            rec:rec,
          }
      });
          dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
          if (result.result=="updated") {
            }
          }
        });
      }
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreEnrollmentModalComponent } from './pre-enrollment-modal.component';

describe('PreEnrollmentModalComponent', () => {
  let component: PreEnrollmentModalComponent;
  let fixture: ComponentFixture<PreEnrollmentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreEnrollmentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreEnrollmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

import { GlobalService } from './../../../global.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import Swal from 'sweetalert2';
const swal = Swal;
@Component({
  selector: 'app-pre-enrollment-modal',
  templateUrl: './pre-enrollment-modal.component.html',
  styleUrls: ['./pre-enrollment-modal.component.css']
})
export class PreEnrollmentModalComponent implements OnInit {

  strandfiltered=[]
  strand=[]
  constructor(private http: Http,public dialogRef: MatDialogRef<PreEnrollmentModalComponent>,@Inject(MAT_DIALOG_DATA) public data: any,public global: GlobalService) { }
  attachment=''
  
  proglevelval='05'
  strandval=''
  strandval1=''
  bdate=''
  sy=''

  fname=''
  mname=''
  lname=''
  suffix=''
  gender=''
  cnumber=''
  cperson=''
  gradfrom=''
  courseval=''
  courseval1=''
  courseval2=''
  address=''
  permPSGC=''
  YearGraduated
  IdNumber=''
  preenrollment=false
  close=false
  ngOnInit() {
    if (this.data.pre===0) {
      this.hspreenrollment()
    }else{
      this.colpreenrollment()
    }
  }

  opennewpic(x){
    const base64ImageData = x;
    const contentType = 'image/png';

    const byteCharacters = atob(base64ImageData.substr(`data:${contentType};base64,`.length));
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += 1024) {
        const slice = byteCharacters.slice(offset, offset + 1024);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    const blob = new Blob(byteArrays, {type: contentType});
    const blobUrl = URL.createObjectURL(blob);

    window.open(blobUrl, '_blank');
  }
colpreenrollment(){

}
hspreenrollment(){
      if (this.global.userinfo!=undefined) {
          this.bdate=this.global.userinfo.dateOfBirth
        this.IdNumber=this.global.requestid()
      this.fname=this.global.userinfo.firstName
      this.mname=this.global.userinfo.middleName
      this.lname=this.global.userinfo.lastName
      this.suffix=this.global.userinfo.suffixName
      this.gender=this.global.userinfo.gender
      this.cnumber=this.global.userdemog.mobileNo  
      }
      this.attachment = "data:image/png;base64,"+this.data.pop
      this.img = this.data.pop

    this.preenrollment=this.data.preenrollment
    this.sy='202122'
    this.cperson=''
    this.gradfrom="University of Saint Louis Tuguegarao"
    this.courseval=''
    this.courseval1=''
    this.courseval2=''
    this.address="Carig Sur, Tuguegarao City, Cagayan"
    this.permPSGC="021529053"

    this.YearGraduated=2021

      this.strandfiltered=this.data.strandfiltered


    this.strandval=this.data.strandval
    this.strandval1=this.data.strandval1
    this.pdate=this.data.data[0].datePaid
}

 exist
 proofOfPayment

 accept = true
 preenroll(){
 	var x=''
    if (this.proglevelval=='05') {    
	    if (this.pdate == '') {
	      x=x+"*Date of Payment is required!<br>"
	    }
	    if (this.img == '') {
	      x=x+"*Proof Payment is required!<br>"
	    }
    }

    if(x==''&&this.accept){
    	this.swalConfirm();
    }else{
  	 this.global.swalAlert("Error Found:", x,"warning")
    }
 }
  
  swalConfirm()
  {
                  this.accept = false
        	var pdate = new Date(this.pdate).toLocaleString();
    		var date = new Date(this.bdate).toLocaleString();
        this.global.swalLoading('')
        	this.http.put(this.global.api+'OnlineRegistration/Applicant/'+ this.data.data[0].applicantNo ,{
		        "ProgramLevel": this.proglevelval,
		        "FirstName": this.fname.toUpperCase(),
		        "MiddleName": this.mname.toUpperCase(),
		        "LastName": this.lname.toUpperCase(),
		        "SuffixName": this.suffix.toUpperCase(),
		        "DateOfBirth": date,
		        "Gender": this.gender,
		        "ContactNumber": this.cnumber,
		        "ContactPerson": this.cperson,
		        "SchoolGraduatedFrom": this.gradfrom,
		        "StrandId": 1,
		        "PreferredCourseId": this.courseval,
		        "AlternativeCourseId1": this.courseval1,
		        "AlternativeCourseId2": this.courseval2,
		        "YearGraduated": this.YearGraduated,
		        "SchoolAddressNoStreet": this.address,
		        "SchoolAddressPSGC":  this.permPSGC,
		        "SHS_PriorityStrandID1": this.strandval,
		        "SHS_PriorityStrandID2": this.strandval1,
		        "TopOfMyClass": false,
		        "Remark": "",
            "PaymentVerified": 0,
		        "SchoolYear": this.sy,
		        "ProofOfPayment": this.img,
		        "EmailAddress": this.IdNumber,
		        "DatePaid": pdate,
		        "NewStudent": true,
		  		  "PaymentType": 1,
  				  "IdNumber": this.IdNumber,
					},this.global.option)
		            .map(response => response.json())
		            .subscribe(res => {
		              this.global.swalSuccess(res.message)
                  this.accept = true
                  this.close = true
		                      },Error=>{
		                        this.global.swalAlertError();
		                        console.log(Error)
							              this.accept = true
		                      });
  }


getstrand(x){
	for (var i = 0; i < this.strandfiltered.length; ++i) {
		if(x==this.strandfiltered[i].strandId){
			return this.strandfiltered[i].strandTitle
		}
	}
}

 filetype=''
 img=''
 pdate=''
  onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
          this.filetype = file.type
          this.attachment = "data:image/png;base64,"+reader.result.toString().split(',')[1]
          this.img = reader.result.toString().split(',')[1]
        }else{
          alert("Invalid Image Type");
        }
      };
    }
   
  }

  onNoClickclose(){
    if (this.close) {
       this.dialogRef.close({result:'updated'});
    }else
       this.dialogRef.close({result:'cancel'});
  }
}

import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { GlobalService } from './../../global.service';
import Swal from 'sweetalert2';
const swal = Swal;
import { PreEnrollmentModalComponent } from './pre-enrollment-modal/pre-enrollment-modal.component';
import { StudentApiService } from './../../student-api.service';
@Component({
  selector: 'app-pre-enrollment',
  templateUrl: './pre-enrollment.component.html',
  styleUrls: ['./pre-enrollment.component.scss']
})
export class PreEnrollmentComponent implements OnInit {

  strandfiltered=[]
  strand=[]
  constructor(public dialog: MatDialog,public global: GlobalService,public api:StudentApiService) { }
  proglevelval='05'
  strandval=''
  strandval1=''
  bdate=''
  sy
  fname=''
  mname=''
  lname=''
  suffix=''
  gender=''
  cnumber=''
  cperson=''
  gradfrom=''
  courseval=''
  courseval1=''
  courseval2=''
  address=''
  permPSGC=''
  YearGraduated
  IdNumber=''

  preenrollment=false
  hspreenrollmentvar=false
  courses=[]

  idpicturevar="assets/noimage.jpg"
  idpicturevarsave=''

  birthcertvar="assets/noimage2.jpg"
  birthcertvarsave=''

  popvar="assets/noimage3.jpg"
  popsave=''

  reportCardVar="assets/noimage4.jpg"
  reportCardSave=''

  dSignvar="assets/noimage5.jpg"
  dSignsave=''

  ngOnInit() {

    // console.log(this.global.studinfo.yearOrGradeLevel, this.global.jhsGrade7Enrollment, this.global.shsGrade11Enrollment, this.global.sy.substring(0,6),this.global.enrollmentsy)
    if(this.global.preenrollmenteligible)
    {
      this.colpreenrollment()
      this.g12()
    }
    else if(this.global.preenrollmenteligibleHS)
    {
      this.global.colpreenrollmentyear=this.global.HSplacementyear
      this.hspreenrollmentvar=true
      this.hspreenrollment()
    }
    else if (this.global.hsfirstyear)
    {
      this.global.colpreenrollmentyear=this.global.HSplacementyear
      this.colpreenrollment()
    }
    else if (this.global.firstyear)
    {
      this.colpreenrollment()
    }
  }

  record=[
     {
      idPicture_Status:3,
      birthCert_Status:3,
      proofOfPayment_Status:3,
      reportCardStatus:3,
      signatureStatus:3,
      idPicture_Remarks:'',
      birthCert_Remarks:'',
      proofOfPayment_Remarks:'',
      reportCardRemarks:'',
      signatureRemarks:'',
    }]
 g12(){

   this.api.apiOnlineRegistrationCoursesWithStrand()
    .subscribe(res => {
      for (var i = 0; i < res.data.length; ++i) {
           if (res.data[i].programLevel=="50") {
             this.courses.push(res.data[i])
           }
         }
    });
 }

   onFileChangeg12(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        if(file.size<10000000){
          if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
            //this.filetype = file.type
            this.popvar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
            this.popsave = reader.result.toString().split(',')[1]
          }else{
            this.global.swalAlert("Invalid Image Type",'','warning');
          }
       }else{
          this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
       }
      };
    }
  }
 colgrade12preenrollment(){
  this.global.swalLoading('')
 }
 colpreenrollment(){

  this.global.swalLoading('')
    this.global.apiPlacement()
        .subscribe(res => {
           this.preference=res.data
           if (!this.hspreenrollmentvar) {
             this.strandval=this.preference.preferredCourseId
           }else{
             this.strandval=this.preference.shS_PriorityStrandId1
           }

          this.preference.examForSchoolYear = this.global.colpreenrollmentyear
          this.api.apiEncryptedString()
            .subscribe(res => {
              this.encryp=res.data
              this.api.apigetphpfileacctgapis(res.data)
                  .subscribe(res => {
                    this.preenrollment=res.data
                    this.proceedPreEnrollment(this.preenrollment)


                  },Error=>{
                      // window.history.back(); //commented out to continue the admission process 02/23/2023
                      this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
                      this.proceedPreEnrollment(this.preenrollment)

                  });
             },Error=>{
                window.history.back();
                this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
              });
      },Error=>{
          window.history.back();
          this.global.swalAlertError(Error);
        });
 }
proceedPreEnrollment(preenrollmentVar){
  if (this.global.hsfirstyear) {
    this.api.apiOnlineRegistrationApplicants(this.global.HSplacementyear,this.global.requestid())
     .subscribe(res => {
          // console.log(res.data)
           if (res.data.length==0) {
             if (this.preenrollment) {
               this.loaddata(1)
             }else{
               this.loaddata()
             }
           }else{
             if (res.data[0].paymentType==1) {
               if (res.data[0].paymentVerified==1||this.preenrollment) {
                 this.loaddata(1)
               }else{
                 this.loaddata()
               }
             }else{
               this.loaddata()
             }
           }
       },Error=>{
         this.global.swalAlertError(Error);
       });
   }else {
      if (this.preenrollment) {
         this.loaddata(1)
       }else{
         this.loaddata()
       }
   }
}
loaddata(x=null){

  this.api.apiPreEnrollmentSubmittedRequirement(this.global.colpreenrollmentyear)
    .subscribe(res => {
      this.global.swalClose()
      if(res.data.length === 0){
        this.idpicturevar="assets/noimage.jpg"
        this.birthcertvar="assets/noimage2.jpg"
        this.popvar="assets/noimage3.jpg"
        this.reportCardVar = "assets/noimage4.jpg"
        this.dSignvar = "assets/noimage5.jpg"
        this.idpicturevarsave=''
        this.birthcertvarsave=''
        this.popsave=''
        this.pdate = ''
        this.dSignsave = ''
        this.reportCardSave = ''
        if(x==1){
          this.record[0].proofOfPayment_Status=2
        }
      }else{
        this.record=res.data
        this.idpicturevar="data:image/png;base64,"+res.data[0].idPicture
        this.birthcertvar="data:image/png;base64,"+res.data[0].birthCert
        this.popvar="data:image/png;base64,"+res.data[0].proofOfPayment
        this.reportCardVar = "data:image/png;base64,"+res.data[0].reportCard
        this.dSignvar = "data:image/png;base64,"+res.data[0].signature
        this.idpicturevarsave=res.data[0].idPicture
        this.birthcertvarsave=res.data[0].birthCert
        this.popsave=res.data[0].proofOfPayment
        this.reportCardSave = res.data[0].reportCard
        this.dSignsave = res.data[0].signature
        this.pdate = res.data[0].dateOfPayment
        if(x==1&&res.data[0].proofOfPayment_Status!=2){
          var pdate
          pdate = new Date(this.pdate).toLocaleString();
           if(pdate=="Invalid Date"){
             pdate=''
           }
          this.api.apiPreEnrollmentSubmittedRequirementPut(
            this.idpicturevarsave,this.record[0].idPicture_Status,this.record[0].idPicture_Remarks,
            this.birthcertvarsave,this.record[0].birthCert_Status,this.record[0].birthCert_Remarks,
            this.reportCardSave,this.record[0].reportCardStatus,this.record[0].reportCardRemarks,
            this.dSignsave,this.record[0].signatureStatus,this.record[0].signatureRemarks,
            this.popsave,2,this.record[0].proofOfPayment_Remarks,pdate,1)
          .subscribe(res => {
              this.colpreenrollment()
          },Error=>{
             this.global.swalAlertError(Error)
            });
        }
      }
    },Error=>{
      window.history.back();
      this.global.swalAlertError(Error)
    });

}
pdate=''
SubmitReq(){
  var x=''
  if (this.idpicturevarsave=='') {
    x=x+"*ID Picture is required.<br>"
  }
  if (this.birthcertvarsave=='') {
    x=x+"*Birth Certificate is required.<br>"
  }
  if (this.reportCardSave=='' && this.global.currentplacementstatus!='JHS') {
    x=x+"*Report Card is required.<br>"
  }
  if (this.dSignsave=='' && this.global.currentplacementstatus!='JHS') {
    x=x+"*Digital Signature is required.<br>"
  }
  if (this.record[0].proofOfPayment_Status!=2) {
    if (this.popsave=='') {
      x=x+"*Proof of payment is required.<br>"
    }
    if (this.pdate=='') {
      x=x+"*Date of payment is required.<br>"
    }
  }
  if (x==='') {
    this.global.swalLoading('')
    var pdate
    pdate = new Date(this.pdate).toLocaleString();
   if(pdate=="Invalid Date"){
     pdate=null
   }
    if (this.record[0].idPicture_Status===3) {
              this.api.apiPreEnrollmentSubmittedRequirementPost(
                this.global.requestid(),
                this.global.colpreenrollmentyear,
                this.idpicturevarsave,
                this.birthcertvarsave,
                this.reportCardSave,
                this.dSignsave,
                this.popsave,
                pdate,1
                )
              .subscribe(res => {
                  this.global.swalSuccess(res.message)
                  this.colpreenrollment()
              },Error=>{
                 this.global.swalAlertError(Error)
                });
    }else{
      if (this.record[0].idPicture_Status!=2) {
        this.record[0].idPicture_Status=0
      }
      if (this.record[0].birthCert_Status!=2) {
        this.record[0].birthCert_Status=0
      }
      if (this.record[0].reportCardStatus!=2) {
        this.record[0].reportCardStatus=0
      }
      if (this.record[0].signatureStatus!=2) {
        this.record[0].signatureStatus=0
      }
      if (this.record[0].proofOfPayment_Status!=2) {
        this.record[0].proofOfPayment_Status=0
      }
      this.api.apiPreEnrollmentSubmittedRequirementPut(
        this.idpicturevarsave,this.record[0].idPicture_Status,this.record[0].idPicture_Remarks,
        this.birthcertvarsave,this.record[0].birthCert_Status,this.record[0].birthCert_Remarks,
        this.reportCardSave,this.record[0].reportCardStatus,this.record[0].reportCardRemarks,
        this.dSignsave,this.record[0].signatureStatus,this.record[0].signatureRemarks,
        this.popsave,this.record[0].proofOfPayment_Status,this.record[0].birthCert_Remarks,
        pdate,1
        )
        .subscribe(res => {
            this.global.swalSuccess(res.message)
            this.colpreenrollment()
        },Error=>{
           this.global.swalAlertError(Error)
          });
    }
  }else{
     this.global.swalAlert("Missing requirement:", x,"warning")
  }
}



 onFileChangeIDPicture(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if(file.size<10000000){
        if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
          //this.filetype = file.type
          this.idpicturevar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
          this.idpicturevarsave = reader.result.toString().split(',')[1]
        }else{
            this.global.swalAlert("Invalid Image Type",'','warning');
        }
       }else{
          this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
       }
      };
    }
}
onFileChangeDSignature(event) {
  let reader = new FileReader();
  if(event.target.files && event.target.files.length > 0) {
    let file = event.target.files[0];
    reader.readAsDataURL(file);
    reader.onload = () => {
      if(file.size<10000000){
      if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
        //this.filetype = file.type
        this.dSignvar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
        this.dSignsave = reader.result.toString().split(',')[1]
      }else{
          this.global.swalAlert("Invalid Image Type",'','warning');
      }
     }else{
        this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
     }
    };
  }
}

 onFileChangeBirthCert(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if(file.size<10000000){
        if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
          //this.filetype = file.type
          this.birthcertvar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
          this.birthcertvarsave = reader.result.toString().split(',')[1]
        }else{
            this.global.swalAlert("Invalid Image Type",'','warning');
        }
       }else{
          this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
       }
      };
    }
}

onFileChangeReportCard(event) {
  let reader = new FileReader();
  if(event.target.files && event.target.files.length > 0) {
    let file = event.target.files[0];
    reader.readAsDataURL(file);
    reader.onload = () => {
      if(file.size<10000000){
      if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
        //this.filetype = file.type
        this.reportCardVar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
        this.reportCardSave = reader.result.toString().split(',')[1]
      }else{
          this.global.swalAlert("Invalid Image Type",'','warning');
      }
     }else{
        this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
     }
    };
  }
}


 onFileChangePoP(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        if(file.size<10000000){
          if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
            //this.filetype = file.type
            this.popvar = "data:image/png;base64,"+reader.result.toString().split(',')[1]
            this.popsave = reader.result.toString().split(',')[1]
          }else{
            this.global.swalAlert("Invalid Image Type",'','warning');
          }
       }else{
          this.global.swalAlert("Image Resolution Too Large",'Provided image size is too large. Current maximum supported size is 10Mb. Please resize your image to upload it.','warning');
       }
      };
    }
}


  hspreenrollment(){
      if (this.global.userinfo!=undefined) {
          this.bdate=this.global.userinfo.dateOfBirth
          this.IdNumber=this.global.requestid()
          this.fname=this.global.userinfo.firstName
          this.mname=this.global.userinfo.middleName
          this.lname=this.global.userinfo.lastName
          this.suffix=this.global.userinfo.suffixName
          this.gender=this.global.userinfo.gender
          this.cnumber=this.global.userdemog.mobileNo
      }
      //this.sy=this.global.HSplacementyear
      this.cperson=''
      this.gradfrom="University of Saint Louis Tuguegarao"
      this.courseval=''
      this.courseval1=''
      this.courseval2=''
      this.address="Carig Sur, Tuguegarao City, Cagayan"
      this.permPSGC="021529053"
      this.YearGraduated=2021

      this.global.swalLoading('')
      this.api.apiPublicAPIStrands()
       .subscribe(res => {
         this.strand=res.data

         for (var i = 0; i < res.data.length; ++i) {
           if (res.data[i].strandCode=='ABM'||res.data[i].strandCode=='HUMSS'||res.data[i].strandCode=='STEM-NH'||res.data[i].strandCode=='STEM-H') {
             this.strandfiltered.push(res.data[i])
           }
         }
         this.strandfiltered=[]
         this.strandfiltered.push({strandId:'900009',strandTitle:'Accountancy, Business and Management Strand'})
         this.strandfiltered.push({strandId:'900011',strandTitle:'Humanities and Social Sciences Strand'})
         this.strandfiltered.push({strandId:'900013',strandTitle:'Science, Technology, Engineering and Mathematics Health Strand'})
         this.strandfiltered.push({strandId:'900010',strandTitle:'Science, Technology, Engineering and Mathematics-Non-Health Strand'})


          this.colpreenrollment()
        },Error=>{
         this.global.swalAlertError(Error)
        });
  }
 exist=false
 data=[]
 proofOfPayment
 encryp=''
 accept = true


 preenroll(){
   var x=''
    if (this.proglevelval=='05') {
    }
    if(x==''&&this.accept){
        this.accept = false
      this.swalConfirm("You are about to Pre-enroll.","Please confirm to proceed!",'question','Confirm',0);
    }else{
     this.global.swalAlert("Error Found:", x,"warning")
    }
 }

 preenrollcollegeinsider(){
    var y=''
    if (this.global.studinfo.yearOrGradeLevel==6) {
      if (this.strandval=='')
        y=y+"*Course is required!<br>"
    }

    // if (this.global.studinfo.yearOrGradeLevel==4) {
    //   // if (this.strandval=='')
    //   if (this.strandval==''||this.strandval== null||this.strandval==undefined)
    //     y=y+"*Strand Priority is required!<br>"
    // }
    if (!this.preenrollment&&this.record[0].proofOfPayment_Status!=2) {
      if (this.popsave=='') {
        y=y+"*Proof of Payment is required!<br>"
      }
      if (this.pdate=='') {
        y=y+"*Payment date is required!<br>"
      }
    }
    if (y=='') {
      this.swalConfirm("You are about to Pre-enroll.","Please confirm to proceed!",'question','Confirm',1);

    }else
     this.global.swalAlert("Error Found:", y,"warning")
 }
 preenrollJHSinsider(){

  var y=''
  if (this.idpicturevarsave=='') {
    y=y+"*ID Picture is required.<br>"
  }
  if (this.birthcertvarsave=='') {
    y=y+"*Birth Certificate is required.<br>"
  }
  if (!this.preenrollment&&this.record[0].proofOfPayment_Status!=2) {
    if (this.popsave=='') {
      y=y+"*Proof of Payment is required!<br>"
    }
    if (this.pdate=='') {
      y=y+"*Payment date is required!<br>"
    }
  }
  if (y=='') {
    this.swalConfirm("You are about to Pre-enroll.","Please confirm to proceed!",'question','Confirm',1);

  }else
   this.global.swalAlert("Error Found:", y,"warning")
}


 message=''
  preference:any=[]
  postSY = '';
  swalConfirm(title,text,type,button,ty)
  {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
                    this.global.swalLoading('')
                    if (this.global.studinfo.yearOrGradeLevel==6 && this.global.studinfo.departmentCode != "ELEM") { //setting up of schoolyear for incoming first year college from SHS department
                     this.postSY =this.global.colpreenrollmentyear
                    }
                    else if (this.global.studinfo.yearOrGradeLevel==4) { //setting up of schoolyear and preferred strand for incoming SHS from HS department
                     this.postSY =this.global.HSplacementyear
                     this.preference.shS_PriorityStrandId1 =  this.strandval
                    }
                    else if(this.global.studinfo.yearOrGradeLevel==6 && this.global.studinfo.departmentCode == "ELEM"){ //setting up of schoolyear for incoming g7 from elem department
                      this.postSY =this.global.HSplacementyear
                    }
                    else{ // settign up of schoolyear for incoming students - outsiders
                      if(this.global.currentplacementstatus == 'JHS' || this.global.currentplacementstatus == 'SHS')//basic ed
                        this.postSY =this.global.HSplacementyear
                      else{
                        this.postSY =this.global.colpreenrollmentyear//college
                      }
                    }

                    if(this.global.currentplacementstatus == 'JHS' || this.global.currentplacementstatus == 'SHS'){
                      this.preference.shS_PriorityStrandId1 =  this.strandval
                    }

                    // this.api.apiPlacementPut(
                    //     this.preference.examDate,
                    //     this.preference.preferredCourse,
                    //     this.preference.alternativeCourse,
                    //     0,
                    //     this.preference.vit,
                    //     this.preference.nvit,
                    //     this.preference.preferredCourseId,
                    //     this.preference.alternativeCourseId1,
                    //     this.preference.alternativeCourseId2,
                    //     this.preference.exemptionType,
                    //     this.preference.strand,
                    //     "P",
                    //     this.preference.gResult,
                    //     this.preference.testScheduleId,
                    //     this.preference.examRoom,
                    //     this.postSY,
                    //     this.preference.elem_ExamResult,
                    //     this.preference.elem_CourseId,
                    //     this.preference.elem_Course,
                    //     this.preference.jhS_ExamResult,
                    //     this.preference.jhS_CourseId,
                    //     this.preference.jhS_Course,
                    //     this.preference.shS_ExamResult,
                    //     this.preference.shS_PriorityStrandId1,
                    //     this.preference.shS_PriorityStrand1,
                    //     this.preference.shS_PriorityStrandId2,
                    //     this.preference.shS_PriorityStrand2)
                    //     .subscribe(res => {
                    //         if(this.record[0].idPicture_Status==3){
                    //            pdate = new Date(this.pdate).toLocaleString();
                    //            if(pdate=="Invalid Date"){
                    //              pdate=''
                    //            }
                    //            this.api.apiPreEnrollmentSubmittedRequirementPost(
                    //             this.global.requestid(),
                    //             this.postSY,
                    //             // this.global.colpreenrollmentyear,
                    //             this.idpicturevarsave,
                    //             this.birthcertvarsave,
                    //             this.reportCardSave,
                    //             this.dSignsave,
                    //             this.popsave,
                    //             pdate,
                    //             1)
                    //             .subscribe(res => {
                    //               if (this.record[0].proofOfPayment_Status!=2) {
                    //                 this.record[0].proofOfPayment_Status=0
                    //               }

                    //               //autoapproved disabled
                    //               // this.api.apiPreEnrollmentSubmittedRequirementPut(
                    //               //     this.idpicturevarsave,2,this.record[0].idPicture_Remarks,
                    //               //     this.birthcertvarsave,2,this.record[0].birthCert_Remarks,
                    //               //     this.reportCardSave,2,this.record[0].reportCardRemarks,
                    //               //     this.dSignsave,2,this.record[0].signatureRemarks,
                    //               //     this.popsave,0,this.record[0].proofOfPayment_Remarks,pdate,1)
                    //               //     .subscribe(res => {
                    //               //         this.colpreenrollment()
                    //               //     },Error=>{
                    //               //        this.global.swalAlertError(Error)
                    //               //       });
                    //               //   this.global.swalSuccess(res.message)
                    //                 this.colpreenrollment()
                    //             },Error=>{
                    //                this.global.swalAlertError(Error)
                    //               });
                    //         }else{
                    //           var pdate
                    //           pdate = new Date(this.pdate).toLocaleString();

                    //            if(pdate=="Invalid Date"){
                    //              pdate=''
                    //            }
                    //             if (this.record[0].proofOfPayment_Status!=2) {
                    //               this.record[0].proofOfPayment_Status=0
                    //             }
                    //             this.api.apiPreEnrollmentSubmittedRequirementPut(
                    //                   this.idpicturevarsave,2,this.record[0].idPicture_Remarks,
                    //                   this.birthcertvarsave,2,this.record[0].birthCert_Remarks,
                    //                   this.reportCardSave,2,this.record[0].reportCardRemarks,
                    //                   this.dSignsave,2,this.record[0].signatureRemarks,
                    //                   this.popsave,0,this.record[0].proofOfPayment_Remarks,pdate,1)
                    //               .subscribe(res => {
                    //                   this.colpreenrollment()
                    //               },Error=>{
                    //                  this.global.swalAlertError(Error)
                    //              });
                    //         }
                    //     });

                    var pdate = new Date(this.pdate).toLocaleString();
                    if(pdate=="Invalid Date"){
                      pdate=''
                    }
                    this.api.apiPreEnrollmentSubmittedRequirementPost(
                      this.global.requestid(),
                      this.postSY,
                      // this.global.colpreenrollmentyear,
                      this.idpicturevarsave,
                      this.birthcertvarsave,
                      this.reportCardSave,
                      this.dSignsave,
                      this.popsave,
                      pdate,
                      1)
                      .subscribe(res => {
                        if (this.record[0].proofOfPayment_Status!=2) {
                          this.record[0].proofOfPayment_Status=0
                        }

                        //autoapproved disabled
                        // this.api.apiPreEnrollmentSubmittedRequirementPut(
                        //     this.idpicturevarsave,2,this.record[0].idPicture_Remarks,
                        //     this.birthcertvarsave,2,this.record[0].birthCert_Remarks,
                        //     this.reportCardSave,2,this.record[0].reportCardRemarks,
                        //     this.dSignsave,2,this.record[0].signatureRemarks,
                        //     this.popsave,0,this.record[0].proofOfPayment_Remarks,pdate,1)
                        //     .subscribe(res => {
                        //         this.colpreenrollment()
                        //     },Error=>{
                        //        this.global.swalAlertError(Error)
                        //       });
                        //   this.global.swalSuccess(res.message)
                          this.colpreenrollment()
                      },Error=>{
                         this.global.swalAlertError(Error)
                        });
        }else{
              this.accept = true
        }
      })
  }
getnotnull(y){
    var x=y
    if (x==null) {
      x=''
    }
    return x
  }

getstrand(x){
	for (var i = 0; i < this.strandfiltered.length; ++i) {
		if(x==this.strandfiltered[i].strandId){
			return this.strandfiltered[i].strandTitle
		}
	}
}

 filetype=''
 attachment=''
 img=''

 onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (file.type.includes('jpg')||file.type.includes('png')||file.type.includes('JPG')||file.type.includes('PNG')||file.type.includes('jpeg')) {
          this.filetype = file.type
          this.attachment = "data:image/png;base64,"+reader.result.toString().split(',')[1]
          this.img = reader.result.toString().split(',')[1]
        }else{
            this.global.swalAlert("Invalid Image Type",'','warning');
        }
      };
    }
  }


opendits(x,y=null,rec=null){
    var wid='500px'
    if (x==2) {
     wid='600px'
    }
    if (y==null) {
        const dialogRef = this.dialog.open(PreEnrollmentModalComponent, {
          width: wid, disableClose: false,data:{
            data:this.data,
            pop:this.proofOfPayment,
            strandfiltered:this.strandfiltered,
            strandval:this.data[0].shS_PriorityStrandID1,
            strandval1:this.data[0].shS_PriorityStrandID2,
            type:x,
            preenrollment:this.preenrollment,
            pre:0,
          }
      });
          dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
          if (result.result=="updated") {
            }
          }
        });
      }else{
        const dialogRef = this.dialog.open(PreEnrollmentModalComponent, {
          width: '850px', disableClose: false,data:{
            pre:1,
            rec:rec,
          }
      });
          dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {
          if (result.result=="updated") {
            }
          }
        });
      }
    }


}

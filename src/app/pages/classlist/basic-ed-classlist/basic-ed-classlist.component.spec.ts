import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicEdClasslistComponent } from './basic-ed-classlist.component';

describe('BasicEdClasslistComponent', () => {
  let component: BasicEdClasslistComponent;
  let fixture: ComponentFixture<BasicEdClasslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicEdClasslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicEdClasslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Component,ViewChild, OnInit,ElementRef } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import {ExcelService} from './../services/sharedServices/excel.service';

@Component({
  selector: 'app-basic-ed-classlist',
  templateUrl: './basic-ed-classlist.component.html',
  styleUrls: ['./basic-ed-classlist.component.css']
})
export class BasicEdClasslistComponent implements OnInit {



	newCodeScheduleArr
	pic=[]

	noclasslist = false
	classlistArr

	selectedSection
  	selectedGradeLevel

	showContentHS = false;
	dataLoad = true;

	CNum = null;
	



  constructor(
  	public dialog: MatDialog,
  	private domSanitizer: DomSanitizer,
  	public global: GlobalService,
  	private http: Http,
  	private excelService:ExcelService){ }

  ngOnInit() {
  	
  }

  getHSClassList(){
    this.http.get(this.global.api+'EmployeePortal/Section/Students/'+this.global.hsSY+'/'+this.selectedSection,this.global.option)
    // this.http.get(this.global.api+'EmployeePortal/ClassList/'+this.CNum+'/2020211?instructorId=0903564',this.global.option)
        .map(response => response.json())
        .subscribe(res => {
            this.classlistArr=res.data;
            if(this.classlistArr.length>0){

              this.noclasslist = false;
              this.showContentHS = true;

              this.pic = []
              for (var i = 0; i < res.data.length; ++i) {
                this.http.get(this.global.api+'EmployeePortal/StudentInfo/'+res.data[i].idNumber+'/'+this.global.sy,this.global.option)
                // this.http.get(this.global.api+'EmployeePortal/StudentInfo/'+res.data[i].idNumber+'/2020211',this.global.option)
                .map(response => response.json())
                .subscribe(res => {
                    if(res.data.idPicture!=null){
                      this.pic.push({
                          picture:this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,'+res.data.idPicture),
                          idNumber: res.data.idNumber
                        }
                      );
                    }
                    else{
                      this.pic.push({
                          picture:null,
                          idNumber: res.data.idNumber
                        }
                      );
                    }
                    
                    this.global.swalClose();
                  },Error=>{
                          //console.log(Error);
                          this.global.swalAlertError();
                          //console.log(Error)
                });
              }

              
      }else{
              this.noclasslist = true;
              this.showContentHS = false;
            }
            
          this.global.swalClose();
          },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }

  clear(){
      this.showContentHS = false;
  }

  submit(param){
  	this.selectedSection = param;

    this.showContentHS= true;

    this.getHSClassList();
  }

getimage(x){
  		for (var i = 0; i < this.pic.length; ++i) {
  			if (this.pic[i].idNumber==x) {
          if(this.pic[i].picture != null)
  				  return this.pic[i].picture
  			}
  		}
      return 'assets/noimage.jpg'
  		
  }


  exportAsXLSXHS():void{
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      arr.push(
      {
        'ID NUMBER':this.classlistArr[i].idNumber,
        "STUDENT'S NAME":this.classlistArr[i].fullName,
        GENDER:this.classlistArr[i].gender,
        "LEARNING MODALITY":this.classlistArr[i].name,
        "ENROLMENT STATUS":this.classlistArr[i].enrollmentStatusDesc,
        
      }
        )
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Grade': this.selectedGradeLevel,
      'Section': this.selectedSection,
    })

    this.excelService.generateExcelHS(arr,CLData, this.global.empInfo.fullname);
  }

}

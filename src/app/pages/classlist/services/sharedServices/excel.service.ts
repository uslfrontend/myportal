import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';

// import * as ExcelJS from 'exceljs/dist/exceljs.min.js';

import * as ExcelJS from "exceljs/dist/exceljs"

import * as fs from 'file-saver';

//import {Row, Workbook, Worksheet} from 'exceljs';


declare const ExcelJS: any;

@Injectable()
export class ExcelService {

    constructor(private datePipe: DatePipe) {}

    async generateExcelHS(data,CLData,tchr) {

        const header = ['ID NUMBER', "STUDENT'S NAME", 'GENDER',"LEARNING MODALITY", 'ENROLMENT STATUS'];
        // Create workbook and worksheet
        var workbook = new ExcelJS.Workbook();

        const worksheet = workbook.addWorksheet();
        var FileName = "Official class list"

        let Grade = worksheet.addRow(['Grade:',CLData[0].selectedGradeLevel]);
        let Section = worksheet.addRow(['Section:',CLData[0].selectedSection]);

        Grade.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Section.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('B1').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B2').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.addRow(['']);
        let Instructor = worksheet.addRow(['Teacher:',tchr]);
        Instructor.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('B6').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.addRow(['']);

        const headerRow = worksheet.addRow(header);
        headerRow.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                },
                bgColor: {
                    argb: 'FFFFFFFF'
                },
            };
            cell.font = {
                color: {
                    argb: '00000000',
                },
                bold: true
            }
            cell.border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };
        });
        //console.table(data)


        data.forEach((element) => {
          let eachRow = [];
          header.forEach((headers) => {
            eachRow.push(element[headers])
          })
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
          } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
          }
        })
        // console.table(worksheet.columns)
        worksheet.getColumn(1).width = 20;
        worksheet.getColumn(2).width = 40;
        worksheet.getColumn(3).width = 10;
        worksheet.getColumn(4).width = 40;
        worksheet.getColumn(5).width = 40;
        workbook.xlsx.writeBuffer().then((data: any) => {
            const blob = new Blob([data], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            fs.saveAs(blob, FileName + '.xlsx');
        });
    }

    async generateExcel(data,CLData,tchr,type) {

        const header = ['ID NUMBER', "STUDENT'S NAME", 'GENDER', 'CRSE & YR',"LEARNING MODALITY"];
        // Create workbook and worksheet
        var workbook = new ExcelJS.Workbook();
        var FileName = '';
        const worksheet = workbook.addWorksheet();
        if(type == 'officiallyEnrolled')
          FileName = "Officially enrolled class list";
        else
          FileName = 'Withdrawn with Permission'

        let Code = worksheet.addRow(['Code:',CLData[0].Code]);
        let Subject = worksheet.addRow(['Subject ID:',CLData[0].SubjectID]);
        let Descriptive = worksheet.addRow(['Descriptive Title:',CLData[0].DescriptiveTitle]);
        let Lecture = worksheet.addRow(['Lecture Units:',CLData[0].LectureUnits]);
        let Laboratory = worksheet.addRow(['Laboratory Units:',CLData[0].LaboratorylabUnits]);

        Code.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Subject.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Descriptive.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Lecture.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Laboratory.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('B1').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B2').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B3').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B4').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B5').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.addRow(['']);
        let Instructor = worksheet.addRow(['Instructor:',tchr]);
        Instructor.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('B6').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.addRow(['']);

        const headerRow = worksheet.addRow(header);
        headerRow.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                },
                bgColor: {
                    argb: 'FFFFFFFF'
                },
            };
            cell.font = {
                color: {
                    argb: '00000000',
                },
                bold: true
            }
            cell.border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };
        });
        //console.table(data)


        data.forEach((element) => {
	      let eachRow = [];
	      header.forEach((headers) => {
	        eachRow.push(element[headers])
	      })
	      if (element.isDeleted === "Y") {
	        let deletedRow = worksheet.addRow(eachRow);
	        deletedRow.eachCell((cell, number) => {
	          cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
	        })
	      } else {
	        const row = worksheet.addRow(eachRow);
	        row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
	      }
	    })
        // console.table(worksheet.columns)
        worksheet.getColumn(1).width = 20;
        worksheet.getColumn(2).width = 40;
        worksheet.getColumn(3).width = 10;
        worksheet.getColumn(4).width = 40;
        worksheet.getColumn(5).width = 40;
        workbook.xlsx.writeBuffer().then((data: any) => {
            const blob = new Blob([data], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            fs.saveAs(blob, FileName + '.xlsx');
        });
    }

    async generateAdmittedExcel(data,CLData,tchr) {

        const header = ['ID NUMBER', "STUDENT'S NAME", 'GENDER', 'CRSE & YR',"LEARNING MODALITY"];
        // const header = ['ID NUMBER', "STUDENT'S NAME", 'CRSE & YR',"LEARNING MODALITY"];
        // Create workbook and worksheet
        var workbook = new ExcelJS.Workbook();

        const worksheet = workbook.addWorksheet();
        var FileName = "Admitted class list"

        let Code = worksheet.addRow(['Code:',CLData[0].Code]);
        let Subject = worksheet.addRow(['Subject ID:',CLData[0].SubjectID]);
        let Descriptive = worksheet.addRow(['Descriptive Title:',CLData[0].DescriptiveTitle]);
        let Lecture = worksheet.addRow(['Lecture Units:',CLData[0].LectureUnits]);
        let Laboratory = worksheet.addRow(['Laboratory Units:',CLData[0].LaboratorylabUnits]);

        Code.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Subject.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Descriptive.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Lecture.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Laboratory.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('B1').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B2').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B3').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B4').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B5').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.addRow(['']);
        let Instructor = worksheet.addRow(['Instructor:',tchr]);
        Instructor.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('B6').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.addRow(['']);


        const headerRow = worksheet.addRow(header);
        headerRow.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                },
                bgColor: {
                    argb: 'FFFFFFFF'
                },
            };
            cell.font = {
                color: {
                    argb: '00000000',
                },
                bold: true
            }
            cell.border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };
        });
        //console.table(data)


        data.forEach((element) => {
          let eachRow = [];
          header.forEach((headers) => {
            eachRow.push(element[headers])
          })
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
          } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
          }
        })
        // console.table(worksheet.columns)
        worksheet.getColumn(1).width = 20;
        worksheet.getColumn(2).width = 40;
        worksheet.getColumn(3).width = 10;
        worksheet.getColumn(4).width = 40;
        worksheet.getColumn(5).width = 40;
        workbook.xlsx.writeBuffer().then((data: any) => {
            const blob = new Blob([data], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            fs.saveAs(blob, FileName + '.xlsx');
        });
    }

    async generateAllExcel(OfficiallyStuds,AdmittedStuds,CLData,tchr) {

        const header = ['ID NUMBER', "STUDENT'S NAME", 'GENDER', 'CRSE & YR',"LEARNING MODALITY"];
        // const header = ['ID NUMBER', "STUDENT'S NAME", 'CRSE & YR',"LEARNING MODALITY"];
        // Create workbook and worksheet
        var workbook = new ExcelJS.Workbook();

        const worksheet = workbook.addWorksheet();
        var FileName = "Admitted and officially enrolled class list"
        await worksheet.protect('123');
        let Code = worksheet.addRow(['Code:',CLData[0].Code]);
        let Subject = worksheet.addRow(['Subject ID:',CLData[0].SubjectID]);
        let Descriptive = worksheet.addRow(['Descriptive Title:',CLData[0].DescriptiveTitle]);
        let Lecture = worksheet.addRow(['Lecture Units:',CLData[0].LectureUnits]);
        let Laboratory = worksheet.addRow(['Laboratory Units:',CLData[0].LaboratorylabUnits]);

        Code.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Subject.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Descriptive.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Lecture.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        Laboratory.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('B1').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B2').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B3').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B4').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('B5').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.getCell('A1').protection = {
          locked: true
        };




        worksheet.addRow(['']);
        let Instructor = worksheet.addRow(['Instructor:',tchr]);
        Instructor.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('B6').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };

        worksheet.addRow(['']);
        let Title1 = worksheet.addRow(['OFFICIALLY ENROLLED STUDENTS']);
        Title1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        const headerRow = worksheet.addRow(header);
        headerRow.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                },
                bgColor: {
                    argb: 'FFFFFFFF'
                },
            };
            cell.font = {
                color: {
                    argb: '00000000',
                },
                bold: true
            }
            cell.border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };
        });
        //console.table(data)


        OfficiallyStuds.forEach((element) => {
          let eachRow = [];
          header.forEach((headers) => {
            eachRow.push(element[headers])
          })
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };
            })
          } else {

            const row = worksheet.addRow(eachRow);
            console.log(eachRow)
            console.log(row._cells[0].protection)
            row._cells[0].protection ={
            locked: false};

            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
          }
        })


        let titleRow1 = worksheet.addRow(['']);
        let Title = worksheet.addRow(['ADMITTED STUDENTS']);
        Title.font = { name: 'Calibri', family: 4, size: 11,bold: true, strike: false };

        const headerRow1 = worksheet.addRow(header);
        headerRow1.eachCell((cell, number) => {
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                },
                bgColor: {
                    argb: 'FFFFFFFF'
                },
            };
            cell.font = {
                color: {
                    argb: '00000000',
                },
                bold: true
            }
            cell.border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };
        });


        AdmittedStuds.forEach((element) => {
          let eachRow = [];
          header.forEach((headers) => {
            eachRow.push(element[headers])
          })
          if (element.isDeleted === "Y") {
            let deletedRow = worksheet.addRow(eachRow);
            deletedRow.eachCell((cell, number) => {
              cell.font = { name: 'Calibri', family: 4, size: 11, bold: false, strike: true };

            })
          } else {
            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }
            row.eachCell((cell, number) => {
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
          }
        })

        // console.table(worksheet.columns)
        worksheet.getColumn(1).width = 20;
        worksheet.getColumn(2).width = 40;
        worksheet.getColumn(3).width = 10;
        worksheet.getColumn(4).width = 40;
        worksheet.getColumn(5).width = 40;

        workbook.xlsx.writeBuffer().then((AdmittedStuds: any) => {
            const blob = new Blob([AdmittedStuds], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            fs.saveAs(blob, FileName + '.xlsx');
        });
    }




async generateExcelOGS(data,CLData,tchr) {

        const header = ['No.','ID Number', "Full name",'Blank', 'Gender',"Grade", 'Remark'];
        // Create workbook and worksheet
        var workbook = new ExcelJS.Workbook();

        const worksheet = workbook.addWorksheet();
        var FileName = CLData[0].sy+" Code "+CLData[0].Code
        await worksheet.protect('CiCT#2020');
        let h1 = worksheet.addRow(['University of Saint Louis']);
        h1.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.mergeCells('A1:G1');
        h1.alignment = { vertical: 'top', horizontal: 'center' };

        let h2 = worksheet.addRow(['Tuguegarao City, Cagayan']);
        h2.font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.mergeCells('A2:G2');
        h2.alignment = { vertical: 'top', horizontal: 'center' };


        worksheet.getCell('A3').value = "FINAL GRADE"
        worksheet.mergeCells('A3:G3');
        worksheet.getCell('A3:G3').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('A3:G3').alignment = { vertical: 'top', horizontal: 'center' };

        worksheet.getCell('A4').value = "Upload Final Grade Template"
        worksheet.mergeCells('A4:G4');
        worksheet.getCell('A4:G4').font = { name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('A4:G4').alignment = { vertical: 'top', horizontal: 'center' };

        worksheet.mergeCells('A5:B5');
        worksheet.getCell('A5').value = "School Year"
        worksheet.getCell('A5').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.mergeCells('C5:D5');
        worksheet.getCell('C5').value = CLData[0].sy
        worksheet.getCell('C5').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('C5:D5').border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };
        worksheet.getCell('F5').value = "Code"
        worksheet.getCell('F5').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('G5').value = CLData[0].Code
        worksheet.getCell('G5').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('G5').border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };

        worksheet.mergeCells('A6:B6');
        worksheet.getCell('A6').value = "Subject ID"
        worksheet.getCell('A6').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.mergeCells('C6:D6');
        worksheet.getCell('C6').value =  CLData[0].SubjectID
        worksheet.getCell('C6').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('C6:D6').border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };

        worksheet.getCell('F6').value = "Lec. Units"
        worksheet.getCell('F6').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('G6').value = CLData[0].LectureUnits
        worksheet.getCell('G6').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('G6').border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };

        worksheet.mergeCells('A7:B7');
        worksheet.getCell('A7').value = "Description"
        worksheet.getCell('A7').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('F7').value = "Lab. Units"
        worksheet.getCell('F7').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };

        worksheet.getCell('G7').value = CLData[0].labUnits
        worksheet.getCell('G7').font = {name: 'Calibri', family: 4, size: 11, bold: true, strike: false };
        worksheet.getCell('G7').border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };



        worksheet.getCell('A8').value = CLData[0].DescriptiveTitle
        worksheet.getCell('A8').outlineLevel = 2;
        worksheet.mergeCells('A8:G9');
        worksheet.getCell('A8:G9').font = {name: 'Calibri', family: 4, size: 11, bold: false, strike: false };
        worksheet.getCell('A8:G9').alignment = { vertical: 'top', horizontal: 'left' };
        worksheet.getCell('A8:G9').border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };


        let h5= worksheet.addRow(['*Grade Legend: D is Dropped; N is NFE; WP is for Withdrawn with Permission']);
        h5.font = { name: 'Calibri', family: 4, size: 8, bold: false, strike: false };
        worksheet.mergeCells('A10:G10');
        h5.alignment = { vertical: 'top', horizontal: 'left' };
        const headerRow = worksheet.addRow(header);

        var temp1=''
        headerRow.eachCell((cell) => {

                cell.alignment = { vertical: 'top', horizontal: 'center' };
            if (cell._address.includes('C')) {
              temp1=cell._address
            }else if(cell._address.includes('D')){
              worksheet.mergeCells(temp1+":"+cell._address);
              temp1=''
            }
            cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                },
                bgColor: {
                    argb: 'FFFFFFFF'
                },
            };
            cell.font = {
                color: {
                    argb: '00000000',
                },
                bold: true
            }
            cell.border = {
                top: {
                    style: 'thin'
                },
                left: {
                    style: 'thin'
                },
                bottom: {
                    style: 'thin'
                },
                right: {
                    style: 'thin'
                }
            };
        });
        data.forEach((element) => {
          let eachRow = [];
          header.forEach((headers) => {
            eachRow.push(element[headers])
          })

            const row = worksheet.addRow(eachRow);
            row.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: {
                    argb: 'FFFFFFFF'
                }
            };
            row.font = {
                color: {
                    argb: '00000000',
                },
                bold: false
            }

            row.eachCell((cell, number) => {
                if (cell._address.includes('F'))
                {
                    cell.protection = {
                      locked: false,
                    };
                }
                if (cell._address.includes('G')) {
                    let x= cell._address.replace( /^\D+/g, '')
                   cell.value = {
                      formula: 'IF(F'+x+'=70,"Failed", IF(AND(F'+x+'>=75,F'+x+'<=100),"Passed", IF(F'+x+'="D","Dropped",IF(F'+x+'="N","No Final Exam",IF(F'+x+'="WP","Widrawn with Permission",IF(F'+x+'="",""))))))',
                    };
                }
            if (cell._address.includes('B')||cell._address.includes('E')||cell._address.includes('F')) {
                cell.alignment = { vertical: 'top', horizontal: 'center' };
            }
                if (cell._address.includes('C')) {
                  temp1=cell._address
                }else if(cell._address.includes('D')){
                  worksheet.mergeCells(temp1+":"+cell._address);
                  temp1=''
                }
                cell.border = {
                    top: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    }
                };
            });
        })
        // console.table(worksheet.columns)
        worksheet.getColumn(1).width = 5;
        worksheet.getColumn(2).width = 12;
        worksheet.getColumn(3).width = 30;
        worksheet.getColumn(4).width = 15;
        worksheet.getColumn(5).width =7.5;
        worksheet.getColumn(6).width =8;
        worksheet.getColumn(7).width =15;
        workbook.xlsx.writeBuffer().then((data: any) => {
            const blob = new Blob([data], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            fs.saveAs(blob, FileName + '.xlsx');
        });
    }
}

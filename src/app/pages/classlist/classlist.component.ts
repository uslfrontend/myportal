import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Component,ViewChild, OnInit,ElementRef } from '@angular/core';
import { Inject,EventEmitter } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { ViewEncapsulation } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { debounceTime, tap, switchMap, finalize } from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import {ExcelService} from './services/sharedServices/excel.service';

import { AdmittedDialogComponent } from './admitted-dialog/admitted-dialog.component';


@Component({
  selector: 'app-classlist',
  templateUrl: './classlist.component.html',
  styleUrls: ['./classlist.component.css']
})
export class ClasslistComponent implements OnInit {

  codeNo = '';

  subjectID;
  descTitle;
  lecUnits;
  labUnits;
  instructor;

  officialEnrollees;
  admittedStuds;
  wpStuds;


  // day
  // time
  // room
  // instructor

  showContentCollege = false;
  dataLoad = true;
  codesummaryArr
  classlistArr
  admittedstudsArr
  wpStudsArr=[];

  CNum = '';
  lastTen

  newCodeListArr

  employeeCtrl='';
  noclasslist = false;


  pic=[]

  newCodeScheduleArr

  constructor(public dialog: MatDialog,private domSanitizer: DomSanitizer,public global: GlobalService,private http: Http,private excelService:ExcelService) { }

  ngOnInit() {
    //console.log(this.global.sy)
    // console.table(this.global.codeListArr)
    this.getMyCodeList();
  }


  presentCodeNum = ''
  lastNewCodeNum = '';

  presentDay = ''
  lastNewDay = '';

  presentTime = ''
  lastNewTime = '';

  // test(){
  //   this.excelService.readExcel();
  // }


  removeDuplicates(){
   //  console.log("Code List")
    // console.table(this.codeListArr)
    //  var Newarray = []

    // for (var i = 0; i < this.codeListArr.length; ++i) {
   //    this.presentCodeNum = this.codeListArr[i].codeNo
   //    this.presentDay = this.codeListArr[i].day
    //   this.presentTime = this.codeListArr[i].time



   //    console.log('Code Number: '+this.lastNewCodeNum +'***'+ this.presentCodeNum)
   //    console.log('Day: '+this.lastNewDay +'***'+ this.presentDay)
   //    console.log('Time: '+this.lastNewTime +'***'+ this.presentTime)


     // if((this.lastNewCodeNum != this.presentCodeNum)&&(this.lastNewDay != this.presentDay)&&(this.lastNewTime != this.presentTime)){
     //   Newarray.push(this.codeListArr[i])
     //   this.lastNewCodeNum = this.codeListArr[i].codeNo
   //    this.lastNewDay = this.codeListArr[i].day
   //    this.lastNewTime = this.codeListArr[i].time
     // }
    // }

    // this.newCodeListArr = Newarray
      const result = Array.from(
        this.global.codeListArr.reduce(
          (m, t) => m.set(t.day&&t.codeNo, t),
          new Map()
          ).values());

      this.newCodeListArr = result
  }




  presentSchedCodeNum = ''
  lastNewSchedCodeNum = '';

  presentSchedDay = ''
  lastNewSchedDay = '';

  removeSchedDuplicates(){
    // console.log("Code Summary")
    // console.table(this.codesummaryArr)
    // var NewSchedarray = []
    // for (var i = 0; i < this.codesummaryArr.length; ++i) {
    //   this.presentSchedCodeNum = this.codesummaryArr[i].codeNo
    //   this.presentSchedDay = this.codesummaryArr[i].day

    //  if((this.lastNewSchedCodeNum != this.presentSchedCodeNum)&&(this.lastNewSchedDay != this.presentSchedDay)){
    //    NewSchedarray.push(this.codesummaryArr[i])
    //    // console.table(this.codesummaryArr[i].codeNo)
    //    // console.table(this.codesummaryArr[i].day)
    //    this.lastNewSchedCodeNum = this.codesummaryArr[i].codeNo
    //    this.lastNewSchedDay = this.codesummaryArr[i].day
    //  }
    // }

    // this.newCodeScheduleArr = NewSchedarray

    // const result = Array.from(
    //     this.codesummaryArr.reduce(
    //       (m, t) => m.set(t.day&&t.codeNo&&t.time, t),
    //       new Map()
    //       ).values());
    //   console.log(result)
      var result=[]
      for (var i = 0; i < this.codesummaryArr.length; ++i) {
        if (result.length==0) {
         result.push(this.codesummaryArr[i])
        }else {
          var nodups=true
          for (var i2 = 0; i2 < result.length; ++i2) {
            if (this.codesummaryArr[i].day==result[i2].day&&this.codesummaryArr[i].time==result[i2].time){
              nodups=false
              break
            }
          }
          if (nodups) {
             result.push(this.codesummaryArr[i])
          }
        }
      }


      this.newCodeScheduleArr = result

  }


  keyDownFunction(event) {

    if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus')
    {

      if (this.CNum != '')
      {

        this.codesummaryArr = '';
        this.getCodeSummary();
        this.getClassList();
      }
      else
      {
        this.showContentCollege = false;
      }
    // code...
    }
  }
  submit(){
      this.keyDownFunction('onoutfocus');
  }



  getMyCodeList(){
      // this.global.swalLoading('')
      /*this.http.get(this.global.api+'ReportTeacher/CodeSummary/'+this.global.sy+'?instructorId='+this.global.requestid(),this.global.option)
      // this.http.get(this.global.api+'ReportTeacher/CodeSummary/2020211'+'?instructorId=0903564',this.global.option)
          .map(response => response.json())
          .subscribe(res => {
              this.codeListArr=res.data;
              if(this.codeListArr.length>0)
              {
                this.noclasslist = false;
                this.removeDuplicates();
              }
              else
              {
                this.noclasslist = true;
              }
              this.global.swalClose();
            },Error=>{
                    //console.log(Error);
                    this.global.swalAlertError();
                    //console.log(Error)
          });*/

          // if(this.global.noclasslist == false)
            this.removeDuplicates();

  }

  getCodeSummary(){
    this.global.swalLoading('')
    this.http.get(this.global.api+'EmployeePortal/CodeSummary/'+this.global.sy+'?codeNo='+this.CNum,this.global.option)
    // this.http.get(this.global.api+'ReportTeacher/CodeSummary/2020211?codeNo='+this.CNum+'&instructorId=0903564',this.global.option)
        .map(response => response.json())
        .subscribe(res => {
            this.codesummaryArr=res.data;
            // console.log(this.codesummaryArr.length)
            // console.log(this.codesummaryArr)
            if(this.codesummaryArr.length>0){
              this.removeSchedDuplicates();
              this.noclasslist = false;
              this.showContentCollege = true;
              this.subjectID = res.data[0].subjectId;
              this.descTitle = res.data[0].subjectTitle;
              this.lecUnits = res.data[0].lecUnits;
              this.labUnits = res.data[0].labUnits;
              this.codeNo = res.data[0].codeNo
              this.officialEnrollees = res.data[0].oe;
              this.instructor=res.data[0].instructor;
            }else{
              this.noclasslist = true;
              this.showContentCollege = false;

            }

            this.global.swalClose();
          },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }



  getClassList(){

    this.global.swalLoading('')
    this.getAdmitted(this.CNum);
    //console.log(this.global.requestid())
    this.http.get(this.global.api+'EmployeePortal/ClassList/'+this.CNum+'/'+this.global.sy+'?instructorId='+this.global.requestid(),this.global.option)
    // this.http.get(this.global.api+'EmployeePortal/ClassList/'+this.CNum+'/2020211?instructorId=0903564',this.global.option)
        .map(response => response.json())
        .subscribe(res => {
            this.classlistArr=res.data;
            //console.log(res.data.length);
             //get Withdrawm-With-Permission count and Array
             if(res.data.length > 0)
             {
               this.wpStuds=0;
              for(var i = 0; i < res.data.length; i++)
              {
                 if(res.data[i].enrollmentStatusDesc=='Withdrawn with Permission')
                 {
                   this.wpStuds=this.wpStuds+1;
                   this.wpStudsArr.push(res.data[i]);
                 }
              }
             }

            if(this.classlistArr.length>0){

              // this.noclasslist = false;

              // this.showContentCollege = true;

              this.pic = []
              for (var i = 0; i < res.data.length; ++i) {
                this.http.get(this.global.api+'EmployeePortal/StudentInfo/'+res.data[i].idNumber+'/'+this.global.sy,this.global.option)
                // this.http.get(this.global.api+'EmployeePortal/StudentInfo/'+res.data[i].idNumber+'/2020211',this.global.option)
                .map(response => response.json())
                .subscribe(res => {
                    if(res.data.idPicture!=null){
                      this.pic.push({
                          picture:this.domSanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,'+res.data.idPicture),
                          idNumber: res.data.idNumber
                        }
                      );
                    }
                    else{
                      this.pic.push({
                          picture:null,
                          idNumber: res.data.idNumber
                        }
                      );
                    }

				          	this.global.swalClose();
				          },Error=>{
				                  //console.log(Error);
				                  this.global.swalAlertError();
				                  //console.log(Error)
				        });
            	}


			}else{
            	// this.noclasslist = true;

              // this.showContentCollege = false;
            }

          this.global.swalClose();
          },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }

  getAdmitted(code){
    this.http.get(this.global.api+'EmployeePortal/AdmittedInCode/'+this.CNum+'/'+this.global.sy,this.global.option)
    // this.http.get(this.global.api+'EmployeePortal/AdmittedInCode/111/2020211',this.global.option)
        .map(response => response.json())
        .subscribe(res => {
            this.admittedstudsArr=res.data;
            this.admittedStuds = this.admittedstudsArr.length;

            this.global.swalClose();
          },Error=>{
                  //console.log(Error);
                  this.global.swalAlertError();
                  //console.log(Error)
        });
  }

  openAdmittedDialog(listToDisplay): void {
    if(listToDisplay=='admitted'){
        const dialogRef = this.dialog.open(AdmittedDialogComponent, {
          width: '900px', disableClose: false,data:{selectedData:this.admittedstudsArr, listToDisplay:listToDisplay}
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result!=undefined) {

          }
        });
    }
    if(listToDisplay=='wp'){
      const dialogRef = this.dialog.open(AdmittedDialogComponent, {
        width: '900px', disableClose: false,data:{selectedData:this.wpStudsArr, listToDisplay:listToDisplay}
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result!=undefined) {

        }
      });
    }
  }

  ctr = 0;
  getLast10(rank): boolean{

    /*if(this.ctr==this.officialEnrollees){

      this.dataLoad = false;
    }else{
      this.ctr+=1;
      this.dataLoad = false;
    }*/
    this.lastTen = this.officialEnrollees-9;
    //console.log('Start of Last 10: '+this.lastTen)

    if(rank>=this.lastTen)
      return true
    else
      return false
  }

  getimage(x){
      for (var i = 0; i < this.pic.length; ++i) {
        if (this.pic[i].idNumber==x) {
          if(this.pic[i].picture != null)
            return this.pic[i].picture
        }
      }
      return 'assets/noimage.jpg'

  }



  clear(){
      this.showContentCollege = false;
  }


  /* exportAsXLSX():void {
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      arr.push(
      {
        'ID NUMBER':this.classlistArr[i].idNumber,
        "STUDENT'S NAME":this.classlistArr[i].fullName,
        GENDER:this.classlistArr[i].gender,
        'CRSE & YR':this.classlistArr[i].courseYr,
        "LEARNING MODALITY":this.classlistArr[i].preferredLearningModality,

      }
        )
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })
   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateExcel(arr,CLData, this.newCodeScheduleArr[0].instructor);
  }

  */

  exportAsXLSX():void {
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      if(this.classlistArr[i].enrollmentStatusDesc!='Withdrawn with Permission'){
        arr.push(
          {
            'ID NUMBER':this.classlistArr[i].idNumber,
            "STUDENT'S NAME":this.classlistArr[i].fullName,
            GENDER:this.classlistArr[i].gender,
            'CRSE & YR':this.classlistArr[i].courseYr,
            "LEARNING MODALITY":this.classlistArr[i].preferredLearningModality,

          }
        )
      }
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })
   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateExcel(arr,CLData, this.codesummaryArr[0].instructor,'officiallyEnrolled');
  }

  exportWPAsXLSX():void {
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      if(this.classlistArr[i].enrollmentStatusDesc=='Withdrawn with Permission'){
          arr.push(
            {
              'ID NUMBER':this.classlistArr[i].idNumber,
              "STUDENT'S NAME":this.classlistArr[i].fullName,
              GENDER:this.classlistArr[i].gender,
              'CRSE & YR':this.classlistArr[i].courseYr,
              "LEARNING MODALITY":this.classlistArr[i].preferredLearningModality,

            }
          )
        }
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })
   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateExcel(arr,CLData, this.codesummaryArr[0].instructor,'WP');
  }

  exportAdmittedAsXLSX():void{
    var arr = []
    for (var i = 0; i < this.admittedstudsArr.length; ++i) {
      arr.push(
      {
        'ID NUMBER':this.admittedstudsArr[i].idNumber,
        "STUDENT'S NAME":this.admittedstudsArr[i].fullName,
        GENDER:this.admittedstudsArr[i].gender,
        'CRSE & YR':this.admittedstudsArr[i].course+' '+this.admittedstudsArr[i].yearOrGradeLevel,
        "LEARNING MODALITY":this.admittedstudsArr[i].preferredLearningModality,
      }
        )
    }
    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })
   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateAdmittedExcel(arr,CLData, this.newCodeScheduleArr[0].instructor);
  }

  exportAllAsXLSX():void{
    var arr = []
    for (var i = 0; i < this.classlistArr.length; ++i) {
      if(this.classlistArr[i].enrollmentStatusDesc!='Withdrawn with Permission'){
        arr.push(
          {
            'ID NUMBER':this.classlistArr[i].idNumber,
            "STUDENT'S NAME":this.classlistArr[i].fullName,
            GENDER:this.classlistArr[i].gender,
            'CRSE & YR':this.classlistArr[i].courseYr,
            "LEARNING MODALITY":this.classlistArr[i].preferredLearningModality,
          }
        )
      }
    }

    var arr2 = []
    for (var i = 0; i < this.admittedstudsArr.length; ++i) {
      arr2.push(
      {
        'ID NUMBER':this.admittedstudsArr[i].idNumber,
        "STUDENT'S NAME":this.admittedstudsArr[i].fullName,
        GENDER:this.admittedstudsArr[i].gender,
        'CRSE & YR':this.admittedstudsArr[i].course+' '+this.admittedstudsArr[i].yearOrGradeLevel,
        "LEARNING MODALITY":this.admittedstudsArr[i].preferredLearningModality,
      }
        )
    }

    var CLData = [];
    // console.log(this.codeNo)
    CLData.push({
      'Code': this.codeNo,
      'SubjectID': this.subjectID,
      'DescriptiveTitle': this.descTitle,
      'LectureUnits': this.lecUnits,
      'LaboratorylabUnits': this.labUnits,
    })

   //this.excelService.exportAsExcelFile(arr, 'Class List');
   this.excelService.generateAllExcel(arr,arr2,CLData, this.newCodeScheduleArr[0].instructor);
  }



}

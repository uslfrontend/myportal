import { Component, OnInit } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../../global.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

import * as jsPDF from 'jspdf';
import * as html2pdf from 'html2pdf.js';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-egs-print',
  templateUrl: './egs-print.component.html',
  styleUrls: ['./egs-print.component.scss']
})
export class EgsPrintComponent implements OnInit {
codesummaryArr

ogsid=0
ogsstatus =''
classlistArr
sy=''
academicdean=''
CNum=''
constructor(public dialogRef: MatDialogRef<EgsPrintComponent>,@Inject(MAT_DIALOG_DATA) public data: any,private http: Http,public global: GlobalService) {

}

  ngOnInit() {
  	this.codesummaryArr=this.data.codesummaryArr
  	this.ogsid=this.data.ogsid
  	this.ogsstatus=this.data.ogsstatus
  	this.classlistArr = this.data.classlistArr
  	this.sy = this.data.sy
  	this.academicdean = this.data.academicdean
  	this.CNum = this.data.CNum

  }
 getpercentage(y,temp){
   var x=0;
   for (var i = 0; i < this.classlistArr.length; ++i) {
     if(this.classlistArr[i].remark==temp){
       x++;
     }
   }
   if (y==0) {
     return x
   }
   var rets 
   rets = (x/this.classlistArr.length)  * 100
   rets=Math.round(rets * 10) / 10
   return rets
 }
  onNoClickclose(): void {
       this.dialogRef.close({result:'cancel'});
  }
  

  egsprint(): void {
  	
    const content:Element = document.getElementById('ContenttoExport');
    const options = {
    	margin: 2,
    	filename: 'EGS.pdf',
    	html2canvas: {},
    	image: {type:'jpeg'},
    	jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'},
    	//pagebreak: { mode: 'avoid-all', before: '#page2el' },
    	// pagebreak: { mode: ['avoid-all', 'css', 'legacy'] },
    	pagebreak: { before: '.beforeClass', after: ['#after1', '#after2'], avoid: 'img' },
    	pdfCallback: pdfCallback
    };

    function pdfCallback(pdfObject) {
    	//console.log("dto ako")
	    var number_of_pages = pdfObject.internal.getNumberOfPages()
	    var pdf_pages = pdfObject.internal.pages
	    var myFooter = " "
	    for (var i = 1; i < pdf_pages.length; i++) {
	        // We are telling our pdfObject that we are now working on this page
	        pdfObject.setPage(i)
	        //pdfObject.setFontSize(10);
	        //pdfObject.setTextColor(128,128,128)
	        //pdfObject.text("University of Saint Louis Tuguegarao", 10, 5)
	        pdfObject.setFontSize(10);
	        pdfObject.text(myFooter, 10, 292);
	        pdfObject.text("Page "+i,182,292);
	    }
	  }

      html2pdf().from(content).set(options);
      html2pdf().from(content).toPdf().get('pdf').then(function (options) {
      	// pdfObj has your jsPDF object in it, use it as you please!
      	pdfCallback(options);
  	    window.open(options.output('bloburl'), '_blank');
  	});
  }

}

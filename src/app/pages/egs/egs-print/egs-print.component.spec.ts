import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EgsPrintComponent } from './egs-print.component';

describe('EgsPrintComponent', () => {
  let component: EgsPrintComponent;
  let fixture: ComponentFixture<EgsPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EgsPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EgsPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

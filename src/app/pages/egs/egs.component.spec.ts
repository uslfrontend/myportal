import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EGSComponent } from './egs.component';

describe('EGSComponent', () => {
  let component: EGSComponent;
  let fixture: ComponentFixture<EGSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EGSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EGSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

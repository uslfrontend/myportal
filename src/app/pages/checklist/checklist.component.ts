import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import Swal from 'sweetalert2';
import { RemainingComponent } from './remaining/remaining.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
const swal = Swal;
import { StudentApiService } from './../../student-api.service';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.scss']
})
export class ChecklistComponent implements OnInit {
data=undefined
	progid=''
  progid2=''
	tabledata=[]
	tableArr=[]
	id=''
  stud
  status=[]

  lname=''
  mname=''
  fname=''
  suffix=''
  course=''
  yearOrGradeLevel=''
  image:any = 'assets/noimage.jpg';
  constructor(public api:StudentApiService,public global: GlobalService,public dialog: MatDialog) { }

      ngOnInit() {
          if (this.global.userinfo!=undefined) {
    		    this.stud=[]
    		    this.notcredited=[]
    		    this.credited=[]
        	  this.data = this.global.studinfo
        	  this.stud = this.global.studinfo
        	  this.id = this.global.userinfo.idNumber
    	      this.fname = this.global.studinfo.firstName
    	      this.mname = this.global.studinfo.middleName
    	      this.lname = this.global.studinfo.lastName
    	      this.suffix = this.global.studinfo.suffixName
    	      this.course = this.global.studinfo.course
    	      this.yearOrGradeLevel = this.global.studinfo.yearOrGradeLevel.toString();
    	      this.progid = this.global.studinfo.programID
            this.keyDownFunction('onoutfocus',1)
          }
      }

  showcred=false
  textcred='Show Remaining Subject(s)'
  invertcred(){
    if (this.showcred==true){
      this.showcred=false
      this.textcred='Show Remaining Subject(s)'
    }
    else{
      this.textcred='Hide Remaining Subject(s)'
      this.showcred=true
    }
  }

  checklistexist=false
  grades=[]
  acadhist=[]
  temporaryconter=0
  keyDownFunction(event,xedia){
  	if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
  	  this.data=undefined
      this.tabledata=undefined
      this.tableArr=undefined
      this.progid2 = this.progid
      this.api.apiStudentPortalCurriculum(this.progid)
          .subscribe(res => {
          	if (res.data!=null) {
	            this.data=res.data
			        this.api.apiStudentPortalID(this.id,this.progid)
			  			.subscribe(res => {
                // console.log(res.data)
                    this.tableArr=[]
                    this.tabledata=[];
                  if (res.data==null) {
                    this.tableArr=[]
                    this.tabledata=[];
                    if (this.temporaryconter==0) {
                      this.createchecklist();
                    }
                    this.temporaryconter++
                  }else{
                      for (var i1 = 0; i1 < res.data.length; ++i1) {
                        if (res.data[i1].subjectId!='FILI 1013'&&res.data[i1].subjectId!='FILI 1023') {
                          this.tableArr.push(res.data[i1])
                          this.tabledata.push(res.data[i1])
                        }
                      }
                      if (this.temporaryconter==0&&res.data.length==0) {
                        this.createchecklist();
                      }
                      this.temporaryconter++
                  }
                  this.grades = [];
                  // console.log(res.data)
                  if (res.data.length==0) {
                    this.checklistexist = false
                  }else{
                    this.checklistexist = true
                  }

                  if (this.checklistexist == true) {
                          this.api.apiAcademicHistoryEvaluation(this.id)
                           .subscribe(res => {
                            this.grades = res.data
                            this.api.apiSubjectAcademicHistoryMapping(this.id,this.progid)
                                    .subscribe(res => {
                                      this.acadhist = res.data
                                      this.api.apiStatusList()
                                            .subscribe(res => {
                                              if (xedia==0) {
                                                this.global.swalClose();
                                              }
                                                this.status=res.data
                                                this.passnotcredited()
                                                this.matchingunsaved()
                                                this.checkevalbuttonfunc()
                                              },Error=>{
                                                this.status=[]
                                                this.global.swalAlertError(Error);
                                             });
                                      },Error=>{
                                        this.acadhist=[]
                                        this.global.swalAlertError(Error);
                                     });
                            },Error=>{
                              this.grades=[]
                              this.global.swalAlertError(Error);
                           });
                  }else{
                     this.grades=[]
                     this.global.swalClose();
                  }


			          },Error=>{
			            //console.log(Error);
			            this.global.swalAlertError(Error);
                  this.tableArr=[]
                  this.tabledata=[]
			         });
          		// code...
          	}else
          	{
    			    this.tableArr=[]
    			    this.tabledata=[]
          	}

          },Error=>{
            //console.log(Error);
            this.global.swalAlertError(Error);
            console.log(Error)
          });
  	  }
   }
 temporarymapping=[]
 gradecredited=[]
 gradenotincluded=[]
 checkgrade(i){
   var grade = ''
   var savedarr = []
   if (this.acadhist!=undefined&&this.grades!=undefined) {
     for (var b = 0; b < this.acadhist.length; ++b) {
       if (this.tableArr[i].id==this.acadhist[b].studentEvaluationCheckListId) {
         for (var temp1 = 0; temp1 < this.grades.length; ++temp1) {
           if (this.acadhist[b].academicHistoryRecordId==this.grades[temp1].recordId) {
             if (grade=='') {
               grade = this.grades[temp1].grade
               savedarr.push(this.grades[temp1])
             }else{
               grade = grade + ", " + this.grades[temp1].grade
               savedarr.push(this.grades[temp1])
             }
           }
         }
       }
     }
     // code...
   }

   if (this.grades!=undefined) {
     for (var g = 0; g < this.grades.length; ++g) {
       if (
         (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
         ) {
           var condition1 = ''
           for (var temp2 = 0; temp2 < savedarr.length; ++temp2) {
             if (savedarr[temp2].recordId==this.grades[g].recordId) {
               condition1 = 'passed'
             }
           }

           if (condition1=='') {
             if (grade == '') {
               grade = this.grades[g].grade
             }else{
               grade = grade + ", " + this.grades[g].grade
             }
           }

           if (this.temporarymapping.length == 0) {
             this.temporarymapping.push({id:this.tableArr[i].id,recordId:this.grades[g].recordId,subjectId:this.grades[g].subjectId,subjectTitle:this.grades[g].subjectTitle,units:this.grades[g].units,grade:this.grades[g].grade})
           }else{
             var condition2 = ''
             for (var temp3 = 0; temp3 < this.temporarymapping.length; ++temp3) {
               if (this.temporarymapping[temp3].recordId==this.grades[g].recordId) {
                 condition2 = 'passed'
                 break
               }
             }
             if (condition2=='') {
                this.temporarymapping.push({id:this.tableArr[i].id,recordId:this.grades[g].recordId,subjectId:this.grades[g].subjectId,subjectTitle:this.grades[g].subjectTitle,units:this.grades[g].units,grade:this.grades[g].grade})
             }
           }

       }
     }
   }
   return grade
 }

 notcredited=[]
 checkcurriculum(g){
   for (var i = 0; i < this.tableArr.length; ++i) {
     if ((this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
       )
       {
        return false
       }
   }
   return true
 }

 credited=[]
 passnotcredited(){
   var pass = 0
   var value = 0
   var pass2 = false
   for (var g = 0; g < this.grades.length; ++g) {
     for (var i = 0; i < this.tableArr.length; ++i) {
       if (
         (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
       ){
         this.credited.push({checklistid:this.tableArr[i].id,acadhist:this.grades[g]})
         pass = 1
         break
       }
     }
     if (pass == 0) {
       pass2 = true
       for (var temp = 0; temp < this.acadhist.length; ++temp) {
           if (this.grades[g].recordId==this.acadhist[temp].academicHistoryRecordId) {
             pass2 = false
             break
           }else{
             pass2 = true
           }
       }
       if (pass2) {
           this.notcredited.push(this.grades[g])
       }
     }
     pass=0
     pass2 = false
   }
 }

  x=''
  sy(x){
    if (x!=null) {
      if (this.x.substring(0,6)==x.substring(0,6)) {
        this.x=x
        return false
      }else
      {
        this.x=x
        return true
      }
    }else
      return false

  }

  y=''
  sem(y){
    if (this.y==y) {
      this.y=y
      return false
    }else
    {
      this.y=y
      return true
    }
  }

  getstudgrade(x){
    for (var i = 0; i < this.stud.length; ++i) {
      if (this.stud[i]==x) {

        break;
      }
    }
  }

  createchecklisttemp=false
  createchecklist(){
    if (this.createchecklisttemp == false) {
      this.createchecklisttemp=true
      this.api.apiStudentPortalPost(this.id,this.progid)
       .subscribe(res => {
       	  this.keyDownFunction('onoutfocus',0)
            this.createchecklisttemp=false
          },Error=>{
            this.createchecklisttemp=false
            this.global.swalAlertError(Error);
        });
    }
  }

  opennotcred(){
    // console.log(this.checklistexist,this.checkevalbutton)
    const dialogRef = this.dialog.open(RemainingComponent, {
          width: '700px', disableClose: true, data:{notcredited:this.notcredited}
        });

        dialogRef.afterClosed().subscribe(result => {
          //console.log(result)
          if (result!=undefined) {
            if (result.result==1) {
              this.keyDownFunction('onoutfocus',0)
            }
          }
        });
  }

  notsaved=[]
  checkcolor(i){
  var grade = 'white'
  var subj=[]
  var acadhist=[]
     if (this.grades!=undefined) {
       for (var g = 0; g < this.grades.length; ++g) {
         if (
           (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
           ) {
             subj.push(this.grades[g])
             if (this.acadhist.length==0) {
               grade = '#fcedd2'
             }else
             for (var l = 0; l < this.acadhist.length; ++l) {
               if (this.grades[g].recordId==this.acadhist[l].academicHistoryRecordId) {
                 acadhist.push(this.grades[g])
               }else{
                 grade = '#fcedd2'
               }

             }
         }
       }
     }
    //  if (subj.length==acadhist.length) {
    //    grade = 'white'
    //  }

     return grade

  }

  checkevalbutton=false
  checkevalbuttonfunc(){
    var color = ''
    for (var i = 0; i < this.tableArr.length; ++i) {
      color=this.checkcolor(i)
      if (color=='#fcedd2') {
        break;
      }
    }
    if (color=='#fcedd2') {//color yellow
      this.checkevalbutton=true
    }else{
      this.checkevalbutton=false
    }
  }

  matching=[]
  matchingunsaved(){
    for (var i = 0; i < this.tableArr.length; ++i) {
       for (var g = 0; g < this.grades.length; ++g) {
         if ( (this.tableArr[i].subjectId.replace(/\s/g, '')==this.grades[g].subjectId.replace(/\s/g, '')&&
         this.tableArr[i].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')==this.grades[g].subjectTitle.replace(/,/g, "").replace(/\s/g, '').replace(/and/g, '').replace(/&/g, '')&&
         this.tableArr[i].totalUnits==this.grades[g].units
         )
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1013"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1013")
         ||
         (this.grades[g].subjectId.replace(/\s/g, '')=="NSTP1023"&&this.tableArr[i].subjectId.replace(/\s/g, '')=="NSTP1023")
           ) {
             if (this.matching.length==0) {
               this.matching.push({id:this.tableArr[i].id,acadhistid:this.grades[g].recordId,subjectTitle:this.tableArr[i].subjectTitle})
             }else
             {
               var condition = true
               for (var zero = 0; zero < this.matching.length; ++zero) {
                 if (this.matching[zero].acadhistid==this.grades[g].recordId) {
                   condition=false
                 }
               }
               if (condition) {
                 this.matching.push({id:this.tableArr[i].id,acadhistid:this.grades[g].recordId,subjectTitle:this.tableArr[i].subjectTitle})
               }
             }
            }
       }
    }
  }
  savefuncvar=false
  savefunc(){
    this.global.swalLoading('Saving...')
    this.savefuncvar=true
    this.saveunsave(this.matching.length-1)
  }

  saveunsave(x){
    if (x>=0) {

      this.api.apiSubjectAcademicHistoryMappingPost(this.matching[x].id,this.matching[x].acadhistid)
         .subscribe(res => {
        	this.api.apiHistoryPost(this.id,this.progid)
                 .subscribe(res => {
      				    this.saveunsave(x-1)
                    },Error=>{
                      this.global.swalAlertError(Error);
                    });
            },Error=>{
              this.savefuncvar=false
              this.global.swalClose();
              this.global.swalAlertError(Error);
          });
    }else{
        this.api.apiRetentionPolicyLastSY()
          .subscribe(res => {
            this.global.filter = res.data
            this.global.swalClose();
            this.global.swalSuccess('Evaluation Saved!')
            this.savefuncvar=false
            this.notcredited=[]
            this.credited=[]
            this.keyDownFunction('onoutfocus',1)
          },Error=>{
            this.global.swalAlertError(Error);
          });

    }
  }

  eval(){
    this.Confirmeval("Save evaluation?","",'warning','Save',' ','','sy');
  }

  Confirmeval(title,text,type,button,d1,d2,remove)
  {
    swal({
        title: title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: button
      }).then((result) => {
        if (result.value) {
          this.savefunc()
        }
      })
   }
}

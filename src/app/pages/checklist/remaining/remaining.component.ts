import { Component, OnInit } from '@angular/core';

import { GlobalService } from './../../../global.service';
import {Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';

@Component({
  selector: 'app-remaining',
  templateUrl: './remaining.component.html',
  styleUrls: ['./remaining.component.scss']
})
export class RemainingComponent implements OnInit {
 notcredited
 constructor(public dialog: MatDialog,public dialogRef: MatDialogRef<RemainingComponent>,@Inject(MAT_DIALOG_DATA) public data: any,private global: GlobalService,private http: Http,) { }


  ngOnInit() {
  	this.notcredited = []
  	for (var i1 = 0; i1 < this.data.notcredited.length; ++i1) {
        if (this.data.notcredited[i1].subjectId!='FILI 1013'&&this.data.notcredited[i1].subjectId!='FILI 1023') {
          this.notcredited.push(this.data.notcredited[i1])
        }
      }
  }

  onNoClickclose(): void {
       this.dialogRef.close(undefined);
  }
}

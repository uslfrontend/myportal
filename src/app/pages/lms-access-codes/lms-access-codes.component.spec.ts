import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LmsAccessCodesComponent } from './lms-access-codes.component';

describe('LmsAccessCodesComponent', () => {
  let component: LmsAccessCodesComponent;
  let fixture: ComponentFixture<LmsAccessCodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LmsAccessCodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LmsAccessCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

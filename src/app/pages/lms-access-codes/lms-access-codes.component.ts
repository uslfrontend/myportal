import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { StudentApiService } from './../../student-api.service';
@Component({
  selector: 'app-lms-access-codes',
  templateUrl: './lms-access-codes.component.html',
  styleUrls: ['./lms-access-codes.component.scss']
})
export class LmsAccessCodesComponent implements OnInit {
	tableArr
  accesscodes:any
  header = new Headers();
  option:any;
  display:any
  ClassSchedule=[]
  constructor(public global: GlobalService,public api:StudentApiService) { }

  ngOnInit() {
    
    for (var i = 0; i < this.global.ClassSchedule.length; ++i) {
      this.ClassSchedule.push({
        codeNo:this.global.ClassSchedule[i].codeNo,
        subjectID:this.global.ClassSchedule[i].subjectID,
        subjectTitle:this.global.ClassSchedule[i].subjectTitle,
        units:this.global.ClassSchedule[i].units,
        accesscode:''
      })
    }
    
    this.api.apigetexcelaccesscodes()
       .subscribe(res => {
            this.accesscodes = res
            	for (var i = 0; i < this.ClassSchedule.length; ++i) {
                this.ClassSchedule[i].codeNo=this.ClassSchedule[i].codeNo.toLowerCase()
                for (var i2 = 0; i2 < this.accesscodes.length; ++i2) {
                  this.accesscodes[i2].code = this.accesscodes[i2].code.toString().toLowerCase()
                  if (this.ClassSchedule[i].codeNo== this.accesscodes[i2].code.toString().substring(0, 3)) {
                    this.ClassSchedule[i].accesscode=this.accesscodes[i2].accesscode
                    this.accesscodes.splice(i2,1)
                    break
                  }
                }

                if (i>0) {
                  if(this.ClassSchedule[i].codeNo==this.ClassSchedule[i-1].codeNo.toString().substring(0, 3)){
                    this.ClassSchedule[i].codeNo = ''
                    this.ClassSchedule[i].subjectID = ''
                    this.ClassSchedule[i].subjectTitle = ''
                    this.ClassSchedule[i].units = ''
                    this.ClassSchedule.splice(i,1)
                  }	
                }
                if (i>1) {
                if(this.ClassSchedule[i-1].codeNo==""){
                  if(this.ClassSchedule[i].codeNo==this.ClassSchedule[i-2].codeNo.toString().substring(0, 3)){
                    this.ClassSchedule[i].codeNo = ''
                    this.ClassSchedule[i].subjectID = ''
                    this.ClassSchedule[i].subjectTitle = ''
                    this.ClassSchedule[i].units = ''
                    this.ClassSchedule.splice(i,1)
                  }
                }
              }
              if (i>2) {
                if(this.ClassSchedule[i-2].codeNo==""&&this.ClassSchedule[i-1].codeNo==''){
                  if(this.ClassSchedule[i].codeNo==this.ClassSchedule[i-3].codeNo.toString().substring(0, 3)){
                    this.ClassSchedule[i].codeNo = ''
                    this.ClassSchedule[i].subjectID = ''
                    this.ClassSchedule[i].subjectTitle = ''
                    this.ClassSchedule[i].units = ''
                    this.ClassSchedule.splice(i,1)
                  }
                }
              }
              if (i>3) {
                if(this.global.ClassSchedule[i-3].codeNo==""&&this.global.ClassSchedule[i-2].codeNo==''){
                  if(this.global.ClassSchedule[i].codeNo==this.global.ClassSchedule[i-4].codeNo.toString().substring(0, 3)){
                    this.ClassSchedule[i].codeNo = ''
                    this.ClassSchedule[i].subjectID = ''
                    this.ClassSchedule[i].subjectTitle = ''
                    this.ClassSchedule[i].units = ''
                    this.ClassSchedule.splice(i,1)
                  }
                }
              }
             }
            	this.tableArr=this.ClassSchedule
          });
     }


}

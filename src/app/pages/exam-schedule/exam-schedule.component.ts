import { Component, OnInit } from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Inject} from '@angular/core';
@Component({
  selector: 'app-exam-schedule',
  templateUrl: './exam-schedule.component.html',
  styleUrls: ['./exam-schedule.component.scss']
})
export class ExamScheduleComponent implements OnInit {
  tableArr
  constructor(public dialog: MatDialog,public global: GlobalService,private http: Http) { }

  ngOnInit() {
  	if (this.global.ExamSchedule==null) {
  		this.global.ExamSchedule=[]
  	}
    this.tableArr = this.global.ExamSchedule
  }

}

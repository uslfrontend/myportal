import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenCodesComponent } from './open-codes.component';

describe('OpenCodesComponent', () => {
  let component: OpenCodesComponent;
  let fixture: ComponentFixture<OpenCodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenCodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenCodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

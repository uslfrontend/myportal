import { Component, OnInit } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';

@Component({
  selector: 'app-open-codes',
  templateUrl: './open-codes.component.html',
  styleUrls: ['./open-codes.component.scss']
})
export class OpenCodesComponent implements OnInit {
codes

fCode = '';
fSubjecID = '';
fDescTitle = '';
fEnrolled = '';
fCsize = '';
fDepartment = '';

tableArr = [];
tabledata = [];

   constructor(public global: GlobalService,private http: Http) { }

 ngOnInit() {
   if (this.global.opencodes!=undefined) {
     this.codes=this.global.opencodes
   }else
    this.tableArr = [];
    this.tabledata = [];
  	 this.http.get(this.global.api+'StudentPortal/OpenCodes',this.global.option)
       .map(response => response.json())
          .subscribe(res => {
            this.global.opencodes=res.data
            this.codes=res.data

            this.tableArr = res.data;
            this.tabledata = res.data;


            //console.log(res.data)
          },Error=>{
            console.log(Error)
          });
  }
  filterall(event){
    if(event.keyCode == 13 || event.keyCode == 9 || event == 'onoutfocus') {
         var filter = {
          codeNo: this.fCode,
          subjectId: this.fSubjecID,
          subjectTitle: this.fDescTitle,
          department: this.fDepartment,

     };
     var users = this.tableArr

     users= users.filter(function(item) {

       for (var key in filter) {
         if (!item[key].toUpperCase().includes(filter[key].toUpperCase()))
           return false;
       }
       return true;

     });
     this.tabledata = users
       }

   }

}

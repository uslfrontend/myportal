import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../../global.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddressLookupComponent } from './../address-lookup/address-lookup.component';
import { Router } from "@angular/router";
import Swal from 'sweetalert2';
import { EnrolmentFormComponent } from './../enrolment-form/enrolment-form.component';
import { StudentApiService } from './../../student-api.service';
const swal = Swal;
@Component({
  selector: 'app-enrollment',
  templateUrl: './enrollment.component.html',
  styleUrls: ['./enrollment.component.scss']
})

export class EnrollmentComponent implements OnInit {
  permPSGC = ''
  homeaddress = ''
  currPSGC = ''
  boardingaddress = ''
  tno = ''
  cno = ''
  email = ''
  currNoStreet = ''
  permNoStreet = ''
  gender = ''
  setarray
  tableArr = []
  inactive = []
  setfiltered = []
  setvar = ''
  warndisplay3 = []
  agree1 = false
  agree2 = false
  agree3 = false
  x = 1;//first year enrollment message pop-up trigger
  constructor(public dialog: MatDialog, public global: GlobalService, private router: Router, public api: StudentApiService) { }

  ngOnInit() {
    this.firstload()
    this.loaddata()


  }


  studinfo
  ClassSchedule = []

  retentionpolicy = false
  RetentionPolicyWholeSY = false
  retentionpolicysubjecttype = false

  checkRetentionpolicyskip() {
    this.retentionpolicy = false
    this.RetentionPolicyWholeSY = false
    this.retentionpolicysubjecttype = false
  }

  firstload() {
    this.global.swalLoading('Checking enrollment details...');
    this.global.apiStudentInfo(this.global.enrollmentsy)
      .subscribe(res => {
        this.studinfo = res.data;
        this.global.apiClassSchedule(this.global.requestid(), this.global.enrollmentsy)
          .subscribe(res => {
            this.ClassSchedule = res.data


            if (this.studinfo.enrollmentStatus == 'For Admission' || (this.studinfo.enrollmentStatus == 'Admitted' && this.studinfo.onlineEnrolled == 1)) {

              this.start();
            }
            else if (this.studinfo.enrollmentStatus == 'Admitted' && this.ClassSchedule.length == 0 && this.studinfo.onlineEnrolled == 0) {
              this.global.swalAlert('You are already admitted!', 'Enroll your subjects through your program chair.', 'warning')
              window.history.back();
            } else {
              this.global.swalClose()
            }
          }, Error => {
            this.global.swalAlertError(Error)
            window.history.back();
          });
      }, Error => {
        this.global.swalAlertError(Error)
        window.history.back();
      });
  }
  start() {
    this.checkRetentionpolicyskip()
    this.permPSGC = ''
    this.homeaddress = ''
    this.currPSGC = ''
    this.boardingaddress = ''
    this.tno = ''
    this.cno = ''
    this.email = ''
    this.currNoStreet = ''
    this.permNoStreet = ''
    this.gender = ''
    this.setarray
    this.tableArr = []
    this.inactive = []
    this.setfiltered = []
    this.setvar = ''
    this.checkenroll = 'true'
    this.warndisplay3 = []
    var filt2 = ''
    var filt3 = ''
    var filt4 = ''
    if (!(this.global.studinfo.yearOrGradeLevel == 1 && this.global.sy.charAt(6) == '1')) {

      if ((this.global.userdemog.cForm138 == false || this.global.userdemog.cForm138 == "False" || this.global.userdemog.cForm138 == null) && this.global.enrollmentype != 2) {
        filt4 = filt4 + '*Form 138<br>'
      }
      if (this.global.userdemog.cnso == false || this.global.userdemog.cnso == "False" || this.global.userdemog.cnso == null) {
        filt4 = filt4 + '*NSO Birth Certificate<br>'
      }
    }

    if (this.studinfo.blacklistedStopped != 0) {
      filt2 = filt2 + '*Blacklisted (Stopped)<br>'
    }
    if (this.studinfo.blacklistedViolation != 0) {
      filt2 = filt2 + '*Blacklisted (Violation)<br>'
    }
    if (this.global.requireAdmission == -1 && this.studinfo.enrollmentStatus == "For Admission" && !this.global.firstyear) {
      filt3 = filt3 + '*Admission is closed. <br>'
    }
    if (filt3 != '') {
      this.global.swalAlert('We are currently undergoing systems maintenance activity. For enrolment-related transactions, please try again later.', filt3, 'warning')
      window.history.back();
    } else
      if (filt2 != '') {
        this.global.swalAlert('Please see OSAS to resolve the ff.', filt2, 'warning')
        window.history.back();
      } else if (filt4 != '') {
        this.global.swalAlert('Please submit the following documents to the Registrar\'s Office:', filt4, 'warning')
        window.history.back();
      } else {
        if (this.studinfo.enrollmentStatus == 'Admitted' && this.global.ClassSchedule.length == 0 && this.studinfo.onlineEnrolled == 0) {//false due to disabling admitted students to enroll set
          this.global.swalAlert('You are already admitted!', 'Enroll your subjects through your program chair.', 'warning')
          window.history.back();
        } else {

          if (this.global.userdemog != undefined) {
            var x = this.global.userdemog
            this.permPSGC = x.permPSGC
            this.homeaddress = x.homeAddress
            this.currPSGC = x.currPSGC
            this.boardingaddress = x.currentAddress
            this.tno = x.telNo
            this.cno = x.mobileNo
            this.email = x.emailAdd
            this.currNoStreet = x.currNoStreet
            this.permNoStreet = x.permNoStreet
            var filt = ''
            if (this.global.filter != null && this.global.filter.evaluated == true) {
              if (this.global.filter.failingMarks_Count != 0) {
                filt = filt + '*Failed Subjects Count(' + this.global.filter.failingMarks_Count + ')<br>'
              }
              if (this.global.filter.nfE_Count != 0) {
                filt = filt + '*No Final Exam Count(' + this.global.filter.nfE_Count + ')<br>'
              }
              if (this.global.filter.retentionPolicyAverage.toLowerCase() == 'failed') {
                filt = filt + '*Retention Policy Average Failed<br>'
              }
              if (this.global.filter.retentionPolicyNoGradeLowerThan_Count != 0) {
                filt = filt + '*Retention Policy No Grade Lower Than Count(' + this.global.filter.retentionPolicyNoGradeLowerThan_Count + ')<br>'
              }
            } else {
              if (this.studinfo.enrollmentStatus == 'For Admission') {
                this.global.swalAlert('Not yet evaluated. Go to checklist and save your evaluation.', '*If error still persists, please contact your Program Chair.', 'warning')
                filt = '1'
              }
            }
            if (this.retentionpolicy) {
              filt = ''
            }

            if (filt == '') {
              var x = this.getyear(this.studinfo.yearOrGradeLevel)
              if (this.global.setarray != null) {
                for (var i = 0; i < this.global.setarray.length; ++i) {
                  //if (this.global.setarray[i].course=='BSN '&&this.global.setarray[i].yearLevel==2) {
                  if (this.global.setarray[i].course.replace(/\s/g, "") == this.studinfo.course.replace(/\s/g, "") + this.studinfo.major.replace(/\s/g, "") && this.global.setarray[i].yearLevel == x) {
                    //if (this.global.setarray[i].course.replace(/\s/g, "")==this.studinfo.course.replace(/\s/g, "")) {
                    this.setfiltered.push(this.global.setarray[i])
                  }
                }
              } else this.global.setarray = []
              if (this.setfiltered.length == 0) {
                this.global.swalAlert('Please contact your Program Chair.', '*No Available Set for your course and year.', 'warning')
                window.history.back();
              } else {
                this.setvar = this.setfiltered[0].headerId.toString()
                this.selectheader(this.setfiltered[0].headerId.toString(), 0)
              }

            } else if (filt == '1') {
              window.history.back();
            } else {
              this.global.swalAlert('Please contact your Program Chair to resolve the following issues:', filt, 'warning')
              window.history.back();
            }
            // console.log(this.setfiltered)
            // console.log(this.setvar)

          }
        }
      }

  }
  checkenroll = 'true'
  selectheader(id, index) {
    this.tableArr = undefined
    this.api.apiSetDetails(id)
      .subscribe(res => {
        this.tableArr = []
        var condition = false
        var subjectId = ''
        var codeNo = ''
        var subjectTitle = ''
        for (var i = 0; i < res.data.length; ++i) {
          condition = false
          subjectId = res.data[i].subjectId
          codeNo = res.data[i].codeNo
          subjectTitle = res.data[i].subjectTitle
          if (i != 0) {
            if (codeNo == res.data[i - 1].codeNo) {
              codeNo = ''
            }
            if (subjectId == res.data[i - 1].subjectId) {
              subjectId = ''
            }
            if (subjectTitle == res.data[i - 1].subjectTitle) {
              subjectTitle = ''
            }
          }
          this.tableArr.push({
            classSize: res.data[i].classSize,
            codeNo: codeNo,
            day: res.data[i].day,
            detailId: res.data[i].detailId,
            headerId: res.data[i].headerId,
            roomNumber: res.data[i].roomNumber,
            subjectId: subjectId,
            subjectTitle: subjectTitle,
            time: res.data[i].time,
            units: res.data[i].units
          })

          if (res.data[i].availableSlot <= 0) {
            if ((index + 1) >= this.setfiltered.length) {
              this.global.swalAlert('Please contact your Program Chair. All available sets are full!', '', 'warning')
              window.history.back();
              break
            } else {
              this.setvar = this.setfiltered[index + 1].headerId.toString()
              this.selectheader(this.setfiltered[index + 1].headerId, index + 1)
              break
            }
          } else {
            condition = true
          }
        }
        if (res.data.length == 0) {
          if ((index + 1) >= this.setfiltered.length) {
            this.global.swalAlert('Please contact your Program Chair. All available sets are full!', '', 'warning')
            window.history.back();
          } else {
            this.setvar = this.setfiltered[index + 1].headerId.toString()
            this.selectheader(this.setfiltered[index + 1].headerId, index + 1)
          }
        }
        if (condition) {
          this.setvar = this.setfiltered[index].headerId.toString()
          var setf = this.setfiltered[index]
          this.setfiltered = []
          this.setfiltered.push(setf)
          this.checkenroll = 'false'
          this.checkwholeSY()
        }
        // console.log(this.tableArr)
      }, Error => {
        this.tableArr = []
        //console.log(Error);
        console.log(Error)
        this.global.swalAlertError(Error);
      });
  }
  checkwholeSY() {

    this.api.apiRetentionPolicyLastSY()
      .subscribe(res => {
        // console.log(res.data)
        var filt = ''
        if (res.data != null) {
          if (res.data.dropped_Count != 0) {
            filt = filt + "*Dropped Count: " + res.data.dropped_Count + "<br>"
          }
          if (res.data.failingMarks_Count != 0) {
            filt = filt + "*Failing Marks Count: " + res.data.failingMarks_Count + "<br>"
          }
          if (res.data.nfE_Count != 0) {
            filt = filt + "*NFE Count: " + res.data.nfE_Count + "<br>"
          }
          if (res.data.subjectNotYetTaken_Count != 0) {
            filt = filt + "*Subject not yet taken Count: " + res.data.subjectNotYetTaken_Count + "<br>"
          }
        }
        //  this.api.apiRetentionPolicyWholeSY(this.global.userinfo.idNumber)
        //     .subscribe(res => {
        //
        //       if (res.data!=null) {
        //         if (res.data.dropped_Count!=0) {
        //         filt=filt +"*Dropped Count: "+res.data.dropped_Count+"<br>"
        //         }
        //         if (res.data.failingMarks_Count!=0) {
        //          filt=filt +"*Failing Marks Count: "+res.data.failingMarks_Count+"<br>"
        //         }
        //         if (res.data.nfE_Count!=0) {
        //           filt=filt +"*NFE Count: "+res.data.nfE_Count+"<br>"
        //         }
        //         // if (res.data.subjectNotYearTaken_Count!=0) {
        //         //   filt=filt +"*Subject not yet taken Count: "+res.data.subjectNotYetTaken_Count+"<br>"
        //         // }
        //       }

        if (filt == '') {

          this.api.apiRetentionPolicySpecificSubjectType(this.global.userinfo.idNumber)
            .subscribe(res => {
              var indicate = 0
              // console.log(res.data[0])
              for (var i1 = 0; i1 < res.data.length; ++i1) {
                if (res.data[i1 + 1] != undefined) {
                  if (res.data[i1 + 1].subjectId == res.data[i1].subjectId) {
                    res.data[i1].retake_GradeNotLowerThan = ''
                    this.warndisplay3.push(res.data[i1] + 1)
                    ++i1;
                    if (res.data[i1].retake_GradeNotLowerThan == 'Failed') {//commented out because of retention policy checking issue on enrolment
                      indicate++
                    }
                  } else {
                    res.data[i1].retake_GradeNotLowerThan = ''
                    this.warndisplay3.push(res.data[i1])
                    if (res.data[i1].policy_GradeNotLowerThan == 'Failed') {//commented out because of retention policy checking issue on enrolment
                      indicate++
                    }
                  }
                } else {
                  res.data[i1].retake_GradeNotLowerThan = ''
                  this.warndisplay3.push(res.data[i1])
                  if (res.data[i1].policy_GradeNotLowerThan == 'Failed') {//commented out because of retention policy checking issue on enrolment
                    indicate++
                  }
                }
              }
              if (((res.data == null || res.data.length == 0) || (this.warndisplay3.length == 1 && this.warndisplay3[0].policy_GradeNotLowerThan == 'Passed') || (this.warndisplay3.length > 1 && indicate == 0))) {
                this.global.swalClose()
              } else {

                if (this.retentionpolicysubjecttype) {
                  this.global.swalClose()
                } else {
                  this.global.swalAlert('Please contact your Program Chair to resolve this issue:', 'Retention policy for specific subject type failed.', 'warning')
                  window.history.back();
                }
              }
            }, Error => {
              this.global.swalClose()
              window.history.back();
              this.global.swalAlertError(Error);
            });
        } else {
          window.history.back();
          this.global.swalAlert("Retention Policy not met! Please contact your Program Chair.", filt, "warning")
        }
      }, Error => {
        this.global.swalAlertError(Error);
      }
      );
  }
  enroll() {
    var x = ''
    if (this.checkenroll == 'true') {
      x = x + "*Select Set!<br>"
    }
    if (this.selected == '') {
      //x = x+"*Learning Modality is required!<br>"
    }
    if (this.cno == '') {
      x = x + "*Contact number is required!<br>"
    }
    if (this.homeaddress == '' || this.homeaddress == null) {
      x = x + "*Home address is required!<br>"
    }
    if (x != '') {
      this.global.swalAlert("Warning", x, 'warning')
    } else {
      this.checkenroll = 'true'
      this.global.swalLoading('Processing...');


      //after checking of the other required fields, old process shoul run this method call to check conflicts on the set. if there's no conflict, the system will call the admitstud method for student admission
      //and the process will go on
      // methods used: checkconflict(), admitstud() enrollcode(), deletecodeloop()
      // this.checkconflict(this.tableArr.length-1)

      //New method for api processed student enrolment
      this.enrollStudent();

    }
  }

  ////this batch of codes are from the old enrolment logic
  //  admitstud(){
  //    this.api.apiAdmitStudentPost(this.global.userinfo.idNumber,this.global.enrollmentsy,this.studinfo.programID,this.getyear(this.studinfo.yearOrGradeLevel))
  //       .subscribe(res => {
  //         this.successcode=[]
  //         this.scodewarn=''
  //         this.enrollcode(this.tableArr.length-1)
  //       },Error=>{
  //         this.global.swalAlertError(Error);
  //         console.log(Error);
  //       });
  //  }
  //  tempconflict=0
  //  tempclasssize=0
  // checkconflict(repeat){
  //   if (repeat>=0){
  //     var codeno=''

  //     // let ctr = 0;
  //     // while(this.tableArr[repeat-ctr].codeNo == ''){
  //     //   if(checkCodeNo(this.tableArr[repeat-ctr].codeNo)){
  //     //     codeno = this.tableArr[repeat-ctr].codeNo;
  //     //     ctr++;
  //     //   }else{
  //     //     codeno = this.tableArr[repeat].codeNo
  //     //     break;
  //     //   }
  //     // }

  //     // for(let i = 0; i<=repeat;i++){
  //     //   if(checkCodeNo(this.tableArr[repeat-i].codeNo)){
  //     //     codeno = this.tableArr[repeat-i].codeNo;
  //     //   }else{
  //     //     codeno = this.tableArr[repeat].codeNo
  //     //     break;
  //     //   }
  //     // }
  //     // function checkCodeNo(param){
  //     //   if(param == '')
  //     //     return true;
  //     //   else
  //     //     return false;
  //     // }

  //     if (this.tableArr[repeat].codeNo=='') {
  //       if (this.tableArr[repeat-1].codeNo=='') {
  //         if (this.tableArr[repeat-2].codeNo=='') {
  //           if (this.tableArr[repeat-3].codeNo=='') {
  //             if (this.tableArr[repeat-4].codeNo=='') {
  //               if (this.tableArr[repeat-5].codeNo=='') {
  //                 codeno = this.tableArr[repeat-6].codeNo
  //               }else
  //                 codeno = this.tableArr[repeat-5].codeNo
  //             }else
  //               codeno = this.tableArr[repeat-4].codeNo
  //           }else
  //             codeno = this.tableArr[repeat-3].codeNo
  //         }else
  //           codeno = this.tableArr[repeat-2].codeNo
  //       }else
  //         codeno = this.tableArr[repeat-1].codeNo
  //     }else
  //         codeno = this.tableArr[repeat].codeNo

  //       this.api.apiConflictSchedules(this.global.userinfo.idNumber,codeno,this.tableArr[repeat].day,this.tableArr[repeat].time,this.global.enrollmentsy)
  //       .subscribe(res2 => {
  //         this.tempconflict = this.tempconflict + res2.data.lenth;
  //         this.api.apiEnrolledSubjectsCodeNo(codeno,this.global.enrollmentsy)
  //           .subscribe(res => {
  //             for (var i = 0; i < res.data.length; ++i) {
  //               if ((res.data[i].oe+res.data[i].res)>=res.data[i].classSize) {
  //                   this.tempclasssize++;
  //               }
  //             }
  //               this.checkconflict(repeat-1)
  //           });
  //       },Error=>{
  //         this.global.swalAlertError(Error);
  //       });
  //   }else{
  //     var warns=''
  //     if (this.tempconflict>0) {
  //         this.tempconflict=0;
  //         warns=warns+"*Conflicting schedules found!<br>"
  //     }
  //     if (this.tempclasssize>0) {
  //         this.tempclasssize=0;
  //         warns=warns+'Set is full and no available set for your course and year.'
  //     }

  //     if (warns=='') {
  //       this.admitstud()
  //     }else{

  //         this.global.swalClose()
  //         this.checkenroll= 'false'
  //         this.global.swalAlert("Please notify your Program Chair to resolve this issue.",warns,'warning')

  //     }
  //   }
  // }
  //   successcode=[]
  //   scodewarn=''
  //   enrollcode(repeat){
  //    if (repeat>=0) {
  //      if (this.tableArr[repeat].codeNo=='') {
  //         this.enrollcode(repeat-1)
  //      }else{
  //        this.api.apiEnrolledSubjectsPost(this.global.userinfo.idNumber,this.tableArr[repeat].codeNo,this.global.enrollmentsy)
  //           .subscribe(res => {
  //             if (res.message == 'Success') {
  //               this.successcode.push(this.tableArr[repeat].codeNo)
  //               this.enrollcode(repeat-1)
  //             }
  //             else{
  //               this.scodewarn=this.scodewarn+"*"+res.message+"<br>"
  //               this.enrollcode(repeat-1)
  //             }
  //           },Error=>{
  //             this.global.swalAlertError(Error)
  //           });
  //      }
  //     }else{
  //       if (this.scodewarn=='') {
  //         this.updateinfo()
  //       }else{
  //         this.deletecodeloop(this.successcode.length-1)
  //       }
  //     }
  //   }

  //   deletecodeloop(repeat){
  //    if (repeat>=0) {
  //      this.api.apiEnrolledSubjectsDelete(this.global.requestid(),this.successcode[repeat],this.global.enrollmentsy)
  //       .subscribe(res => {
  //          this.deletecodeloop(repeat-1)
  //       },Error=>{
  //         this.global.swalAlertError(Error)
  //       });
  //     }else{
  //       this.api.apiEnrollmentHistoryDelete(this.global.enrollmentsy)
  //         .subscribe(res => {
  //            this.global.swalAlert("Enrolment failed due to the ff.",this.scodewarn,'warning')
  //            window.history.back();
  //         },Error=>{
  //         this.global.swalAlertError(Error)
  //         });
  //     }
  //   }


  enrollStudent() {
    // console.log("enrollStudent")
    this.api.apiAdmitEnrollStudent(this.setvar, this.global.userinfo.idNumber, this.global.enrollmentsy, this.studinfo.programID, this.getyear(this.studinfo.yearOrGradeLevel))
      .subscribe(res => {
        // console.log(res)
        if (res.data[0].code == 5) {
          this.global.swalAlert('Conratulations!', res.data[0].message, 'success')

          this.updateinfo(res.data[0].message);
        } else
          this.global.swalAlert('Please contact your Program Chair to resolve this issue:', res.data[0].message, 'warning')
      }, Error => {
        this.global.swalAlertError(Error);
        console.log(Error);
      });
  }

  async updateinfo(message) {
    this.api.apiContactInfo(this.cno, this.tno, this.email, this.permNoStreet, this.permPSGC, this.currNoStreet, this.currPSGC)
      .subscribe(res => {
        this.global.swalClose()
        this.global.apiEnrollmentHistory().subscribe(res => {
            this.x = this.checkIfIncomingFirstYear(res.data)
            this.global.getallapi(this.x, message)
          }, Error => {
            this.global.swalAlertError(Error);
        });


      }, Error => {
        this.global.swalAlertError(Error)
        console.log(Error)
      });

  }

  checkIfIncomingFirstYear(array){
    var prog_level = array[array.length - 1].programLevel;
    var y_level = array[array.length - 1].yearOrGradeLevel;
    var semester = array[array.length - 1].schoolYear;
    if ((prog_level == 50 || prog_level == 60) && y_level == 1) {//proglevel: 50 = baccalaureate; 60 = post baccalaureate
      if(semester.charAt(6) == 1)//check if first semester enrollment for first years
        return 1;
      else
        return 4;
    }else{
      return 4;
    }
  }


  displaycs(x) {
    if (x == 'S') {
      return 'Single'
    }
    if (x == 'M') {
      return 'Married'
    }
    if (x == 'W') {
      return 'Widow'
    }
  }
  displayg(x) {
    if (x == 'M') {
      return 'Male'
    }
    if (x == 'F') {
      return 'Female'
    }
  }

  openDialog(lookup): void {
    const dialogRef = this.dialog.open(AddressLookupComponent, {
      width: '500px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.result != 'cancel') {
        if (lookup == 1) {
          this.permPSGC = result.data;
          this.homeaddress = result.result;
        } else {
          this.currPSGC = result.data;
          this.boardingaddress = result.result;
        }
      }
    });
  }
  getyear(x) {
    if (x >= 5) {
      return x
    }
    if (this.global.enrollmentsy.charAt(6) == '1') {
      x = x + 1;
      if (this.global.firstyear) {
        x = 1
      }
    }
    return x;
  }
  confirmenroll() {
    this.swalConfirm("You are about to enroll.", "Please confirm to proceed!", 'question', 'Confirm');
  }

  swalConfirm(title, text, type, button) {
    swal({
      title: title,
      text: text,
      type: type,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: button
    }).then((result) => {
      if (result.value) {
        this.enroll()
      }
    })
  }

  swalConfirmsetfull(title, text, type) {
    swal({
      title: title,
      text: text,
      type: type,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "OK"
    }).then((result) => {
      if (result.value) {
        this.start()
      }
    })
  }
  ViewPrint(): void {
    const dialogRef = this.dialog.open(EnrolmentFormComponent, {
      width: '75%', data: { x: 'col', gender: this.global.userinfo.gender, id: this.global.requestid(), name: this.global.userinfo.lastName + ", " + this.global.userinfo.firstName + " " + this.global.userinfo.middleName + " " + this.global.userinfo.suffixName, course: this.studinfo.course, major: this.studinfo.major, year: this.studinfo.yearOrGradeLevel, subjects: this.global.ClassSchedule }, disableClose: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == undefined) {
        // code...
      } else
        if (result.result != 'cancel') {
        }
    });
  }




  selected = ''
  array = []
  exist = false
  arrayexist = []
  loaddata() {
    this.array = undefined

    this.loadfambg();
    if (this.global.userdemog != undefined) {

      var x = this.global.userdemog
      this.permPSGC = x.permPSGC
      this.homeaddress = x.homeAddress
      this.personalInfo[0].homeaddress = this.homeaddress
      this.currPSGC = x.currPSGC
      this.boardingaddress = x.currentAddress
      this.tno = x.telNo

      this.cno = x.mobileNo
      this.personalInfo[0].cno = this.cno

      this.email = x.emailAdd
      this.personalInfo[0].email = this.email

      this.currNoStreet = x.currNoStreet
      this.permNoStreet = x.permNoStreet
    }
    // this.http.get(this.global.api+'StudentPortal/PreferredLearningModality/'+this.global.enrollmentsy+'?idNumber='+this.global.requestid(),this.global.option)
    //          .map(response => response.json())
    //          .subscribe(res => {
    //            if (res.data.length==0) {
    //              this.exist=false
    //            }else{
    //              this.exist=true
    //              this.arrayexist=res.data
    //              this.selected =this.arrayexist[0].id
    //            }
    //            this.http.get(this.global.api+'StudentPortal/LearningModalities/'+this.global.enrollmentsy,this.global.option)
    //                      .map(response => response.json())
    //                      .subscribe(res => {
    //                        this.array=res.data
    //                      },Error => {
    //                        this.array=[]
    //                        this.global.swalAlertError(Error);
    //                      });
    //          },Error => {
    //            this.global.swalAlertError(Error);
    //          });
  }
  checktemp = false
  check2() {
    if (!this.checktemp) {
      this.checktemp = true
      var sy = this.global.enrollmentsy
      if (this.global.studinfo.departmentCode == 'ELEM' || this.global.studinfo.departmentCode == 'HS') {
        sy = this.global.enrollmentsy.slice(0, -1)
      }
      this.api.apiLearningModalityPost(this.selected, sy)
        .subscribe(res => {
          this.checktemp = false
          this.loaddata()
        }, Error => {
          this.global.swalAlertError(Error);
          console.log(Error);
          this.checktemp = false
        });
    }
  }

  /////////////////////////////////////
  familyarray = []
  familyarrayinfo = []
  fatherInfo;
  motherInfo;
  guardianInfo;
  sgfAddress = ''
  sgfId = ''
  average = ''
  sgfsy = ''
  // slaSy=''
  // slaId=''
  // lsaAddress=''
  slaplaceholder = ''

  preSchool_name = '';
  preSchool_yrGraduated = '';
  preSchool_schoolAddress = '';

  elementary_name = '';
  elementary_yrGraduated = '';
  elementary_schoolAddress = '';

  jhs_name = '';
  jhs_yrGraduated = '';
  jhs_schoolAddress = '';

  shs_name = '';
  shs_yrGraduated = '';
  shs_schoolAddress = '';

  arrayschool
  educarray = []

  personalInfo = [{
    cno: "",
    email: "",
    homeaddress: "",
    preSchool_name: "",
    preSchool_yrGraduated: "",
    preSchool_schoolAddress: "",
    elementary_name: "",
    elementary_yrGraduated: "",
    elementary_schoolAddress: "",
    jhs_name: "",
    jhs_yrGraduated: "",
    jhs_schoolAddress: "",
    shs_name: "",
    shs_yrGraduated: "",
    shs_schoolAddress: "",

  }];

  loadfambg() {
    this.api.apiStudentPortalFamilyBG()
      .subscribe(res => {
        this.familyarray = res.data
        this.familyarray.forEach(element => {

          if (element.relDesc == "Father") {

            this.api.apiStudentPortalgetParentInfoBG(element.memberIdNumber.toString())
              .subscribe(res => {
                this.fatherInfo = [];
                this.fatherInfo = res.data
                this.global.familyarrayinfo.push({
                  parentName: this.fatherInfo.parentName,
                  occupation: this.fatherInfo.occupation,
                  officeAddress: this.fatherInfo.officeAddress,
                  contactNo: this.fatherInfo.cellphoneNo,
                  ofw: this.fatherInfo.ofw,
                  relDesc: element.relDesc
                })
              });
          } else if (element.relDesc == "Mother") {
            this.api.apiStudentPortalgetParentInfoBG(element.memberIdNumber)
              .subscribe(res => {
                this.motherInfo = [];
                this.motherInfo = res.data
                this.global.familyarrayinfo.push({
                  parentName: this.motherInfo.parentName,
                  occupation: this.motherInfo.occupation,
                  officeAddress: this.motherInfo.officeAddress,
                  contactNo: this.motherInfo.cellphoneNo,
                  ofw: this.motherInfo.ofw,
                  relDesc: element.relDesc
                })
              }, Error => {
                console.log(Error)
              });

          } else if (element.relDesc == 'Brother/Sister' || element.relDesc == 'Guardian' || element.relDesc == 'Cousin' || element.relDesc == 'Uncle/Aunt' || element.relDesc == 'Grand Parent' || element.relDesc == 'StepFather' || element.relDesc == 'StepMother' || element.relDesc == 'Spouse') {
            this.api.apiStudentPortalgetParentInfoBG(element.memberIdNumber.toString())
              .subscribe(res => {
                this.guardianInfo = [];
                this.guardianInfo = res.data
                this.global.familyarrayinfo.push({
                  parentName: this.guardianInfo.parentName,
                  occupation: this.guardianInfo.occupation,
                  officeAddress: this.guardianInfo.officeAddress,
                  contactNo: this.guardianInfo.cellphoneNo,
                  ofw: this.guardianInfo.ofw,
                  relDesc: element.relDesc
                })
              });
          }
        });

        this.loadeducbg()
      });
  }
  loadeducbg() {
    this.api.apiStudentPortalEducBG()
      .subscribe(res => {

        if (res.data[0] != undefined) {
          this.educarray = res.data
          this.educarray.forEach(element => {
            if (element.programLevel == 10) {//elementary

              this.elementary_name = element.schoolName;
              this.elementary_yrGraduated = element.yearGraduated;
              this.elementary_schoolAddress = element.otherInfo;
              this.personalInfo[0].elementary_name = this.elementary_name;
              this.personalInfo[0].elementary_yrGraduated = this.elementary_yrGraduated;
              this.personalInfo[0].elementary_schoolAddress = this.elementary_schoolAddress;

            }
            else if (element.programLevel == 20) {//secondary, jhs and shs
              if (element.programName == 'Senior High School') {

                this.shs_name = element.schoolName;
                this.shs_yrGraduated = element.yearGraduated;
                this.shs_schoolAddress = element.otherInfo;

                this.personalInfo[0].shs_name = this.shs_name;
                this.personalInfo[0].shs_yrGraduated = this.shs_yrGraduated;
                this.personalInfo[0].shs_schoolAddress = this.shs_schoolAddress;
              } else {

                this.jhs_name = element.schoolName;
                this.jhs_yrGraduated = element.yearGraduated;
                this.jhs_schoolAddress = element.otherInfo;

                this.personalInfo[0].jhs_name = this.jhs_name;
                this.personalInfo[0].jhs_yrGraduated = this.jhs_yrGraduated;
                this.personalInfo[0].jhs_schoolAddress = this.jhs_schoolAddress;

              }
            } else if (element.programLevel == 0) {//pre-school
              this.preSchool_name = element.schoolName;
              this.preSchool_yrGraduated = element.yearGraduated;
              this.preSchool_schoolAddress = element.otherInfo;

              this.personalInfo[0].preSchool_name = this.preSchool_name;
              this.personalInfo[0].preSchool_yrGraduated = this.preSchool_yrGraduated;
              this.personalInfo[0].preSchool_schoolAddress = this.preSchool_schoolAddress;
            }
          });
        }
        // console.log(this.educarray)
      });

  }

}

import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService } from './../../global.service';

import {Http, Headers, RequestOptions} from '@angular/http';
@Component({
  selector: 'app-payslip',
  templateUrl: './payslip.component.html',
  styleUrls: ['./payslip.component.scss']
})
export class PayslipComponent implements OnInit {

file
pdfBlob
url
base64
value=''
array=[]
encryp=''
  constructor(private http: Http,private _sanitizer: DomSanitizer,public global: GlobalService,) { }

  ngOnInit() {
    this.global.swalLoading('')
             this.http.get(this.global.api+'EmployeePortal/EncryptedString',this.global.option)
              .map(response => response.json())
              .subscribe(res => {
                this.encryp=res.data
                 //this.http.get(this.global.acctgApi+'getphpfile/acctgapis.php?en='+encodeURIComponent(this.encryp)+'&action=payslipDropdown')
                  this.http.get(this.global.api+'EmployeePortal/EmployeePayrollPeriod',this.global.option) //Employee Active Pay Periods
                     .map(response => response.json())
                     .subscribe(res => {
                        this.array=res.data
                        if(res.data.length!=0){
                          this.value=res.data[0].recordID
                          this.generatePDF(1)
                        }else{
                          this.global.swalAlert("Information","Payslip record doesn't exist. Please refer to the payroll officer. Thank you.",'info');
                        }

                      },Error=>{
                        window.history.back();
                        this.global.swalAlert("Server not found!","ACCOUNTING SERVER MAY NOT BE AVAILABLE!",'warning');
                      });
                },Error=>{this.global.swalAlertError(Error);});

	}
  generate(){
    this.global.swalLoading('')
    //this.http.get(this.global.acctgApi+'getpdf/payslipbase64.php?en='+encodeURIComponent(this.encryp)+'&value='+encodeURIComponent(this.value))
    this.http.get(this.global.api + 'EmployeePortal/PaySlipPDF/'+encodeURIComponent(this.value),this.global.option)
       .map(response => response.text())
       .subscribe(res => {        
        //console.log(res);
         this.global.swalClose()
          this.url = this._sanitizer.bypassSecurityTrustResourceUrl('data:application/pdf;base64,'+res);
          this.base64=res;          
       },Error=>{
          this.global.swalAlertError(Error);
          //console.log(Error)
        });
   //this._sanitizer.bypassSecurityTrustResourceUrl('http://usl.edu.ph/pages/getpdf/payslip.php?en='+encodeURIComponent(this.encryp)+'&value='+encodeURIComponent(this.value));
    //console.log('http://usl.edu.ph/pages/getpdf/payslip.php?en='+encodeURIComponent(this.encryp)+'&value='+encodeURIComponent(this.value))
  }

  generatePDF(todo)
  {
    this.global.swalLoading('')
    this.url=this.global.api+"EmployeePortal/PaySlipPDF/"+encodeURIComponent(this.value);

    var xhr = new XMLHttpRequest();
    xhr.open('GET', this.url, true);
    xhr.setRequestHeader('Authorization', 'Bearer ' + encodeURIComponent(this.global.token));
    xhr.setRequestHeader('Content-Type', 'application/pdf');
    xhr.setRequestHeader('Accept', 'application/pdf');
    xhr.responseType = 'blob';
    xhr.onload = function (this) {
          if (this.readyState == 4 && this.status === 200) {                
            var file = window.URL.createObjectURL(this.response);  
            if(todo==1)              
            {
              document.querySelector("iframe").src = file;
            }
            else
            {
              window.open(file);
            }
          }   
    };
    xhr.send();    
    this.global.swalClose()
  }

  printdownload(){
    var byteCharacters = atob(this.base64);
    var byteNumbers = new Array(byteCharacters.length);
    for (var i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    var byteArray = new Uint8Array(byteNumbers);
    var file = new Blob([byteArray], { type: 'application/pdf;base64' });
    var fileURL = URL.createObjectURL(file);
  }

}

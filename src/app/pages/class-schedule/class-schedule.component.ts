import { Component, OnInit } from '@angular/core';

import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalService } from './../../global.service';
import { Inject} from '@angular/core';
@Component({
  selector: 'app-class-schedule',
  templateUrl: './class-schedule.component.html',
  styleUrls: ['./class-schedule.component.scss']
})
export class ClassScheduleComponent implements OnInit {
  tableArr
  constructor(public global: GlobalService,private http: Http) { }

  ngOnInit() {
    console.log(this.global.ClassSchedule)
    this.tableArr = this.global.ClassSchedule;
  }

}
